package imports.io.istio.extensions;

/**
 * Configuration for a Wasm VM.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:32:56.644Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.extensions.$Module.class, fqn = "ioistioextensions.WasmPluginSpecVmConfig")
@software.amazon.jsii.Jsii.Proxy(WasmPluginSpecVmConfig.Jsii$Proxy.class)
public interface WasmPluginSpecVmConfig extends software.amazon.jsii.JsiiSerializable {

    /**
     * Specifies environment variables to be injected to this VM.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.extensions.WasmPluginSpecVmConfigEnv> getEnv() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link WasmPluginSpecVmConfig}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link WasmPluginSpecVmConfig}
     */
    public static final class Builder implements software.amazon.jsii.Builder<WasmPluginSpecVmConfig> {
        java.util.List<imports.io.istio.extensions.WasmPluginSpecVmConfigEnv> env;

        /**
         * Sets the value of {@link WasmPluginSpecVmConfig#getEnv}
         * @param env Specifies environment variables to be injected to this VM.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder env(java.util.List<? extends imports.io.istio.extensions.WasmPluginSpecVmConfigEnv> env) {
            this.env = (java.util.List<imports.io.istio.extensions.WasmPluginSpecVmConfigEnv>)env;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link WasmPluginSpecVmConfig}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public WasmPluginSpecVmConfig build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link WasmPluginSpecVmConfig}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements WasmPluginSpecVmConfig {
        private final java.util.List<imports.io.istio.extensions.WasmPluginSpecVmConfigEnv> env;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.env = software.amazon.jsii.Kernel.get(this, "env", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.extensions.WasmPluginSpecVmConfigEnv.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.env = (java.util.List<imports.io.istio.extensions.WasmPluginSpecVmConfigEnv>)builder.env;
        }

        @Override
        public final java.util.List<imports.io.istio.extensions.WasmPluginSpecVmConfigEnv> getEnv() {
            return this.env;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getEnv() != null) {
                data.set("env", om.valueToTree(this.getEnv()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistioextensions.WasmPluginSpecVmConfig"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            WasmPluginSpecVmConfig.Jsii$Proxy that = (WasmPluginSpecVmConfig.Jsii$Proxy) o;

            return this.env != null ? this.env.equals(that.env) : that.env == null;
        }

        @Override
        public final int hashCode() {
            int result = this.env != null ? this.env.hashCode() : 0;
            return result;
        }
    }
}
