package imports.io.istio.networking;

/**
 * Match on envoy cluster attributes.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.441Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesMatchCluster")
@software.amazon.jsii.Jsii.Proxy(EnvoyFilterSpecConfigPatchesMatchCluster.Jsii$Proxy.class)
public interface EnvoyFilterSpecConfigPatchesMatchCluster extends software.amazon.jsii.JsiiSerializable {

    /**
     * The exact name of the cluster to match.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * The service port for which this cluster was generated.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPortNumber() {
        return null;
    }

    /**
     * The fully qualified service name for this cluster.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getService() {
        return null;
    }

    /**
     * The subset associated with the service.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getSubset() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link EnvoyFilterSpecConfigPatchesMatchCluster}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link EnvoyFilterSpecConfigPatchesMatchCluster}
     */
    public static final class Builder implements software.amazon.jsii.Builder<EnvoyFilterSpecConfigPatchesMatchCluster> {
        java.lang.String name;
        java.lang.Number portNumber;
        java.lang.String service;
        java.lang.String subset;

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchCluster#getName}
         * @param name The exact name of the cluster to match.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchCluster#getPortNumber}
         * @param portNumber The service port for which this cluster was generated.
         * @return {@code this}
         */
        public Builder portNumber(java.lang.Number portNumber) {
            this.portNumber = portNumber;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchCluster#getService}
         * @param service The fully qualified service name for this cluster.
         * @return {@code this}
         */
        public Builder service(java.lang.String service) {
            this.service = service;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchCluster#getSubset}
         * @param subset The subset associated with the service.
         * @return {@code this}
         */
        public Builder subset(java.lang.String subset) {
            this.subset = subset;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link EnvoyFilterSpecConfigPatchesMatchCluster}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public EnvoyFilterSpecConfigPatchesMatchCluster build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link EnvoyFilterSpecConfigPatchesMatchCluster}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements EnvoyFilterSpecConfigPatchesMatchCluster {
        private final java.lang.String name;
        private final java.lang.Number portNumber;
        private final java.lang.String service;
        private final java.lang.String subset;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.portNumber = software.amazon.jsii.Kernel.get(this, "portNumber", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.service = software.amazon.jsii.Kernel.get(this, "service", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.subset = software.amazon.jsii.Kernel.get(this, "subset", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.name = builder.name;
            this.portNumber = builder.portNumber;
            this.service = builder.service;
            this.subset = builder.subset;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final java.lang.Number getPortNumber() {
            return this.portNumber;
        }

        @Override
        public final java.lang.String getService() {
            return this.service;
        }

        @Override
        public final java.lang.String getSubset() {
            return this.subset;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getPortNumber() != null) {
                data.set("portNumber", om.valueToTree(this.getPortNumber()));
            }
            if (this.getService() != null) {
                data.set("service", om.valueToTree(this.getService()));
            }
            if (this.getSubset() != null) {
                data.set("subset", om.valueToTree(this.getSubset()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.EnvoyFilterSpecConfigPatchesMatchCluster"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EnvoyFilterSpecConfigPatchesMatchCluster.Jsii$Proxy that = (EnvoyFilterSpecConfigPatchesMatchCluster.Jsii$Proxy) o;

            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            if (this.portNumber != null ? !this.portNumber.equals(that.portNumber) : that.portNumber != null) return false;
            if (this.service != null ? !this.service.equals(that.service) : that.service != null) return false;
            return this.subset != null ? this.subset.equals(that.subset) : that.subset == null;
        }

        @Override
        public final int hashCode() {
            int result = this.name != null ? this.name.hashCode() : 0;
            result = 31 * result + (this.portNumber != null ? this.portNumber.hashCode() : 0);
            result = 31 * result + (this.service != null ? this.service.hashCode() : 0);
            result = 31 * result + (this.subset != null ? this.subset.hashCode() : 0);
            return result;
        }
    }
}
