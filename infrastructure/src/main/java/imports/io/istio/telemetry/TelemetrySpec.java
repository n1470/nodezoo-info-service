package imports.io.istio.telemetry;

/**
 * Telemetry configuration for workloads.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/telemetry.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.286Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpec")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpec.Jsii$Proxy.class)
public interface TelemetrySpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLogging> getAccessLogging() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.telemetry.TelemetrySpecMetrics> getMetrics() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecSelector getSelector() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.telemetry.TelemetrySpecTracing> getTracing() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpec> {
        java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLogging> accessLogging;
        java.util.List<imports.io.istio.telemetry.TelemetrySpecMetrics> metrics;
        imports.io.istio.telemetry.TelemetrySpecSelector selector;
        java.util.List<imports.io.istio.telemetry.TelemetrySpecTracing> tracing;

        /**
         * Sets the value of {@link TelemetrySpec#getAccessLogging}
         * @param accessLogging Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder accessLogging(java.util.List<? extends imports.io.istio.telemetry.TelemetrySpecAccessLogging> accessLogging) {
            this.accessLogging = (java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLogging>)accessLogging;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpec#getMetrics}
         * @param metrics Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder metrics(java.util.List<? extends imports.io.istio.telemetry.TelemetrySpecMetrics> metrics) {
            this.metrics = (java.util.List<imports.io.istio.telemetry.TelemetrySpecMetrics>)metrics;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpec#getSelector}
         * @param selector Optional.
         * @return {@code this}
         */
        public Builder selector(imports.io.istio.telemetry.TelemetrySpecSelector selector) {
            this.selector = selector;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpec#getTracing}
         * @param tracing Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder tracing(java.util.List<? extends imports.io.istio.telemetry.TelemetrySpecTracing> tracing) {
            this.tracing = (java.util.List<imports.io.istio.telemetry.TelemetrySpecTracing>)tracing;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpec {
        private final java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLogging> accessLogging;
        private final java.util.List<imports.io.istio.telemetry.TelemetrySpecMetrics> metrics;
        private final imports.io.istio.telemetry.TelemetrySpecSelector selector;
        private final java.util.List<imports.io.istio.telemetry.TelemetrySpecTracing> tracing;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.accessLogging = software.amazon.jsii.Kernel.get(this, "accessLogging", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecAccessLogging.class)));
            this.metrics = software.amazon.jsii.Kernel.get(this, "metrics", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecMetrics.class)));
            this.selector = software.amazon.jsii.Kernel.get(this, "selector", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecSelector.class));
            this.tracing = software.amazon.jsii.Kernel.get(this, "tracing", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecTracing.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.accessLogging = (java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLogging>)builder.accessLogging;
            this.metrics = (java.util.List<imports.io.istio.telemetry.TelemetrySpecMetrics>)builder.metrics;
            this.selector = builder.selector;
            this.tracing = (java.util.List<imports.io.istio.telemetry.TelemetrySpecTracing>)builder.tracing;
        }

        @Override
        public final java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLogging> getAccessLogging() {
            return this.accessLogging;
        }

        @Override
        public final java.util.List<imports.io.istio.telemetry.TelemetrySpecMetrics> getMetrics() {
            return this.metrics;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecSelector getSelector() {
            return this.selector;
        }

        @Override
        public final java.util.List<imports.io.istio.telemetry.TelemetrySpecTracing> getTracing() {
            return this.tracing;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAccessLogging() != null) {
                data.set("accessLogging", om.valueToTree(this.getAccessLogging()));
            }
            if (this.getMetrics() != null) {
                data.set("metrics", om.valueToTree(this.getMetrics()));
            }
            if (this.getSelector() != null) {
                data.set("selector", om.valueToTree(this.getSelector()));
            }
            if (this.getTracing() != null) {
                data.set("tracing", om.valueToTree(this.getTracing()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpec.Jsii$Proxy that = (TelemetrySpec.Jsii$Proxy) o;

            if (this.accessLogging != null ? !this.accessLogging.equals(that.accessLogging) : that.accessLogging != null) return false;
            if (this.metrics != null ? !this.metrics.equals(that.metrics) : that.metrics != null) return false;
            if (this.selector != null ? !this.selector.equals(that.selector) : that.selector != null) return false;
            return this.tracing != null ? this.tracing.equals(that.tracing) : that.tracing == null;
        }

        @Override
        public final int hashCode() {
            int result = this.accessLogging != null ? this.accessLogging.hashCode() : 0;
            result = 31 * result + (this.metrics != null ? this.metrics.hashCode() : 0);
            result = 31 * result + (this.selector != null ? this.selector.hashCode() : 0);
            result = 31 * result + (this.tracing != null ? this.tracing.hashCode() : 0);
            return result;
        }
    }
}
