package imports.io.istio.telemetry;

/**
 * Optional.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.287Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecAccessLoggingFilter")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpecAccessLoggingFilter.Jsii$Proxy.class)
public interface TelemetrySpecAccessLoggingFilter extends software.amazon.jsii.JsiiSerializable {

    /**
     * CEL expression for selecting when requests/connections should be logged.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getExpression() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpecAccessLoggingFilter}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpecAccessLoggingFilter}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpecAccessLoggingFilter> {
        java.lang.String expression;

        /**
         * Sets the value of {@link TelemetrySpecAccessLoggingFilter#getExpression}
         * @param expression CEL expression for selecting when requests/connections should be logged.
         * @return {@code this}
         */
        public Builder expression(java.lang.String expression) {
            this.expression = expression;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpecAccessLoggingFilter}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpecAccessLoggingFilter build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpecAccessLoggingFilter}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpecAccessLoggingFilter {
        private final java.lang.String expression;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.expression = software.amazon.jsii.Kernel.get(this, "expression", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.expression = builder.expression;
        }

        @Override
        public final java.lang.String getExpression() {
            return this.expression;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getExpression() != null) {
                data.set("expression", om.valueToTree(this.getExpression()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpecAccessLoggingFilter"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpecAccessLoggingFilter.Jsii$Proxy that = (TelemetrySpecAccessLoggingFilter.Jsii$Proxy) o;

            return this.expression != null ? this.expression.equals(that.expression) : that.expression == null;
        }

        @Override
        public final int hashCode() {
            int result = this.expression != null ? this.expression.hashCode() : 0;
            return result;
        }
    }
}
