package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.441Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesApplyTo")
public enum EnvoyFilterSpecConfigPatchesApplyTo {
    /**
     * INVALID.
     */
    INVALID,
    /**
     * LISTENER.
     */
    LISTENER,
    /**
     * FILTER_CHAIN.
     */
    FILTER_CHAIN,
    /**
     * NETWORK_FILTER.
     */
    NETWORK_FILTER,
    /**
     * HTTP_FILTER.
     */
    HTTP_FILTER,
    /**
     * ROUTE_CONFIGURATION.
     */
    ROUTE_CONFIGURATION,
    /**
     * VIRTUAL_HOST.
     */
    VIRTUAL_HOST,
    /**
     * HTTP_ROUTE.
     */
    HTTP_ROUTE,
    /**
     * CLUSTER.
     */
    CLUSTER,
    /**
     * EXTENSION_CONFIG.
     */
    EXTENSION_CONFIG,
    /**
     * BOOTSTRAP.
     */
    BOOTSTRAP,
}
