package imports.io.istio.networking;

/**
 * Optional: Maximum TLS protocol version.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.444Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.GatewaySpecServersTlsMaxProtocolVersion")
public enum GatewaySpecServersTlsMaxProtocolVersion {
    /**
     * TLS_AUTO.
     */
    TLS_AUTO,
    /**
     * TLSV1_0.
     */
    TLSV1_0,
    /**
     * TLSV1_1.
     */
    TLSV1_1,
    /**
     * TLSV1_2.
     */
    TLSV1_2,
    /**
     * TLSV1_3.
     */
    TLSV1_3,
}
