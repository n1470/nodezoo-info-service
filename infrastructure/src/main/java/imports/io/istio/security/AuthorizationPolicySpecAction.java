package imports.io.istio.security;

/**
 * Optional.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.455Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.AuthorizationPolicySpecAction")
public enum AuthorizationPolicySpecAction {
    /**
     * ALLOW.
     */
    ALLOW,
    /**
     * DENY.
     */
    DENY,
    /**
     * AUDIT.
     */
    AUDIT,
    /**
     * CUSTOM.
     */
    CUSTOM,
}
