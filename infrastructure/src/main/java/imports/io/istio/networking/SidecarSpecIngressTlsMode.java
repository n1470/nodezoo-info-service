package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.463Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.SidecarSpecIngressTlsMode")
public enum SidecarSpecIngressTlsMode {
    /**
     * PASSTHROUGH.
     */
    PASSTHROUGH,
    /**
     * SIMPLE.
     */
    SIMPLE,
    /**
     * MUTUAL.
     */
    MUTUAL,
    /**
     * AUTO_PASSTHROUGH.
     */
    AUTO_PASSTHROUGH,
    /**
     * ISTIO_MUTUAL.
     */
    ISTIO_MUTUAL,
}
