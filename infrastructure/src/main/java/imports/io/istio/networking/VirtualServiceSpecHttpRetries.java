package imports.io.istio.networking;

/**
 * Retry policy for HTTP requests.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.467Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpRetries")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpRetries.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpRetries extends software.amazon.jsii.JsiiSerializable {

    /**
     * Number of retries to be allowed for a given request.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getAttempts() {
        return null;
    }

    /**
     * Timeout per attempt for a given request, including the initial call and any retries.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getPerTryTimeout() {
        return null;
    }

    /**
     * Specifies the conditions under which retry takes place.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getRetryOn() {
        return null;
    }

    /**
     * Flag to specify whether the retries should retry to other localities.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getRetryRemoteLocalities() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpRetries}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpRetries}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpRetries> {
        java.lang.Number attempts;
        java.lang.String perTryTimeout;
        java.lang.String retryOn;
        java.lang.Boolean retryRemoteLocalities;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRetries#getAttempts}
         * @param attempts Number of retries to be allowed for a given request.
         * @return {@code this}
         */
        public Builder attempts(java.lang.Number attempts) {
            this.attempts = attempts;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRetries#getPerTryTimeout}
         * @param perTryTimeout Timeout per attempt for a given request, including the initial call and any retries.
         * @return {@code this}
         */
        public Builder perTryTimeout(java.lang.String perTryTimeout) {
            this.perTryTimeout = perTryTimeout;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRetries#getRetryOn}
         * @param retryOn Specifies the conditions under which retry takes place.
         * @return {@code this}
         */
        public Builder retryOn(java.lang.String retryOn) {
            this.retryOn = retryOn;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRetries#getRetryRemoteLocalities}
         * @param retryRemoteLocalities Flag to specify whether the retries should retry to other localities.
         * @return {@code this}
         */
        public Builder retryRemoteLocalities(java.lang.Boolean retryRemoteLocalities) {
            this.retryRemoteLocalities = retryRemoteLocalities;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpRetries}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpRetries build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpRetries}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpRetries {
        private final java.lang.Number attempts;
        private final java.lang.String perTryTimeout;
        private final java.lang.String retryOn;
        private final java.lang.Boolean retryRemoteLocalities;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.attempts = software.amazon.jsii.Kernel.get(this, "attempts", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.perTryTimeout = software.amazon.jsii.Kernel.get(this, "perTryTimeout", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.retryOn = software.amazon.jsii.Kernel.get(this, "retryOn", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.retryRemoteLocalities = software.amazon.jsii.Kernel.get(this, "retryRemoteLocalities", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.attempts = builder.attempts;
            this.perTryTimeout = builder.perTryTimeout;
            this.retryOn = builder.retryOn;
            this.retryRemoteLocalities = builder.retryRemoteLocalities;
        }

        @Override
        public final java.lang.Number getAttempts() {
            return this.attempts;
        }

        @Override
        public final java.lang.String getPerTryTimeout() {
            return this.perTryTimeout;
        }

        @Override
        public final java.lang.String getRetryOn() {
            return this.retryOn;
        }

        @Override
        public final java.lang.Boolean getRetryRemoteLocalities() {
            return this.retryRemoteLocalities;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAttempts() != null) {
                data.set("attempts", om.valueToTree(this.getAttempts()));
            }
            if (this.getPerTryTimeout() != null) {
                data.set("perTryTimeout", om.valueToTree(this.getPerTryTimeout()));
            }
            if (this.getRetryOn() != null) {
                data.set("retryOn", om.valueToTree(this.getRetryOn()));
            }
            if (this.getRetryRemoteLocalities() != null) {
                data.set("retryRemoteLocalities", om.valueToTree(this.getRetryRemoteLocalities()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpRetries"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpRetries.Jsii$Proxy that = (VirtualServiceSpecHttpRetries.Jsii$Proxy) o;

            if (this.attempts != null ? !this.attempts.equals(that.attempts) : that.attempts != null) return false;
            if (this.perTryTimeout != null ? !this.perTryTimeout.equals(that.perTryTimeout) : that.perTryTimeout != null) return false;
            if (this.retryOn != null ? !this.retryOn.equals(that.retryOn) : that.retryOn != null) return false;
            return this.retryRemoteLocalities != null ? this.retryRemoteLocalities.equals(that.retryRemoteLocalities) : that.retryRemoteLocalities == null;
        }

        @Override
        public final int hashCode() {
            int result = this.attempts != null ? this.attempts.hashCode() : 0;
            result = 31 * result + (this.perTryTimeout != null ? this.perTryTimeout.hashCode() : 0);
            result = 31 * result + (this.retryOn != null ? this.retryOn.hashCode() : 0);
            result = 31 * result + (this.retryRemoteLocalities != null ? this.retryRemoteLocalities.hashCode() : 0);
            return result;
        }
    }
}
