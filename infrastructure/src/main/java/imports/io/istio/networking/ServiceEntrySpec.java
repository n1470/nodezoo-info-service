package imports.io.istio.networking;

/**
 * Configuration affecting service registry.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/networking/service-entry.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.461Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.ServiceEntrySpec")
@software.amazon.jsii.Jsii.Proxy(ServiceEntrySpec.Jsii$Proxy.class)
public interface ServiceEntrySpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * The virtual IP addresses associated with the service.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getAddresses() {
        return null;
    }

    /**
     * One or more endpoints associated with the service.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.ServiceEntrySpecEndpoints> getEndpoints() {
        return null;
    }

    /**
     * A list of namespaces to which this service is exported.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getExportTo() {
        return null;
    }

    /**
     * The hosts associated with the ServiceEntry.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getHosts() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.ServiceEntrySpecLocation getLocation() {
        return null;
    }

    /**
     * The ports associated with the external service.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.ServiceEntrySpecPorts> getPorts() {
        return null;
    }

    /**
     * Service discovery mode for the hosts.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.ServiceEntrySpecResolution getResolution() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getSubjectAltNames() {
        return null;
    }

    /**
     * Applicable only for MESH_INTERNAL services.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.ServiceEntrySpecWorkloadSelector getWorkloadSelector() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link ServiceEntrySpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link ServiceEntrySpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<ServiceEntrySpec> {
        java.util.List<java.lang.String> addresses;
        java.util.List<imports.io.istio.networking.ServiceEntrySpecEndpoints> endpoints;
        java.util.List<java.lang.String> exportTo;
        java.util.List<java.lang.String> hosts;
        imports.io.istio.networking.ServiceEntrySpecLocation location;
        java.util.List<imports.io.istio.networking.ServiceEntrySpecPorts> ports;
        imports.io.istio.networking.ServiceEntrySpecResolution resolution;
        java.util.List<java.lang.String> subjectAltNames;
        imports.io.istio.networking.ServiceEntrySpecWorkloadSelector workloadSelector;

        /**
         * Sets the value of {@link ServiceEntrySpec#getAddresses}
         * @param addresses The virtual IP addresses associated with the service.
         * @return {@code this}
         */
        public Builder addresses(java.util.List<java.lang.String> addresses) {
            this.addresses = addresses;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpec#getEndpoints}
         * @param endpoints One or more endpoints associated with the service.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder endpoints(java.util.List<? extends imports.io.istio.networking.ServiceEntrySpecEndpoints> endpoints) {
            this.endpoints = (java.util.List<imports.io.istio.networking.ServiceEntrySpecEndpoints>)endpoints;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpec#getExportTo}
         * @param exportTo A list of namespaces to which this service is exported.
         * @return {@code this}
         */
        public Builder exportTo(java.util.List<java.lang.String> exportTo) {
            this.exportTo = exportTo;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpec#getHosts}
         * @param hosts The hosts associated with the ServiceEntry.
         * @return {@code this}
         */
        public Builder hosts(java.util.List<java.lang.String> hosts) {
            this.hosts = hosts;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpec#getLocation}
         * @param location the value to be set.
         * @return {@code this}
         */
        public Builder location(imports.io.istio.networking.ServiceEntrySpecLocation location) {
            this.location = location;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpec#getPorts}
         * @param ports The ports associated with the external service.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder ports(java.util.List<? extends imports.io.istio.networking.ServiceEntrySpecPorts> ports) {
            this.ports = (java.util.List<imports.io.istio.networking.ServiceEntrySpecPorts>)ports;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpec#getResolution}
         * @param resolution Service discovery mode for the hosts.
         * @return {@code this}
         */
        public Builder resolution(imports.io.istio.networking.ServiceEntrySpecResolution resolution) {
            this.resolution = resolution;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpec#getSubjectAltNames}
         * @param subjectAltNames the value to be set.
         * @return {@code this}
         */
        public Builder subjectAltNames(java.util.List<java.lang.String> subjectAltNames) {
            this.subjectAltNames = subjectAltNames;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpec#getWorkloadSelector}
         * @param workloadSelector Applicable only for MESH_INTERNAL services.
         * @return {@code this}
         */
        public Builder workloadSelector(imports.io.istio.networking.ServiceEntrySpecWorkloadSelector workloadSelector) {
            this.workloadSelector = workloadSelector;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link ServiceEntrySpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public ServiceEntrySpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link ServiceEntrySpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements ServiceEntrySpec {
        private final java.util.List<java.lang.String> addresses;
        private final java.util.List<imports.io.istio.networking.ServiceEntrySpecEndpoints> endpoints;
        private final java.util.List<java.lang.String> exportTo;
        private final java.util.List<java.lang.String> hosts;
        private final imports.io.istio.networking.ServiceEntrySpecLocation location;
        private final java.util.List<imports.io.istio.networking.ServiceEntrySpecPorts> ports;
        private final imports.io.istio.networking.ServiceEntrySpecResolution resolution;
        private final java.util.List<java.lang.String> subjectAltNames;
        private final imports.io.istio.networking.ServiceEntrySpecWorkloadSelector workloadSelector;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.addresses = software.amazon.jsii.Kernel.get(this, "addresses", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.endpoints = software.amazon.jsii.Kernel.get(this, "endpoints", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.ServiceEntrySpecEndpoints.class)));
            this.exportTo = software.amazon.jsii.Kernel.get(this, "exportTo", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.hosts = software.amazon.jsii.Kernel.get(this, "hosts", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.location = software.amazon.jsii.Kernel.get(this, "location", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.ServiceEntrySpecLocation.class));
            this.ports = software.amazon.jsii.Kernel.get(this, "ports", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.ServiceEntrySpecPorts.class)));
            this.resolution = software.amazon.jsii.Kernel.get(this, "resolution", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.ServiceEntrySpecResolution.class));
            this.subjectAltNames = software.amazon.jsii.Kernel.get(this, "subjectAltNames", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.workloadSelector = software.amazon.jsii.Kernel.get(this, "workloadSelector", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.ServiceEntrySpecWorkloadSelector.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.addresses = builder.addresses;
            this.endpoints = (java.util.List<imports.io.istio.networking.ServiceEntrySpecEndpoints>)builder.endpoints;
            this.exportTo = builder.exportTo;
            this.hosts = builder.hosts;
            this.location = builder.location;
            this.ports = (java.util.List<imports.io.istio.networking.ServiceEntrySpecPorts>)builder.ports;
            this.resolution = builder.resolution;
            this.subjectAltNames = builder.subjectAltNames;
            this.workloadSelector = builder.workloadSelector;
        }

        @Override
        public final java.util.List<java.lang.String> getAddresses() {
            return this.addresses;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.ServiceEntrySpecEndpoints> getEndpoints() {
            return this.endpoints;
        }

        @Override
        public final java.util.List<java.lang.String> getExportTo() {
            return this.exportTo;
        }

        @Override
        public final java.util.List<java.lang.String> getHosts() {
            return this.hosts;
        }

        @Override
        public final imports.io.istio.networking.ServiceEntrySpecLocation getLocation() {
            return this.location;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.ServiceEntrySpecPorts> getPorts() {
            return this.ports;
        }

        @Override
        public final imports.io.istio.networking.ServiceEntrySpecResolution getResolution() {
            return this.resolution;
        }

        @Override
        public final java.util.List<java.lang.String> getSubjectAltNames() {
            return this.subjectAltNames;
        }

        @Override
        public final imports.io.istio.networking.ServiceEntrySpecWorkloadSelector getWorkloadSelector() {
            return this.workloadSelector;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAddresses() != null) {
                data.set("addresses", om.valueToTree(this.getAddresses()));
            }
            if (this.getEndpoints() != null) {
                data.set("endpoints", om.valueToTree(this.getEndpoints()));
            }
            if (this.getExportTo() != null) {
                data.set("exportTo", om.valueToTree(this.getExportTo()));
            }
            if (this.getHosts() != null) {
                data.set("hosts", om.valueToTree(this.getHosts()));
            }
            if (this.getLocation() != null) {
                data.set("location", om.valueToTree(this.getLocation()));
            }
            if (this.getPorts() != null) {
                data.set("ports", om.valueToTree(this.getPorts()));
            }
            if (this.getResolution() != null) {
                data.set("resolution", om.valueToTree(this.getResolution()));
            }
            if (this.getSubjectAltNames() != null) {
                data.set("subjectAltNames", om.valueToTree(this.getSubjectAltNames()));
            }
            if (this.getWorkloadSelector() != null) {
                data.set("workloadSelector", om.valueToTree(this.getWorkloadSelector()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.ServiceEntrySpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ServiceEntrySpec.Jsii$Proxy that = (ServiceEntrySpec.Jsii$Proxy) o;

            if (this.addresses != null ? !this.addresses.equals(that.addresses) : that.addresses != null) return false;
            if (this.endpoints != null ? !this.endpoints.equals(that.endpoints) : that.endpoints != null) return false;
            if (this.exportTo != null ? !this.exportTo.equals(that.exportTo) : that.exportTo != null) return false;
            if (this.hosts != null ? !this.hosts.equals(that.hosts) : that.hosts != null) return false;
            if (this.location != null ? !this.location.equals(that.location) : that.location != null) return false;
            if (this.ports != null ? !this.ports.equals(that.ports) : that.ports != null) return false;
            if (this.resolution != null ? !this.resolution.equals(that.resolution) : that.resolution != null) return false;
            if (this.subjectAltNames != null ? !this.subjectAltNames.equals(that.subjectAltNames) : that.subjectAltNames != null) return false;
            return this.workloadSelector != null ? this.workloadSelector.equals(that.workloadSelector) : that.workloadSelector == null;
        }

        @Override
        public final int hashCode() {
            int result = this.addresses != null ? this.addresses.hashCode() : 0;
            result = 31 * result + (this.endpoints != null ? this.endpoints.hashCode() : 0);
            result = 31 * result + (this.exportTo != null ? this.exportTo.hashCode() : 0);
            result = 31 * result + (this.hosts != null ? this.hosts.hashCode() : 0);
            result = 31 * result + (this.location != null ? this.location.hashCode() : 0);
            result = 31 * result + (this.ports != null ? this.ports.hashCode() : 0);
            result = 31 * result + (this.resolution != null ? this.resolution.hashCode() : 0);
            result = 31 * result + (this.subjectAltNames != null ? this.subjectAltNames.hashCode() : 0);
            result = 31 * result + (this.workloadSelector != null ? this.workloadSelector.hashCode() : 0);
            return result;
        }
    }
}
