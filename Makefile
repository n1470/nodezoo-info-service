.phony: app_dev_run
app_dev_run:
	@sbt app/run

.phony: test
test:
	@sbt test

.phony: clean
clean:
	@sbt clean && rm -rf infrastructure/dist

.phony: package
package:
	@sbt app/Universal/packageZipTarball

.phony: dev_deploy
dev_deploy:
	@set -ev
	@kubectl get ns nodezoo 1>/dev/null 2>&1 || kubectl create ns nodezoo
	@kubectl apply -f infrastructure/dist/nodezoo

MAKE_RC_FILE := $(strip $(wildcard .makerc))
ifneq (${MAKE_RC_FILE},)
include .makerc
endif

export PROJECT_NAME := nodezoo-info-service
PROJECT_SLUG := n1470/${PROJECT_NAME}
GITLAB_REPO := registry.gitlab.com
APP_VERSION = $(shell sbt app/version | tail -n1 | awk '{ print $$2 }')

ifdef AWS_PROFILE
	export AWS_PROFILE
	AWS_CLI = aws --profile ${AWS_PROFILE}
else
	AWS_CLI = aws
endif

AWS_ACCOUNT_ID = $(shell ${AWS_CLI} sts get-caller-identity \
					--output json --query 'Account' | awk -F'"' '{ print $$2 }')

ifdef AWS_REGION
	AWS_CLI_REGION := ${AWS_REGION}
else
	AWS_CLI_REGION := us-east-1
endif

ECR_REPO_URL = ${AWS_ACCOUNT_ID}.dkr.ecr.${AWS_CLI_REGION}.amazonaws.com

ECR_IMAGE = ${ECR_REPO_URL}/${PROJECT_NAME}

DIST_CONTENT_ROOT := dist/nodezoo

export OUTPUT_PATH := "infrastructure/${DIST_CONTENT_ROOT}"
export AWS_REGION := ${AWS_CLI_REGION}

DOCKER_USERNAME = $(shell docker info | grep "Username" | awk '{ print $$2 }')

dev_dist: clean
	@export IMAGE_TAG="${DOCKER_USERNAME}/${PROJECT_NAME}:${APP_VERSION}" && \
	if [ "$(basename $$PWD)" != "infrastructure" ]; then \
  	  cd infrastructure && cdk8s synth; \
	else \
	  cdk8s synth; \
	fi

dev_eks_dist: clean
	@export IMAGE_TAG="${DOCKER_USERNAME}/${PROJECT_NAME}:${APP_VERSION}" && \
	export EKS_DIST=true && \
	if [ "$(basename $$PWD)" != "infrastructure" ]; then \
  	  cd infrastructure && cdk8s synth; \
	else \
	  cdk8s synth; \
	fi

export DIST_REPO_ROOT := infrastructure/${DIST_CONTENT_ROOT}
export DEPLOY_REPO_ROOT := infrastructure/deploy

dist: clean
	@export EKS_DIST=1 && \
	export PULL_IMAGE_SECRET=1 && \
	export IMAGE_TAG=${ECR_IMAGE}:${APP_VERSION} && \
	if [ "$(basename $$PWD)" != "infrastructure" ]; then \
  	  cd infrastructure && cdk8s synth; \
	else \
	  cdk8s synth; \
	fi

.phony: deploy
deploy: dist
	@./infrastructure/bin/cicd_deploy

.phony: ci_image
ci_image:
	@docker build --build-arg PROJECT_PATH=${PROJECT_SLUG} -t ${GITLAB_REPO}/${PROJECT_SLUG} .

.phony: dev_app_image
dev_app_image:
	@cd app && docker build --build-arg PROJECT_PATH=${PROJECT_SLUG} -t ${DOCKER_USERNAME}/${PROJECT_NAME}:${APP_VERSION} .

.phony: app_image
app_image:
	@cd app && docker build --build-arg PROJECT_PATH=${PROJECT_SLUG} -t ${ECR_IMAGE}:${APP_VERSION} .

FIND_APP_REPOSITORY = $(shell ${AWS_CLI} ecr --region ${AWS_CLI_REGION} \
						describe-repositories \
						--query 'repositories[?repositoryName==`${PROJECT_NAME}`].repositoryName | join(`,`, @)' | \
						awk -F'"' '{ print $$2 }')

.phony: app_repository
app_repository:
	@if [ -z "${FIND_APP_REPOSITORY}" ]; then \
		${AWS_CLI} ecr --region ${AWS_CLI_REGION} create-repository \
		--repository-name ${PROJECT_NAME} --image-tag-mutability IMMUTABLE; \
	else \
	  echo "${PROJECT_NAME} ECR repository already exists"; \
	fi

.phony: push_dev_app_image
push_dev_app_image:
	@docker push ${DOCKER_USERNAME}/${PROJECT_NAME}:${APP_VERSION}

.phony: push_app_image
push_app_image:
	@set -ev
	@${AWS_CLI} ecr get-login-password \
	--region ${AWS_CLI_REGION} | docker login --username AWS --password-stdin ${ECR_REPO_URL}
	@docker push ${ECR_IMAGE}:${APP_VERSION}

.phony: test_macros
test_macros:
	@set -ev
	@echo APP_VERSION=${APP_VERSION}
	@echo AWS_ACCOUNT_ID=${AWS_ACCOUNT_ID}
	@echo ECR_REPO_URL=${ECR_REPO_URL}
	@echo ECR_IMAGE=${ECR_IMAGE}
