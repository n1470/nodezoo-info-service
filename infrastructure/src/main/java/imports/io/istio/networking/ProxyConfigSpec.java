package imports.io.istio.networking;

/**
 * Provides configuration for individual workloads.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/networking/proxy-config.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.460Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.ProxyConfigSpec")
@software.amazon.jsii.Jsii.Proxy(ProxyConfigSpec.Jsii$Proxy.class)
public interface ProxyConfigSpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * The number of worker threads to run.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getConcurrency() {
        return null;
    }

    /**
     * Additional environment variables for the proxy.
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getEnvironmentVariables() {
        return null;
    }

    /**
     * Specifies the details of the proxy image.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.ProxyConfigSpecImage getImage() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.ProxyConfigSpecSelector getSelector() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link ProxyConfigSpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link ProxyConfigSpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<ProxyConfigSpec> {
        java.lang.Number concurrency;
        java.util.Map<java.lang.String, java.lang.String> environmentVariables;
        imports.io.istio.networking.ProxyConfigSpecImage image;
        imports.io.istio.networking.ProxyConfigSpecSelector selector;

        /**
         * Sets the value of {@link ProxyConfigSpec#getConcurrency}
         * @param concurrency The number of worker threads to run.
         * @return {@code this}
         */
        public Builder concurrency(java.lang.Number concurrency) {
            this.concurrency = concurrency;
            return this;
        }

        /**
         * Sets the value of {@link ProxyConfigSpec#getEnvironmentVariables}
         * @param environmentVariables Additional environment variables for the proxy.
         * @return {@code this}
         */
        public Builder environmentVariables(java.util.Map<java.lang.String, java.lang.String> environmentVariables) {
            this.environmentVariables = environmentVariables;
            return this;
        }

        /**
         * Sets the value of {@link ProxyConfigSpec#getImage}
         * @param image Specifies the details of the proxy image.
         * @return {@code this}
         */
        public Builder image(imports.io.istio.networking.ProxyConfigSpecImage image) {
            this.image = image;
            return this;
        }

        /**
         * Sets the value of {@link ProxyConfigSpec#getSelector}
         * @param selector Optional.
         * @return {@code this}
         */
        public Builder selector(imports.io.istio.networking.ProxyConfigSpecSelector selector) {
            this.selector = selector;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link ProxyConfigSpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public ProxyConfigSpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link ProxyConfigSpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements ProxyConfigSpec {
        private final java.lang.Number concurrency;
        private final java.util.Map<java.lang.String, java.lang.String> environmentVariables;
        private final imports.io.istio.networking.ProxyConfigSpecImage image;
        private final imports.io.istio.networking.ProxyConfigSpecSelector selector;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.concurrency = software.amazon.jsii.Kernel.get(this, "concurrency", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.environmentVariables = software.amazon.jsii.Kernel.get(this, "environmentVariables", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.image = software.amazon.jsii.Kernel.get(this, "image", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.ProxyConfigSpecImage.class));
            this.selector = software.amazon.jsii.Kernel.get(this, "selector", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.ProxyConfigSpecSelector.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.concurrency = builder.concurrency;
            this.environmentVariables = builder.environmentVariables;
            this.image = builder.image;
            this.selector = builder.selector;
        }

        @Override
        public final java.lang.Number getConcurrency() {
            return this.concurrency;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getEnvironmentVariables() {
            return this.environmentVariables;
        }

        @Override
        public final imports.io.istio.networking.ProxyConfigSpecImage getImage() {
            return this.image;
        }

        @Override
        public final imports.io.istio.networking.ProxyConfigSpecSelector getSelector() {
            return this.selector;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getConcurrency() != null) {
                data.set("concurrency", om.valueToTree(this.getConcurrency()));
            }
            if (this.getEnvironmentVariables() != null) {
                data.set("environmentVariables", om.valueToTree(this.getEnvironmentVariables()));
            }
            if (this.getImage() != null) {
                data.set("image", om.valueToTree(this.getImage()));
            }
            if (this.getSelector() != null) {
                data.set("selector", om.valueToTree(this.getSelector()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.ProxyConfigSpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ProxyConfigSpec.Jsii$Proxy that = (ProxyConfigSpec.Jsii$Proxy) o;

            if (this.concurrency != null ? !this.concurrency.equals(that.concurrency) : that.concurrency != null) return false;
            if (this.environmentVariables != null ? !this.environmentVariables.equals(that.environmentVariables) : that.environmentVariables != null) return false;
            if (this.image != null ? !this.image.equals(that.image) : that.image != null) return false;
            return this.selector != null ? this.selector.equals(that.selector) : that.selector == null;
        }

        @Override
        public final int hashCode() {
            int result = this.concurrency != null ? this.concurrency.hashCode() : 0;
            result = 31 * result + (this.environmentVariables != null ? this.environmentVariables.hashCode() : 0);
            result = 31 * result + (this.image != null ? this.image.hashCode() : 0);
            result = 31 * result + (this.selector != null ? this.selector.hashCode() : 0);
            return result;
        }
    }
}
