package tao;

import org.cdk8s.Testing;
import org.junit.jupiter.api.Test;
import tao.manifest.ImagePullSecretManifest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

class InfoServiceChartTest {

    @Test
    void testSynth() {
        final var app = Testing.app();
        final var secretManifestProps = new ImagePullSecretManifest.Props("123", "sa-south-1", "pass");
        final var npmSvcChartProps = new InfoServiceChartProps(InfoServiceChartProps.DEFAULT_REPLICAS,
                secretManifestProps,
                null,
                "123.dkr.ecr.us-west-1.amazonaws.com/nodezoo-npm-service:0.0.1",
                false);
        final var chart = new InfoServiceChart(app, "nodezoo-test", npmSvcChartProps);
        final var manifest = Testing.synth(chart);
        assertThat(manifest, is(not(empty())));
        assertThat(manifest, hasSize(5));
    }
}
