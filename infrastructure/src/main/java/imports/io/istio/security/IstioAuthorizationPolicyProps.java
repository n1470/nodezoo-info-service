package imports.io.istio.security;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.461Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.IstioAuthorizationPolicyProps")
@software.amazon.jsii.Jsii.Proxy(IstioAuthorizationPolicyProps.Jsii$Proxy.class)
public interface IstioAuthorizationPolicyProps extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable org.cdk8s.ApiObjectMetadata getMetadata() {
        return null;
    }

    /**
     * Configuration for access control on workloads.
     * <p>
     * See more details at: https://istio.io/docs/reference/config/security/authorization-policy.html
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.AuthorizationPolicySpec getSpec() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link IstioAuthorizationPolicyProps}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link IstioAuthorizationPolicyProps}
     */
    public static final class Builder implements software.amazon.jsii.Builder<IstioAuthorizationPolicyProps> {
        org.cdk8s.ApiObjectMetadata metadata;
        imports.io.istio.security.AuthorizationPolicySpec spec;

        /**
         * Sets the value of {@link IstioAuthorizationPolicyProps#getMetadata}
         * @param metadata the value to be set.
         * @return {@code this}
         */
        public Builder metadata(org.cdk8s.ApiObjectMetadata metadata) {
            this.metadata = metadata;
            return this;
        }

        /**
         * Sets the value of {@link IstioAuthorizationPolicyProps#getSpec}
         * @param spec Configuration for access control on workloads.
         *             See more details at: https://istio.io/docs/reference/config/security/authorization-policy.html
         * @return {@code this}
         */
        public Builder spec(imports.io.istio.security.AuthorizationPolicySpec spec) {
            this.spec = spec;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link IstioAuthorizationPolicyProps}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public IstioAuthorizationPolicyProps build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link IstioAuthorizationPolicyProps}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements IstioAuthorizationPolicyProps {
        private final org.cdk8s.ApiObjectMetadata metadata;
        private final imports.io.istio.security.AuthorizationPolicySpec spec;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.metadata = software.amazon.jsii.Kernel.get(this, "metadata", software.amazon.jsii.NativeType.forClass(org.cdk8s.ApiObjectMetadata.class));
            this.spec = software.amazon.jsii.Kernel.get(this, "spec", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpec.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.metadata = builder.metadata;
            this.spec = builder.spec;
        }

        @Override
        public final org.cdk8s.ApiObjectMetadata getMetadata() {
            return this.metadata;
        }

        @Override
        public final imports.io.istio.security.AuthorizationPolicySpec getSpec() {
            return this.spec;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getMetadata() != null) {
                data.set("metadata", om.valueToTree(this.getMetadata()));
            }
            if (this.getSpec() != null) {
                data.set("spec", om.valueToTree(this.getSpec()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.IstioAuthorizationPolicyProps"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            IstioAuthorizationPolicyProps.Jsii$Proxy that = (IstioAuthorizationPolicyProps.Jsii$Proxy) o;

            if (this.metadata != null ? !this.metadata.equals(that.metadata) : that.metadata != null) return false;
            return this.spec != null ? this.spec.equals(that.spec) : that.spec == null;
        }

        @Override
        public final int hashCode() {
            int result = this.metadata != null ? this.metadata.hashCode() : 0;
            result = 31 * result + (this.spec != null ? this.spec.hashCode() : 0);
            return result;
        }
    }
}
