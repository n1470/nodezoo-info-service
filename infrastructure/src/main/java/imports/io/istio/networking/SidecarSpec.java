package imports.io.istio.networking;

/**
 * Configuration affecting network reachability of a sidecar.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/networking/sidecar.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.461Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.SidecarSpec")
@software.amazon.jsii.Jsii.Proxy(SidecarSpec.Jsii$Proxy.class)
public interface SidecarSpec extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.SidecarSpecEgress> getEgress() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.SidecarSpecIngress> getIngress() {
        return null;
    }

    /**
     * Configuration for the outbound traffic policy.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecOutboundTrafficPolicy getOutboundTrafficPolicy() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecWorkloadSelector getWorkloadSelector() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link SidecarSpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link SidecarSpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<SidecarSpec> {
        java.util.List<imports.io.istio.networking.SidecarSpecEgress> egress;
        java.util.List<imports.io.istio.networking.SidecarSpecIngress> ingress;
        imports.io.istio.networking.SidecarSpecOutboundTrafficPolicy outboundTrafficPolicy;
        imports.io.istio.networking.SidecarSpecWorkloadSelector workloadSelector;

        /**
         * Sets the value of {@link SidecarSpec#getEgress}
         * @param egress the value to be set.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder egress(java.util.List<? extends imports.io.istio.networking.SidecarSpecEgress> egress) {
            this.egress = (java.util.List<imports.io.istio.networking.SidecarSpecEgress>)egress;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpec#getIngress}
         * @param ingress the value to be set.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder ingress(java.util.List<? extends imports.io.istio.networking.SidecarSpecIngress> ingress) {
            this.ingress = (java.util.List<imports.io.istio.networking.SidecarSpecIngress>)ingress;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpec#getOutboundTrafficPolicy}
         * @param outboundTrafficPolicy Configuration for the outbound traffic policy.
         * @return {@code this}
         */
        public Builder outboundTrafficPolicy(imports.io.istio.networking.SidecarSpecOutboundTrafficPolicy outboundTrafficPolicy) {
            this.outboundTrafficPolicy = outboundTrafficPolicy;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpec#getWorkloadSelector}
         * @param workloadSelector the value to be set.
         * @return {@code this}
         */
        public Builder workloadSelector(imports.io.istio.networking.SidecarSpecWorkloadSelector workloadSelector) {
            this.workloadSelector = workloadSelector;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link SidecarSpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public SidecarSpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link SidecarSpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements SidecarSpec {
        private final java.util.List<imports.io.istio.networking.SidecarSpecEgress> egress;
        private final java.util.List<imports.io.istio.networking.SidecarSpecIngress> ingress;
        private final imports.io.istio.networking.SidecarSpecOutboundTrafficPolicy outboundTrafficPolicy;
        private final imports.io.istio.networking.SidecarSpecWorkloadSelector workloadSelector;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.egress = software.amazon.jsii.Kernel.get(this, "egress", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecEgress.class)));
            this.ingress = software.amazon.jsii.Kernel.get(this, "ingress", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecIngress.class)));
            this.outboundTrafficPolicy = software.amazon.jsii.Kernel.get(this, "outboundTrafficPolicy", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecOutboundTrafficPolicy.class));
            this.workloadSelector = software.amazon.jsii.Kernel.get(this, "workloadSelector", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecWorkloadSelector.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.egress = (java.util.List<imports.io.istio.networking.SidecarSpecEgress>)builder.egress;
            this.ingress = (java.util.List<imports.io.istio.networking.SidecarSpecIngress>)builder.ingress;
            this.outboundTrafficPolicy = builder.outboundTrafficPolicy;
            this.workloadSelector = builder.workloadSelector;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.SidecarSpecEgress> getEgress() {
            return this.egress;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.SidecarSpecIngress> getIngress() {
            return this.ingress;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecOutboundTrafficPolicy getOutboundTrafficPolicy() {
            return this.outboundTrafficPolicy;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecWorkloadSelector getWorkloadSelector() {
            return this.workloadSelector;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getEgress() != null) {
                data.set("egress", om.valueToTree(this.getEgress()));
            }
            if (this.getIngress() != null) {
                data.set("ingress", om.valueToTree(this.getIngress()));
            }
            if (this.getOutboundTrafficPolicy() != null) {
                data.set("outboundTrafficPolicy", om.valueToTree(this.getOutboundTrafficPolicy()));
            }
            if (this.getWorkloadSelector() != null) {
                data.set("workloadSelector", om.valueToTree(this.getWorkloadSelector()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.SidecarSpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SidecarSpec.Jsii$Proxy that = (SidecarSpec.Jsii$Proxy) o;

            if (this.egress != null ? !this.egress.equals(that.egress) : that.egress != null) return false;
            if (this.ingress != null ? !this.ingress.equals(that.ingress) : that.ingress != null) return false;
            if (this.outboundTrafficPolicy != null ? !this.outboundTrafficPolicy.equals(that.outboundTrafficPolicy) : that.outboundTrafficPolicy != null) return false;
            return this.workloadSelector != null ? this.workloadSelector.equals(that.workloadSelector) : that.workloadSelector == null;
        }

        @Override
        public final int hashCode() {
            int result = this.egress != null ? this.egress.hashCode() : 0;
            result = 31 * result + (this.ingress != null ? this.ingress.hashCode() : 0);
            result = 31 * result + (this.outboundTrafficPolicy != null ? this.outboundTrafficPolicy.hashCode() : 0);
            result = 31 * result + (this.workloadSelector != null ? this.workloadSelector.hashCode() : 0);
            return result;
        }
    }
}
