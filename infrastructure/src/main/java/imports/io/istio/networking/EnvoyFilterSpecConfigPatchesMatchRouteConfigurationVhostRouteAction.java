package imports.io.istio.networking;

/**
 * Match a route with specific action type.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.443Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRouteAction")
public enum EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRouteAction {
    /**
     * ANY.
     */
    ANY,
    /**
     * ROUTE.
     */
    ROUTE,
    /**
     * REDIRECT.
     */
    REDIRECT,
    /**
     * DIRECT_RESPONSE.
     */
    DIRECT_RESPONSE,
}
