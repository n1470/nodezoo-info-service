package imports.io.istio.networking;

/**
 * Specify if http1.1 connection should be upgraded to http2 for the associated destination.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.436Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecTrafficPolicyConnectionPoolHttpH2UpgradePolicy")
public enum DestinationRuleSpecTrafficPolicyConnectionPoolHttpH2UpgradePolicy {
    /**
     * DEFAULT.
     */
    DEFAULT,
    /**
     * DO_NOT_UPGRADE.
     */
    DO_NOT_UPGRADE,
    /**
     * UPGRADE.
     */
    UPGRADE,
}
