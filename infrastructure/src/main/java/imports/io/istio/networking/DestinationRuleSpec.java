package imports.io.istio.networking;

/**
 * Configuration affecting load balancing, outlier detection, etc.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/networking/destination-rule.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.425Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpec")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpec.Jsii$Proxy.class)
public interface DestinationRuleSpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * A list of namespaces to which this destination rule is exported.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getExportTo() {
        return null;
    }

    /**
     * The name of a service from the service registry.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getHost() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsets> getSubsets() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicy getTrafficPolicy() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecWorkloadSelector getWorkloadSelector() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpec> {
        java.util.List<java.lang.String> exportTo;
        java.lang.String host;
        java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsets> subsets;
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicy trafficPolicy;
        imports.io.istio.networking.DestinationRuleSpecWorkloadSelector workloadSelector;

        /**
         * Sets the value of {@link DestinationRuleSpec#getExportTo}
         * @param exportTo A list of namespaces to which this destination rule is exported.
         * @return {@code this}
         */
        public Builder exportTo(java.util.List<java.lang.String> exportTo) {
            this.exportTo = exportTo;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpec#getHost}
         * @param host The name of a service from the service registry.
         * @return {@code this}
         */
        public Builder host(java.lang.String host) {
            this.host = host;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpec#getSubsets}
         * @param subsets the value to be set.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder subsets(java.util.List<? extends imports.io.istio.networking.DestinationRuleSpecSubsets> subsets) {
            this.subsets = (java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsets>)subsets;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpec#getTrafficPolicy}
         * @param trafficPolicy the value to be set.
         * @return {@code this}
         */
        public Builder trafficPolicy(imports.io.istio.networking.DestinationRuleSpecTrafficPolicy trafficPolicy) {
            this.trafficPolicy = trafficPolicy;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpec#getWorkloadSelector}
         * @param workloadSelector the value to be set.
         * @return {@code this}
         */
        public Builder workloadSelector(imports.io.istio.networking.DestinationRuleSpecWorkloadSelector workloadSelector) {
            this.workloadSelector = workloadSelector;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpec {
        private final java.util.List<java.lang.String> exportTo;
        private final java.lang.String host;
        private final java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsets> subsets;
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicy trafficPolicy;
        private final imports.io.istio.networking.DestinationRuleSpecWorkloadSelector workloadSelector;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.exportTo = software.amazon.jsii.Kernel.get(this, "exportTo", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.host = software.amazon.jsii.Kernel.get(this, "host", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.subsets = software.amazon.jsii.Kernel.get(this, "subsets", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsets.class)));
            this.trafficPolicy = software.amazon.jsii.Kernel.get(this, "trafficPolicy", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicy.class));
            this.workloadSelector = software.amazon.jsii.Kernel.get(this, "workloadSelector", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecWorkloadSelector.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.exportTo = builder.exportTo;
            this.host = builder.host;
            this.subsets = (java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsets>)builder.subsets;
            this.trafficPolicy = builder.trafficPolicy;
            this.workloadSelector = builder.workloadSelector;
        }

        @Override
        public final java.util.List<java.lang.String> getExportTo() {
            return this.exportTo;
        }

        @Override
        public final java.lang.String getHost() {
            return this.host;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsets> getSubsets() {
            return this.subsets;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicy getTrafficPolicy() {
            return this.trafficPolicy;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecWorkloadSelector getWorkloadSelector() {
            return this.workloadSelector;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getExportTo() != null) {
                data.set("exportTo", om.valueToTree(this.getExportTo()));
            }
            if (this.getHost() != null) {
                data.set("host", om.valueToTree(this.getHost()));
            }
            if (this.getSubsets() != null) {
                data.set("subsets", om.valueToTree(this.getSubsets()));
            }
            if (this.getTrafficPolicy() != null) {
                data.set("trafficPolicy", om.valueToTree(this.getTrafficPolicy()));
            }
            if (this.getWorkloadSelector() != null) {
                data.set("workloadSelector", om.valueToTree(this.getWorkloadSelector()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpec.Jsii$Proxy that = (DestinationRuleSpec.Jsii$Proxy) o;

            if (this.exportTo != null ? !this.exportTo.equals(that.exportTo) : that.exportTo != null) return false;
            if (this.host != null ? !this.host.equals(that.host) : that.host != null) return false;
            if (this.subsets != null ? !this.subsets.equals(that.subsets) : that.subsets != null) return false;
            if (this.trafficPolicy != null ? !this.trafficPolicy.equals(that.trafficPolicy) : that.trafficPolicy != null) return false;
            return this.workloadSelector != null ? this.workloadSelector.equals(that.workloadSelector) : that.workloadSelector == null;
        }

        @Override
        public final int hashCode() {
            int result = this.exportTo != null ? this.exportTo.hashCode() : 0;
            result = 31 * result + (this.host != null ? this.host.hashCode() : 0);
            result = 31 * result + (this.subsets != null ? this.subsets.hashCode() : 0);
            result = 31 * result + (this.trafficPolicy != null ? this.trafficPolicy.hashCode() : 0);
            result = 31 * result + (this.workloadSelector != null ? this.workloadSelector.hashCode() : 0);
            return result;
        }
    }
}
