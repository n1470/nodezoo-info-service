package imports.io.istio.security;

/**
 * Operation specifies the operation of a request.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.457Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.AuthorizationPolicySpecRulesToOperation")
@software.amazon.jsii.Jsii.Proxy(AuthorizationPolicySpecRulesToOperation.Jsii$Proxy.class)
public interface AuthorizationPolicySpecRulesToOperation extends software.amazon.jsii.JsiiSerializable {

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getHosts() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getMethods() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotHosts() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotMethods() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotPaths() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotPorts() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getPaths() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getPorts() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link AuthorizationPolicySpecRulesToOperation}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link AuthorizationPolicySpecRulesToOperation}
     */
    public static final class Builder implements software.amazon.jsii.Builder<AuthorizationPolicySpecRulesToOperation> {
        java.util.List<java.lang.String> hosts;
        java.util.List<java.lang.String> methods;
        java.util.List<java.lang.String> notHosts;
        java.util.List<java.lang.String> notMethods;
        java.util.List<java.lang.String> notPaths;
        java.util.List<java.lang.String> notPorts;
        java.util.List<java.lang.String> paths;
        java.util.List<java.lang.String> ports;

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesToOperation#getHosts}
         * @param hosts Optional.
         * @return {@code this}
         */
        public Builder hosts(java.util.List<java.lang.String> hosts) {
            this.hosts = hosts;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesToOperation#getMethods}
         * @param methods Optional.
         * @return {@code this}
         */
        public Builder methods(java.util.List<java.lang.String> methods) {
            this.methods = methods;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesToOperation#getNotHosts}
         * @param notHosts Optional.
         * @return {@code this}
         */
        public Builder notHosts(java.util.List<java.lang.String> notHosts) {
            this.notHosts = notHosts;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesToOperation#getNotMethods}
         * @param notMethods Optional.
         * @return {@code this}
         */
        public Builder notMethods(java.util.List<java.lang.String> notMethods) {
            this.notMethods = notMethods;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesToOperation#getNotPaths}
         * @param notPaths Optional.
         * @return {@code this}
         */
        public Builder notPaths(java.util.List<java.lang.String> notPaths) {
            this.notPaths = notPaths;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesToOperation#getNotPorts}
         * @param notPorts Optional.
         * @return {@code this}
         */
        public Builder notPorts(java.util.List<java.lang.String> notPorts) {
            this.notPorts = notPorts;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesToOperation#getPaths}
         * @param paths Optional.
         * @return {@code this}
         */
        public Builder paths(java.util.List<java.lang.String> paths) {
            this.paths = paths;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesToOperation#getPorts}
         * @param ports Optional.
         * @return {@code this}
         */
        public Builder ports(java.util.List<java.lang.String> ports) {
            this.ports = ports;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link AuthorizationPolicySpecRulesToOperation}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public AuthorizationPolicySpecRulesToOperation build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link AuthorizationPolicySpecRulesToOperation}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements AuthorizationPolicySpecRulesToOperation {
        private final java.util.List<java.lang.String> hosts;
        private final java.util.List<java.lang.String> methods;
        private final java.util.List<java.lang.String> notHosts;
        private final java.util.List<java.lang.String> notMethods;
        private final java.util.List<java.lang.String> notPaths;
        private final java.util.List<java.lang.String> notPorts;
        private final java.util.List<java.lang.String> paths;
        private final java.util.List<java.lang.String> ports;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.hosts = software.amazon.jsii.Kernel.get(this, "hosts", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.methods = software.amazon.jsii.Kernel.get(this, "methods", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.notHosts = software.amazon.jsii.Kernel.get(this, "notHosts", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.notMethods = software.amazon.jsii.Kernel.get(this, "notMethods", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.notPaths = software.amazon.jsii.Kernel.get(this, "notPaths", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.notPorts = software.amazon.jsii.Kernel.get(this, "notPorts", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.paths = software.amazon.jsii.Kernel.get(this, "paths", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.ports = software.amazon.jsii.Kernel.get(this, "ports", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.hosts = builder.hosts;
            this.methods = builder.methods;
            this.notHosts = builder.notHosts;
            this.notMethods = builder.notMethods;
            this.notPaths = builder.notPaths;
            this.notPorts = builder.notPorts;
            this.paths = builder.paths;
            this.ports = builder.ports;
        }

        @Override
        public final java.util.List<java.lang.String> getHosts() {
            return this.hosts;
        }

        @Override
        public final java.util.List<java.lang.String> getMethods() {
            return this.methods;
        }

        @Override
        public final java.util.List<java.lang.String> getNotHosts() {
            return this.notHosts;
        }

        @Override
        public final java.util.List<java.lang.String> getNotMethods() {
            return this.notMethods;
        }

        @Override
        public final java.util.List<java.lang.String> getNotPaths() {
            return this.notPaths;
        }

        @Override
        public final java.util.List<java.lang.String> getNotPorts() {
            return this.notPorts;
        }

        @Override
        public final java.util.List<java.lang.String> getPaths() {
            return this.paths;
        }

        @Override
        public final java.util.List<java.lang.String> getPorts() {
            return this.ports;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getHosts() != null) {
                data.set("hosts", om.valueToTree(this.getHosts()));
            }
            if (this.getMethods() != null) {
                data.set("methods", om.valueToTree(this.getMethods()));
            }
            if (this.getNotHosts() != null) {
                data.set("notHosts", om.valueToTree(this.getNotHosts()));
            }
            if (this.getNotMethods() != null) {
                data.set("notMethods", om.valueToTree(this.getNotMethods()));
            }
            if (this.getNotPaths() != null) {
                data.set("notPaths", om.valueToTree(this.getNotPaths()));
            }
            if (this.getNotPorts() != null) {
                data.set("notPorts", om.valueToTree(this.getNotPorts()));
            }
            if (this.getPaths() != null) {
                data.set("paths", om.valueToTree(this.getPaths()));
            }
            if (this.getPorts() != null) {
                data.set("ports", om.valueToTree(this.getPorts()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.AuthorizationPolicySpecRulesToOperation"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AuthorizationPolicySpecRulesToOperation.Jsii$Proxy that = (AuthorizationPolicySpecRulesToOperation.Jsii$Proxy) o;

            if (this.hosts != null ? !this.hosts.equals(that.hosts) : that.hosts != null) return false;
            if (this.methods != null ? !this.methods.equals(that.methods) : that.methods != null) return false;
            if (this.notHosts != null ? !this.notHosts.equals(that.notHosts) : that.notHosts != null) return false;
            if (this.notMethods != null ? !this.notMethods.equals(that.notMethods) : that.notMethods != null) return false;
            if (this.notPaths != null ? !this.notPaths.equals(that.notPaths) : that.notPaths != null) return false;
            if (this.notPorts != null ? !this.notPorts.equals(that.notPorts) : that.notPorts != null) return false;
            if (this.paths != null ? !this.paths.equals(that.paths) : that.paths != null) return false;
            return this.ports != null ? this.ports.equals(that.ports) : that.ports == null;
        }

        @Override
        public final int hashCode() {
            int result = this.hosts != null ? this.hosts.hashCode() : 0;
            result = 31 * result + (this.methods != null ? this.methods.hashCode() : 0);
            result = 31 * result + (this.notHosts != null ? this.notHosts.hashCode() : 0);
            result = 31 * result + (this.notMethods != null ? this.notMethods.hashCode() : 0);
            result = 31 * result + (this.notPaths != null ? this.notPaths.hashCode() : 0);
            result = 31 * result + (this.notPorts != null ? this.notPorts.hashCode() : 0);
            result = 31 * result + (this.paths != null ? this.paths.hashCode() : 0);
            result = 31 * result + (this.ports != null ? this.ports.hashCode() : 0);
            return result;
        }
    }
}
