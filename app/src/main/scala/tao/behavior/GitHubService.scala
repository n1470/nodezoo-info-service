package tao.behavior

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import tao.core.PackageQuery

object GitHubService extends GenericPackageService {

  def apply(baseUrl: String): Behavior[PackageQuery] = Behaviors.receive { (context, message) =>
    receive(
      requestUrl = s"$baseUrl/nodezoo-github-service/app/info?q=${message.packageName}",
      sourceLabel = "github",
      context,
      message
    )
    
    Behaviors.same
  }
}
