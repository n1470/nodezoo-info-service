package imports.io.istio.networking;

/**
 * Configuration affecting label/content routing, sni routing, etc.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/networking/virtual-service.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.463Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpec")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpec.Jsii$Proxy.class)
public interface VirtualServiceSpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * A list of namespaces to which this virtual service is exported.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getExportTo() {
        return null;
    }

    /**
     * The names of gateways and sidecars that should apply these routes.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getGateways() {
        return null;
    }

    /**
     * The destination hosts to which traffic is being sent.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getHosts() {
        return null;
    }

    /**
     * An ordered list of route rules for HTTP traffic.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.VirtualServiceSpecHttp> getHttp() {
        return null;
    }

    /**
     * An ordered list of route rules for opaque TCP traffic.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.VirtualServiceSpecTcp> getTcp() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.VirtualServiceSpecTls> getTls() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpec> {
        java.util.List<java.lang.String> exportTo;
        java.util.List<java.lang.String> gateways;
        java.util.List<java.lang.String> hosts;
        java.util.List<imports.io.istio.networking.VirtualServiceSpecHttp> http;
        java.util.List<imports.io.istio.networking.VirtualServiceSpecTcp> tcp;
        java.util.List<imports.io.istio.networking.VirtualServiceSpecTls> tls;

        /**
         * Sets the value of {@link VirtualServiceSpec#getExportTo}
         * @param exportTo A list of namespaces to which this virtual service is exported.
         * @return {@code this}
         */
        public Builder exportTo(java.util.List<java.lang.String> exportTo) {
            this.exportTo = exportTo;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpec#getGateways}
         * @param gateways The names of gateways and sidecars that should apply these routes.
         * @return {@code this}
         */
        public Builder gateways(java.util.List<java.lang.String> gateways) {
            this.gateways = gateways;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpec#getHosts}
         * @param hosts The destination hosts to which traffic is being sent.
         * @return {@code this}
         */
        public Builder hosts(java.util.List<java.lang.String> hosts) {
            this.hosts = hosts;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpec#getHttp}
         * @param http An ordered list of route rules for HTTP traffic.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder http(java.util.List<? extends imports.io.istio.networking.VirtualServiceSpecHttp> http) {
            this.http = (java.util.List<imports.io.istio.networking.VirtualServiceSpecHttp>)http;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpec#getTcp}
         * @param tcp An ordered list of route rules for opaque TCP traffic.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder tcp(java.util.List<? extends imports.io.istio.networking.VirtualServiceSpecTcp> tcp) {
            this.tcp = (java.util.List<imports.io.istio.networking.VirtualServiceSpecTcp>)tcp;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpec#getTls}
         * @param tls the value to be set.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder tls(java.util.List<? extends imports.io.istio.networking.VirtualServiceSpecTls> tls) {
            this.tls = (java.util.List<imports.io.istio.networking.VirtualServiceSpecTls>)tls;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpec {
        private final java.util.List<java.lang.String> exportTo;
        private final java.util.List<java.lang.String> gateways;
        private final java.util.List<java.lang.String> hosts;
        private final java.util.List<imports.io.istio.networking.VirtualServiceSpecHttp> http;
        private final java.util.List<imports.io.istio.networking.VirtualServiceSpecTcp> tcp;
        private final java.util.List<imports.io.istio.networking.VirtualServiceSpecTls> tls;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.exportTo = software.amazon.jsii.Kernel.get(this, "exportTo", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.gateways = software.amazon.jsii.Kernel.get(this, "gateways", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.hosts = software.amazon.jsii.Kernel.get(this, "hosts", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.http = software.amazon.jsii.Kernel.get(this, "http", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttp.class)));
            this.tcp = software.amazon.jsii.Kernel.get(this, "tcp", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecTcp.class)));
            this.tls = software.amazon.jsii.Kernel.get(this, "tls", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecTls.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.exportTo = builder.exportTo;
            this.gateways = builder.gateways;
            this.hosts = builder.hosts;
            this.http = (java.util.List<imports.io.istio.networking.VirtualServiceSpecHttp>)builder.http;
            this.tcp = (java.util.List<imports.io.istio.networking.VirtualServiceSpecTcp>)builder.tcp;
            this.tls = (java.util.List<imports.io.istio.networking.VirtualServiceSpecTls>)builder.tls;
        }

        @Override
        public final java.util.List<java.lang.String> getExportTo() {
            return this.exportTo;
        }

        @Override
        public final java.util.List<java.lang.String> getGateways() {
            return this.gateways;
        }

        @Override
        public final java.util.List<java.lang.String> getHosts() {
            return this.hosts;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.VirtualServiceSpecHttp> getHttp() {
            return this.http;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.VirtualServiceSpecTcp> getTcp() {
            return this.tcp;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.VirtualServiceSpecTls> getTls() {
            return this.tls;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getExportTo() != null) {
                data.set("exportTo", om.valueToTree(this.getExportTo()));
            }
            if (this.getGateways() != null) {
                data.set("gateways", om.valueToTree(this.getGateways()));
            }
            if (this.getHosts() != null) {
                data.set("hosts", om.valueToTree(this.getHosts()));
            }
            if (this.getHttp() != null) {
                data.set("http", om.valueToTree(this.getHttp()));
            }
            if (this.getTcp() != null) {
                data.set("tcp", om.valueToTree(this.getTcp()));
            }
            if (this.getTls() != null) {
                data.set("tls", om.valueToTree(this.getTls()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpec.Jsii$Proxy that = (VirtualServiceSpec.Jsii$Proxy) o;

            if (this.exportTo != null ? !this.exportTo.equals(that.exportTo) : that.exportTo != null) return false;
            if (this.gateways != null ? !this.gateways.equals(that.gateways) : that.gateways != null) return false;
            if (this.hosts != null ? !this.hosts.equals(that.hosts) : that.hosts != null) return false;
            if (this.http != null ? !this.http.equals(that.http) : that.http != null) return false;
            if (this.tcp != null ? !this.tcp.equals(that.tcp) : that.tcp != null) return false;
            return this.tls != null ? this.tls.equals(that.tls) : that.tls == null;
        }

        @Override
        public final int hashCode() {
            int result = this.exportTo != null ? this.exportTo.hashCode() : 0;
            result = 31 * result + (this.gateways != null ? this.gateways.hashCode() : 0);
            result = 31 * result + (this.hosts != null ? this.hosts.hashCode() : 0);
            result = 31 * result + (this.http != null ? this.http.hashCode() : 0);
            result = 31 * result + (this.tcp != null ? this.tcp.hashCode() : 0);
            result = 31 * result + (this.tls != null ? this.tls.hashCode() : 0);
            return result;
        }
    }
}
