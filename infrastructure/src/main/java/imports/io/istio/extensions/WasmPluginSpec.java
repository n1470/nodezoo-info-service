package imports.io.istio.extensions;

/**
 * Extend the functionality provided by the Istio proxy through WebAssembly filters.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/proxy_extensions/wasm-plugin.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:32:56.643Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.extensions.$Module.class, fqn = "ioistioextensions.WasmPluginSpec")
@software.amazon.jsii.Jsii.Proxy(WasmPluginSpec.Jsii$Proxy.class)
public interface WasmPluginSpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * The pull behaviour to be applied when fetching an OCI image.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.extensions.WasmPluginSpecImagePullPolicy getImagePullPolicy() {
        return null;
    }

    /**
     * Credentials to use for OCI image pulling.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getImagePullSecret() {
        return null;
    }

    /**
     * Determines where in the filter chain this `WasmPlugin` is to be injected.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.extensions.WasmPluginSpecPhase getPhase() {
        return null;
    }

    /**
     * The configuration that will be passed on to the plugin.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Object getPluginConfig() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getPluginName() {
        return null;
    }

    /**
     * Determines ordering of `WasmPlugins` in the same `phase`.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPriority() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.extensions.WasmPluginSpecSelector getSelector() {
        return null;
    }

    /**
     * SHA256 checksum that will be used to verify Wasm module or OCI container.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getSha256() {
        return null;
    }

    /**
     * URL of a Wasm module or OCI container.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getUrl() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getVerificationKey() {
        return null;
    }

    /**
     * Configuration for a Wasm VM.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.extensions.WasmPluginSpecVmConfig getVmConfig() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link WasmPluginSpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link WasmPluginSpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<WasmPluginSpec> {
        imports.io.istio.extensions.WasmPluginSpecImagePullPolicy imagePullPolicy;
        java.lang.String imagePullSecret;
        imports.io.istio.extensions.WasmPluginSpecPhase phase;
        java.lang.Object pluginConfig;
        java.lang.String pluginName;
        java.lang.Number priority;
        imports.io.istio.extensions.WasmPluginSpecSelector selector;
        java.lang.String sha256;
        java.lang.String url;
        java.lang.String verificationKey;
        imports.io.istio.extensions.WasmPluginSpecVmConfig vmConfig;

        /**
         * Sets the value of {@link WasmPluginSpec#getImagePullPolicy}
         * @param imagePullPolicy The pull behaviour to be applied when fetching an OCI image.
         * @return {@code this}
         */
        public Builder imagePullPolicy(imports.io.istio.extensions.WasmPluginSpecImagePullPolicy imagePullPolicy) {
            this.imagePullPolicy = imagePullPolicy;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getImagePullSecret}
         * @param imagePullSecret Credentials to use for OCI image pulling.
         * @return {@code this}
         */
        public Builder imagePullSecret(java.lang.String imagePullSecret) {
            this.imagePullSecret = imagePullSecret;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getPhase}
         * @param phase Determines where in the filter chain this `WasmPlugin` is to be injected.
         * @return {@code this}
         */
        public Builder phase(imports.io.istio.extensions.WasmPluginSpecPhase phase) {
            this.phase = phase;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getPluginConfig}
         * @param pluginConfig The configuration that will be passed on to the plugin.
         * @return {@code this}
         */
        public Builder pluginConfig(java.lang.Object pluginConfig) {
            this.pluginConfig = pluginConfig;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getPluginName}
         * @param pluginName the value to be set.
         * @return {@code this}
         */
        public Builder pluginName(java.lang.String pluginName) {
            this.pluginName = pluginName;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getPriority}
         * @param priority Determines ordering of `WasmPlugins` in the same `phase`.
         * @return {@code this}
         */
        public Builder priority(java.lang.Number priority) {
            this.priority = priority;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getSelector}
         * @param selector the value to be set.
         * @return {@code this}
         */
        public Builder selector(imports.io.istio.extensions.WasmPluginSpecSelector selector) {
            this.selector = selector;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getSha256}
         * @param sha256 SHA256 checksum that will be used to verify Wasm module or OCI container.
         * @return {@code this}
         */
        public Builder sha256(java.lang.String sha256) {
            this.sha256 = sha256;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getUrl}
         * @param url URL of a Wasm module or OCI container.
         * @return {@code this}
         */
        public Builder url(java.lang.String url) {
            this.url = url;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getVerificationKey}
         * @param verificationKey the value to be set.
         * @return {@code this}
         */
        public Builder verificationKey(java.lang.String verificationKey) {
            this.verificationKey = verificationKey;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpec#getVmConfig}
         * @param vmConfig Configuration for a Wasm VM.
         * @return {@code this}
         */
        public Builder vmConfig(imports.io.istio.extensions.WasmPluginSpecVmConfig vmConfig) {
            this.vmConfig = vmConfig;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link WasmPluginSpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public WasmPluginSpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link WasmPluginSpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements WasmPluginSpec {
        private final imports.io.istio.extensions.WasmPluginSpecImagePullPolicy imagePullPolicy;
        private final java.lang.String imagePullSecret;
        private final imports.io.istio.extensions.WasmPluginSpecPhase phase;
        private final java.lang.Object pluginConfig;
        private final java.lang.String pluginName;
        private final java.lang.Number priority;
        private final imports.io.istio.extensions.WasmPluginSpecSelector selector;
        private final java.lang.String sha256;
        private final java.lang.String url;
        private final java.lang.String verificationKey;
        private final imports.io.istio.extensions.WasmPluginSpecVmConfig vmConfig;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.imagePullPolicy = software.amazon.jsii.Kernel.get(this, "imagePullPolicy", software.amazon.jsii.NativeType.forClass(imports.io.istio.extensions.WasmPluginSpecImagePullPolicy.class));
            this.imagePullSecret = software.amazon.jsii.Kernel.get(this, "imagePullSecret", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.phase = software.amazon.jsii.Kernel.get(this, "phase", software.amazon.jsii.NativeType.forClass(imports.io.istio.extensions.WasmPluginSpecPhase.class));
            this.pluginConfig = software.amazon.jsii.Kernel.get(this, "pluginConfig", software.amazon.jsii.NativeType.forClass(java.lang.Object.class));
            this.pluginName = software.amazon.jsii.Kernel.get(this, "pluginName", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.priority = software.amazon.jsii.Kernel.get(this, "priority", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.selector = software.amazon.jsii.Kernel.get(this, "selector", software.amazon.jsii.NativeType.forClass(imports.io.istio.extensions.WasmPluginSpecSelector.class));
            this.sha256 = software.amazon.jsii.Kernel.get(this, "sha256", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.url = software.amazon.jsii.Kernel.get(this, "url", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.verificationKey = software.amazon.jsii.Kernel.get(this, "verificationKey", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.vmConfig = software.amazon.jsii.Kernel.get(this, "vmConfig", software.amazon.jsii.NativeType.forClass(imports.io.istio.extensions.WasmPluginSpecVmConfig.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.imagePullPolicy = builder.imagePullPolicy;
            this.imagePullSecret = builder.imagePullSecret;
            this.phase = builder.phase;
            this.pluginConfig = builder.pluginConfig;
            this.pluginName = builder.pluginName;
            this.priority = builder.priority;
            this.selector = builder.selector;
            this.sha256 = builder.sha256;
            this.url = builder.url;
            this.verificationKey = builder.verificationKey;
            this.vmConfig = builder.vmConfig;
        }

        @Override
        public final imports.io.istio.extensions.WasmPluginSpecImagePullPolicy getImagePullPolicy() {
            return this.imagePullPolicy;
        }

        @Override
        public final java.lang.String getImagePullSecret() {
            return this.imagePullSecret;
        }

        @Override
        public final imports.io.istio.extensions.WasmPluginSpecPhase getPhase() {
            return this.phase;
        }

        @Override
        public final java.lang.Object getPluginConfig() {
            return this.pluginConfig;
        }

        @Override
        public final java.lang.String getPluginName() {
            return this.pluginName;
        }

        @Override
        public final java.lang.Number getPriority() {
            return this.priority;
        }

        @Override
        public final imports.io.istio.extensions.WasmPluginSpecSelector getSelector() {
            return this.selector;
        }

        @Override
        public final java.lang.String getSha256() {
            return this.sha256;
        }

        @Override
        public final java.lang.String getUrl() {
            return this.url;
        }

        @Override
        public final java.lang.String getVerificationKey() {
            return this.verificationKey;
        }

        @Override
        public final imports.io.istio.extensions.WasmPluginSpecVmConfig getVmConfig() {
            return this.vmConfig;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getImagePullPolicy() != null) {
                data.set("imagePullPolicy", om.valueToTree(this.getImagePullPolicy()));
            }
            if (this.getImagePullSecret() != null) {
                data.set("imagePullSecret", om.valueToTree(this.getImagePullSecret()));
            }
            if (this.getPhase() != null) {
                data.set("phase", om.valueToTree(this.getPhase()));
            }
            if (this.getPluginConfig() != null) {
                data.set("pluginConfig", om.valueToTree(this.getPluginConfig()));
            }
            if (this.getPluginName() != null) {
                data.set("pluginName", om.valueToTree(this.getPluginName()));
            }
            if (this.getPriority() != null) {
                data.set("priority", om.valueToTree(this.getPriority()));
            }
            if (this.getSelector() != null) {
                data.set("selector", om.valueToTree(this.getSelector()));
            }
            if (this.getSha256() != null) {
                data.set("sha256", om.valueToTree(this.getSha256()));
            }
            if (this.getUrl() != null) {
                data.set("url", om.valueToTree(this.getUrl()));
            }
            if (this.getVerificationKey() != null) {
                data.set("verificationKey", om.valueToTree(this.getVerificationKey()));
            }
            if (this.getVmConfig() != null) {
                data.set("vmConfig", om.valueToTree(this.getVmConfig()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistioextensions.WasmPluginSpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            WasmPluginSpec.Jsii$Proxy that = (WasmPluginSpec.Jsii$Proxy) o;

            if (this.imagePullPolicy != null ? !this.imagePullPolicy.equals(that.imagePullPolicy) : that.imagePullPolicy != null) return false;
            if (this.imagePullSecret != null ? !this.imagePullSecret.equals(that.imagePullSecret) : that.imagePullSecret != null) return false;
            if (this.phase != null ? !this.phase.equals(that.phase) : that.phase != null) return false;
            if (this.pluginConfig != null ? !this.pluginConfig.equals(that.pluginConfig) : that.pluginConfig != null) return false;
            if (this.pluginName != null ? !this.pluginName.equals(that.pluginName) : that.pluginName != null) return false;
            if (this.priority != null ? !this.priority.equals(that.priority) : that.priority != null) return false;
            if (this.selector != null ? !this.selector.equals(that.selector) : that.selector != null) return false;
            if (this.sha256 != null ? !this.sha256.equals(that.sha256) : that.sha256 != null) return false;
            if (this.url != null ? !this.url.equals(that.url) : that.url != null) return false;
            if (this.verificationKey != null ? !this.verificationKey.equals(that.verificationKey) : that.verificationKey != null) return false;
            return this.vmConfig != null ? this.vmConfig.equals(that.vmConfig) : that.vmConfig == null;
        }

        @Override
        public final int hashCode() {
            int result = this.imagePullPolicy != null ? this.imagePullPolicy.hashCode() : 0;
            result = 31 * result + (this.imagePullSecret != null ? this.imagePullSecret.hashCode() : 0);
            result = 31 * result + (this.phase != null ? this.phase.hashCode() : 0);
            result = 31 * result + (this.pluginConfig != null ? this.pluginConfig.hashCode() : 0);
            result = 31 * result + (this.pluginName != null ? this.pluginName.hashCode() : 0);
            result = 31 * result + (this.priority != null ? this.priority.hashCode() : 0);
            result = 31 * result + (this.selector != null ? this.selector.hashCode() : 0);
            result = 31 * result + (this.sha256 != null ? this.sha256.hashCode() : 0);
            result = 31 * result + (this.url != null ? this.url.hashCode() : 0);
            result = 31 * result + (this.verificationKey != null ? this.verificationKey.hashCode() : 0);
            result = 31 * result + (this.vmConfig != null ? this.vmConfig.hashCode() : 0);
            return result;
        }
    }
}
