package imports.io.istio.networking;

/**
 * Traffic policies that apply to this subset.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.427Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicy")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecSubsetsTrafficPolicy.Jsii$Proxy.class)
public interface DestinationRuleSpecSubsetsTrafficPolicy extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyConnectionPool getConnectionPool() {
        return null;
    }

    /**
     * Settings controlling the load balancer algorithms.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer getLoadBalancer() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection getOutlierDetection() {
        return null;
    }

    /**
     * Traffic policies specific to individual ports.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettings> getPortLevelSettings() {
        return null;
    }

    /**
     * TLS related settings for connections to the upstream service.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTls getTls() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTunnel getTunnel() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecSubsetsTrafficPolicy}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecSubsetsTrafficPolicy}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecSubsetsTrafficPolicy> {
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyConnectionPool connectionPool;
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer loadBalancer;
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection outlierDetection;
        java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettings> portLevelSettings;
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTls tls;
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTunnel tunnel;

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicy#getConnectionPool}
         * @param connectionPool the value to be set.
         * @return {@code this}
         */
        public Builder connectionPool(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyConnectionPool connectionPool) {
            this.connectionPool = connectionPool;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicy#getLoadBalancer}
         * @param loadBalancer Settings controlling the load balancer algorithms.
         * @return {@code this}
         */
        public Builder loadBalancer(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer loadBalancer) {
            this.loadBalancer = loadBalancer;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicy#getOutlierDetection}
         * @param outlierDetection the value to be set.
         * @return {@code this}
         */
        public Builder outlierDetection(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection outlierDetection) {
            this.outlierDetection = outlierDetection;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicy#getPortLevelSettings}
         * @param portLevelSettings Traffic policies specific to individual ports.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder portLevelSettings(java.util.List<? extends imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettings> portLevelSettings) {
            this.portLevelSettings = (java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettings>)portLevelSettings;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicy#getTls}
         * @param tls TLS related settings for connections to the upstream service.
         * @return {@code this}
         */
        public Builder tls(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTls tls) {
            this.tls = tls;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicy#getTunnel}
         * @param tunnel the value to be set.
         * @return {@code this}
         */
        public Builder tunnel(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTunnel tunnel) {
            this.tunnel = tunnel;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecSubsetsTrafficPolicy}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecSubsetsTrafficPolicy build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecSubsetsTrafficPolicy}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecSubsetsTrafficPolicy {
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyConnectionPool connectionPool;
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer loadBalancer;
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection outlierDetection;
        private final java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettings> portLevelSettings;
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTls tls;
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTunnel tunnel;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.connectionPool = software.amazon.jsii.Kernel.get(this, "connectionPool", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyConnectionPool.class));
            this.loadBalancer = software.amazon.jsii.Kernel.get(this, "loadBalancer", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer.class));
            this.outlierDetection = software.amazon.jsii.Kernel.get(this, "outlierDetection", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection.class));
            this.portLevelSettings = software.amazon.jsii.Kernel.get(this, "portLevelSettings", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettings.class)));
            this.tls = software.amazon.jsii.Kernel.get(this, "tls", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTls.class));
            this.tunnel = software.amazon.jsii.Kernel.get(this, "tunnel", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTunnel.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.connectionPool = builder.connectionPool;
            this.loadBalancer = builder.loadBalancer;
            this.outlierDetection = builder.outlierDetection;
            this.portLevelSettings = (java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettings>)builder.portLevelSettings;
            this.tls = builder.tls;
            this.tunnel = builder.tunnel;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyConnectionPool getConnectionPool() {
            return this.connectionPool;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer getLoadBalancer() {
            return this.loadBalancer;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection getOutlierDetection() {
            return this.outlierDetection;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettings> getPortLevelSettings() {
            return this.portLevelSettings;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTls getTls() {
            return this.tls;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyTunnel getTunnel() {
            return this.tunnel;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getConnectionPool() != null) {
                data.set("connectionPool", om.valueToTree(this.getConnectionPool()));
            }
            if (this.getLoadBalancer() != null) {
                data.set("loadBalancer", om.valueToTree(this.getLoadBalancer()));
            }
            if (this.getOutlierDetection() != null) {
                data.set("outlierDetection", om.valueToTree(this.getOutlierDetection()));
            }
            if (this.getPortLevelSettings() != null) {
                data.set("portLevelSettings", om.valueToTree(this.getPortLevelSettings()));
            }
            if (this.getTls() != null) {
                data.set("tls", om.valueToTree(this.getTls()));
            }
            if (this.getTunnel() != null) {
                data.set("tunnel", om.valueToTree(this.getTunnel()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicy"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecSubsetsTrafficPolicy.Jsii$Proxy that = (DestinationRuleSpecSubsetsTrafficPolicy.Jsii$Proxy) o;

            if (this.connectionPool != null ? !this.connectionPool.equals(that.connectionPool) : that.connectionPool != null) return false;
            if (this.loadBalancer != null ? !this.loadBalancer.equals(that.loadBalancer) : that.loadBalancer != null) return false;
            if (this.outlierDetection != null ? !this.outlierDetection.equals(that.outlierDetection) : that.outlierDetection != null) return false;
            if (this.portLevelSettings != null ? !this.portLevelSettings.equals(that.portLevelSettings) : that.portLevelSettings != null) return false;
            if (this.tls != null ? !this.tls.equals(that.tls) : that.tls != null) return false;
            return this.tunnel != null ? this.tunnel.equals(that.tunnel) : that.tunnel == null;
        }

        @Override
        public final int hashCode() {
            int result = this.connectionPool != null ? this.connectionPool.hashCode() : 0;
            result = 31 * result + (this.loadBalancer != null ? this.loadBalancer.hashCode() : 0);
            result = 31 * result + (this.outlierDetection != null ? this.outlierDetection.hashCode() : 0);
            result = 31 * result + (this.portLevelSettings != null ? this.portLevelSettings.hashCode() : 0);
            result = 31 * result + (this.tls != null ? this.tls.hashCode() : 0);
            result = 31 * result + (this.tunnel != null ? this.tunnel.hashCode() : 0);
            return result;
        }
    }
}
