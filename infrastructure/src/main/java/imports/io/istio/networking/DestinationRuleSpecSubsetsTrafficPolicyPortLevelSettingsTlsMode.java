package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.435Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsTlsMode")
public enum DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsTlsMode {
    /**
     * DISABLE.
     */
    DISABLE,
    /**
     * SIMPLE.
     */
    SIMPLE,
    /**
     * MUTUAL.
     */
    MUTUAL,
    /**
     * ISTIO_MUTUAL.
     */
    ISTIO_MUTUAL,
}
