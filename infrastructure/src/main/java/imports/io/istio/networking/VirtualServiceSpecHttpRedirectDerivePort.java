package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.467Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpRedirectDerivePort")
public enum VirtualServiceSpecHttpRedirectDerivePort {
    /**
     * FROM_PROTOCOL_DEFAULT.
     */
    FROM_PROTOCOL_DEFAULT,
    /**
     * FROM_REQUEST_PORT.
     */
    FROM_REQUEST_PORT,
}
