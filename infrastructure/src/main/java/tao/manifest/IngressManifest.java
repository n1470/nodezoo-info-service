package tao.manifest;

import imports.k8s.*;
import org.jetbrains.annotations.NotNull;
import tao.InfoServiceChart;

import java.util.List;
import java.util.Map;

public class IngressManifest {

    public static final String ID = "info-service-ingress";

    public interface IngressProps {
        List<IngressRule> rules();
        Map<String, String> annotations();
    }

    public IngressManifest(@NotNull InfoServiceChart chart,
                           @NotNull IngressProps ingressManifestProps) {
        final var selector = Map.of("name", ID);
        final var objectMeta = ObjectMeta.builder()
                .name(ID)
                .labels(selector)
                .annotations(ingressManifestProps.annotations())
                .build();
        final var spec = IngressSpec.builder()
                .rules(ingressManifestProps.rules())
                .build();
        final var ingressProps = KubeIngressProps.builder()
                .metadata(objectMeta)
                .spec(spec)
                .build();
        new KubeIngress(chart, "ingress", ingressProps);
    }

    public static class IngressPropsBuilder {

        public static AlbProps buildAlb(@NotNull String serviceName, int servicePort) {
            return new AlbProps(buildHttpIngress(serviceName, servicePort));
        }

        private static HttpIngressRuleValue buildHttpIngress(@NotNull String serviceName, int servicePort) {
            return HttpIngressRuleValue.builder()
                    .paths(List.of(HttpIngressPath.builder()
                            .path("/")
                            .pathType("Prefix")
                            .backend(IngressBackend.builder()
                                    .service(IngressServiceBackend.builder()
                                            .name(serviceName)
                                            .port(ServiceBackendPort.builder()
                                                    .number(servicePort)
                                                    .build())
                                            .build())
                                    .build())
                            .build()))
                    .build();
        }
    }

    public record AlbProps(@NotNull HttpIngressRuleValue httpIngress) implements IngressProps {

        @Override
        public List<IngressRule> rules() {
            final var rule = IngressRule.builder()
                    .http(this.httpIngress)
                    .build();
            return List.of(rule);
        }

        @Override
        public Map<String, String> annotations() {
            return Map.of("kubernetes.io/ingress.class", "alb");
        }
    }
}
