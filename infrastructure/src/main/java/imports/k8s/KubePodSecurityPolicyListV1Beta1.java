package imports.k8s;

/**
 * PodSecurityPolicyList is a list of PodSecurityPolicy objects.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.278Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.KubePodSecurityPolicyListV1Beta1")
public class KubePodSecurityPolicyListV1Beta1 extends org.cdk8s.ApiObject {

    protected KubePodSecurityPolicyListV1Beta1(final software.amazon.jsii.JsiiObjectRef objRef) {
        super(objRef);
    }

    protected KubePodSecurityPolicyListV1Beta1(final software.amazon.jsii.JsiiObject.InitializationMode initializationMode) {
        super(initializationMode);
    }

    static {
        GVK = software.amazon.jsii.JsiiObject.jsiiStaticGet(KubePodSecurityPolicyListV1Beta1.class, "GVK", software.amazon.jsii.NativeType.forClass(org.cdk8s.GroupVersionKind.class));
    }

    /**
     * Defines a "io.k8s.api.policy.v1beta1.PodSecurityPolicyList" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     * @param props initialization props. This parameter is required.
     */
    public KubePodSecurityPolicyListV1Beta1(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull String id, final @org.jetbrains.annotations.NotNull KubePodSecurityPolicyListV1Beta1Props props) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required"), java.util.Objects.requireNonNull(props, "props is required") });
    }

    /**
     * Renders a Kubernetes manifest for "io.k8s.api.policy.v1beta1.PodSecurityPolicyList".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     * <p>
     * @param props initialization props. This parameter is required.
     */
    public static @org.jetbrains.annotations.NotNull Object manifest(final @org.jetbrains.annotations.NotNull KubePodSecurityPolicyListV1Beta1Props props) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(KubePodSecurityPolicyListV1Beta1.class, "manifest", software.amazon.jsii.NativeType.forClass(Object.class), new Object[] { java.util.Objects.requireNonNull(props, "props is required") });
    }

    /**
     * Renders the object to Kubernetes JSON.
     */
    @Override
    public @org.jetbrains.annotations.NotNull Object toJson() {
        return software.amazon.jsii.Kernel.call(this, "toJson", software.amazon.jsii.NativeType.forClass(Object.class));
    }

    /**
     * Returns the apiVersion and kind for "io.k8s.api.policy.v1beta1.PodSecurityPolicyList".
     */
    public final static org.cdk8s.GroupVersionKind GVK;

    /**
     * A fluent builder for {@link KubePodSecurityPolicyListV1Beta1}.
     */
    public static final class Builder implements software.amazon.jsii.Builder<KubePodSecurityPolicyListV1Beta1> {
        /**
         * @return a new instance of {@link Builder}.
         * @param scope the scope in which to define this object. This parameter is required.
         * @param id a scope-local name for the object. This parameter is required.
         */
        public static Builder create(final software.constructs.Construct scope, final String id) {
            return new Builder(scope, id);
        }

        private final software.constructs.Construct scope;
        private final String id;
        private final KubePodSecurityPolicyListV1Beta1Props.Builder props;

        private Builder(final software.constructs.Construct scope, final String id) {
            this.scope = scope;
            this.id = id;
            this.props = new KubePodSecurityPolicyListV1Beta1Props.Builder();
        }

        /**
         * items is a list of schema objects.
         * <p>
         * @return {@code this}
         * @param items items is a list of schema objects. This parameter is required.
         */
        public Builder items(final java.util.List<? extends KubePodSecurityPolicyV1Beta1Props> items) {
            this.props.items(items);
            return this;
        }

        /**
         * Standard list metadata.
         * <p>
         * More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
         * <p>
         * @return {@code this}
         * @param metadata Standard list metadata. This parameter is required.
         */
        public Builder metadata(final ListMeta metadata) {
            this.props.metadata(metadata);
            return this;
        }

        /**
         * @returns a newly built instance of {@link KubePodSecurityPolicyListV1Beta1}.
         */
        @Override
        public KubePodSecurityPolicyListV1Beta1 build() {
            return new KubePodSecurityPolicyListV1Beta1(
                this.scope,
                this.id,
                this.props.build()
            );
        }
    }
}
