package imports.io.istio.security;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.456Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.AuthorizationPolicySpecRulesFrom")
@software.amazon.jsii.Jsii.Proxy(AuthorizationPolicySpecRulesFrom.Jsii$Proxy.class)
public interface AuthorizationPolicySpecRulesFrom extends software.amazon.jsii.JsiiSerializable {

    /**
     * Source specifies the source of a request.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.AuthorizationPolicySpecRulesFromSource getSource() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link AuthorizationPolicySpecRulesFrom}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link AuthorizationPolicySpecRulesFrom}
     */
    public static final class Builder implements software.amazon.jsii.Builder<AuthorizationPolicySpecRulesFrom> {
        imports.io.istio.security.AuthorizationPolicySpecRulesFromSource source;

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFrom#getSource}
         * @param source Source specifies the source of a request.
         * @return {@code this}
         */
        public Builder source(imports.io.istio.security.AuthorizationPolicySpecRulesFromSource source) {
            this.source = source;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link AuthorizationPolicySpecRulesFrom}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public AuthorizationPolicySpecRulesFrom build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link AuthorizationPolicySpecRulesFrom}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements AuthorizationPolicySpecRulesFrom {
        private final imports.io.istio.security.AuthorizationPolicySpecRulesFromSource source;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.source = software.amazon.jsii.Kernel.get(this, "source", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpecRulesFromSource.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.source = builder.source;
        }

        @Override
        public final imports.io.istio.security.AuthorizationPolicySpecRulesFromSource getSource() {
            return this.source;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getSource() != null) {
                data.set("source", om.valueToTree(this.getSource()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.AuthorizationPolicySpecRulesFrom"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AuthorizationPolicySpecRulesFrom.Jsii$Proxy that = (AuthorizationPolicySpecRulesFrom.Jsii$Proxy) o;

            return this.source != null ? this.source.equals(that.source) : that.source == null;
        }

        @Override
        public final int hashCode() {
            int result = this.source != null ? this.source.hashCode() : 0;
            return result;
        }
    }
}
