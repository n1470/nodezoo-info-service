package imports.io.istio.networking;

/**
 * Match on envoy listener attributes.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.442Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesMatchListener")
@software.amazon.jsii.Jsii.Proxy(EnvoyFilterSpecConfigPatchesMatchListener.Jsii$Proxy.class)
public interface EnvoyFilterSpecConfigPatchesMatchListener extends software.amazon.jsii.JsiiSerializable {

    /**
     * Match a specific filter chain in a listener.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChain getFilterChain() {
        return null;
    }

    /**
     * Match a specific listener by its name.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getPortName() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPortNumber() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link EnvoyFilterSpecConfigPatchesMatchListener}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link EnvoyFilterSpecConfigPatchesMatchListener}
     */
    public static final class Builder implements software.amazon.jsii.Builder<EnvoyFilterSpecConfigPatchesMatchListener> {
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChain filterChain;
        java.lang.String name;
        java.lang.String portName;
        java.lang.Number portNumber;

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListener#getFilterChain}
         * @param filterChain Match a specific filter chain in a listener.
         * @return {@code this}
         */
        public Builder filterChain(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChain filterChain) {
            this.filterChain = filterChain;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListener#getName}
         * @param name Match a specific listener by its name.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListener#getPortName}
         * @param portName the value to be set.
         * @return {@code this}
         */
        public Builder portName(java.lang.String portName) {
            this.portName = portName;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListener#getPortNumber}
         * @param portNumber the value to be set.
         * @return {@code this}
         */
        public Builder portNumber(java.lang.Number portNumber) {
            this.portNumber = portNumber;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link EnvoyFilterSpecConfigPatchesMatchListener}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public EnvoyFilterSpecConfigPatchesMatchListener build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link EnvoyFilterSpecConfigPatchesMatchListener}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements EnvoyFilterSpecConfigPatchesMatchListener {
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChain filterChain;
        private final java.lang.String name;
        private final java.lang.String portName;
        private final java.lang.Number portNumber;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.filterChain = software.amazon.jsii.Kernel.get(this, "filterChain", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChain.class));
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.portName = software.amazon.jsii.Kernel.get(this, "portName", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.portNumber = software.amazon.jsii.Kernel.get(this, "portNumber", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.filterChain = builder.filterChain;
            this.name = builder.name;
            this.portName = builder.portName;
            this.portNumber = builder.portNumber;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChain getFilterChain() {
            return this.filterChain;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final java.lang.String getPortName() {
            return this.portName;
        }

        @Override
        public final java.lang.Number getPortNumber() {
            return this.portNumber;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getFilterChain() != null) {
                data.set("filterChain", om.valueToTree(this.getFilterChain()));
            }
            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getPortName() != null) {
                data.set("portName", om.valueToTree(this.getPortName()));
            }
            if (this.getPortNumber() != null) {
                data.set("portNumber", om.valueToTree(this.getPortNumber()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.EnvoyFilterSpecConfigPatchesMatchListener"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EnvoyFilterSpecConfigPatchesMatchListener.Jsii$Proxy that = (EnvoyFilterSpecConfigPatchesMatchListener.Jsii$Proxy) o;

            if (this.filterChain != null ? !this.filterChain.equals(that.filterChain) : that.filterChain != null) return false;
            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            if (this.portName != null ? !this.portName.equals(that.portName) : that.portName != null) return false;
            return this.portNumber != null ? this.portNumber.equals(that.portNumber) : that.portNumber == null;
        }

        @Override
        public final int hashCode() {
            int result = this.filterChain != null ? this.filterChain.hashCode() : 0;
            result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
            result = 31 * result + (this.portName != null ? this.portName.hashCode() : 0);
            result = 31 * result + (this.portNumber != null ? this.portNumber.hashCode() : 0);
            return result;
        }
    }
}
