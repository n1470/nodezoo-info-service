package tao

import akka.Done
import akka.actor.CoordinatedShutdown
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Directives._
import tao.behavior.{GitHubService, NpmService}
import tao.core.{PackageInfo, PackageQuery}
import tao.json.PackageInfoFactory

import java.util.concurrent.TimeUnit
import scala.concurrent.Future
import scala.concurrent.duration.{DurationInt, FiniteDuration}
import scala.util.{Failure, Success}


object InfoServer extends App {

  import system.executionContext

  private val rootBehavior = Behaviors.setup[PackageInfo] { context =>
    import akka.actor.typed.scaladsl.AskPattern._
    import akka.util.Timeout

    val appConfig = AppConfig(args,
      defaults = new AppConfig(port = 8080,
        timeout = 10,
        npmServiceConfig = PackageServiceConfig(baseUrl = "http://nodezoo-npm-service"),
        gitHubServiceConfig = PackageServiceConfig(baseUrl = "http://nodezoo-github-service")
      )
    )

    implicit val contextSystem: ActorSystem[Nothing] = context.system
    implicit val timeout: Timeout = appConfig.timeout.seconds

    val npmServiceSystem = context.spawn(NpmService(appConfig.npmServiceConfig.baseUrl), "NpmServiceSystem")
    val ghServiceSystem = context.spawn(GitHubService(appConfig.gitHubServiceConfig.baseUrl), "GithubServiceSystem")

    val route =
      pathPrefix("info") {
        get {
          parameters("q") { pkgName =>
            val futures = Seq(npmServiceSystem, ghServiceSystem)
              .map(_.ask[PackageInfo](PackageQuery(pkgName, _)))
            val result = Future
              .reduceLeft(futures)((R, T) => PackageInfoFactory(Seq(R, T)))
              .map(_.content)
            onComplete(result) {
              case Success(value) =>
                if (!value.equals("{}")) {
                  complete(HttpEntity(ContentTypes.`application/json`, value))
                } else {
                  complete(StatusCodes.NotFound)
                }
              case Failure(exception) => complete(StatusCodes.InternalServerError, exception.getMessage)
            }
          }
        }
      }

    val bindingFuture = Http().newServerAt("0.0.0.0", appConfig.port).bind(route)
    context.log.info(appConfig.toString)
    context.log.info(s"Server now online. Please navigate to http://0.0.0.0:${appConfig.port}/info")
    CoordinatedShutdown(system).addTask(CoordinatedShutdown.PhaseServiceUnbind, "http_shutdown") { () =>
      bindingFuture
        .flatMap(_.terminate(hardDeadline = FiniteDuration(30, TimeUnit.SECONDS)))
        .map { _ => Done }
    }

    Behaviors.empty
  }

  val system: ActorSystem[PackageInfo] = ActorSystem(rootBehavior, "info-system")
}
