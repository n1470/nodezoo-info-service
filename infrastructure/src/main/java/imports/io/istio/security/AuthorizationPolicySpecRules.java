package imports.io.istio.security;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.455Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.AuthorizationPolicySpecRules")
@software.amazon.jsii.Jsii.Proxy(AuthorizationPolicySpecRules.Jsii$Proxy.class)
public interface AuthorizationPolicySpecRules extends software.amazon.jsii.JsiiSerializable {

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesFrom> getFrom() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesTo> getTo() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesWhen> getWhen() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link AuthorizationPolicySpecRules}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link AuthorizationPolicySpecRules}
     */
    public static final class Builder implements software.amazon.jsii.Builder<AuthorizationPolicySpecRules> {
        java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesFrom> from;
        java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesTo> to;
        java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesWhen> when;

        /**
         * Sets the value of {@link AuthorizationPolicySpecRules#getFrom}
         * @param from Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder from(java.util.List<? extends imports.io.istio.security.AuthorizationPolicySpecRulesFrom> from) {
            this.from = (java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesFrom>)from;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRules#getTo}
         * @param to Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder to(java.util.List<? extends imports.io.istio.security.AuthorizationPolicySpecRulesTo> to) {
            this.to = (java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesTo>)to;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRules#getWhen}
         * @param when Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder when(java.util.List<? extends imports.io.istio.security.AuthorizationPolicySpecRulesWhen> when) {
            this.when = (java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesWhen>)when;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link AuthorizationPolicySpecRules}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public AuthorizationPolicySpecRules build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link AuthorizationPolicySpecRules}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements AuthorizationPolicySpecRules {
        private final java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesFrom> from;
        private final java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesTo> to;
        private final java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesWhen> when;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.from = software.amazon.jsii.Kernel.get(this, "from", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpecRulesFrom.class)));
            this.to = software.amazon.jsii.Kernel.get(this, "to", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpecRulesTo.class)));
            this.when = software.amazon.jsii.Kernel.get(this, "when", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpecRulesWhen.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.from = (java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesFrom>)builder.from;
            this.to = (java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesTo>)builder.to;
            this.when = (java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesWhen>)builder.when;
        }

        @Override
        public final java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesFrom> getFrom() {
            return this.from;
        }

        @Override
        public final java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesTo> getTo() {
            return this.to;
        }

        @Override
        public final java.util.List<imports.io.istio.security.AuthorizationPolicySpecRulesWhen> getWhen() {
            return this.when;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getFrom() != null) {
                data.set("from", om.valueToTree(this.getFrom()));
            }
            if (this.getTo() != null) {
                data.set("to", om.valueToTree(this.getTo()));
            }
            if (this.getWhen() != null) {
                data.set("when", om.valueToTree(this.getWhen()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.AuthorizationPolicySpecRules"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AuthorizationPolicySpecRules.Jsii$Proxy that = (AuthorizationPolicySpecRules.Jsii$Proxy) o;

            if (this.from != null ? !this.from.equals(that.from) : that.from != null) return false;
            if (this.to != null ? !this.to.equals(that.to) : that.to != null) return false;
            return this.when != null ? this.when.equals(that.when) : that.when == null;
        }

        @Override
        public final int hashCode() {
            int result = this.from != null ? this.from.hashCode() : 0;
            result = 31 * result + (this.to != null ? this.to.hashCode() : 0);
            result = 31 * result + (this.when != null ? this.when.hashCode() : 0);
            return result;
        }
    }
}
