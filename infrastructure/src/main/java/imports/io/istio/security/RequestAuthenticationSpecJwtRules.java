package imports.io.istio.security;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.464Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.RequestAuthenticationSpecJwtRules")
@software.amazon.jsii.Jsii.Proxy(RequestAuthenticationSpecJwtRules.Jsii$Proxy.class)
public interface RequestAuthenticationSpecJwtRules extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getAudiences() {
        return null;
    }

    /**
     * If set to true, the original token will be kept for the upstream request.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getForwardOriginalToken() {
        return null;
    }

    /**
     * List of header locations from which JWT is expected.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRulesFromHeaders> getFromHeaders() {
        return null;
    }

    /**
     * List of query parameters from which JWT is expected.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getFromParams() {
        return null;
    }

    /**
     * Identifies the issuer that issued the JWT.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getIssuer() {
        return null;
    }

    /**
     * JSON Web Key Set of public keys to validate signature of the JWT.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getJwks() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getJwksUri() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getOutputPayloadToHeader() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link RequestAuthenticationSpecJwtRules}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link RequestAuthenticationSpecJwtRules}
     */
    public static final class Builder implements software.amazon.jsii.Builder<RequestAuthenticationSpecJwtRules> {
        java.util.List<java.lang.String> audiences;
        java.lang.Boolean forwardOriginalToken;
        java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRulesFromHeaders> fromHeaders;
        java.util.List<java.lang.String> fromParams;
        java.lang.String issuer;
        java.lang.String jwks;
        java.lang.String jwksUri;
        java.lang.String outputPayloadToHeader;

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRules#getAudiences}
         * @param audiences the value to be set.
         * @return {@code this}
         */
        public Builder audiences(java.util.List<java.lang.String> audiences) {
            this.audiences = audiences;
            return this;
        }

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRules#getForwardOriginalToken}
         * @param forwardOriginalToken If set to true, the original token will be kept for the upstream request.
         * @return {@code this}
         */
        public Builder forwardOriginalToken(java.lang.Boolean forwardOriginalToken) {
            this.forwardOriginalToken = forwardOriginalToken;
            return this;
        }

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRules#getFromHeaders}
         * @param fromHeaders List of header locations from which JWT is expected.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder fromHeaders(java.util.List<? extends imports.io.istio.security.RequestAuthenticationSpecJwtRulesFromHeaders> fromHeaders) {
            this.fromHeaders = (java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRulesFromHeaders>)fromHeaders;
            return this;
        }

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRules#getFromParams}
         * @param fromParams List of query parameters from which JWT is expected.
         * @return {@code this}
         */
        public Builder fromParams(java.util.List<java.lang.String> fromParams) {
            this.fromParams = fromParams;
            return this;
        }

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRules#getIssuer}
         * @param issuer Identifies the issuer that issued the JWT.
         * @return {@code this}
         */
        public Builder issuer(java.lang.String issuer) {
            this.issuer = issuer;
            return this;
        }

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRules#getJwks}
         * @param jwks JSON Web Key Set of public keys to validate signature of the JWT.
         * @return {@code this}
         */
        public Builder jwks(java.lang.String jwks) {
            this.jwks = jwks;
            return this;
        }

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRules#getJwksUri}
         * @param jwksUri the value to be set.
         * @return {@code this}
         */
        public Builder jwksUri(java.lang.String jwksUri) {
            this.jwksUri = jwksUri;
            return this;
        }

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRules#getOutputPayloadToHeader}
         * @param outputPayloadToHeader the value to be set.
         * @return {@code this}
         */
        public Builder outputPayloadToHeader(java.lang.String outputPayloadToHeader) {
            this.outputPayloadToHeader = outputPayloadToHeader;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link RequestAuthenticationSpecJwtRules}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public RequestAuthenticationSpecJwtRules build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link RequestAuthenticationSpecJwtRules}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements RequestAuthenticationSpecJwtRules {
        private final java.util.List<java.lang.String> audiences;
        private final java.lang.Boolean forwardOriginalToken;
        private final java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRulesFromHeaders> fromHeaders;
        private final java.util.List<java.lang.String> fromParams;
        private final java.lang.String issuer;
        private final java.lang.String jwks;
        private final java.lang.String jwksUri;
        private final java.lang.String outputPayloadToHeader;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.audiences = software.amazon.jsii.Kernel.get(this, "audiences", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.forwardOriginalToken = software.amazon.jsii.Kernel.get(this, "forwardOriginalToken", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
            this.fromHeaders = software.amazon.jsii.Kernel.get(this, "fromHeaders", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.security.RequestAuthenticationSpecJwtRulesFromHeaders.class)));
            this.fromParams = software.amazon.jsii.Kernel.get(this, "fromParams", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.issuer = software.amazon.jsii.Kernel.get(this, "issuer", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.jwks = software.amazon.jsii.Kernel.get(this, "jwks", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.jwksUri = software.amazon.jsii.Kernel.get(this, "jwksUri", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.outputPayloadToHeader = software.amazon.jsii.Kernel.get(this, "outputPayloadToHeader", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.audiences = builder.audiences;
            this.forwardOriginalToken = builder.forwardOriginalToken;
            this.fromHeaders = (java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRulesFromHeaders>)builder.fromHeaders;
            this.fromParams = builder.fromParams;
            this.issuer = builder.issuer;
            this.jwks = builder.jwks;
            this.jwksUri = builder.jwksUri;
            this.outputPayloadToHeader = builder.outputPayloadToHeader;
        }

        @Override
        public final java.util.List<java.lang.String> getAudiences() {
            return this.audiences;
        }

        @Override
        public final java.lang.Boolean getForwardOriginalToken() {
            return this.forwardOriginalToken;
        }

        @Override
        public final java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRulesFromHeaders> getFromHeaders() {
            return this.fromHeaders;
        }

        @Override
        public final java.util.List<java.lang.String> getFromParams() {
            return this.fromParams;
        }

        @Override
        public final java.lang.String getIssuer() {
            return this.issuer;
        }

        @Override
        public final java.lang.String getJwks() {
            return this.jwks;
        }

        @Override
        public final java.lang.String getJwksUri() {
            return this.jwksUri;
        }

        @Override
        public final java.lang.String getOutputPayloadToHeader() {
            return this.outputPayloadToHeader;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAudiences() != null) {
                data.set("audiences", om.valueToTree(this.getAudiences()));
            }
            if (this.getForwardOriginalToken() != null) {
                data.set("forwardOriginalToken", om.valueToTree(this.getForwardOriginalToken()));
            }
            if (this.getFromHeaders() != null) {
                data.set("fromHeaders", om.valueToTree(this.getFromHeaders()));
            }
            if (this.getFromParams() != null) {
                data.set("fromParams", om.valueToTree(this.getFromParams()));
            }
            if (this.getIssuer() != null) {
                data.set("issuer", om.valueToTree(this.getIssuer()));
            }
            if (this.getJwks() != null) {
                data.set("jwks", om.valueToTree(this.getJwks()));
            }
            if (this.getJwksUri() != null) {
                data.set("jwksUri", om.valueToTree(this.getJwksUri()));
            }
            if (this.getOutputPayloadToHeader() != null) {
                data.set("outputPayloadToHeader", om.valueToTree(this.getOutputPayloadToHeader()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.RequestAuthenticationSpecJwtRules"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RequestAuthenticationSpecJwtRules.Jsii$Proxy that = (RequestAuthenticationSpecJwtRules.Jsii$Proxy) o;

            if (this.audiences != null ? !this.audiences.equals(that.audiences) : that.audiences != null) return false;
            if (this.forwardOriginalToken != null ? !this.forwardOriginalToken.equals(that.forwardOriginalToken) : that.forwardOriginalToken != null) return false;
            if (this.fromHeaders != null ? !this.fromHeaders.equals(that.fromHeaders) : that.fromHeaders != null) return false;
            if (this.fromParams != null ? !this.fromParams.equals(that.fromParams) : that.fromParams != null) return false;
            if (this.issuer != null ? !this.issuer.equals(that.issuer) : that.issuer != null) return false;
            if (this.jwks != null ? !this.jwks.equals(that.jwks) : that.jwks != null) return false;
            if (this.jwksUri != null ? !this.jwksUri.equals(that.jwksUri) : that.jwksUri != null) return false;
            return this.outputPayloadToHeader != null ? this.outputPayloadToHeader.equals(that.outputPayloadToHeader) : that.outputPayloadToHeader == null;
        }

        @Override
        public final int hashCode() {
            int result = this.audiences != null ? this.audiences.hashCode() : 0;
            result = 31 * result + (this.forwardOriginalToken != null ? this.forwardOriginalToken.hashCode() : 0);
            result = 31 * result + (this.fromHeaders != null ? this.fromHeaders.hashCode() : 0);
            result = 31 * result + (this.fromParams != null ? this.fromParams.hashCode() : 0);
            result = 31 * result + (this.issuer != null ? this.issuer.hashCode() : 0);
            result = 31 * result + (this.jwks != null ? this.jwks.hashCode() : 0);
            result = 31 * result + (this.jwksUri != null ? this.jwksUri.hashCode() : 0);
            result = 31 * result + (this.outputPayloadToHeader != null ? this.outputPayloadToHeader.hashCode() : 0);
            return result;
        }
    }
}
