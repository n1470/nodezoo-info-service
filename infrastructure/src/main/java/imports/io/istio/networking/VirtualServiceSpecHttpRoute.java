package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.467Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpRoute")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpRoute.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpRoute extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpRouteDestination getDestination() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpRouteHeaders getHeaders() {
        return null;
    }

    /**
     * Weight specifies the relative proportion of traffic to be forwarded to the destination.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getWeight() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpRoute}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpRoute}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpRoute> {
        imports.io.istio.networking.VirtualServiceSpecHttpRouteDestination destination;
        imports.io.istio.networking.VirtualServiceSpecHttpRouteHeaders headers;
        java.lang.Number weight;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRoute#getDestination}
         * @param destination the value to be set.
         * @return {@code this}
         */
        public Builder destination(imports.io.istio.networking.VirtualServiceSpecHttpRouteDestination destination) {
            this.destination = destination;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRoute#getHeaders}
         * @param headers the value to be set.
         * @return {@code this}
         */
        public Builder headers(imports.io.istio.networking.VirtualServiceSpecHttpRouteHeaders headers) {
            this.headers = headers;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRoute#getWeight}
         * @param weight Weight specifies the relative proportion of traffic to be forwarded to the destination.
         * @return {@code this}
         */
        public Builder weight(java.lang.Number weight) {
            this.weight = weight;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpRoute}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpRoute build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpRoute}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpRoute {
        private final imports.io.istio.networking.VirtualServiceSpecHttpRouteDestination destination;
        private final imports.io.istio.networking.VirtualServiceSpecHttpRouteHeaders headers;
        private final java.lang.Number weight;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.destination = software.amazon.jsii.Kernel.get(this, "destination", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpRouteDestination.class));
            this.headers = software.amazon.jsii.Kernel.get(this, "headers", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpRouteHeaders.class));
            this.weight = software.amazon.jsii.Kernel.get(this, "weight", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.destination = builder.destination;
            this.headers = builder.headers;
            this.weight = builder.weight;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpRouteDestination getDestination() {
            return this.destination;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpRouteHeaders getHeaders() {
            return this.headers;
        }

        @Override
        public final java.lang.Number getWeight() {
            return this.weight;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getDestination() != null) {
                data.set("destination", om.valueToTree(this.getDestination()));
            }
            if (this.getHeaders() != null) {
                data.set("headers", om.valueToTree(this.getHeaders()));
            }
            if (this.getWeight() != null) {
                data.set("weight", om.valueToTree(this.getWeight()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpRoute"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpRoute.Jsii$Proxy that = (VirtualServiceSpecHttpRoute.Jsii$Proxy) o;

            if (this.destination != null ? !this.destination.equals(that.destination) : that.destination != null) return false;
            if (this.headers != null ? !this.headers.equals(that.headers) : that.headers != null) return false;
            return this.weight != null ? this.weight.equals(that.weight) : that.weight == null;
        }

        @Override
        public final int hashCode() {
            int result = this.destination != null ? this.destination.hashCode() : 0;
            result = 31 * result + (this.headers != null ? this.headers.hashCode() : 0);
            result = 31 * result + (this.weight != null ? this.weight.hashCode() : 0);
            return result;
        }
    }
}
