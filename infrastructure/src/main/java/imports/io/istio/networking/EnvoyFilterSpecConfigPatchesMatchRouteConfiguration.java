package imports.io.istio.networking;

/**
 * Match on envoy HTTP route configuration attributes.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.442Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesMatchRouteConfiguration")
@software.amazon.jsii.Jsii.Proxy(EnvoyFilterSpecConfigPatchesMatchRouteConfiguration.Jsii$Proxy.class)
public interface EnvoyFilterSpecConfigPatchesMatchRouteConfiguration extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getGateway() {
        return null;
    }

    /**
     * Route configuration name to match on.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * Applicable only for GATEWAY context.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getPortName() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPortNumber() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhost getVhost() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfiguration}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link EnvoyFilterSpecConfigPatchesMatchRouteConfiguration}
     */
    public static final class Builder implements software.amazon.jsii.Builder<EnvoyFilterSpecConfigPatchesMatchRouteConfiguration> {
        java.lang.String gateway;
        java.lang.String name;
        java.lang.String portName;
        java.lang.Number portNumber;
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhost vhost;

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfiguration#getGateway}
         * @param gateway the value to be set.
         * @return {@code this}
         */
        public Builder gateway(java.lang.String gateway) {
            this.gateway = gateway;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfiguration#getName}
         * @param name Route configuration name to match on.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfiguration#getPortName}
         * @param portName Applicable only for GATEWAY context.
         * @return {@code this}
         */
        public Builder portName(java.lang.String portName) {
            this.portName = portName;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfiguration#getPortNumber}
         * @param portNumber the value to be set.
         * @return {@code this}
         */
        public Builder portNumber(java.lang.Number portNumber) {
            this.portNumber = portNumber;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfiguration#getVhost}
         * @param vhost the value to be set.
         * @return {@code this}
         */
        public Builder vhost(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhost vhost) {
            this.vhost = vhost;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfiguration}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public EnvoyFilterSpecConfigPatchesMatchRouteConfiguration build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link EnvoyFilterSpecConfigPatchesMatchRouteConfiguration}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements EnvoyFilterSpecConfigPatchesMatchRouteConfiguration {
        private final java.lang.String gateway;
        private final java.lang.String name;
        private final java.lang.String portName;
        private final java.lang.Number portNumber;
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhost vhost;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.gateway = software.amazon.jsii.Kernel.get(this, "gateway", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.portName = software.amazon.jsii.Kernel.get(this, "portName", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.portNumber = software.amazon.jsii.Kernel.get(this, "portNumber", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.vhost = software.amazon.jsii.Kernel.get(this, "vhost", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhost.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.gateway = builder.gateway;
            this.name = builder.name;
            this.portName = builder.portName;
            this.portNumber = builder.portNumber;
            this.vhost = builder.vhost;
        }

        @Override
        public final java.lang.String getGateway() {
            return this.gateway;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final java.lang.String getPortName() {
            return this.portName;
        }

        @Override
        public final java.lang.Number getPortNumber() {
            return this.portNumber;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhost getVhost() {
            return this.vhost;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getGateway() != null) {
                data.set("gateway", om.valueToTree(this.getGateway()));
            }
            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getPortName() != null) {
                data.set("portName", om.valueToTree(this.getPortName()));
            }
            if (this.getPortNumber() != null) {
                data.set("portNumber", om.valueToTree(this.getPortNumber()));
            }
            if (this.getVhost() != null) {
                data.set("vhost", om.valueToTree(this.getVhost()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.EnvoyFilterSpecConfigPatchesMatchRouteConfiguration"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EnvoyFilterSpecConfigPatchesMatchRouteConfiguration.Jsii$Proxy that = (EnvoyFilterSpecConfigPatchesMatchRouteConfiguration.Jsii$Proxy) o;

            if (this.gateway != null ? !this.gateway.equals(that.gateway) : that.gateway != null) return false;
            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            if (this.portName != null ? !this.portName.equals(that.portName) : that.portName != null) return false;
            if (this.portNumber != null ? !this.portNumber.equals(that.portNumber) : that.portNumber != null) return false;
            return this.vhost != null ? this.vhost.equals(that.vhost) : that.vhost == null;
        }

        @Override
        public final int hashCode() {
            int result = this.gateway != null ? this.gateway.hashCode() : 0;
            result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
            result = 31 * result + (this.portName != null ? this.portName.hashCode() : 0);
            result = 31 * result + (this.portNumber != null ? this.portNumber.hashCode() : 0);
            result = 31 * result + (this.vhost != null ? this.vhost.hashCode() : 0);
            return result;
        }
    }
}
