package tao.manifest;

import imports.k8s.*;
import org.jetbrains.annotations.NotNull;
import tao.InfoServiceChart;

import java.util.List;
import java.util.Map;

public class ServiceManifest {

    private final KubeService service;

    public record Props(int servicePort,
                        int targetPort,
                        boolean isEks,
                        @NotNull String serviceName) {}

    public ServiceManifest(InfoServiceChart infoServiceChart, Props serviceManifestProps) {
        final var port = ServicePort
                .builder()
                .port(serviceManifestProps.servicePort())
                .targetPort(IntOrString.fromNumber(serviceManifestProps.targetPort()))
                .build();
        final var servicePorts = List.of(port);
        final var serviceSpecBuilder = ServiceSpec.builder();
        if (serviceManifestProps.isEks()) {
            serviceSpecBuilder.type("NodePort");
        }

        final var serviceSpec = serviceSpecBuilder
                .selector(getSelector())
                .ports(servicePorts)
                .build();
        final var serviceProps = KubeServiceProps
                .builder()
                .spec(serviceSpec)
                .metadata(ObjectMeta.builder().name(serviceManifestProps.serviceName()).build())
                .build();
        this.service = new KubeService(infoServiceChart, "service", serviceProps);
    }

    @NotNull
    public Map<String, String> getSelector() {
        return Map.of("app", "info-service-prod");
    }

    public KubeService getService() {
        return this.service;
    }
}
