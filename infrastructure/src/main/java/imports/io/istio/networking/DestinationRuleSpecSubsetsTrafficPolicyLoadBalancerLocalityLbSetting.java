package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.430Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting.Jsii$Proxy.class)
public interface DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting extends software.amazon.jsii.JsiiSerializable {

    /**
     * Optional: only one of distribute, failover or failoverPriority can be set.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingDistribute> getDistribute() {
        return null;
    }

    /**
     * enable locality load balancing, this is DestinationRule-level and will override mesh wide settings in entirety.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getEnabled() {
        return null;
    }

    /**
     * Optional: only one of distribute, failover or failoverPriority can be set.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingFailover> getFailover() {
        return null;
    }

    /**
     * failoverPriority is an ordered list of labels used to sort endpoints to do priority based load balancing.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getFailoverPriority() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting> {
        java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingDistribute> distribute;
        java.lang.Boolean enabled;
        java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingFailover> failover;
        java.util.List<java.lang.String> failoverPriority;

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting#getDistribute}
         * @param distribute Optional: only one of distribute, failover or failoverPriority can be set.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder distribute(java.util.List<? extends imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingDistribute> distribute) {
            this.distribute = (java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingDistribute>)distribute;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting#getEnabled}
         * @param enabled enable locality load balancing, this is DestinationRule-level and will override mesh wide settings in entirety.
         * @return {@code this}
         */
        public Builder enabled(java.lang.Boolean enabled) {
            this.enabled = enabled;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting#getFailover}
         * @param failover Optional: only one of distribute, failover or failoverPriority can be set.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder failover(java.util.List<? extends imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingFailover> failover) {
            this.failover = (java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingFailover>)failover;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting#getFailoverPriority}
         * @param failoverPriority failoverPriority is an ordered list of labels used to sort endpoints to do priority based load balancing.
         * @return {@code this}
         */
        public Builder failoverPriority(java.util.List<java.lang.String> failoverPriority) {
            this.failoverPriority = failoverPriority;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting {
        private final java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingDistribute> distribute;
        private final java.lang.Boolean enabled;
        private final java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingFailover> failover;
        private final java.util.List<java.lang.String> failoverPriority;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.distribute = software.amazon.jsii.Kernel.get(this, "distribute", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingDistribute.class)));
            this.enabled = software.amazon.jsii.Kernel.get(this, "enabled", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
            this.failover = software.amazon.jsii.Kernel.get(this, "failover", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingFailover.class)));
            this.failoverPriority = software.amazon.jsii.Kernel.get(this, "failoverPriority", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.distribute = (java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingDistribute>)builder.distribute;
            this.enabled = builder.enabled;
            this.failover = (java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingFailover>)builder.failover;
            this.failoverPriority = builder.failoverPriority;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingDistribute> getDistribute() {
            return this.distribute;
        }

        @Override
        public final java.lang.Boolean getEnabled() {
            return this.enabled;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSettingFailover> getFailover() {
            return this.failover;
        }

        @Override
        public final java.util.List<java.lang.String> getFailoverPriority() {
            return this.failoverPriority;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getDistribute() != null) {
                data.set("distribute", om.valueToTree(this.getDistribute()));
            }
            if (this.getEnabled() != null) {
                data.set("enabled", om.valueToTree(this.getEnabled()));
            }
            if (this.getFailover() != null) {
                data.set("failover", om.valueToTree(this.getFailover()));
            }
            if (this.getFailoverPriority() != null) {
                data.set("failoverPriority", om.valueToTree(this.getFailoverPriority()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting.Jsii$Proxy that = (DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting.Jsii$Proxy) o;

            if (this.distribute != null ? !this.distribute.equals(that.distribute) : that.distribute != null) return false;
            if (this.enabled != null ? !this.enabled.equals(that.enabled) : that.enabled != null) return false;
            if (this.failover != null ? !this.failover.equals(that.failover) : that.failover != null) return false;
            return this.failoverPriority != null ? this.failoverPriority.equals(that.failoverPriority) : that.failoverPriority == null;
        }

        @Override
        public final int hashCode() {
            int result = this.distribute != null ? this.distribute.hashCode() : 0;
            result = 31 * result + (this.enabled != null ? this.enabled.hashCode() : 0);
            result = 31 * result + (this.failover != null ? this.failover.hashCode() : 0);
            result = 31 * result + (this.failoverPriority != null ? this.failoverPriority.hashCode() : 0);
            return result;
        }
    }
}
