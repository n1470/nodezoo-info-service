package tao.json

import org.scalatest.flatspec.AnyFlatSpec
import tao.core.PackageInfo

import java.io.StringReader
import javax.json.Json

class PackageInfoFactorySpec extends AnyFlatSpec {

  behavior of "PackageInfoFactorySpec"

  it should "merge" in {
    val qs1Response = Json.createObjectBuilder()
      .add("id", "npm-123")
      .add("name", "react")
      .add("author", Json.createObjectBuilder().add("id", "npm-auth-123"))
      .add("options", Json.createObjectBuilder().add("format", "tar.gz"))
      .build()

    val qs1Info = PackageInfo(sourceLabel = "npm", content = qs1Response.toString)

    val qs2Response = Json.createObjectBuilder()
      .add("id", "gh-123")
      .add("fullName", "facebook/react")
      .add("author", Json.createObjectBuilder().add("id", "gh-auth-123"))
      .add("repo", Json.createObjectBuilder().add("url", "https://some.url"))
      .build()

    val qs2Info = PackageInfo(sourceLabel = "github", content = qs2Response.toString)

    val mergedPackageInfo = PackageInfoFactory(Seq(qs1Info, qs2Info))

    val expectedResponse = Json.createObjectBuilder()
      .add("id", Json.createObjectBuilder()
        .add("npm", "npm-123")
        .add("github", "gh-123"))
      .add("author", Json.createObjectBuilder()
        .add("npm", Json.createObjectBuilder().add("id", "npm-auth-123"))
        .add("github", Json.createObjectBuilder().add("id", "gh-auth-123")))
      .add("name", "react")
      .add("fullName", "facebook/react")
      .add("options", Json.createObjectBuilder().add("format", "tar.gz"))
      .add("repo", Json.createObjectBuilder().add("url", "https://some.url"))
      .build()

    val actualJson = Json.createReader(new StringReader(mergedPackageInfo.content)).readObject()

    assert(expectedResponse == actualJson)
  }
}
