package imports.io.istio.telemetry;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.287Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecMetricsOverrides")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpecMetricsOverrides.Jsii$Proxy.class)
public interface TelemetrySpecMetricsOverrides extends software.amazon.jsii.JsiiSerializable {

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getDisabled() {
        return null;
    }

    /**
     * Match allows provides the scope of the override.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatch getMatch() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverrides> getTagOverrides() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpecMetricsOverrides}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpecMetricsOverrides}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpecMetricsOverrides> {
        java.lang.Boolean disabled;
        imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatch match;
        java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverrides> tagOverrides;

        /**
         * Sets the value of {@link TelemetrySpecMetricsOverrides#getDisabled}
         * @param disabled Optional.
         * @return {@code this}
         */
        public Builder disabled(java.lang.Boolean disabled) {
            this.disabled = disabled;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecMetricsOverrides#getMatch}
         * @param match Match allows provides the scope of the override.
         * @return {@code this}
         */
        public Builder match(imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatch match) {
            this.match = match;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecMetricsOverrides#getTagOverrides}
         * @param tagOverrides Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder tagOverrides(java.util.Map<java.lang.String, ? extends imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverrides> tagOverrides) {
            this.tagOverrides = (java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverrides>)tagOverrides;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpecMetricsOverrides}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpecMetricsOverrides build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpecMetricsOverrides}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpecMetricsOverrides {
        private final java.lang.Boolean disabled;
        private final imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatch match;
        private final java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverrides> tagOverrides;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.disabled = software.amazon.jsii.Kernel.get(this, "disabled", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
            this.match = software.amazon.jsii.Kernel.get(this, "match", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatch.class));
            this.tagOverrides = software.amazon.jsii.Kernel.get(this, "tagOverrides", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverrides.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.disabled = builder.disabled;
            this.match = builder.match;
            this.tagOverrides = (java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverrides>)builder.tagOverrides;
        }

        @Override
        public final java.lang.Boolean getDisabled() {
            return this.disabled;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatch getMatch() {
            return this.match;
        }

        @Override
        public final java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverrides> getTagOverrides() {
            return this.tagOverrides;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getDisabled() != null) {
                data.set("disabled", om.valueToTree(this.getDisabled()));
            }
            if (this.getMatch() != null) {
                data.set("match", om.valueToTree(this.getMatch()));
            }
            if (this.getTagOverrides() != null) {
                data.set("tagOverrides", om.valueToTree(this.getTagOverrides()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpecMetricsOverrides"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpecMetricsOverrides.Jsii$Proxy that = (TelemetrySpecMetricsOverrides.Jsii$Proxy) o;

            if (this.disabled != null ? !this.disabled.equals(that.disabled) : that.disabled != null) return false;
            if (this.match != null ? !this.match.equals(that.match) : that.match != null) return false;
            return this.tagOverrides != null ? this.tagOverrides.equals(that.tagOverrides) : that.tagOverrides == null;
        }

        @Override
        public final int hashCode() {
            int result = this.disabled != null ? this.disabled.hashCode() : 0;
            result = 31 * result + (this.match != null ? this.match.hashCode() : 0);
            result = 31 * result + (this.tagOverrides != null ? this.tagOverrides.hashCode() : 0);
            return result;
        }
    }
}
