package imports.k8s;

/**
 * AzureFile represents an Azure File Service mount on the host and bind mount to the pod.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.154Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.AzureFileVolumeSource")
@software.amazon.jsii.Jsii.Proxy(AzureFileVolumeSource.Jsii$Proxy.class)
public interface AzureFileVolumeSource extends software.amazon.jsii.JsiiSerializable {

    /**
     * the name of secret that contains Azure Storage Account Name and Key.
     */
    @org.jetbrains.annotations.NotNull String getSecretName();

    /**
     * Share Name.
     */
    @org.jetbrains.annotations.NotNull String getShareName();

    /**
     * Defaults to false (read/write).
     * <p>
     * ReadOnly here will force the ReadOnly setting in VolumeMounts.
     * <p>
     * Default: false (read/write). ReadOnly here will force the ReadOnly setting in VolumeMounts.
     */
    default @org.jetbrains.annotations.Nullable Boolean getReadOnly() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link AzureFileVolumeSource}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link AzureFileVolumeSource}
     */
    public static final class Builder implements software.amazon.jsii.Builder<AzureFileVolumeSource> {
        String secretName;
        String shareName;
        Boolean readOnly;

        /**
         * Sets the value of {@link AzureFileVolumeSource#getSecretName}
         * @param secretName the name of secret that contains Azure Storage Account Name and Key. This parameter is required.
         * @return {@code this}
         */
        public Builder secretName(String secretName) {
            this.secretName = secretName;
            return this;
        }

        /**
         * Sets the value of {@link AzureFileVolumeSource#getShareName}
         * @param shareName Share Name. This parameter is required.
         * @return {@code this}
         */
        public Builder shareName(String shareName) {
            this.shareName = shareName;
            return this;
        }

        /**
         * Sets the value of {@link AzureFileVolumeSource#getReadOnly}
         * @param readOnly Defaults to false (read/write).
         *                 ReadOnly here will force the ReadOnly setting in VolumeMounts.
         * @return {@code this}
         */
        public Builder readOnly(Boolean readOnly) {
            this.readOnly = readOnly;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link AzureFileVolumeSource}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public AzureFileVolumeSource build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link AzureFileVolumeSource}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements AzureFileVolumeSource {
        private final String secretName;
        private final String shareName;
        private final Boolean readOnly;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.secretName = software.amazon.jsii.Kernel.get(this, "secretName", software.amazon.jsii.NativeType.forClass(String.class));
            this.shareName = software.amazon.jsii.Kernel.get(this, "shareName", software.amazon.jsii.NativeType.forClass(String.class));
            this.readOnly = software.amazon.jsii.Kernel.get(this, "readOnly", software.amazon.jsii.NativeType.forClass(Boolean.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.secretName = java.util.Objects.requireNonNull(builder.secretName, "secretName is required");
            this.shareName = java.util.Objects.requireNonNull(builder.shareName, "shareName is required");
            this.readOnly = builder.readOnly;
        }

        @Override
        public final String getSecretName() {
            return this.secretName;
        }

        @Override
        public final String getShareName() {
            return this.shareName;
        }

        @Override
        public final Boolean getReadOnly() {
            return this.readOnly;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            data.set("secretName", om.valueToTree(this.getSecretName()));
            data.set("shareName", om.valueToTree(this.getShareName()));
            if (this.getReadOnly() != null) {
                data.set("readOnly", om.valueToTree(this.getReadOnly()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("k8s.AzureFileVolumeSource"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Jsii$Proxy that = (Jsii$Proxy) o;

            if (!secretName.equals(that.secretName)) return false;
            if (!shareName.equals(that.shareName)) return false;
            return this.readOnly != null ? this.readOnly.equals(that.readOnly) : that.readOnly == null;
        }

        @Override
        public final int hashCode() {
            int result = this.secretName.hashCode();
            result = 31 * result + (this.shareName.hashCode());
            result = 31 * result + (this.readOnly != null ? this.readOnly.hashCode() : 0);
            return result;
        }
    }
}
