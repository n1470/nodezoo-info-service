variable "aws_profile" {
  type = string
  description = "AWS profile to use"
  default = "default"
}
