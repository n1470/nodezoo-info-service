package imports.io.istio.security;

/**
 * Defines the mTLS mode used for peer authentication.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.464Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.PeerAuthenticationSpecPortLevelMtlsMode")
public enum PeerAuthenticationSpecPortLevelMtlsMode {
    /**
     * UNSET.
     */
    UNSET,
    /**
     * DISABLE.
     */
    DISABLE,
    /**
     * PERMISSIVE.
     */
    PERMISSIVE,
    /**
     * STRICT.
     */
    STRICT,
}
