package imports.io.istio.networking;

/**
 * The specific config generation context to match on.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.442Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesMatchContext")
public enum EnvoyFilterSpecConfigPatchesMatchContext {
    /**
     * ANY.
     */
    ANY,
    /**
     * SIDECAR_INBOUND.
     */
    SIDECAR_INBOUND,
    /**
     * SIDECAR_OUTBOUND.
     */
    SIDECAR_OUTBOUND,
    /**
     * GATEWAY.
     */
    GATEWAY,
}
