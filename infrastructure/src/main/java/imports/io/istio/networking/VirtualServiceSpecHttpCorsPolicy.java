package imports.io.istio.networking;

/**
 * Cross-Origin Resource Sharing policy (CORS).
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.464Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpCorsPolicy")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpCorsPolicy.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpCorsPolicy extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getAllowCredentials() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getAllowHeaders() {
        return null;
    }

    /**
     * List of HTTP methods allowed to access the resource.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getAllowMethods() {
        return null;
    }

    /**
     * The list of origins that are allowed to perform CORS requests.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getAllowOrigin() {
        return null;
    }

    /**
     * String patterns that match allowed origins.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicyAllowOrigins> getAllowOrigins() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getExposeHeaders() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getMaxAge() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpCorsPolicy}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpCorsPolicy}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpCorsPolicy> {
        java.lang.Boolean allowCredentials;
        java.util.List<java.lang.String> allowHeaders;
        java.util.List<java.lang.String> allowMethods;
        java.util.List<java.lang.String> allowOrigin;
        java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicyAllowOrigins> allowOrigins;
        java.util.List<java.lang.String> exposeHeaders;
        java.lang.String maxAge;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpCorsPolicy#getAllowCredentials}
         * @param allowCredentials the value to be set.
         * @return {@code this}
         */
        public Builder allowCredentials(java.lang.Boolean allowCredentials) {
            this.allowCredentials = allowCredentials;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpCorsPolicy#getAllowHeaders}
         * @param allowHeaders the value to be set.
         * @return {@code this}
         */
        public Builder allowHeaders(java.util.List<java.lang.String> allowHeaders) {
            this.allowHeaders = allowHeaders;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpCorsPolicy#getAllowMethods}
         * @param allowMethods List of HTTP methods allowed to access the resource.
         * @return {@code this}
         */
        public Builder allowMethods(java.util.List<java.lang.String> allowMethods) {
            this.allowMethods = allowMethods;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpCorsPolicy#getAllowOrigin}
         * @param allowOrigin The list of origins that are allowed to perform CORS requests.
         * @return {@code this}
         */
        public Builder allowOrigin(java.util.List<java.lang.String> allowOrigin) {
            this.allowOrigin = allowOrigin;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpCorsPolicy#getAllowOrigins}
         * @param allowOrigins String patterns that match allowed origins.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder allowOrigins(java.util.List<? extends imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicyAllowOrigins> allowOrigins) {
            this.allowOrigins = (java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicyAllowOrigins>)allowOrigins;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpCorsPolicy#getExposeHeaders}
         * @param exposeHeaders the value to be set.
         * @return {@code this}
         */
        public Builder exposeHeaders(java.util.List<java.lang.String> exposeHeaders) {
            this.exposeHeaders = exposeHeaders;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpCorsPolicy#getMaxAge}
         * @param maxAge the value to be set.
         * @return {@code this}
         */
        public Builder maxAge(java.lang.String maxAge) {
            this.maxAge = maxAge;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpCorsPolicy}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpCorsPolicy build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpCorsPolicy}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpCorsPolicy {
        private final java.lang.Boolean allowCredentials;
        private final java.util.List<java.lang.String> allowHeaders;
        private final java.util.List<java.lang.String> allowMethods;
        private final java.util.List<java.lang.String> allowOrigin;
        private final java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicyAllowOrigins> allowOrigins;
        private final java.util.List<java.lang.String> exposeHeaders;
        private final java.lang.String maxAge;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.allowCredentials = software.amazon.jsii.Kernel.get(this, "allowCredentials", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
            this.allowHeaders = software.amazon.jsii.Kernel.get(this, "allowHeaders", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.allowMethods = software.amazon.jsii.Kernel.get(this, "allowMethods", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.allowOrigin = software.amazon.jsii.Kernel.get(this, "allowOrigin", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.allowOrigins = software.amazon.jsii.Kernel.get(this, "allowOrigins", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicyAllowOrigins.class)));
            this.exposeHeaders = software.amazon.jsii.Kernel.get(this, "exposeHeaders", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.maxAge = software.amazon.jsii.Kernel.get(this, "maxAge", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.allowCredentials = builder.allowCredentials;
            this.allowHeaders = builder.allowHeaders;
            this.allowMethods = builder.allowMethods;
            this.allowOrigin = builder.allowOrigin;
            this.allowOrigins = (java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicyAllowOrigins>)builder.allowOrigins;
            this.exposeHeaders = builder.exposeHeaders;
            this.maxAge = builder.maxAge;
        }

        @Override
        public final java.lang.Boolean getAllowCredentials() {
            return this.allowCredentials;
        }

        @Override
        public final java.util.List<java.lang.String> getAllowHeaders() {
            return this.allowHeaders;
        }

        @Override
        public final java.util.List<java.lang.String> getAllowMethods() {
            return this.allowMethods;
        }

        @Override
        public final java.util.List<java.lang.String> getAllowOrigin() {
            return this.allowOrigin;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicyAllowOrigins> getAllowOrigins() {
            return this.allowOrigins;
        }

        @Override
        public final java.util.List<java.lang.String> getExposeHeaders() {
            return this.exposeHeaders;
        }

        @Override
        public final java.lang.String getMaxAge() {
            return this.maxAge;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAllowCredentials() != null) {
                data.set("allowCredentials", om.valueToTree(this.getAllowCredentials()));
            }
            if (this.getAllowHeaders() != null) {
                data.set("allowHeaders", om.valueToTree(this.getAllowHeaders()));
            }
            if (this.getAllowMethods() != null) {
                data.set("allowMethods", om.valueToTree(this.getAllowMethods()));
            }
            if (this.getAllowOrigin() != null) {
                data.set("allowOrigin", om.valueToTree(this.getAllowOrigin()));
            }
            if (this.getAllowOrigins() != null) {
                data.set("allowOrigins", om.valueToTree(this.getAllowOrigins()));
            }
            if (this.getExposeHeaders() != null) {
                data.set("exposeHeaders", om.valueToTree(this.getExposeHeaders()));
            }
            if (this.getMaxAge() != null) {
                data.set("maxAge", om.valueToTree(this.getMaxAge()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpCorsPolicy"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpCorsPolicy.Jsii$Proxy that = (VirtualServiceSpecHttpCorsPolicy.Jsii$Proxy) o;

            if (this.allowCredentials != null ? !this.allowCredentials.equals(that.allowCredentials) : that.allowCredentials != null) return false;
            if (this.allowHeaders != null ? !this.allowHeaders.equals(that.allowHeaders) : that.allowHeaders != null) return false;
            if (this.allowMethods != null ? !this.allowMethods.equals(that.allowMethods) : that.allowMethods != null) return false;
            if (this.allowOrigin != null ? !this.allowOrigin.equals(that.allowOrigin) : that.allowOrigin != null) return false;
            if (this.allowOrigins != null ? !this.allowOrigins.equals(that.allowOrigins) : that.allowOrigins != null) return false;
            if (this.exposeHeaders != null ? !this.exposeHeaders.equals(that.exposeHeaders) : that.exposeHeaders != null) return false;
            return this.maxAge != null ? this.maxAge.equals(that.maxAge) : that.maxAge == null;
        }

        @Override
        public final int hashCode() {
            int result = this.allowCredentials != null ? this.allowCredentials.hashCode() : 0;
            result = 31 * result + (this.allowHeaders != null ? this.allowHeaders.hashCode() : 0);
            result = 31 * result + (this.allowMethods != null ? this.allowMethods.hashCode() : 0);
            result = 31 * result + (this.allowOrigin != null ? this.allowOrigin.hashCode() : 0);
            result = 31 * result + (this.allowOrigins != null ? this.allowOrigins.hashCode() : 0);
            result = 31 * result + (this.exposeHeaders != null ? this.exposeHeaders.hashCode() : 0);
            result = 31 * result + (this.maxAge != null ? this.maxAge.hashCode() : 0);
            return result;
        }
    }
}
