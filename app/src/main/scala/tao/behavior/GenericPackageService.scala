package tao.behavior

import akka.actor.typed.scaladsl.ActorContext
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse, StatusCodes}
import akka.util.ByteString
import org.slf4j.{Logger, LoggerFactory}
import tao.core.{PackageInfo, PackageQuery}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait GenericPackageService {

  private def logger: Logger = LoggerFactory.getLogger(this.getClass)

  def receive(requestUrl: String,
              sourceLabel: String,
              context: ActorContext[PackageQuery],
              message: PackageQuery): Unit = {
    context.log.debug(s"Fetching package info for ${message.packageName}")
    implicit val system: ActorSystem[Nothing] = context.system
    implicit val executionContext: ExecutionContext = context.executionContext
    val request = HttpRequest(uri = requestUrl)
    val responseFuture: Future[HttpResponse] = Http().singleRequest(request)
    responseFuture
      .onComplete {
        case Success(response) =>
          if (response.status == StatusCodes.OK) {
            response.entity.dataBytes.runFold(ByteString(""))(_ ++ _).onComplete { maybeData =>
              for {
                data <- maybeData
              } message.actorRef ! PackageInfo(sourceLabel, content = data.utf8String)
            }
          } else {
            logger.error(f"Received status code: ${response.status.value} and reason: ${response.status.reason()}")
            message.actorRef ! PackageInfo(sourceLabel, content = "{}")
          }
        case Failure(exception) =>
          logger.error(exception.getMessage, exception)
          message.actorRef ! PackageInfo(sourceLabel, content = "{}")
      }
  }
}
