package imports.io.istio.networking;

/**
 * Match on listener/route configuration/cluster.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.441Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesMatch")
@software.amazon.jsii.Jsii.Proxy(EnvoyFilterSpecConfigPatchesMatch.Jsii$Proxy.class)
public interface EnvoyFilterSpecConfigPatchesMatch extends software.amazon.jsii.JsiiSerializable {

    /**
     * Match on envoy cluster attributes.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchCluster getCluster() {
        return null;
    }

    /**
     * The specific config generation context to match on.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchContext getContext() {
        return null;
    }

    /**
     * Match on envoy listener attributes.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListener getListener() {
        return null;
    }

    /**
     * Match on properties associated with a proxy.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchProxy getProxy() {
        return null;
    }

    /**
     * Match on envoy HTTP route configuration attributes.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfiguration getRouteConfiguration() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link EnvoyFilterSpecConfigPatchesMatch}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link EnvoyFilterSpecConfigPatchesMatch}
     */
    public static final class Builder implements software.amazon.jsii.Builder<EnvoyFilterSpecConfigPatchesMatch> {
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchCluster cluster;
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchContext context;
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListener listener;
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchProxy proxy;
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfiguration routeConfiguration;

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatch#getCluster}
         * @param cluster Match on envoy cluster attributes.
         * @return {@code this}
         */
        public Builder cluster(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchCluster cluster) {
            this.cluster = cluster;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatch#getContext}
         * @param context The specific config generation context to match on.
         * @return {@code this}
         */
        public Builder context(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchContext context) {
            this.context = context;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatch#getListener}
         * @param listener Match on envoy listener attributes.
         * @return {@code this}
         */
        public Builder listener(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListener listener) {
            this.listener = listener;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatch#getProxy}
         * @param proxy Match on properties associated with a proxy.
         * @return {@code this}
         */
        public Builder proxy(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchProxy proxy) {
            this.proxy = proxy;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatch#getRouteConfiguration}
         * @param routeConfiguration Match on envoy HTTP route configuration attributes.
         * @return {@code this}
         */
        public Builder routeConfiguration(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfiguration routeConfiguration) {
            this.routeConfiguration = routeConfiguration;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link EnvoyFilterSpecConfigPatchesMatch}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public EnvoyFilterSpecConfigPatchesMatch build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link EnvoyFilterSpecConfigPatchesMatch}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements EnvoyFilterSpecConfigPatchesMatch {
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchCluster cluster;
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchContext context;
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListener listener;
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchProxy proxy;
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfiguration routeConfiguration;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.cluster = software.amazon.jsii.Kernel.get(this, "cluster", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchCluster.class));
            this.context = software.amazon.jsii.Kernel.get(this, "context", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchContext.class));
            this.listener = software.amazon.jsii.Kernel.get(this, "listener", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListener.class));
            this.proxy = software.amazon.jsii.Kernel.get(this, "proxy", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchProxy.class));
            this.routeConfiguration = software.amazon.jsii.Kernel.get(this, "routeConfiguration", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfiguration.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.cluster = builder.cluster;
            this.context = builder.context;
            this.listener = builder.listener;
            this.proxy = builder.proxy;
            this.routeConfiguration = builder.routeConfiguration;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchCluster getCluster() {
            return this.cluster;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchContext getContext() {
            return this.context;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListener getListener() {
            return this.listener;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchProxy getProxy() {
            return this.proxy;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfiguration getRouteConfiguration() {
            return this.routeConfiguration;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getCluster() != null) {
                data.set("cluster", om.valueToTree(this.getCluster()));
            }
            if (this.getContext() != null) {
                data.set("context", om.valueToTree(this.getContext()));
            }
            if (this.getListener() != null) {
                data.set("listener", om.valueToTree(this.getListener()));
            }
            if (this.getProxy() != null) {
                data.set("proxy", om.valueToTree(this.getProxy()));
            }
            if (this.getRouteConfiguration() != null) {
                data.set("routeConfiguration", om.valueToTree(this.getRouteConfiguration()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.EnvoyFilterSpecConfigPatchesMatch"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EnvoyFilterSpecConfigPatchesMatch.Jsii$Proxy that = (EnvoyFilterSpecConfigPatchesMatch.Jsii$Proxy) o;

            if (this.cluster != null ? !this.cluster.equals(that.cluster) : that.cluster != null) return false;
            if (this.context != null ? !this.context.equals(that.context) : that.context != null) return false;
            if (this.listener != null ? !this.listener.equals(that.listener) : that.listener != null) return false;
            if (this.proxy != null ? !this.proxy.equals(that.proxy) : that.proxy != null) return false;
            return this.routeConfiguration != null ? this.routeConfiguration.equals(that.routeConfiguration) : that.routeConfiguration == null;
        }

        @Override
        public final int hashCode() {
            int result = this.cluster != null ? this.cluster.hashCode() : 0;
            result = 31 * result + (this.context != null ? this.context.hashCode() : 0);
            result = 31 * result + (this.listener != null ? this.listener.hashCode() : 0);
            result = 31 * result + (this.proxy != null ? this.proxy.hashCode() : 0);
            result = 31 * result + (this.routeConfiguration != null ? this.routeConfiguration.hashCode() : 0);
            return result;
        }
    }
}
