package tao.manifest;

import imports.io.istio.networking.*;
import org.cdk8s.ApiObjectMetadata;
import org.cdk8s.Chart;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

public class IstioGatewayManifest {

    private final String name;

    public record Props(
            @NotNull String name,
            @NotNull String namespace,
            int httpPort
    ) {
        public String id() {
            return name;
        }
    }

    public IstioGatewayManifest(@NotNull Chart chart, @NotNull Props gatewayProps) {
        final var metadata = ApiObjectMetadata
                .builder()
                .name(gatewayProps.name)
                .namespace(gatewayProps.namespace)
                .build();
        final var serversPort = GatewaySpecServersPort
                .builder()
                .name("http")
                .protocol("HTTP")
                .number(gatewayProps.httpPort)
                .build();
        final var servers = GatewaySpecServers
                .builder()
                .port(serversPort)
                .hosts(List.of("*"))
                .build();
        final var spec = GatewaySpec
                .builder()
                .servers(List.of(servers))
                .selector(Map.of("istio", "ingressgateway"))
                .build();
        final var props = IstioGatewayProps
                .builder()
                .metadata(metadata)
                .spec(spec)
                .build();

        var gateway = new IstioGateway(chart, gatewayProps.id(), props);
        this.name = gateway.getName();
    }

    public String getName() {
        return name;
    }
}
