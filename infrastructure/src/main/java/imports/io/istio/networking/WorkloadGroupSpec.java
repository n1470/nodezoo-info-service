package imports.io.istio.networking;

/**
 * Describes a collection of workload instances.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/networking/workload-group.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.470Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.WorkloadGroupSpec")
@software.amazon.jsii.Jsii.Proxy(WorkloadGroupSpec.Jsii$Proxy.class)
public interface WorkloadGroupSpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * Metadata that will be used for all corresponding `WorkloadEntries`.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.WorkloadGroupSpecMetadata getMetadata() {
        return null;
    }

    /**
     * `ReadinessProbe` describes the configuration the user must provide for healthchecking on their workload.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.WorkloadGroupSpecProbe getProbe() {
        return null;
    }

    /**
     * Template to be used for the generation of `WorkloadEntry` resources that belong to this `WorkloadGroup`.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.WorkloadGroupSpecTemplate getTemplate() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link WorkloadGroupSpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link WorkloadGroupSpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<WorkloadGroupSpec> {
        imports.io.istio.networking.WorkloadGroupSpecMetadata metadata;
        imports.io.istio.networking.WorkloadGroupSpecProbe probe;
        imports.io.istio.networking.WorkloadGroupSpecTemplate template;

        /**
         * Sets the value of {@link WorkloadGroupSpec#getMetadata}
         * @param metadata Metadata that will be used for all corresponding `WorkloadEntries`.
         * @return {@code this}
         */
        public Builder metadata(imports.io.istio.networking.WorkloadGroupSpecMetadata metadata) {
            this.metadata = metadata;
            return this;
        }

        /**
         * Sets the value of {@link WorkloadGroupSpec#getProbe}
         * @param probe `ReadinessProbe` describes the configuration the user must provide for healthchecking on their workload.
         * @return {@code this}
         */
        public Builder probe(imports.io.istio.networking.WorkloadGroupSpecProbe probe) {
            this.probe = probe;
            return this;
        }

        /**
         * Sets the value of {@link WorkloadGroupSpec#getTemplate}
         * @param template Template to be used for the generation of `WorkloadEntry` resources that belong to this `WorkloadGroup`.
         * @return {@code this}
         */
        public Builder template(imports.io.istio.networking.WorkloadGroupSpecTemplate template) {
            this.template = template;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link WorkloadGroupSpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public WorkloadGroupSpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link WorkloadGroupSpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements WorkloadGroupSpec {
        private final imports.io.istio.networking.WorkloadGroupSpecMetadata metadata;
        private final imports.io.istio.networking.WorkloadGroupSpecProbe probe;
        private final imports.io.istio.networking.WorkloadGroupSpecTemplate template;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.metadata = software.amazon.jsii.Kernel.get(this, "metadata", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.WorkloadGroupSpecMetadata.class));
            this.probe = software.amazon.jsii.Kernel.get(this, "probe", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.WorkloadGroupSpecProbe.class));
            this.template = software.amazon.jsii.Kernel.get(this, "template", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.WorkloadGroupSpecTemplate.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.metadata = builder.metadata;
            this.probe = builder.probe;
            this.template = builder.template;
        }

        @Override
        public final imports.io.istio.networking.WorkloadGroupSpecMetadata getMetadata() {
            return this.metadata;
        }

        @Override
        public final imports.io.istio.networking.WorkloadGroupSpecProbe getProbe() {
            return this.probe;
        }

        @Override
        public final imports.io.istio.networking.WorkloadGroupSpecTemplate getTemplate() {
            return this.template;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getMetadata() != null) {
                data.set("metadata", om.valueToTree(this.getMetadata()));
            }
            if (this.getProbe() != null) {
                data.set("probe", om.valueToTree(this.getProbe()));
            }
            if (this.getTemplate() != null) {
                data.set("template", om.valueToTree(this.getTemplate()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.WorkloadGroupSpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            WorkloadGroupSpec.Jsii$Proxy that = (WorkloadGroupSpec.Jsii$Proxy) o;

            if (this.metadata != null ? !this.metadata.equals(that.metadata) : that.metadata != null) return false;
            if (this.probe != null ? !this.probe.equals(that.probe) : that.probe != null) return false;
            return this.template != null ? this.template.equals(that.template) : that.template == null;
        }

        @Override
        public final int hashCode() {
            int result = this.metadata != null ? this.metadata.hashCode() : 0;
            result = 31 * result + (this.probe != null ? this.probe.hashCode() : 0);
            result = 31 * result + (this.template != null ? this.template.hashCode() : 0);
            return result;
        }
    }
}
