package imports.io.istio.extensions;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:32:56.645Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.extensions.$Module.class, fqn = "ioistioextensions.WasmPluginSpecVmConfigEnvValueFrom")
public enum WasmPluginSpecVmConfigEnvValueFrom {
    /**
     * INLINE.
     */
    INLINE,
    /**
     * HOST.
     */
    HOST,
}
