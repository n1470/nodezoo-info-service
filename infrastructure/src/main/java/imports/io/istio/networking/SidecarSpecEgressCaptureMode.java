package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.462Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.SidecarSpecEgressCaptureMode")
public enum SidecarSpecEgressCaptureMode {
    /**
     * DEFAULT.
     */
    DEFAULT,
    /**
     * IPTABLES.
     */
    IPTABLES,
    /**
     * NONE.
     */
    NONE,
}
