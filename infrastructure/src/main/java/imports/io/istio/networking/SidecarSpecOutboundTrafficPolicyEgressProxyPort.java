package imports.io.istio.networking;

/**
 * Specifies the port on the host that is being addressed.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.463Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.SidecarSpecOutboundTrafficPolicyEgressProxyPort")
@software.amazon.jsii.Jsii.Proxy(SidecarSpecOutboundTrafficPolicyEgressProxyPort.Jsii$Proxy.class)
public interface SidecarSpecOutboundTrafficPolicyEgressProxyPort extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getNumber() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link SidecarSpecOutboundTrafficPolicyEgressProxyPort}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link SidecarSpecOutboundTrafficPolicyEgressProxyPort}
     */
    public static final class Builder implements software.amazon.jsii.Builder<SidecarSpecOutboundTrafficPolicyEgressProxyPort> {
        java.lang.Number number;

        /**
         * Sets the value of {@link SidecarSpecOutboundTrafficPolicyEgressProxyPort#getNumber}
         * @param number the value to be set.
         * @return {@code this}
         */
        public Builder number(java.lang.Number number) {
            this.number = number;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link SidecarSpecOutboundTrafficPolicyEgressProxyPort}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public SidecarSpecOutboundTrafficPolicyEgressProxyPort build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link SidecarSpecOutboundTrafficPolicyEgressProxyPort}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements SidecarSpecOutboundTrafficPolicyEgressProxyPort {
        private final java.lang.Number number;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.number = software.amazon.jsii.Kernel.get(this, "number", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.number = builder.number;
        }

        @Override
        public final java.lang.Number getNumber() {
            return this.number;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getNumber() != null) {
                data.set("number", om.valueToTree(this.getNumber()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.SidecarSpecOutboundTrafficPolicyEgressProxyPort"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SidecarSpecOutboundTrafficPolicyEgressProxyPort.Jsii$Proxy that = (SidecarSpecOutboundTrafficPolicyEgressProxyPort.Jsii$Proxy) o;

            return this.number != null ? this.number.equals(that.number) : that.number == null;
        }

        @Override
        public final int hashCode() {
            int result = this.number != null ? this.number.hashCode() : 0;
            return result;
        }
    }
}
