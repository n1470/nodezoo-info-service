package imports.k8s;

/**
 * PodSecurityPolicySpec defines the policy enforced.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.335Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.PodSecurityPolicySpecV1Beta1")
@software.amazon.jsii.Jsii.Proxy(PodSecurityPolicySpecV1Beta1.Jsii$Proxy.class)
public interface PodSecurityPolicySpecV1Beta1 extends software.amazon.jsii.JsiiSerializable {

    /**
     * fsGroup is the strategy that will dictate what fs group is used by the SecurityContext.
     */
    @org.jetbrains.annotations.NotNull FsGroupStrategyOptionsV1Beta1 getFsGroup();

    /**
     * runAsUser is the strategy that will dictate the allowable RunAsUser values that may be set.
     */
    @org.jetbrains.annotations.NotNull RunAsUserStrategyOptionsV1Beta1 getRunAsUser();

    /**
     * seLinux is the strategy that will dictate the allowable labels that may be set.
     */
    @org.jetbrains.annotations.NotNull SeLinuxStrategyOptionsV1Beta1 getSeLinux();

    /**
     * supplementalGroups is the strategy that will dictate what supplemental groups are used by the SecurityContext.
     */
    @org.jetbrains.annotations.NotNull SupplementalGroupsStrategyOptionsV1Beta1 getSupplementalGroups();

    /**
     * allowedCapabilities is a list of capabilities that can be requested to add to the container.
     * <p>
     * Capabilities in this field may be added at the pod author's discretion. You must not list a capability in both allowedCapabilities and requiredDropCapabilities.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<String> getAllowedCapabilities() {
        return null;
    }

    /**
     * AllowedCSIDrivers is an allowlist of inline CSI drivers that must be explicitly set to be embedded within a pod spec.
     * <p>
     * An empty value indicates that any CSI driver can be used for inline ephemeral volumes. This is a beta field, and is only honored if the API server enables the CSIInlineVolume feature gate.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<AllowedCsiDriverV1Beta1> getAllowedCsiDrivers() {
        return null;
    }

    /**
     * allowedFlexVolumes is an allowlist of Flexvolumes.
     * <p>
     * Empty or nil indicates that all Flexvolumes may be used.  This parameter is effective only when the usage of the Flexvolumes is allowed in the "volumes" field.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<AllowedFlexVolumeV1Beta1> getAllowedFlexVolumes() {
        return null;
    }

    /**
     * allowedHostPaths is an allowlist of host paths.
     * <p>
     * Empty indicates that all host paths may be used.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<AllowedHostPathV1Beta1> getAllowedHostPaths() {
        return null;
    }

    /**
     * AllowedProcMountTypes is an allowlist of allowed ProcMountTypes.
     * <p>
     * Empty or nil indicates that only the DefaultProcMountType may be used. This requires the ProcMountType feature flag to be enabled.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<String> getAllowedProcMountTypes() {
        return null;
    }

    /**
     * allowedUnsafeSysctls is a list of explicitly allowed unsafe sysctls, defaults to none.
     * <p>
     * Each entry is either a plain sysctl name or ends in "*" in which case it is considered as a prefix of allowed sysctls. Single * means all unsafe sysctls are allowed. Kubelet has to allowlist all allowed unsafe sysctls explicitly to avoid rejection.
     * <p>
     * Examples: e.g. "foo/<em>" allows "foo/bar", "foo/baz", etc. e.g. "foo.</em>" allows "foo.bar", "foo.baz", etc.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<String> getAllowedUnsafeSysctls() {
        return null;
    }

    /**
     * allowPrivilegeEscalation determines if a pod can request to allow privilege escalation.
     * <p>
     * If unspecified, defaults to true.
     */
    default @org.jetbrains.annotations.Nullable Boolean getAllowPrivilegeEscalation() {
        return null;
    }

    /**
     * defaultAddCapabilities is the default set of capabilities that will be added to the container unless the pod spec specifically drops the capability.
     * <p>
     * You may not list a capability in both defaultAddCapabilities and requiredDropCapabilities. Capabilities added here are implicitly allowed, and need not be included in the allowedCapabilities list.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<String> getDefaultAddCapabilities() {
        return null;
    }

    /**
     * defaultAllowPrivilegeEscalation controls the default setting for whether a process can gain more privileges than its parent process.
     */
    default @org.jetbrains.annotations.Nullable Boolean getDefaultAllowPrivilegeEscalation() {
        return null;
    }

    /**
     * forbiddenSysctls is a list of explicitly forbidden sysctls, defaults to none.
     * <p>
     * Each entry is either a plain sysctl name or ends in "*" in which case it is considered as a prefix of forbidden sysctls. Single * means all sysctls are forbidden.
     * <p>
     * Examples: e.g. "foo/<em>" forbids "foo/bar", "foo/baz", etc. e.g. "foo.</em>" forbids "foo.bar", "foo.baz", etc.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<String> getForbiddenSysctls() {
        return null;
    }

    /**
     * hostIPC determines if the policy allows the use of HostIPC in the pod spec.
     */
    default @org.jetbrains.annotations.Nullable Boolean getHostIpc() {
        return null;
    }

    /**
     * hostNetwork determines if the policy allows the use of HostNetwork in the pod spec.
     */
    default @org.jetbrains.annotations.Nullable Boolean getHostNetwork() {
        return null;
    }

    /**
     * hostPID determines if the policy allows the use of HostPID in the pod spec.
     */
    default @org.jetbrains.annotations.Nullable Boolean getHostPid() {
        return null;
    }

    /**
     * hostPorts determines which host port ranges are allowed to be exposed.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<HostPortRangeV1Beta1> getHostPorts() {
        return null;
    }

    /**
     * privileged determines if a pod can request to be run as privileged.
     */
    default @org.jetbrains.annotations.Nullable Boolean getPrivileged() {
        return null;
    }

    /**
     * readOnlyRootFilesystem when set to true will force containers to run with a read only root file system.
     * <p>
     * If the container specifically requests to run with a non-read only root file system the PSP should deny the pod. If set to false the container may run with a read only root file system if it wishes but it will not be forced to.
     */
    default @org.jetbrains.annotations.Nullable Boolean getReadOnlyRootFilesystem() {
        return null;
    }

    /**
     * requiredDropCapabilities are the capabilities that will be dropped from the container.
     * <p>
     * These are required to be dropped and cannot be added.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<String> getRequiredDropCapabilities() {
        return null;
    }

    /**
     * RunAsGroup is the strategy that will dictate the allowable RunAsGroup values that may be set.
     * <p>
     * If this field is omitted, the pod's RunAsGroup can take any value. This field requires the RunAsGroup feature gate to be enabled.
     */
    default @org.jetbrains.annotations.Nullable RunAsGroupStrategyOptionsV1Beta1 getRunAsGroup() {
        return null;
    }

    /**
     * runtimeClass is the strategy that will dictate the allowable RuntimeClasses for a pod.
     * <p>
     * If this field is omitted, the pod's runtimeClassName field is unrestricted. Enforcement of this field depends on the RuntimeClass feature gate being enabled.
     */
    default @org.jetbrains.annotations.Nullable RuntimeClassStrategyOptionsV1Beta1 getRuntimeClass() {
        return null;
    }

    /**
     * volumes is an allowlist of volume plugins.
     * <p>
     * Empty indicates that no volumes may be used. To allow all volumes you may use '*'.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<String> getVolumes() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link PodSecurityPolicySpecV1Beta1}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link PodSecurityPolicySpecV1Beta1}
     */
    public static final class Builder implements software.amazon.jsii.Builder<PodSecurityPolicySpecV1Beta1> {
        FsGroupStrategyOptionsV1Beta1 fsGroup;
        RunAsUserStrategyOptionsV1Beta1 runAsUser;
        SeLinuxStrategyOptionsV1Beta1 seLinux;
        SupplementalGroupsStrategyOptionsV1Beta1 supplementalGroups;
        java.util.List<String> allowedCapabilities;
        java.util.List<AllowedCsiDriverV1Beta1> allowedCsiDrivers;
        java.util.List<AllowedFlexVolumeV1Beta1> allowedFlexVolumes;
        java.util.List<AllowedHostPathV1Beta1> allowedHostPaths;
        java.util.List<String> allowedProcMountTypes;
        java.util.List<String> allowedUnsafeSysctls;
        Boolean allowPrivilegeEscalation;
        java.util.List<String> defaultAddCapabilities;
        Boolean defaultAllowPrivilegeEscalation;
        java.util.List<String> forbiddenSysctls;
        Boolean hostIpc;
        Boolean hostNetwork;
        Boolean hostPid;
        java.util.List<HostPortRangeV1Beta1> hostPorts;
        Boolean privileged;
        Boolean readOnlyRootFilesystem;
        java.util.List<String> requiredDropCapabilities;
        RunAsGroupStrategyOptionsV1Beta1 runAsGroup;
        RuntimeClassStrategyOptionsV1Beta1 runtimeClass;
        java.util.List<String> volumes;

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getFsGroup}
         * @param fsGroup fsGroup is the strategy that will dictate what fs group is used by the SecurityContext. This parameter is required.
         * @return {@code this}
         */
        public Builder fsGroup(FsGroupStrategyOptionsV1Beta1 fsGroup) {
            this.fsGroup = fsGroup;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getRunAsUser}
         * @param runAsUser runAsUser is the strategy that will dictate the allowable RunAsUser values that may be set. This parameter is required.
         * @return {@code this}
         */
        public Builder runAsUser(RunAsUserStrategyOptionsV1Beta1 runAsUser) {
            this.runAsUser = runAsUser;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getSeLinux}
         * @param seLinux seLinux is the strategy that will dictate the allowable labels that may be set. This parameter is required.
         * @return {@code this}
         */
        public Builder seLinux(SeLinuxStrategyOptionsV1Beta1 seLinux) {
            this.seLinux = seLinux;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getSupplementalGroups}
         * @param supplementalGroups supplementalGroups is the strategy that will dictate what supplemental groups are used by the SecurityContext. This parameter is required.
         * @return {@code this}
         */
        public Builder supplementalGroups(SupplementalGroupsStrategyOptionsV1Beta1 supplementalGroups) {
            this.supplementalGroups = supplementalGroups;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getAllowedCapabilities}
         * @param allowedCapabilities allowedCapabilities is a list of capabilities that can be requested to add to the container.
         *                            Capabilities in this field may be added at the pod author's discretion. You must not list a capability in both allowedCapabilities and requiredDropCapabilities.
         * @return {@code this}
         */
        public Builder allowedCapabilities(java.util.List<String> allowedCapabilities) {
            this.allowedCapabilities = allowedCapabilities;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getAllowedCsiDrivers}
         * @param allowedCsiDrivers AllowedCSIDrivers is an allowlist of inline CSI drivers that must be explicitly set to be embedded within a pod spec.
         *                          An empty value indicates that any CSI driver can be used for inline ephemeral volumes. This is a beta field, and is only honored if the API server enables the CSIInlineVolume feature gate.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder allowedCsiDrivers(java.util.List<? extends AllowedCsiDriverV1Beta1> allowedCsiDrivers) {
            this.allowedCsiDrivers = (java.util.List<AllowedCsiDriverV1Beta1>)allowedCsiDrivers;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getAllowedFlexVolumes}
         * @param allowedFlexVolumes allowedFlexVolumes is an allowlist of Flexvolumes.
         *                           Empty or nil indicates that all Flexvolumes may be used.  This parameter is effective only when the usage of the Flexvolumes is allowed in the "volumes" field.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder allowedFlexVolumes(java.util.List<? extends AllowedFlexVolumeV1Beta1> allowedFlexVolumes) {
            this.allowedFlexVolumes = (java.util.List<AllowedFlexVolumeV1Beta1>)allowedFlexVolumes;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getAllowedHostPaths}
         * @param allowedHostPaths allowedHostPaths is an allowlist of host paths.
         *                         Empty indicates that all host paths may be used.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder allowedHostPaths(java.util.List<? extends AllowedHostPathV1Beta1> allowedHostPaths) {
            this.allowedHostPaths = (java.util.List<AllowedHostPathV1Beta1>)allowedHostPaths;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getAllowedProcMountTypes}
         * @param allowedProcMountTypes AllowedProcMountTypes is an allowlist of allowed ProcMountTypes.
         *                              Empty or nil indicates that only the DefaultProcMountType may be used. This requires the ProcMountType feature flag to be enabled.
         * @return {@code this}
         */
        public Builder allowedProcMountTypes(java.util.List<String> allowedProcMountTypes) {
            this.allowedProcMountTypes = allowedProcMountTypes;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getAllowedUnsafeSysctls}
         * @param allowedUnsafeSysctls allowedUnsafeSysctls is a list of explicitly allowed unsafe sysctls, defaults to none.
         *                             Each entry is either a plain sysctl name or ends in "*" in which case it is considered as a prefix of allowed sysctls. Single * means all unsafe sysctls are allowed. Kubelet has to allowlist all allowed unsafe sysctls explicitly to avoid rejection.
         *                             <p>
         *                             Examples: e.g. "foo/<em>" allows "foo/bar", "foo/baz", etc. e.g. "foo.</em>" allows "foo.bar", "foo.baz", etc.
         * @return {@code this}
         */
        public Builder allowedUnsafeSysctls(java.util.List<String> allowedUnsafeSysctls) {
            this.allowedUnsafeSysctls = allowedUnsafeSysctls;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getAllowPrivilegeEscalation}
         * @param allowPrivilegeEscalation allowPrivilegeEscalation determines if a pod can request to allow privilege escalation.
         *                                 If unspecified, defaults to true.
         * @return {@code this}
         */
        public Builder allowPrivilegeEscalation(Boolean allowPrivilegeEscalation) {
            this.allowPrivilegeEscalation = allowPrivilegeEscalation;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getDefaultAddCapabilities}
         * @param defaultAddCapabilities defaultAddCapabilities is the default set of capabilities that will be added to the container unless the pod spec specifically drops the capability.
         *                               You may not list a capability in both defaultAddCapabilities and requiredDropCapabilities. Capabilities added here are implicitly allowed, and need not be included in the allowedCapabilities list.
         * @return {@code this}
         */
        public Builder defaultAddCapabilities(java.util.List<String> defaultAddCapabilities) {
            this.defaultAddCapabilities = defaultAddCapabilities;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getDefaultAllowPrivilegeEscalation}
         * @param defaultAllowPrivilegeEscalation defaultAllowPrivilegeEscalation controls the default setting for whether a process can gain more privileges than its parent process.
         * @return {@code this}
         */
        public Builder defaultAllowPrivilegeEscalation(Boolean defaultAllowPrivilegeEscalation) {
            this.defaultAllowPrivilegeEscalation = defaultAllowPrivilegeEscalation;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getForbiddenSysctls}
         * @param forbiddenSysctls forbiddenSysctls is a list of explicitly forbidden sysctls, defaults to none.
         *                         Each entry is either a plain sysctl name or ends in "*" in which case it is considered as a prefix of forbidden sysctls. Single * means all sysctls are forbidden.
         *                         <p>
         *                         Examples: e.g. "foo/<em>" forbids "foo/bar", "foo/baz", etc. e.g. "foo.</em>" forbids "foo.bar", "foo.baz", etc.
         * @return {@code this}
         */
        public Builder forbiddenSysctls(java.util.List<String> forbiddenSysctls) {
            this.forbiddenSysctls = forbiddenSysctls;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getHostIpc}
         * @param hostIpc hostIPC determines if the policy allows the use of HostIPC in the pod spec.
         * @return {@code this}
         */
        public Builder hostIpc(Boolean hostIpc) {
            this.hostIpc = hostIpc;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getHostNetwork}
         * @param hostNetwork hostNetwork determines if the policy allows the use of HostNetwork in the pod spec.
         * @return {@code this}
         */
        public Builder hostNetwork(Boolean hostNetwork) {
            this.hostNetwork = hostNetwork;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getHostPid}
         * @param hostPid hostPID determines if the policy allows the use of HostPID in the pod spec.
         * @return {@code this}
         */
        public Builder hostPid(Boolean hostPid) {
            this.hostPid = hostPid;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getHostPorts}
         * @param hostPorts hostPorts determines which host port ranges are allowed to be exposed.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder hostPorts(java.util.List<? extends HostPortRangeV1Beta1> hostPorts) {
            this.hostPorts = (java.util.List<HostPortRangeV1Beta1>)hostPorts;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getPrivileged}
         * @param privileged privileged determines if a pod can request to be run as privileged.
         * @return {@code this}
         */
        public Builder privileged(Boolean privileged) {
            this.privileged = privileged;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getReadOnlyRootFilesystem}
         * @param readOnlyRootFilesystem readOnlyRootFilesystem when set to true will force containers to run with a read only root file system.
         *                               If the container specifically requests to run with a non-read only root file system the PSP should deny the pod. If set to false the container may run with a read only root file system if it wishes but it will not be forced to.
         * @return {@code this}
         */
        public Builder readOnlyRootFilesystem(Boolean readOnlyRootFilesystem) {
            this.readOnlyRootFilesystem = readOnlyRootFilesystem;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getRequiredDropCapabilities}
         * @param requiredDropCapabilities requiredDropCapabilities are the capabilities that will be dropped from the container.
         *                                 These are required to be dropped and cannot be added.
         * @return {@code this}
         */
        public Builder requiredDropCapabilities(java.util.List<String> requiredDropCapabilities) {
            this.requiredDropCapabilities = requiredDropCapabilities;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getRunAsGroup}
         * @param runAsGroup RunAsGroup is the strategy that will dictate the allowable RunAsGroup values that may be set.
         *                   If this field is omitted, the pod's RunAsGroup can take any value. This field requires the RunAsGroup feature gate to be enabled.
         * @return {@code this}
         */
        public Builder runAsGroup(RunAsGroupStrategyOptionsV1Beta1 runAsGroup) {
            this.runAsGroup = runAsGroup;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getRuntimeClass}
         * @param runtimeClass runtimeClass is the strategy that will dictate the allowable RuntimeClasses for a pod.
         *                     If this field is omitted, the pod's runtimeClassName field is unrestricted. Enforcement of this field depends on the RuntimeClass feature gate being enabled.
         * @return {@code this}
         */
        public Builder runtimeClass(RuntimeClassStrategyOptionsV1Beta1 runtimeClass) {
            this.runtimeClass = runtimeClass;
            return this;
        }

        /**
         * Sets the value of {@link PodSecurityPolicySpecV1Beta1#getVolumes}
         * @param volumes volumes is an allowlist of volume plugins.
         *                Empty indicates that no volumes may be used. To allow all volumes you may use '*'.
         * @return {@code this}
         */
        public Builder volumes(java.util.List<String> volumes) {
            this.volumes = volumes;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link PodSecurityPolicySpecV1Beta1}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public PodSecurityPolicySpecV1Beta1 build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link PodSecurityPolicySpecV1Beta1}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements PodSecurityPolicySpecV1Beta1 {
        private final FsGroupStrategyOptionsV1Beta1 fsGroup;
        private final RunAsUserStrategyOptionsV1Beta1 runAsUser;
        private final SeLinuxStrategyOptionsV1Beta1 seLinux;
        private final SupplementalGroupsStrategyOptionsV1Beta1 supplementalGroups;
        private final java.util.List<String> allowedCapabilities;
        private final java.util.List<AllowedCsiDriverV1Beta1> allowedCsiDrivers;
        private final java.util.List<AllowedFlexVolumeV1Beta1> allowedFlexVolumes;
        private final java.util.List<AllowedHostPathV1Beta1> allowedHostPaths;
        private final java.util.List<String> allowedProcMountTypes;
        private final java.util.List<String> allowedUnsafeSysctls;
        private final Boolean allowPrivilegeEscalation;
        private final java.util.List<String> defaultAddCapabilities;
        private final Boolean defaultAllowPrivilegeEscalation;
        private final java.util.List<String> forbiddenSysctls;
        private final Boolean hostIpc;
        private final Boolean hostNetwork;
        private final Boolean hostPid;
        private final java.util.List<HostPortRangeV1Beta1> hostPorts;
        private final Boolean privileged;
        private final Boolean readOnlyRootFilesystem;
        private final java.util.List<String> requiredDropCapabilities;
        private final RunAsGroupStrategyOptionsV1Beta1 runAsGroup;
        private final RuntimeClassStrategyOptionsV1Beta1 runtimeClass;
        private final java.util.List<String> volumes;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.fsGroup = software.amazon.jsii.Kernel.get(this, "fsGroup", software.amazon.jsii.NativeType.forClass(FsGroupStrategyOptionsV1Beta1.class));
            this.runAsUser = software.amazon.jsii.Kernel.get(this, "runAsUser", software.amazon.jsii.NativeType.forClass(RunAsUserStrategyOptionsV1Beta1.class));
            this.seLinux = software.amazon.jsii.Kernel.get(this, "seLinux", software.amazon.jsii.NativeType.forClass(SeLinuxStrategyOptionsV1Beta1.class));
            this.supplementalGroups = software.amazon.jsii.Kernel.get(this, "supplementalGroups", software.amazon.jsii.NativeType.forClass(SupplementalGroupsStrategyOptionsV1Beta1.class));
            this.allowedCapabilities = software.amazon.jsii.Kernel.get(this, "allowedCapabilities", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(String.class)));
            this.allowedCsiDrivers = software.amazon.jsii.Kernel.get(this, "allowedCsiDrivers", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(AllowedCsiDriverV1Beta1.class)));
            this.allowedFlexVolumes = software.amazon.jsii.Kernel.get(this, "allowedFlexVolumes", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(AllowedFlexVolumeV1Beta1.class)));
            this.allowedHostPaths = software.amazon.jsii.Kernel.get(this, "allowedHostPaths", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(AllowedHostPathV1Beta1.class)));
            this.allowedProcMountTypes = software.amazon.jsii.Kernel.get(this, "allowedProcMountTypes", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(String.class)));
            this.allowedUnsafeSysctls = software.amazon.jsii.Kernel.get(this, "allowedUnsafeSysctls", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(String.class)));
            this.allowPrivilegeEscalation = software.amazon.jsii.Kernel.get(this, "allowPrivilegeEscalation", software.amazon.jsii.NativeType.forClass(Boolean.class));
            this.defaultAddCapabilities = software.amazon.jsii.Kernel.get(this, "defaultAddCapabilities", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(String.class)));
            this.defaultAllowPrivilegeEscalation = software.amazon.jsii.Kernel.get(this, "defaultAllowPrivilegeEscalation", software.amazon.jsii.NativeType.forClass(Boolean.class));
            this.forbiddenSysctls = software.amazon.jsii.Kernel.get(this, "forbiddenSysctls", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(String.class)));
            this.hostIpc = software.amazon.jsii.Kernel.get(this, "hostIpc", software.amazon.jsii.NativeType.forClass(Boolean.class));
            this.hostNetwork = software.amazon.jsii.Kernel.get(this, "hostNetwork", software.amazon.jsii.NativeType.forClass(Boolean.class));
            this.hostPid = software.amazon.jsii.Kernel.get(this, "hostPid", software.amazon.jsii.NativeType.forClass(Boolean.class));
            this.hostPorts = software.amazon.jsii.Kernel.get(this, "hostPorts", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(HostPortRangeV1Beta1.class)));
            this.privileged = software.amazon.jsii.Kernel.get(this, "privileged", software.amazon.jsii.NativeType.forClass(Boolean.class));
            this.readOnlyRootFilesystem = software.amazon.jsii.Kernel.get(this, "readOnlyRootFilesystem", software.amazon.jsii.NativeType.forClass(Boolean.class));
            this.requiredDropCapabilities = software.amazon.jsii.Kernel.get(this, "requiredDropCapabilities", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(String.class)));
            this.runAsGroup = software.amazon.jsii.Kernel.get(this, "runAsGroup", software.amazon.jsii.NativeType.forClass(RunAsGroupStrategyOptionsV1Beta1.class));
            this.runtimeClass = software.amazon.jsii.Kernel.get(this, "runtimeClass", software.amazon.jsii.NativeType.forClass(RuntimeClassStrategyOptionsV1Beta1.class));
            this.volumes = software.amazon.jsii.Kernel.get(this, "volumes", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(String.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.fsGroup = java.util.Objects.requireNonNull(builder.fsGroup, "fsGroup is required");
            this.runAsUser = java.util.Objects.requireNonNull(builder.runAsUser, "runAsUser is required");
            this.seLinux = java.util.Objects.requireNonNull(builder.seLinux, "seLinux is required");
            this.supplementalGroups = java.util.Objects.requireNonNull(builder.supplementalGroups, "supplementalGroups is required");
            this.allowedCapabilities = builder.allowedCapabilities;
            this.allowedCsiDrivers = (java.util.List<AllowedCsiDriverV1Beta1>)builder.allowedCsiDrivers;
            this.allowedFlexVolumes = (java.util.List<AllowedFlexVolumeV1Beta1>)builder.allowedFlexVolumes;
            this.allowedHostPaths = (java.util.List<AllowedHostPathV1Beta1>)builder.allowedHostPaths;
            this.allowedProcMountTypes = builder.allowedProcMountTypes;
            this.allowedUnsafeSysctls = builder.allowedUnsafeSysctls;
            this.allowPrivilegeEscalation = builder.allowPrivilegeEscalation;
            this.defaultAddCapabilities = builder.defaultAddCapabilities;
            this.defaultAllowPrivilegeEscalation = builder.defaultAllowPrivilegeEscalation;
            this.forbiddenSysctls = builder.forbiddenSysctls;
            this.hostIpc = builder.hostIpc;
            this.hostNetwork = builder.hostNetwork;
            this.hostPid = builder.hostPid;
            this.hostPorts = (java.util.List<HostPortRangeV1Beta1>)builder.hostPorts;
            this.privileged = builder.privileged;
            this.readOnlyRootFilesystem = builder.readOnlyRootFilesystem;
            this.requiredDropCapabilities = builder.requiredDropCapabilities;
            this.runAsGroup = builder.runAsGroup;
            this.runtimeClass = builder.runtimeClass;
            this.volumes = builder.volumes;
        }

        @Override
        public final FsGroupStrategyOptionsV1Beta1 getFsGroup() {
            return this.fsGroup;
        }

        @Override
        public final RunAsUserStrategyOptionsV1Beta1 getRunAsUser() {
            return this.runAsUser;
        }

        @Override
        public final SeLinuxStrategyOptionsV1Beta1 getSeLinux() {
            return this.seLinux;
        }

        @Override
        public final SupplementalGroupsStrategyOptionsV1Beta1 getSupplementalGroups() {
            return this.supplementalGroups;
        }

        @Override
        public final java.util.List<String> getAllowedCapabilities() {
            return this.allowedCapabilities;
        }

        @Override
        public final java.util.List<AllowedCsiDriverV1Beta1> getAllowedCsiDrivers() {
            return this.allowedCsiDrivers;
        }

        @Override
        public final java.util.List<AllowedFlexVolumeV1Beta1> getAllowedFlexVolumes() {
            return this.allowedFlexVolumes;
        }

        @Override
        public final java.util.List<AllowedHostPathV1Beta1> getAllowedHostPaths() {
            return this.allowedHostPaths;
        }

        @Override
        public final java.util.List<String> getAllowedProcMountTypes() {
            return this.allowedProcMountTypes;
        }

        @Override
        public final java.util.List<String> getAllowedUnsafeSysctls() {
            return this.allowedUnsafeSysctls;
        }

        @Override
        public final Boolean getAllowPrivilegeEscalation() {
            return this.allowPrivilegeEscalation;
        }

        @Override
        public final java.util.List<String> getDefaultAddCapabilities() {
            return this.defaultAddCapabilities;
        }

        @Override
        public final Boolean getDefaultAllowPrivilegeEscalation() {
            return this.defaultAllowPrivilegeEscalation;
        }

        @Override
        public final java.util.List<String> getForbiddenSysctls() {
            return this.forbiddenSysctls;
        }

        @Override
        public final Boolean getHostIpc() {
            return this.hostIpc;
        }

        @Override
        public final Boolean getHostNetwork() {
            return this.hostNetwork;
        }

        @Override
        public final Boolean getHostPid() {
            return this.hostPid;
        }

        @Override
        public final java.util.List<HostPortRangeV1Beta1> getHostPorts() {
            return this.hostPorts;
        }

        @Override
        public final Boolean getPrivileged() {
            return this.privileged;
        }

        @Override
        public final Boolean getReadOnlyRootFilesystem() {
            return this.readOnlyRootFilesystem;
        }

        @Override
        public final java.util.List<String> getRequiredDropCapabilities() {
            return this.requiredDropCapabilities;
        }

        @Override
        public final RunAsGroupStrategyOptionsV1Beta1 getRunAsGroup() {
            return this.runAsGroup;
        }

        @Override
        public final RuntimeClassStrategyOptionsV1Beta1 getRuntimeClass() {
            return this.runtimeClass;
        }

        @Override
        public final java.util.List<String> getVolumes() {
            return this.volumes;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            data.set("fsGroup", om.valueToTree(this.getFsGroup()));
            data.set("runAsUser", om.valueToTree(this.getRunAsUser()));
            data.set("seLinux", om.valueToTree(this.getSeLinux()));
            data.set("supplementalGroups", om.valueToTree(this.getSupplementalGroups()));
            if (this.getAllowedCapabilities() != null) {
                data.set("allowedCapabilities", om.valueToTree(this.getAllowedCapabilities()));
            }
            if (this.getAllowedCsiDrivers() != null) {
                data.set("allowedCsiDrivers", om.valueToTree(this.getAllowedCsiDrivers()));
            }
            if (this.getAllowedFlexVolumes() != null) {
                data.set("allowedFlexVolumes", om.valueToTree(this.getAllowedFlexVolumes()));
            }
            if (this.getAllowedHostPaths() != null) {
                data.set("allowedHostPaths", om.valueToTree(this.getAllowedHostPaths()));
            }
            if (this.getAllowedProcMountTypes() != null) {
                data.set("allowedProcMountTypes", om.valueToTree(this.getAllowedProcMountTypes()));
            }
            if (this.getAllowedUnsafeSysctls() != null) {
                data.set("allowedUnsafeSysctls", om.valueToTree(this.getAllowedUnsafeSysctls()));
            }
            if (this.getAllowPrivilegeEscalation() != null) {
                data.set("allowPrivilegeEscalation", om.valueToTree(this.getAllowPrivilegeEscalation()));
            }
            if (this.getDefaultAddCapabilities() != null) {
                data.set("defaultAddCapabilities", om.valueToTree(this.getDefaultAddCapabilities()));
            }
            if (this.getDefaultAllowPrivilegeEscalation() != null) {
                data.set("defaultAllowPrivilegeEscalation", om.valueToTree(this.getDefaultAllowPrivilegeEscalation()));
            }
            if (this.getForbiddenSysctls() != null) {
                data.set("forbiddenSysctls", om.valueToTree(this.getForbiddenSysctls()));
            }
            if (this.getHostIpc() != null) {
                data.set("hostIpc", om.valueToTree(this.getHostIpc()));
            }
            if (this.getHostNetwork() != null) {
                data.set("hostNetwork", om.valueToTree(this.getHostNetwork()));
            }
            if (this.getHostPid() != null) {
                data.set("hostPid", om.valueToTree(this.getHostPid()));
            }
            if (this.getHostPorts() != null) {
                data.set("hostPorts", om.valueToTree(this.getHostPorts()));
            }
            if (this.getPrivileged() != null) {
                data.set("privileged", om.valueToTree(this.getPrivileged()));
            }
            if (this.getReadOnlyRootFilesystem() != null) {
                data.set("readOnlyRootFilesystem", om.valueToTree(this.getReadOnlyRootFilesystem()));
            }
            if (this.getRequiredDropCapabilities() != null) {
                data.set("requiredDropCapabilities", om.valueToTree(this.getRequiredDropCapabilities()));
            }
            if (this.getRunAsGroup() != null) {
                data.set("runAsGroup", om.valueToTree(this.getRunAsGroup()));
            }
            if (this.getRuntimeClass() != null) {
                data.set("runtimeClass", om.valueToTree(this.getRuntimeClass()));
            }
            if (this.getVolumes() != null) {
                data.set("volumes", om.valueToTree(this.getVolumes()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("k8s.PodSecurityPolicySpecV1Beta1"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Jsii$Proxy that = (Jsii$Proxy) o;

            if (!fsGroup.equals(that.fsGroup)) return false;
            if (!runAsUser.equals(that.runAsUser)) return false;
            if (!seLinux.equals(that.seLinux)) return false;
            if (!supplementalGroups.equals(that.supplementalGroups)) return false;
            if (this.allowedCapabilities != null ? !this.allowedCapabilities.equals(that.allowedCapabilities) : that.allowedCapabilities != null) return false;
            if (this.allowedCsiDrivers != null ? !this.allowedCsiDrivers.equals(that.allowedCsiDrivers) : that.allowedCsiDrivers != null) return false;
            if (this.allowedFlexVolumes != null ? !this.allowedFlexVolumes.equals(that.allowedFlexVolumes) : that.allowedFlexVolumes != null) return false;
            if (this.allowedHostPaths != null ? !this.allowedHostPaths.equals(that.allowedHostPaths) : that.allowedHostPaths != null) return false;
            if (this.allowedProcMountTypes != null ? !this.allowedProcMountTypes.equals(that.allowedProcMountTypes) : that.allowedProcMountTypes != null) return false;
            if (this.allowedUnsafeSysctls != null ? !this.allowedUnsafeSysctls.equals(that.allowedUnsafeSysctls) : that.allowedUnsafeSysctls != null) return false;
            if (this.allowPrivilegeEscalation != null ? !this.allowPrivilegeEscalation.equals(that.allowPrivilegeEscalation) : that.allowPrivilegeEscalation != null) return false;
            if (this.defaultAddCapabilities != null ? !this.defaultAddCapabilities.equals(that.defaultAddCapabilities) : that.defaultAddCapabilities != null) return false;
            if (this.defaultAllowPrivilegeEscalation != null ? !this.defaultAllowPrivilegeEscalation.equals(that.defaultAllowPrivilegeEscalation) : that.defaultAllowPrivilegeEscalation != null) return false;
            if (this.forbiddenSysctls != null ? !this.forbiddenSysctls.equals(that.forbiddenSysctls) : that.forbiddenSysctls != null) return false;
            if (this.hostIpc != null ? !this.hostIpc.equals(that.hostIpc) : that.hostIpc != null) return false;
            if (this.hostNetwork != null ? !this.hostNetwork.equals(that.hostNetwork) : that.hostNetwork != null) return false;
            if (this.hostPid != null ? !this.hostPid.equals(that.hostPid) : that.hostPid != null) return false;
            if (this.hostPorts != null ? !this.hostPorts.equals(that.hostPorts) : that.hostPorts != null) return false;
            if (this.privileged != null ? !this.privileged.equals(that.privileged) : that.privileged != null) return false;
            if (this.readOnlyRootFilesystem != null ? !this.readOnlyRootFilesystem.equals(that.readOnlyRootFilesystem) : that.readOnlyRootFilesystem != null) return false;
            if (this.requiredDropCapabilities != null ? !this.requiredDropCapabilities.equals(that.requiredDropCapabilities) : that.requiredDropCapabilities != null) return false;
            if (this.runAsGroup != null ? !this.runAsGroup.equals(that.runAsGroup) : that.runAsGroup != null) return false;
            if (this.runtimeClass != null ? !this.runtimeClass.equals(that.runtimeClass) : that.runtimeClass != null) return false;
            return this.volumes != null ? this.volumes.equals(that.volumes) : that.volumes == null;
        }

        @Override
        public final int hashCode() {
            int result = this.fsGroup.hashCode();
            result = 31 * result + (this.runAsUser.hashCode());
            result = 31 * result + (this.seLinux.hashCode());
            result = 31 * result + (this.supplementalGroups.hashCode());
            result = 31 * result + (this.allowedCapabilities != null ? this.allowedCapabilities.hashCode() : 0);
            result = 31 * result + (this.allowedCsiDrivers != null ? this.allowedCsiDrivers.hashCode() : 0);
            result = 31 * result + (this.allowedFlexVolumes != null ? this.allowedFlexVolumes.hashCode() : 0);
            result = 31 * result + (this.allowedHostPaths != null ? this.allowedHostPaths.hashCode() : 0);
            result = 31 * result + (this.allowedProcMountTypes != null ? this.allowedProcMountTypes.hashCode() : 0);
            result = 31 * result + (this.allowedUnsafeSysctls != null ? this.allowedUnsafeSysctls.hashCode() : 0);
            result = 31 * result + (this.allowPrivilegeEscalation != null ? this.allowPrivilegeEscalation.hashCode() : 0);
            result = 31 * result + (this.defaultAddCapabilities != null ? this.defaultAddCapabilities.hashCode() : 0);
            result = 31 * result + (this.defaultAllowPrivilegeEscalation != null ? this.defaultAllowPrivilegeEscalation.hashCode() : 0);
            result = 31 * result + (this.forbiddenSysctls != null ? this.forbiddenSysctls.hashCode() : 0);
            result = 31 * result + (this.hostIpc != null ? this.hostIpc.hashCode() : 0);
            result = 31 * result + (this.hostNetwork != null ? this.hostNetwork.hashCode() : 0);
            result = 31 * result + (this.hostPid != null ? this.hostPid.hashCode() : 0);
            result = 31 * result + (this.hostPorts != null ? this.hostPorts.hashCode() : 0);
            result = 31 * result + (this.privileged != null ? this.privileged.hashCode() : 0);
            result = 31 * result + (this.readOnlyRootFilesystem != null ? this.readOnlyRootFilesystem.hashCode() : 0);
            result = 31 * result + (this.requiredDropCapabilities != null ? this.requiredDropCapabilities.hashCode() : 0);
            result = 31 * result + (this.runAsGroup != null ? this.runAsGroup.hashCode() : 0);
            result = 31 * result + (this.runtimeClass != null ? this.runtimeClass.hashCode() : 0);
            result = 31 * result + (this.volumes != null ? this.volumes.hashCode() : 0);
            return result;
        }
    }
}
