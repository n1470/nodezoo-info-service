package imports.io.istio.networking;

/**
 * Service discovery mode for the hosts.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.461Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.ServiceEntrySpecResolution")
public enum ServiceEntrySpecResolution {
    /**
     * NONE.
     */
    NONE,
    /**
     * STATIC.
     */
    STATIC,
    /**
     * DNS.
     */
    DNS,
    /**
     * DNS_ROUND_ROBIN.
     */
    DNS_ROUND_ROBIN,
}
