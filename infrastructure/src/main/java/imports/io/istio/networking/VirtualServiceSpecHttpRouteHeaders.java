package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.468Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpRouteHeaders")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpRouteHeaders.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpRouteHeaders extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersRequest getRequest() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersResponse getResponse() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpRouteHeaders}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpRouteHeaders}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpRouteHeaders> {
        imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersRequest request;
        imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersResponse response;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRouteHeaders#getRequest}
         * @param request the value to be set.
         * @return {@code this}
         */
        public Builder request(imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersRequest request) {
            this.request = request;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRouteHeaders#getResponse}
         * @param response the value to be set.
         * @return {@code this}
         */
        public Builder response(imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersResponse response) {
            this.response = response;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpRouteHeaders}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpRouteHeaders build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpRouteHeaders}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpRouteHeaders {
        private final imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersRequest request;
        private final imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersResponse response;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.request = software.amazon.jsii.Kernel.get(this, "request", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersRequest.class));
            this.response = software.amazon.jsii.Kernel.get(this, "response", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersResponse.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.request = builder.request;
            this.response = builder.response;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersRequest getRequest() {
            return this.request;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpRouteHeadersResponse getResponse() {
            return this.response;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getRequest() != null) {
                data.set("request", om.valueToTree(this.getRequest()));
            }
            if (this.getResponse() != null) {
                data.set("response", om.valueToTree(this.getResponse()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpRouteHeaders"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpRouteHeaders.Jsii$Proxy that = (VirtualServiceSpecHttpRouteHeaders.Jsii$Proxy) o;

            if (this.request != null ? !this.request.equals(that.request) : that.request != null) return false;
            return this.response != null ? this.response.equals(that.response) : that.response == null;
        }

        @Override
        public final int hashCode() {
            int result = this.request != null ? this.request.hashCode() : 0;
            result = 31 * result + (this.response != null ? this.response.hashCode() : 0);
            return result;
        }
    }
}
