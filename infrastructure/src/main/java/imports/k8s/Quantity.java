package imports.k8s;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.347Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.Quantity")
public class Quantity extends software.amazon.jsii.JsiiObject {

    protected Quantity(final software.amazon.jsii.JsiiObjectRef objRef) {
        super(objRef);
    }

    protected Quantity(final software.amazon.jsii.JsiiObject.InitializationMode initializationMode) {
        super(initializationMode);
    }

    public static @org.jetbrains.annotations.NotNull Quantity fromNumber(final @org.jetbrains.annotations.NotNull Number value) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(Quantity.class, "fromNumber", software.amazon.jsii.NativeType.forClass(Quantity.class), new Object[] { java.util.Objects.requireNonNull(value, "value is required") });
    }

    public static @org.jetbrains.annotations.NotNull Quantity fromString(final @org.jetbrains.annotations.NotNull String value) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(Quantity.class, "fromString", software.amazon.jsii.NativeType.forClass(Quantity.class), new Object[] { java.util.Objects.requireNonNull(value, "value is required") });
    }

    public @org.jetbrains.annotations.NotNull Object getValue() {
        return software.amazon.jsii.Kernel.get(this, "value", software.amazon.jsii.NativeType.forClass(Object.class));
    }
}
