package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.437Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute.Jsii$Proxy.class)
public interface DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute extends software.amazon.jsii.JsiiSerializable {

    /**
     * Originating locality, '/' separated, e.g.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getFrom() {
        return null;
    }

    /**
     * Map of upstream localities to traffic distribution weights.
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.Number> getTo() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute> {
        java.lang.String from;
        java.util.Map<java.lang.String, java.lang.Number> to;

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute#getFrom}
         * @param from Originating locality, '/' separated, e.g.
         * @return {@code this}
         */
        public Builder from(java.lang.String from) {
            this.from = from;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute#getTo}
         * @param to Map of upstream localities to traffic distribution weights.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder to(java.util.Map<java.lang.String, ? extends java.lang.Number> to) {
            this.to = (java.util.Map<java.lang.String, java.lang.Number>)to;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute {
        private final java.lang.String from;
        private final java.util.Map<java.lang.String, java.lang.Number> to;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.from = software.amazon.jsii.Kernel.get(this, "from", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.to = software.amazon.jsii.Kernel.get(this, "to", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.Number.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.from = builder.from;
            this.to = (java.util.Map<java.lang.String, java.lang.Number>)builder.to;
        }

        @Override
        public final java.lang.String getFrom() {
            return this.from;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.Number> getTo() {
            return this.to;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getFrom() != null) {
                data.set("from", om.valueToTree(this.getFrom()));
            }
            if (this.getTo() != null) {
                data.set("to", om.valueToTree(this.getTo()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute.Jsii$Proxy that = (DestinationRuleSpecTrafficPolicyLoadBalancerLocalityLbSettingDistribute.Jsii$Proxy) o;

            if (this.from != null ? !this.from.equals(that.from) : that.from != null) return false;
            return this.to != null ? this.to.equals(that.to) : that.to == null;
        }

        @Override
        public final int hashCode() {
            int result = this.from != null ? this.from.hashCode() : 0;
            result = 31 * result + (this.to != null ? this.to.hashCode() : 0);
            return result;
        }
    }
}
