package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.466Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpMatch")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpMatch.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpMatch extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpMatchAuthority getAuthority() {
        return null;
    }

    /**
     * Names of gateways where the rule should be applied.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getGateways() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchHeaders> getHeaders() {
        return null;
    }

    /**
     * Flag to specify whether the URI matching should be case-insensitive.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getIgnoreUriCase() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpMatchMethod getMethod() {
        return null;
    }

    /**
     * The name assigned to a match.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * Specifies the ports on the host that is being addressed.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPort() {
        return null;
    }

    /**
     * Query parameters for matching.
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchQueryParams> getQueryParams() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpMatchScheme getScheme() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getSourceLabels() {
        return null;
    }

    /**
     * Source namespace constraining the applicability of a rule to workloads in that namespace.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getSourceNamespace() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpMatchUri getUri() {
        return null;
    }

    /**
     * withoutHeader has the same syntax with the header, but has opposite meaning.
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchWithoutHeaders> getWithoutHeaders() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpMatch}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpMatch}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpMatch> {
        imports.io.istio.networking.VirtualServiceSpecHttpMatchAuthority authority;
        java.util.List<java.lang.String> gateways;
        java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchHeaders> headers;
        java.lang.Boolean ignoreUriCase;
        imports.io.istio.networking.VirtualServiceSpecHttpMatchMethod method;
        java.lang.String name;
        java.lang.Number port;
        java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchQueryParams> queryParams;
        imports.io.istio.networking.VirtualServiceSpecHttpMatchScheme scheme;
        java.util.Map<java.lang.String, java.lang.String> sourceLabels;
        java.lang.String sourceNamespace;
        imports.io.istio.networking.VirtualServiceSpecHttpMatchUri uri;
        java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchWithoutHeaders> withoutHeaders;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getAuthority}
         * @param authority the value to be set.
         * @return {@code this}
         */
        public Builder authority(imports.io.istio.networking.VirtualServiceSpecHttpMatchAuthority authority) {
            this.authority = authority;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getGateways}
         * @param gateways Names of gateways where the rule should be applied.
         * @return {@code this}
         */
        public Builder gateways(java.util.List<java.lang.String> gateways) {
            this.gateways = gateways;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getHeaders}
         * @param headers the value to be set.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder headers(java.util.Map<java.lang.String, ? extends imports.io.istio.networking.VirtualServiceSpecHttpMatchHeaders> headers) {
            this.headers = (java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchHeaders>)headers;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getIgnoreUriCase}
         * @param ignoreUriCase Flag to specify whether the URI matching should be case-insensitive.
         * @return {@code this}
         */
        public Builder ignoreUriCase(java.lang.Boolean ignoreUriCase) {
            this.ignoreUriCase = ignoreUriCase;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getMethod}
         * @param method the value to be set.
         * @return {@code this}
         */
        public Builder method(imports.io.istio.networking.VirtualServiceSpecHttpMatchMethod method) {
            this.method = method;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getName}
         * @param name The name assigned to a match.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getPort}
         * @param port Specifies the ports on the host that is being addressed.
         * @return {@code this}
         */
        public Builder port(java.lang.Number port) {
            this.port = port;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getQueryParams}
         * @param queryParams Query parameters for matching.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder queryParams(java.util.Map<java.lang.String, ? extends imports.io.istio.networking.VirtualServiceSpecHttpMatchQueryParams> queryParams) {
            this.queryParams = (java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchQueryParams>)queryParams;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getScheme}
         * @param scheme the value to be set.
         * @return {@code this}
         */
        public Builder scheme(imports.io.istio.networking.VirtualServiceSpecHttpMatchScheme scheme) {
            this.scheme = scheme;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getSourceLabels}
         * @param sourceLabels the value to be set.
         * @return {@code this}
         */
        public Builder sourceLabels(java.util.Map<java.lang.String, java.lang.String> sourceLabels) {
            this.sourceLabels = sourceLabels;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getSourceNamespace}
         * @param sourceNamespace Source namespace constraining the applicability of a rule to workloads in that namespace.
         * @return {@code this}
         */
        public Builder sourceNamespace(java.lang.String sourceNamespace) {
            this.sourceNamespace = sourceNamespace;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getUri}
         * @param uri the value to be set.
         * @return {@code this}
         */
        public Builder uri(imports.io.istio.networking.VirtualServiceSpecHttpMatchUri uri) {
            this.uri = uri;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMatch#getWithoutHeaders}
         * @param withoutHeaders withoutHeader has the same syntax with the header, but has opposite meaning.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder withoutHeaders(java.util.Map<java.lang.String, ? extends imports.io.istio.networking.VirtualServiceSpecHttpMatchWithoutHeaders> withoutHeaders) {
            this.withoutHeaders = (java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchWithoutHeaders>)withoutHeaders;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpMatch}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpMatch build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpMatch}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpMatch {
        private final imports.io.istio.networking.VirtualServiceSpecHttpMatchAuthority authority;
        private final java.util.List<java.lang.String> gateways;
        private final java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchHeaders> headers;
        private final java.lang.Boolean ignoreUriCase;
        private final imports.io.istio.networking.VirtualServiceSpecHttpMatchMethod method;
        private final java.lang.String name;
        private final java.lang.Number port;
        private final java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchQueryParams> queryParams;
        private final imports.io.istio.networking.VirtualServiceSpecHttpMatchScheme scheme;
        private final java.util.Map<java.lang.String, java.lang.String> sourceLabels;
        private final java.lang.String sourceNamespace;
        private final imports.io.istio.networking.VirtualServiceSpecHttpMatchUri uri;
        private final java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchWithoutHeaders> withoutHeaders;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.authority = software.amazon.jsii.Kernel.get(this, "authority", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMatchAuthority.class));
            this.gateways = software.amazon.jsii.Kernel.get(this, "gateways", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.headers = software.amazon.jsii.Kernel.get(this, "headers", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMatchHeaders.class)));
            this.ignoreUriCase = software.amazon.jsii.Kernel.get(this, "ignoreUriCase", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
            this.method = software.amazon.jsii.Kernel.get(this, "method", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMatchMethod.class));
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.queryParams = software.amazon.jsii.Kernel.get(this, "queryParams", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMatchQueryParams.class)));
            this.scheme = software.amazon.jsii.Kernel.get(this, "scheme", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMatchScheme.class));
            this.sourceLabels = software.amazon.jsii.Kernel.get(this, "sourceLabels", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.sourceNamespace = software.amazon.jsii.Kernel.get(this, "sourceNamespace", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.uri = software.amazon.jsii.Kernel.get(this, "uri", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMatchUri.class));
            this.withoutHeaders = software.amazon.jsii.Kernel.get(this, "withoutHeaders", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMatchWithoutHeaders.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.authority = builder.authority;
            this.gateways = builder.gateways;
            this.headers = (java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchHeaders>)builder.headers;
            this.ignoreUriCase = builder.ignoreUriCase;
            this.method = builder.method;
            this.name = builder.name;
            this.port = builder.port;
            this.queryParams = (java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchQueryParams>)builder.queryParams;
            this.scheme = builder.scheme;
            this.sourceLabels = builder.sourceLabels;
            this.sourceNamespace = builder.sourceNamespace;
            this.uri = builder.uri;
            this.withoutHeaders = (java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchWithoutHeaders>)builder.withoutHeaders;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpMatchAuthority getAuthority() {
            return this.authority;
        }

        @Override
        public final java.util.List<java.lang.String> getGateways() {
            return this.gateways;
        }

        @Override
        public final java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchHeaders> getHeaders() {
            return this.headers;
        }

        @Override
        public final java.lang.Boolean getIgnoreUriCase() {
            return this.ignoreUriCase;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpMatchMethod getMethod() {
            return this.method;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final java.lang.Number getPort() {
            return this.port;
        }

        @Override
        public final java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchQueryParams> getQueryParams() {
            return this.queryParams;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpMatchScheme getScheme() {
            return this.scheme;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getSourceLabels() {
            return this.sourceLabels;
        }

        @Override
        public final java.lang.String getSourceNamespace() {
            return this.sourceNamespace;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpMatchUri getUri() {
            return this.uri;
        }

        @Override
        public final java.util.Map<java.lang.String, imports.io.istio.networking.VirtualServiceSpecHttpMatchWithoutHeaders> getWithoutHeaders() {
            return this.withoutHeaders;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAuthority() != null) {
                data.set("authority", om.valueToTree(this.getAuthority()));
            }
            if (this.getGateways() != null) {
                data.set("gateways", om.valueToTree(this.getGateways()));
            }
            if (this.getHeaders() != null) {
                data.set("headers", om.valueToTree(this.getHeaders()));
            }
            if (this.getIgnoreUriCase() != null) {
                data.set("ignoreUriCase", om.valueToTree(this.getIgnoreUriCase()));
            }
            if (this.getMethod() != null) {
                data.set("method", om.valueToTree(this.getMethod()));
            }
            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }
            if (this.getQueryParams() != null) {
                data.set("queryParams", om.valueToTree(this.getQueryParams()));
            }
            if (this.getScheme() != null) {
                data.set("scheme", om.valueToTree(this.getScheme()));
            }
            if (this.getSourceLabels() != null) {
                data.set("sourceLabels", om.valueToTree(this.getSourceLabels()));
            }
            if (this.getSourceNamespace() != null) {
                data.set("sourceNamespace", om.valueToTree(this.getSourceNamespace()));
            }
            if (this.getUri() != null) {
                data.set("uri", om.valueToTree(this.getUri()));
            }
            if (this.getWithoutHeaders() != null) {
                data.set("withoutHeaders", om.valueToTree(this.getWithoutHeaders()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpMatch"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpMatch.Jsii$Proxy that = (VirtualServiceSpecHttpMatch.Jsii$Proxy) o;

            if (this.authority != null ? !this.authority.equals(that.authority) : that.authority != null) return false;
            if (this.gateways != null ? !this.gateways.equals(that.gateways) : that.gateways != null) return false;
            if (this.headers != null ? !this.headers.equals(that.headers) : that.headers != null) return false;
            if (this.ignoreUriCase != null ? !this.ignoreUriCase.equals(that.ignoreUriCase) : that.ignoreUriCase != null) return false;
            if (this.method != null ? !this.method.equals(that.method) : that.method != null) return false;
            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            if (this.port != null ? !this.port.equals(that.port) : that.port != null) return false;
            if (this.queryParams != null ? !this.queryParams.equals(that.queryParams) : that.queryParams != null) return false;
            if (this.scheme != null ? !this.scheme.equals(that.scheme) : that.scheme != null) return false;
            if (this.sourceLabels != null ? !this.sourceLabels.equals(that.sourceLabels) : that.sourceLabels != null) return false;
            if (this.sourceNamespace != null ? !this.sourceNamespace.equals(that.sourceNamespace) : that.sourceNamespace != null) return false;
            if (this.uri != null ? !this.uri.equals(that.uri) : that.uri != null) return false;
            return this.withoutHeaders != null ? this.withoutHeaders.equals(that.withoutHeaders) : that.withoutHeaders == null;
        }

        @Override
        public final int hashCode() {
            int result = this.authority != null ? this.authority.hashCode() : 0;
            result = 31 * result + (this.gateways != null ? this.gateways.hashCode() : 0);
            result = 31 * result + (this.headers != null ? this.headers.hashCode() : 0);
            result = 31 * result + (this.ignoreUriCase != null ? this.ignoreUriCase.hashCode() : 0);
            result = 31 * result + (this.method != null ? this.method.hashCode() : 0);
            result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            result = 31 * result + (this.queryParams != null ? this.queryParams.hashCode() : 0);
            result = 31 * result + (this.scheme != null ? this.scheme.hashCode() : 0);
            result = 31 * result + (this.sourceLabels != null ? this.sourceLabels.hashCode() : 0);
            result = 31 * result + (this.sourceNamespace != null ? this.sourceNamespace.hashCode() : 0);
            result = 31 * result + (this.uri != null ? this.uri.hashCode() : 0);
            result = 31 * result + (this.withoutHeaders != null ? this.withoutHeaders.hashCode() : 0);
            return result;
        }
    }
}
