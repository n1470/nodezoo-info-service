package imports.io.istio.networking;

/**
 * Settings controlling the load balancer algorithms.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.429Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer.Jsii$Proxy.class)
public interface DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash getConsistentHash() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting getLocalityLbSetting() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerSimple getSimple() {
        return null;
    }

    /**
     * Represents the warmup duration of Service.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getWarmupDurationSecs() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer> {
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash consistentHash;
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting localityLbSetting;
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerSimple simple;
        java.lang.String warmupDurationSecs;

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer#getConsistentHash}
         * @param consistentHash the value to be set.
         * @return {@code this}
         */
        public Builder consistentHash(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash consistentHash) {
            this.consistentHash = consistentHash;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer#getLocalityLbSetting}
         * @param localityLbSetting the value to be set.
         * @return {@code this}
         */
        public Builder localityLbSetting(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting localityLbSetting) {
            this.localityLbSetting = localityLbSetting;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer#getSimple}
         * @param simple the value to be set.
         * @return {@code this}
         */
        public Builder simple(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerSimple simple) {
            this.simple = simple;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer#getWarmupDurationSecs}
         * @param warmupDurationSecs Represents the warmup duration of Service.
         * @return {@code this}
         */
        public Builder warmupDurationSecs(java.lang.String warmupDurationSecs) {
            this.warmupDurationSecs = warmupDurationSecs;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer {
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash consistentHash;
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting localityLbSetting;
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerSimple simple;
        private final java.lang.String warmupDurationSecs;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.consistentHash = software.amazon.jsii.Kernel.get(this, "consistentHash", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash.class));
            this.localityLbSetting = software.amazon.jsii.Kernel.get(this, "localityLbSetting", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting.class));
            this.simple = software.amazon.jsii.Kernel.get(this, "simple", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerSimple.class));
            this.warmupDurationSecs = software.amazon.jsii.Kernel.get(this, "warmupDurationSecs", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.consistentHash = builder.consistentHash;
            this.localityLbSetting = builder.localityLbSetting;
            this.simple = builder.simple;
            this.warmupDurationSecs = builder.warmupDurationSecs;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash getConsistentHash() {
            return this.consistentHash;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerLocalityLbSetting getLocalityLbSetting() {
            return this.localityLbSetting;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerSimple getSimple() {
            return this.simple;
        }

        @Override
        public final java.lang.String getWarmupDurationSecs() {
            return this.warmupDurationSecs;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getConsistentHash() != null) {
                data.set("consistentHash", om.valueToTree(this.getConsistentHash()));
            }
            if (this.getLocalityLbSetting() != null) {
                data.set("localityLbSetting", om.valueToTree(this.getLocalityLbSetting()));
            }
            if (this.getSimple() != null) {
                data.set("simple", om.valueToTree(this.getSimple()));
            }
            if (this.getWarmupDurationSecs() != null) {
                data.set("warmupDurationSecs", om.valueToTree(this.getWarmupDurationSecs()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer.Jsii$Proxy that = (DestinationRuleSpecSubsetsTrafficPolicyLoadBalancer.Jsii$Proxy) o;

            if (this.consistentHash != null ? !this.consistentHash.equals(that.consistentHash) : that.consistentHash != null) return false;
            if (this.localityLbSetting != null ? !this.localityLbSetting.equals(that.localityLbSetting) : that.localityLbSetting != null) return false;
            if (this.simple != null ? !this.simple.equals(that.simple) : that.simple != null) return false;
            return this.warmupDurationSecs != null ? this.warmupDurationSecs.equals(that.warmupDurationSecs) : that.warmupDurationSecs == null;
        }

        @Override
        public final int hashCode() {
            int result = this.consistentHash != null ? this.consistentHash.hashCode() : 0;
            result = 31 * result + (this.localityLbSetting != null ? this.localityLbSetting.hashCode() : 0);
            result = 31 * result + (this.simple != null ? this.simple.hashCode() : 0);
            result = 31 * result + (this.warmupDurationSecs != null ? this.warmupDurationSecs.hashCode() : 0);
            return result;
        }
    }
}
