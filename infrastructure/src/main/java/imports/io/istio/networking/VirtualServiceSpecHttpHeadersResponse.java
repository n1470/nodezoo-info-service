package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.465Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpHeadersResponse")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpHeadersResponse.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpHeadersResponse extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getAdd() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getRemove() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getSet() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpHeadersResponse}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpHeadersResponse}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpHeadersResponse> {
        java.util.Map<java.lang.String, java.lang.String> add;
        java.util.List<java.lang.String> remove;
        java.util.Map<java.lang.String, java.lang.String> set;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpHeadersResponse#getAdd}
         * @param add the value to be set.
         * @return {@code this}
         */
        public Builder add(java.util.Map<java.lang.String, java.lang.String> add) {
            this.add = add;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpHeadersResponse#getRemove}
         * @param remove the value to be set.
         * @return {@code this}
         */
        public Builder remove(java.util.List<java.lang.String> remove) {
            this.remove = remove;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpHeadersResponse#getSet}
         * @param set the value to be set.
         * @return {@code this}
         */
        public Builder set(java.util.Map<java.lang.String, java.lang.String> set) {
            this.set = set;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpHeadersResponse}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpHeadersResponse build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpHeadersResponse}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpHeadersResponse {
        private final java.util.Map<java.lang.String, java.lang.String> add;
        private final java.util.List<java.lang.String> remove;
        private final java.util.Map<java.lang.String, java.lang.String> set;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.add = software.amazon.jsii.Kernel.get(this, "add", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.remove = software.amazon.jsii.Kernel.get(this, "remove", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.set = software.amazon.jsii.Kernel.get(this, "set", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.add = builder.add;
            this.remove = builder.remove;
            this.set = builder.set;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getAdd() {
            return this.add;
        }

        @Override
        public final java.util.List<java.lang.String> getRemove() {
            return this.remove;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getSet() {
            return this.set;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAdd() != null) {
                data.set("add", om.valueToTree(this.getAdd()));
            }
            if (this.getRemove() != null) {
                data.set("remove", om.valueToTree(this.getRemove()));
            }
            if (this.getSet() != null) {
                data.set("set", om.valueToTree(this.getSet()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpHeadersResponse"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpHeadersResponse.Jsii$Proxy that = (VirtualServiceSpecHttpHeadersResponse.Jsii$Proxy) o;

            if (this.add != null ? !this.add.equals(that.add) : that.add != null) return false;
            if (this.remove != null ? !this.remove.equals(that.remove) : that.remove != null) return false;
            return this.set != null ? this.set.equals(that.set) : that.set == null;
        }

        @Override
        public final int hashCode() {
            int result = this.add != null ? this.add.hashCode() : 0;
            result = 31 * result + (this.remove != null ? this.remove.hashCode() : 0);
            result = 31 * result + (this.set != null ? this.set.hashCode() : 0);
            return result;
        }
    }
}
