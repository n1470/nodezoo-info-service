package imports.k8s;

/**
 * ConfigMap holds configuration data for pods to consume.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.231Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.KubeConfigMap")
public class KubeConfigMap extends org.cdk8s.ApiObject {

    protected KubeConfigMap(final software.amazon.jsii.JsiiObjectRef objRef) {
        super(objRef);
    }

    protected KubeConfigMap(final software.amazon.jsii.JsiiObject.InitializationMode initializationMode) {
        super(initializationMode);
    }

    static {
        GVK = software.amazon.jsii.JsiiObject.jsiiStaticGet(KubeConfigMap.class, "GVK", software.amazon.jsii.NativeType.forClass(org.cdk8s.GroupVersionKind.class));
    }

    /**
     * Defines a "io.k8s.api.core.v1.ConfigMap" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     * @param props initialization props.
     */
    public KubeConfigMap(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull String id, final @org.jetbrains.annotations.Nullable KubeConfigMapProps props) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required"), props });
    }

    /**
     * Defines a "io.k8s.api.core.v1.ConfigMap" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     */
    public KubeConfigMap(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull String id) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required") });
    }

    /**
     * Renders a Kubernetes manifest for "io.k8s.api.core.v1.ConfigMap".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     * <p>
     * @param props initialization props.
     */
    public static @org.jetbrains.annotations.NotNull Object manifest(final @org.jetbrains.annotations.Nullable KubeConfigMapProps props) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(KubeConfigMap.class, "manifest", software.amazon.jsii.NativeType.forClass(Object.class), new Object[] { props });
    }

    /**
     * Renders a Kubernetes manifest for "io.k8s.api.core.v1.ConfigMap".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     */
    public static @org.jetbrains.annotations.NotNull Object manifest() {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(KubeConfigMap.class, "manifest", software.amazon.jsii.NativeType.forClass(Object.class));
    }

    /**
     * Renders the object to Kubernetes JSON.
     */
    @Override
    public @org.jetbrains.annotations.NotNull Object toJson() {
        return software.amazon.jsii.Kernel.call(this, "toJson", software.amazon.jsii.NativeType.forClass(Object.class));
    }

    /**
     * Returns the apiVersion and kind for "io.k8s.api.core.v1.ConfigMap".
     */
    public final static org.cdk8s.GroupVersionKind GVK;

    /**
     * A fluent builder for {@link KubeConfigMap}.
     */
    public static final class Builder implements software.amazon.jsii.Builder<KubeConfigMap> {
        /**
         * @return a new instance of {@link Builder}.
         * @param scope the scope in which to define this object. This parameter is required.
         * @param id a scope-local name for the object. This parameter is required.
         */
        public static Builder create(final software.constructs.Construct scope, final String id) {
            return new Builder(scope, id);
        }

        private final software.constructs.Construct scope;
        private final String id;
        private KubeConfigMapProps.Builder props;

        private Builder(final software.constructs.Construct scope, final String id) {
            this.scope = scope;
            this.id = id;
        }

        /**
         * BinaryData contains the binary data.
         * <p>
         * Each key must consist of alphanumeric characters, '-', '_' or '.'. BinaryData can contain byte sequences that are not in the UTF-8 range. The keys stored in BinaryData must not overlap with the ones in the Data field, this is enforced during validation process. Using this field will require 1.10+ apiserver and kubelet.
         * <p>
         * @return {@code this}
         * @param binaryData BinaryData contains the binary data. This parameter is required.
         */
        public Builder binaryData(final java.util.Map<String, String> binaryData) {
            this.props().binaryData(binaryData);
            return this;
        }

        /**
         * Data contains the configuration data.
         * <p>
         * Each key must consist of alphanumeric characters, '-', '_' or '.'. Values with non-UTF-8 byte sequences must use the BinaryData field. The keys stored in Data must not overlap with the keys in the BinaryData field, this is enforced during validation process.
         * <p>
         * @return {@code this}
         * @param data Data contains the configuration data. This parameter is required.
         */
        public Builder data(final java.util.Map<String, String> data) {
            this.props().data(data);
            return this;
        }

        /**
         * Immutable, if set to true, ensures that data stored in the ConfigMap cannot be updated (only object metadata can be modified).
         * <p>
         * If not set to true, the field can be modified at any time. Defaulted to nil.
         * <p>
         * @return {@code this}
         * @param immutable Immutable, if set to true, ensures that data stored in the ConfigMap cannot be updated (only object metadata can be modified). This parameter is required.
         */
        public Builder immutable(final Boolean immutable) {
            this.props().immutable(immutable);
            return this;
        }

        /**
         * Standard object's metadata.
         * <p>
         * More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
         * <p>
         * @return {@code this}
         * @param metadata Standard object's metadata. This parameter is required.
         */
        public Builder metadata(final ObjectMeta metadata) {
            this.props().metadata(metadata);
            return this;
        }

        /**
         * @returns a newly built instance of {@link KubeConfigMap}.
         */
        @Override
        public KubeConfigMap build() {
            return new KubeConfigMap(
                this.scope,
                this.id,
                this.props != null ? this.props.build() : null
            );
        }

        private KubeConfigMapProps.Builder props() {
            if (this.props == null) {
                this.props = new KubeConfigMapProps.Builder();
            }
            return this.props;
        }
    }
}
