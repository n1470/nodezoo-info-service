package tao;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.auth.credentials.*;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ecr.EcrClient;
import software.amazon.awssdk.services.sts.StsClient;
import tao.core.AwsGateway;

public class ManifestAwsGateway implements AwsGateway {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final Region region;

    public ManifestAwsGateway(@NotNull String region) {
        this.region = Region.of(region);
    }

    @Override
    public AwsCredentialsProvider buildProvider(String profileName) {
        AwsCredentials awsCredentials;
        if (profileName != null) {
            try(final var provider = ProfileCredentialsProvider.create(profileName)) {
                awsCredentials = provider.resolveCredentials();
            }
        } else {
            try(final var provider = DefaultCredentialsProvider.create()) {
                awsCredentials = provider.resolveCredentials();
            }
        }

        return StaticCredentialsProvider.create(awsCredentials);
    }

    @Override
    public String fetchAwsAccountId(AwsCredentialsProvider credentialsProvider) {
        logger.info("Fetching AWS Account ID...");
        String awsAccountId;
        try(final var stsClient = StsClient.builder()
                .region(this.region)
                .credentialsProvider(credentialsProvider)
                .build()) {
            final var response = stsClient.getCallerIdentity();
            awsAccountId = response.account();
        }

        return awsAccountId;
    }

    @Override
    public String fetchEcrAuthToken(AwsCredentialsProvider credentialsProvider) {
        logger.info("Fetching AWS ECR password...");
        String token;
        try(final var ecrClient = EcrClient.builder()
                .region(this.region)
                .credentialsProvider(credentialsProvider)
                .build()) {
            token = ecrClient.getAuthorizationToken().authorizationData().get(0).authorizationToken();
        }

        return token;
    }
}
