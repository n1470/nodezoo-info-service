package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.461Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.ServiceEntrySpecLocation")
public enum ServiceEntrySpecLocation {
    /**
     * MESH_EXTERNAL.
     */
    MESH_EXTERNAL,
    /**
     * MESH_INTERNAL.
     */
    MESH_INTERNAL,
}
