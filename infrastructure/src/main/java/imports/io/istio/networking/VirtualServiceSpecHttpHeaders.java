package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.465Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpHeaders")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpHeaders.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpHeaders extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpHeadersRequest getRequest() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpHeadersResponse getResponse() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpHeaders}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpHeaders}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpHeaders> {
        imports.io.istio.networking.VirtualServiceSpecHttpHeadersRequest request;
        imports.io.istio.networking.VirtualServiceSpecHttpHeadersResponse response;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpHeaders#getRequest}
         * @param request the value to be set.
         * @return {@code this}
         */
        public Builder request(imports.io.istio.networking.VirtualServiceSpecHttpHeadersRequest request) {
            this.request = request;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpHeaders#getResponse}
         * @param response the value to be set.
         * @return {@code this}
         */
        public Builder response(imports.io.istio.networking.VirtualServiceSpecHttpHeadersResponse response) {
            this.response = response;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpHeaders}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpHeaders build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpHeaders}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpHeaders {
        private final imports.io.istio.networking.VirtualServiceSpecHttpHeadersRequest request;
        private final imports.io.istio.networking.VirtualServiceSpecHttpHeadersResponse response;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.request = software.amazon.jsii.Kernel.get(this, "request", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpHeadersRequest.class));
            this.response = software.amazon.jsii.Kernel.get(this, "response", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpHeadersResponse.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.request = builder.request;
            this.response = builder.response;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpHeadersRequest getRequest() {
            return this.request;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpHeadersResponse getResponse() {
            return this.response;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getRequest() != null) {
                data.set("request", om.valueToTree(this.getRequest()));
            }
            if (this.getResponse() != null) {
                data.set("response", om.valueToTree(this.getResponse()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpHeaders"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpHeaders.Jsii$Proxy that = (VirtualServiceSpecHttpHeaders.Jsii$Proxy) o;

            if (this.request != null ? !this.request.equals(that.request) : that.request != null) return false;
            return this.response != null ? this.response.equals(that.response) : that.response == null;
        }

        @Override
        public final int hashCode() {
            int result = this.request != null ? this.request.hashCode() : 0;
            result = 31 * result + (this.response != null ? this.response.hashCode() : 0);
            return result;
        }
    }
}
