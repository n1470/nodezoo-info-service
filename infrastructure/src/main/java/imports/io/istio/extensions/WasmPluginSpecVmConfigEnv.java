package imports.io.istio.extensions;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:32:56.644Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.extensions.$Module.class, fqn = "ioistioextensions.WasmPluginSpecVmConfigEnv")
@software.amazon.jsii.Jsii.Proxy(WasmPluginSpecVmConfigEnv.Jsii$Proxy.class)
public interface WasmPluginSpecVmConfigEnv extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * Value for the environment variable.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getValue() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.extensions.WasmPluginSpecVmConfigEnvValueFrom getValueFrom() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link WasmPluginSpecVmConfigEnv}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link WasmPluginSpecVmConfigEnv}
     */
    public static final class Builder implements software.amazon.jsii.Builder<WasmPluginSpecVmConfigEnv> {
        java.lang.String name;
        java.lang.String value;
        imports.io.istio.extensions.WasmPluginSpecVmConfigEnvValueFrom valueFrom;

        /**
         * Sets the value of {@link WasmPluginSpecVmConfigEnv#getName}
         * @param name the value to be set.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpecVmConfigEnv#getValue}
         * @param value Value for the environment variable.
         * @return {@code this}
         */
        public Builder value(java.lang.String value) {
            this.value = value;
            return this;
        }

        /**
         * Sets the value of {@link WasmPluginSpecVmConfigEnv#getValueFrom}
         * @param valueFrom the value to be set.
         * @return {@code this}
         */
        public Builder valueFrom(imports.io.istio.extensions.WasmPluginSpecVmConfigEnvValueFrom valueFrom) {
            this.valueFrom = valueFrom;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link WasmPluginSpecVmConfigEnv}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public WasmPluginSpecVmConfigEnv build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link WasmPluginSpecVmConfigEnv}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements WasmPluginSpecVmConfigEnv {
        private final java.lang.String name;
        private final java.lang.String value;
        private final imports.io.istio.extensions.WasmPluginSpecVmConfigEnvValueFrom valueFrom;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.value = software.amazon.jsii.Kernel.get(this, "value", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.valueFrom = software.amazon.jsii.Kernel.get(this, "valueFrom", software.amazon.jsii.NativeType.forClass(imports.io.istio.extensions.WasmPluginSpecVmConfigEnvValueFrom.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.name = builder.name;
            this.value = builder.value;
            this.valueFrom = builder.valueFrom;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final java.lang.String getValue() {
            return this.value;
        }

        @Override
        public final imports.io.istio.extensions.WasmPluginSpecVmConfigEnvValueFrom getValueFrom() {
            return this.valueFrom;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getValue() != null) {
                data.set("value", om.valueToTree(this.getValue()));
            }
            if (this.getValueFrom() != null) {
                data.set("valueFrom", om.valueToTree(this.getValueFrom()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistioextensions.WasmPluginSpecVmConfigEnv"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            WasmPluginSpecVmConfigEnv.Jsii$Proxy that = (WasmPluginSpecVmConfigEnv.Jsii$Proxy) o;

            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            if (this.value != null ? !this.value.equals(that.value) : that.value != null) return false;
            return this.valueFrom != null ? this.valueFrom.equals(that.valueFrom) : that.valueFrom == null;
        }

        @Override
        public final int hashCode() {
            int result = this.name != null ? this.name.hashCode() : 0;
            result = 31 * result + (this.value != null ? this.value.hashCode() : 0);
            result = 31 * result + (this.valueFrom != null ? this.valueFrom.hashCode() : 0);
            return result;
        }
    }
}
