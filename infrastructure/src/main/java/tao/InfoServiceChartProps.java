package tao;

import org.apache.commons.cli.*;
import org.jetbrains.annotations.NotNull;
import tao.core.AwsGateway;
import tao.manifest.ImagePullSecretManifest;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.Objects;

public class InfoServiceChartProps {
    static final int DEFAULT_REPLICAS = 1;

    static final String DEFAULT_AWS_REGION = "us-east-1";
    static final String[] DEFAULT_PATH_FRAGMENTS = new String[] { "dist", "nodezoo" };

    private final ImagePullSecretManifest.Props secretManifestProps;
    private final String imageTag;

    private final boolean isEks;

    public Path getOutputPath() {
        return outputPath;
    }

    public int getReplicas() {
        return replicas;
    }

    public ImagePullSecretManifest.Props getSecretManifestProps() {
        return secretManifestProps;
    }

    public String getImageTag() {
        return imageTag;
    }

    public boolean isEks() {
        return isEks;
    }

    public static Builder builder(@NotNull String[] args) {
        return new Builder(args);
    }

    public static class Builder {

        private final String[] args;
        private AwsGateway infraAwsGateway;

        public Builder(String[] args) {
            this.args = args;
        }

        public Builder withInfraAwsGateway(AwsGateway infraAwsGateway) {
            this.infraAwsGateway = infraAwsGateway;
            return this;
        }

        public InfoServiceChartProps build() {
            try {
                final var cmd = getCommandLine();
                final var outdirPath = parseOutputPath(cmd);
                final var replicas =  parseReplicas(cmd);
                final var isEksDeployment = cmd.hasOption("eks");
                final var secretManifestProps = cmd.hasOption('s') ?
                        buildSecretManifestProps(cmd) :
                        null;
                final var imageTag = cmd.getArgs()[0];
                if (secretManifestProps != null) {
                    return new InfoServiceChartProps(replicas,
                            secretManifestProps,
                            outdirPath,
                            imageTag,
                            isEksDeployment);
                }

                return new InfoServiceChartProps(replicas, outdirPath, imageTag, isEksDeployment);
            } catch (ParseException ex) {
                throw new RuntimeException(ex);
            }
        }

        private CommandLine getCommandLine() throws ParseException {
            final var options = new Options();
            options.addOption(
                    "o",
                    "output",
                    true,
                    "Output directory, default: " + String.join("/", DEFAULT_PATH_FRAGMENTS));
            options.addOption(
                    "c",
                    "count",
                    true,
                    "Count of replicas, default: " + DEFAULT_REPLICAS);
            options.addOption(
                    "p",
                    "aws-profile",
                    true,
                    "AWS profile to use, overrides environment variables"
            );
            options.addOption(
                    "l",
                    "aws-region",
                    true,
                    "AWS region code"
            );
            options.addOption(
                    "s",
                    "pull-image-secret",
                    false,
                    "Pull the ECR secret"
            );
            options.addOption(
                    "k",
                    "eks",
                    false,
                    "Generate k8s service manifests to deploy to EKS"
            );

            final CommandLineParser parser = new DefaultParser();
            return parser.parse(options, args);
        }

        private Path parseOutputPath(CommandLine cmd) {
            String[] pathFragments;
            if (cmd.hasOption('o')) {
                pathFragments = cmd.getOptionValue('o').split("/");
            } else {
                pathFragments = DEFAULT_PATH_FRAGMENTS;
            }

            return Path.of(pathFragments[0], Arrays.copyOfRange(pathFragments, 1, pathFragments.length));
        }

        private int parseReplicas(CommandLine cmd) {
            if (cmd.hasOption('c')) {
                final var optVal = cmd.getOptionValue('c');
                return Integer.parseInt(optVal);
            }

            return DEFAULT_REPLICAS;
        }

        private ImagePullSecretManifest.Props buildSecretManifestProps(CommandLine commandLine) {
            final var awsRegion = commandLine.hasOption("aws-region") ?
                    commandLine.getOptionValue("aws-region") :
                    DEFAULT_AWS_REGION;
            final var awsProfile = commandLine.getOptionValue("aws-profile");
            final var awsGw = Objects
                    .requireNonNullElseGet(this.infraAwsGateway, () -> new ManifestAwsGateway(awsRegion));

            final var credentialsProvider = awsGw.buildProvider(awsProfile);
            final String awsAccountId = awsGw.fetchAwsAccountId(credentialsProvider);
            final String ecrAuthToken = awsGw.fetchEcrAuthToken(credentialsProvider);

            return new ImagePullSecretManifest.Props(awsAccountId, awsRegion, ecrAuthToken);
        }
    }

    private final Path outputPath;
    private final int replicas;

    public InfoServiceChartProps(int replicas,
                                 @NotNull ImagePullSecretManifest.Props secretManifestProps,
                                 Path outputPath,
                                 @NotNull String imageTag,
                                 boolean isEks) {
        this.replicas = replicas;
        this.secretManifestProps = secretManifestProps;
        this.outputPath = outputPath;
        this.imageTag = imageTag;
        this.isEks = isEks;
    }

    public InfoServiceChartProps(int replicas,
                                 Path outputPath,
                                 @NotNull String imageTag,
                                 boolean isEks) {
        this.replicas = replicas;
        this.secretManifestProps = null;
        this.outputPath = outputPath;
        this.imageTag = imageTag;
        this.isEks = isEks;
    }
}
