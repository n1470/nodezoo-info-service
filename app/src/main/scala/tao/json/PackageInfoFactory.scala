package tao.json

import tao.core.PackageInfo

import scala.jdk.CollectionConverters._
import java.io.StringReader
import javax.json.{Json, JsonObjectBuilder, JsonValue}
import scala.collection.mutable

object PackageInfoFactory {

  def apply(responses: Seq[PackageInfo]): PackageInfo = {
    val mergedResponse = combineToMap(responses)
    val mergedResponseJson = combineToJson(mergedResponse).build()
    PackageInfo(content = mergedResponseJson.toString, sourceLabel = "")
  }

  private def combineToMap(responses: Seq[PackageInfo]): mutable.Map[String, mutable.Map[String, JsonValue]] = {
    val mergedResponse = mutable.Map[String, mutable.Map[String, JsonValue]]()
    for (response <- responses) {
      val contentJson = Json.createReader(new StringReader(response.content)).readObject()
      for (entry <- contentJson.entrySet().asScala) {
        var mergedValue: mutable.Map[String, JsonValue] = null
        if (mergedResponse.contains(entry.getKey)) {
          mergedValue = mergedResponse(entry.getKey)
          mergedValue += (response.sourceLabel -> entry.getValue)
        } else {
          val entryValue = mutable.Map[String, JsonValue]()
          entryValue += (response.sourceLabel -> entry.getValue)
          mergedResponse += (entry.getKey -> entryValue)
        }
      }
    }
    mergedResponse
  }

  private def combineToJson(mergedResponse: mutable.Map[String, mutable.Map[String, JsonValue]]): JsonObjectBuilder = {
    val mergedResponseJson = Json.createObjectBuilder()
    for (key <- mergedResponse.keys) {
      if (mergedResponse(key).size > 1) {
        val mergedValueJson = Json.createObjectBuilder()
        for ((source, value) <- mergedResponse(key)) {
          mergedValueJson.add(source, value)
        }
        mergedResponseJson.add(key, mergedValueJson)

      } else {
        mergedResponseJson.add(key, mergedResponse(key).values.head)
      }
    }
    mergedResponseJson
  }
}
