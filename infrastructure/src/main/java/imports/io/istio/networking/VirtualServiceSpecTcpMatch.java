package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.468Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecTcpMatch")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecTcpMatch.Jsii$Proxy.class)
public interface VirtualServiceSpecTcpMatch extends software.amazon.jsii.JsiiSerializable {

    /**
     * IPv4 or IPv6 ip addresses of destination with optional subnet.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getDestinationSubnets() {
        return null;
    }

    /**
     * Names of gateways where the rule should be applied.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getGateways() {
        return null;
    }

    /**
     * Specifies the port on the host that is being addressed.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPort() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getSourceLabels() {
        return null;
    }

    /**
     * Source namespace constraining the applicability of a rule to workloads in that namespace.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getSourceNamespace() {
        return null;
    }

    /**
     * IPv4 or IPv6 ip address of source with optional subnet.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getSourceSubnet() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecTcpMatch}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecTcpMatch}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecTcpMatch> {
        java.util.List<java.lang.String> destinationSubnets;
        java.util.List<java.lang.String> gateways;
        java.lang.Number port;
        java.util.Map<java.lang.String, java.lang.String> sourceLabels;
        java.lang.String sourceNamespace;
        java.lang.String sourceSubnet;

        /**
         * Sets the value of {@link VirtualServiceSpecTcpMatch#getDestinationSubnets}
         * @param destinationSubnets IPv4 or IPv6 ip addresses of destination with optional subnet.
         * @return {@code this}
         */
        public Builder destinationSubnets(java.util.List<java.lang.String> destinationSubnets) {
            this.destinationSubnets = destinationSubnets;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTcpMatch#getGateways}
         * @param gateways Names of gateways where the rule should be applied.
         * @return {@code this}
         */
        public Builder gateways(java.util.List<java.lang.String> gateways) {
            this.gateways = gateways;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTcpMatch#getPort}
         * @param port Specifies the port on the host that is being addressed.
         * @return {@code this}
         */
        public Builder port(java.lang.Number port) {
            this.port = port;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTcpMatch#getSourceLabels}
         * @param sourceLabels the value to be set.
         * @return {@code this}
         */
        public Builder sourceLabels(java.util.Map<java.lang.String, java.lang.String> sourceLabels) {
            this.sourceLabels = sourceLabels;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTcpMatch#getSourceNamespace}
         * @param sourceNamespace Source namespace constraining the applicability of a rule to workloads in that namespace.
         * @return {@code this}
         */
        public Builder sourceNamespace(java.lang.String sourceNamespace) {
            this.sourceNamespace = sourceNamespace;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTcpMatch#getSourceSubnet}
         * @param sourceSubnet IPv4 or IPv6 ip address of source with optional subnet.
         * @return {@code this}
         */
        public Builder sourceSubnet(java.lang.String sourceSubnet) {
            this.sourceSubnet = sourceSubnet;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecTcpMatch}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecTcpMatch build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecTcpMatch}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecTcpMatch {
        private final java.util.List<java.lang.String> destinationSubnets;
        private final java.util.List<java.lang.String> gateways;
        private final java.lang.Number port;
        private final java.util.Map<java.lang.String, java.lang.String> sourceLabels;
        private final java.lang.String sourceNamespace;
        private final java.lang.String sourceSubnet;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.destinationSubnets = software.amazon.jsii.Kernel.get(this, "destinationSubnets", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.gateways = software.amazon.jsii.Kernel.get(this, "gateways", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.sourceLabels = software.amazon.jsii.Kernel.get(this, "sourceLabels", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.sourceNamespace = software.amazon.jsii.Kernel.get(this, "sourceNamespace", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.sourceSubnet = software.amazon.jsii.Kernel.get(this, "sourceSubnet", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.destinationSubnets = builder.destinationSubnets;
            this.gateways = builder.gateways;
            this.port = builder.port;
            this.sourceLabels = builder.sourceLabels;
            this.sourceNamespace = builder.sourceNamespace;
            this.sourceSubnet = builder.sourceSubnet;
        }

        @Override
        public final java.util.List<java.lang.String> getDestinationSubnets() {
            return this.destinationSubnets;
        }

        @Override
        public final java.util.List<java.lang.String> getGateways() {
            return this.gateways;
        }

        @Override
        public final java.lang.Number getPort() {
            return this.port;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getSourceLabels() {
            return this.sourceLabels;
        }

        @Override
        public final java.lang.String getSourceNamespace() {
            return this.sourceNamespace;
        }

        @Override
        public final java.lang.String getSourceSubnet() {
            return this.sourceSubnet;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getDestinationSubnets() != null) {
                data.set("destinationSubnets", om.valueToTree(this.getDestinationSubnets()));
            }
            if (this.getGateways() != null) {
                data.set("gateways", om.valueToTree(this.getGateways()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }
            if (this.getSourceLabels() != null) {
                data.set("sourceLabels", om.valueToTree(this.getSourceLabels()));
            }
            if (this.getSourceNamespace() != null) {
                data.set("sourceNamespace", om.valueToTree(this.getSourceNamespace()));
            }
            if (this.getSourceSubnet() != null) {
                data.set("sourceSubnet", om.valueToTree(this.getSourceSubnet()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecTcpMatch"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecTcpMatch.Jsii$Proxy that = (VirtualServiceSpecTcpMatch.Jsii$Proxy) o;

            if (this.destinationSubnets != null ? !this.destinationSubnets.equals(that.destinationSubnets) : that.destinationSubnets != null) return false;
            if (this.gateways != null ? !this.gateways.equals(that.gateways) : that.gateways != null) return false;
            if (this.port != null ? !this.port.equals(that.port) : that.port != null) return false;
            if (this.sourceLabels != null ? !this.sourceLabels.equals(that.sourceLabels) : that.sourceLabels != null) return false;
            if (this.sourceNamespace != null ? !this.sourceNamespace.equals(that.sourceNamespace) : that.sourceNamespace != null) return false;
            return this.sourceSubnet != null ? this.sourceSubnet.equals(that.sourceSubnet) : that.sourceSubnet == null;
        }

        @Override
        public final int hashCode() {
            int result = this.destinationSubnets != null ? this.destinationSubnets.hashCode() : 0;
            result = 31 * result + (this.gateways != null ? this.gateways.hashCode() : 0);
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            result = 31 * result + (this.sourceLabels != null ? this.sourceLabels.hashCode() : 0);
            result = 31 * result + (this.sourceNamespace != null ? this.sourceNamespace.hashCode() : 0);
            result = 31 * result + (this.sourceSubnet != null ? this.sourceSubnet.hashCode() : 0);
            return result;
        }
    }
}
