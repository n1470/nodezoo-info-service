package imports.io.istio.telemetry;

/**
 * Operation controls whether or not to update/add a tag, or to remove it.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.288Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecMetricsOverridesTagOverridesOperation")
public enum TelemetrySpecMetricsOverridesTagOverridesOperation {
    /**
     * UPSERT.
     */
    UPSERT,
    /**
     * REMOVE.
     */
    REMOVE,
}
