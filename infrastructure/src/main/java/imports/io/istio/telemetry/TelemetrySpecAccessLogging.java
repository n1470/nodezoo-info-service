package imports.io.istio.telemetry;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.286Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecAccessLogging")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpecAccessLogging.Jsii$Proxy.class)
public interface TelemetrySpecAccessLogging extends software.amazon.jsii.JsiiSerializable {

    /**
     * Controls logging.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getDisabled() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecAccessLoggingFilter getFilter() {
        return null;
    }

    /**
     * Allows tailoring of logging behavior to specific conditions.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecAccessLoggingMatch getMatch() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLoggingProviders> getProviders() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpecAccessLogging}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpecAccessLogging}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpecAccessLogging> {
        java.lang.Boolean disabled;
        imports.io.istio.telemetry.TelemetrySpecAccessLoggingFilter filter;
        imports.io.istio.telemetry.TelemetrySpecAccessLoggingMatch match;
        java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLoggingProviders> providers;

        /**
         * Sets the value of {@link TelemetrySpecAccessLogging#getDisabled}
         * @param disabled Controls logging.
         * @return {@code this}
         */
        public Builder disabled(java.lang.Boolean disabled) {
            this.disabled = disabled;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecAccessLogging#getFilter}
         * @param filter Optional.
         * @return {@code this}
         */
        public Builder filter(imports.io.istio.telemetry.TelemetrySpecAccessLoggingFilter filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecAccessLogging#getMatch}
         * @param match Allows tailoring of logging behavior to specific conditions.
         * @return {@code this}
         */
        public Builder match(imports.io.istio.telemetry.TelemetrySpecAccessLoggingMatch match) {
            this.match = match;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecAccessLogging#getProviders}
         * @param providers Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder providers(java.util.List<? extends imports.io.istio.telemetry.TelemetrySpecAccessLoggingProviders> providers) {
            this.providers = (java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLoggingProviders>)providers;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpecAccessLogging}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpecAccessLogging build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpecAccessLogging}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpecAccessLogging {
        private final java.lang.Boolean disabled;
        private final imports.io.istio.telemetry.TelemetrySpecAccessLoggingFilter filter;
        private final imports.io.istio.telemetry.TelemetrySpecAccessLoggingMatch match;
        private final java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLoggingProviders> providers;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.disabled = software.amazon.jsii.Kernel.get(this, "disabled", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
            this.filter = software.amazon.jsii.Kernel.get(this, "filter", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecAccessLoggingFilter.class));
            this.match = software.amazon.jsii.Kernel.get(this, "match", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecAccessLoggingMatch.class));
            this.providers = software.amazon.jsii.Kernel.get(this, "providers", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecAccessLoggingProviders.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.disabled = builder.disabled;
            this.filter = builder.filter;
            this.match = builder.match;
            this.providers = (java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLoggingProviders>)builder.providers;
        }

        @Override
        public final java.lang.Boolean getDisabled() {
            return this.disabled;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecAccessLoggingFilter getFilter() {
            return this.filter;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecAccessLoggingMatch getMatch() {
            return this.match;
        }

        @Override
        public final java.util.List<imports.io.istio.telemetry.TelemetrySpecAccessLoggingProviders> getProviders() {
            return this.providers;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getDisabled() != null) {
                data.set("disabled", om.valueToTree(this.getDisabled()));
            }
            if (this.getFilter() != null) {
                data.set("filter", om.valueToTree(this.getFilter()));
            }
            if (this.getMatch() != null) {
                data.set("match", om.valueToTree(this.getMatch()));
            }
            if (this.getProviders() != null) {
                data.set("providers", om.valueToTree(this.getProviders()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpecAccessLogging"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpecAccessLogging.Jsii$Proxy that = (TelemetrySpecAccessLogging.Jsii$Proxy) o;

            if (this.disabled != null ? !this.disabled.equals(that.disabled) : that.disabled != null) return false;
            if (this.filter != null ? !this.filter.equals(that.filter) : that.filter != null) return false;
            if (this.match != null ? !this.match.equals(that.match) : that.match != null) return false;
            return this.providers != null ? this.providers.equals(that.providers) : that.providers == null;
        }

        @Override
        public final int hashCode() {
            int result = this.disabled != null ? this.disabled.hashCode() : 0;
            result = 31 * result + (this.filter != null ? this.filter.hashCode() : 0);
            result = 31 * result + (this.match != null ? this.match.hashCode() : 0);
            result = 31 * result + (this.providers != null ? this.providers.hashCode() : 0);
            return result;
        }
    }
}
