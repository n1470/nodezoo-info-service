package imports.io.istio.security;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.457Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.AuthorizationPolicySpecRulesTo")
@software.amazon.jsii.Jsii.Proxy(AuthorizationPolicySpecRulesTo.Jsii$Proxy.class)
public interface AuthorizationPolicySpecRulesTo extends software.amazon.jsii.JsiiSerializable {

    /**
     * Operation specifies the operation of a request.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.AuthorizationPolicySpecRulesToOperation getOperation() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link AuthorizationPolicySpecRulesTo}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link AuthorizationPolicySpecRulesTo}
     */
    public static final class Builder implements software.amazon.jsii.Builder<AuthorizationPolicySpecRulesTo> {
        imports.io.istio.security.AuthorizationPolicySpecRulesToOperation operation;

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesTo#getOperation}
         * @param operation Operation specifies the operation of a request.
         * @return {@code this}
         */
        public Builder operation(imports.io.istio.security.AuthorizationPolicySpecRulesToOperation operation) {
            this.operation = operation;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link AuthorizationPolicySpecRulesTo}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public AuthorizationPolicySpecRulesTo build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link AuthorizationPolicySpecRulesTo}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements AuthorizationPolicySpecRulesTo {
        private final imports.io.istio.security.AuthorizationPolicySpecRulesToOperation operation;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.operation = software.amazon.jsii.Kernel.get(this, "operation", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpecRulesToOperation.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.operation = builder.operation;
        }

        @Override
        public final imports.io.istio.security.AuthorizationPolicySpecRulesToOperation getOperation() {
            return this.operation;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getOperation() != null) {
                data.set("operation", om.valueToTree(this.getOperation()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.AuthorizationPolicySpecRulesTo"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AuthorizationPolicySpecRulesTo.Jsii$Proxy that = (AuthorizationPolicySpecRulesTo.Jsii$Proxy) o;

            return this.operation != null ? this.operation.equals(that.operation) : that.operation == null;
        }

        @Override
        public final int hashCode() {
            int result = this.operation != null ? this.operation.hashCode() : 0;
            return result;
        }
    }
}
