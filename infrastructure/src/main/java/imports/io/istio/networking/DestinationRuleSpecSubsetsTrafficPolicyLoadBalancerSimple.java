package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.431Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerSimple")
public enum DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerSimple {
    /**
     * UNSPECIFIED.
     */
    UNSPECIFIED,
    /**
     * LEAST_CONN.
     */
    LEAST_CONN,
    /**
     * RANDOM.
     */
    RANDOM,
    /**
     * PASSTHROUGH.
     */
    PASSTHROUGH,
    /**
     * ROUND_ROBIN.
     */
    ROUND_ROBIN,
    /**
     * LEAST_REQUEST.
     */
    LEAST_REQUEST,
}
