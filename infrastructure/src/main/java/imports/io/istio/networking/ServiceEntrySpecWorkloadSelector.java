package imports.io.istio.networking;

/**
 * Applicable only for MESH_INTERNAL services.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.461Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.ServiceEntrySpecWorkloadSelector")
@software.amazon.jsii.Jsii.Proxy(ServiceEntrySpecWorkloadSelector.Jsii$Proxy.class)
public interface ServiceEntrySpecWorkloadSelector extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getLabels() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link ServiceEntrySpecWorkloadSelector}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link ServiceEntrySpecWorkloadSelector}
     */
    public static final class Builder implements software.amazon.jsii.Builder<ServiceEntrySpecWorkloadSelector> {
        java.util.Map<java.lang.String, java.lang.String> labels;

        /**
         * Sets the value of {@link ServiceEntrySpecWorkloadSelector#getLabels}
         * @param labels the value to be set.
         * @return {@code this}
         */
        public Builder labels(java.util.Map<java.lang.String, java.lang.String> labels) {
            this.labels = labels;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link ServiceEntrySpecWorkloadSelector}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public ServiceEntrySpecWorkloadSelector build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link ServiceEntrySpecWorkloadSelector}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements ServiceEntrySpecWorkloadSelector {
        private final java.util.Map<java.lang.String, java.lang.String> labels;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.labels = software.amazon.jsii.Kernel.get(this, "labels", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.labels = builder.labels;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getLabels() {
            return this.labels;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getLabels() != null) {
                data.set("labels", om.valueToTree(this.getLabels()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.ServiceEntrySpecWorkloadSelector"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ServiceEntrySpecWorkloadSelector.Jsii$Proxy that = (ServiceEntrySpecWorkloadSelector.Jsii$Proxy) o;

            return this.labels != null ? this.labels.equals(that.labels) : that.labels == null;
        }

        @Override
        public final int hashCode() {
            int result = this.labels != null ? this.labels.hashCode() : 0;
            return result;
        }
    }
}
