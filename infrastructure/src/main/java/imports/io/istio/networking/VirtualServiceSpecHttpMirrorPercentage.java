package imports.io.istio.networking;

/**
 * Percentage of the traffic to be mirrored by the `mirror` field.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.467Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpMirrorPercentage")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpMirrorPercentage.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpMirrorPercentage extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getValue() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpMirrorPercentage}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpMirrorPercentage}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpMirrorPercentage> {
        java.lang.Number value;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMirrorPercentage#getValue}
         * @param value the value to be set.
         * @return {@code this}
         */
        public Builder value(java.lang.Number value) {
            this.value = value;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpMirrorPercentage}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpMirrorPercentage build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpMirrorPercentage}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpMirrorPercentage {
        private final java.lang.Number value;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.value = software.amazon.jsii.Kernel.get(this, "value", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.value = builder.value;
        }

        @Override
        public final java.lang.Number getValue() {
            return this.value;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getValue() != null) {
                data.set("value", om.valueToTree(this.getValue()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpMirrorPercentage"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpMirrorPercentage.Jsii$Proxy that = (VirtualServiceSpecHttpMirrorPercentage.Jsii$Proxy) o;

            return this.value != null ? this.value.equals(that.value) : that.value == null;
        }

        @Override
        public final int hashCode() {
            int result = this.value != null ? this.value.hashCode() : 0;
            return result;
        }
    }
}
