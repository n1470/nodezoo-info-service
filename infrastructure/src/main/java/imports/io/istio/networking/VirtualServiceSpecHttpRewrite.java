package imports.io.istio.networking;

/**
 * Rewrite HTTP URIs and Authority headers.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.467Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpRewrite")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpRewrite.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpRewrite extends software.amazon.jsii.JsiiSerializable {

    /**
     * rewrite the Authority/Host header with this value.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getAuthority() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getUri() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpRewrite}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpRewrite}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpRewrite> {
        java.lang.String authority;
        java.lang.String uri;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRewrite#getAuthority}
         * @param authority rewrite the Authority/Host header with this value.
         * @return {@code this}
         */
        public Builder authority(java.lang.String authority) {
            this.authority = authority;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRewrite#getUri}
         * @param uri the value to be set.
         * @return {@code this}
         */
        public Builder uri(java.lang.String uri) {
            this.uri = uri;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpRewrite}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpRewrite build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpRewrite}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpRewrite {
        private final java.lang.String authority;
        private final java.lang.String uri;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.authority = software.amazon.jsii.Kernel.get(this, "authority", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.uri = software.amazon.jsii.Kernel.get(this, "uri", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.authority = builder.authority;
            this.uri = builder.uri;
        }

        @Override
        public final java.lang.String getAuthority() {
            return this.authority;
        }

        @Override
        public final java.lang.String getUri() {
            return this.uri;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAuthority() != null) {
                data.set("authority", om.valueToTree(this.getAuthority()));
            }
            if (this.getUri() != null) {
                data.set("uri", om.valueToTree(this.getUri()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpRewrite"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpRewrite.Jsii$Proxy that = (VirtualServiceSpecHttpRewrite.Jsii$Proxy) o;

            if (this.authority != null ? !this.authority.equals(that.authority) : that.authority != null) return false;
            return this.uri != null ? this.uri.equals(that.uri) : that.uri == null;
        }

        @Override
        public final int hashCode() {
            int result = this.authority != null ? this.authority.hashCode() : 0;
            result = 31 * result + (this.uri != null ? this.uri.hashCode() : 0);
            return result;
        }
    }
}
