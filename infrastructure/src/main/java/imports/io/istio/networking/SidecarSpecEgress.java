package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.462Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.SidecarSpecEgress")
@software.amazon.jsii.Jsii.Proxy(SidecarSpecEgress.Jsii$Proxy.class)
public interface SidecarSpecEgress extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getBind() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecEgressCaptureMode getCaptureMode() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getHosts() {
        return null;
    }

    /**
     * The port associated with the listener.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecEgressPort getPort() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link SidecarSpecEgress}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link SidecarSpecEgress}
     */
    public static final class Builder implements software.amazon.jsii.Builder<SidecarSpecEgress> {
        java.lang.String bind;
        imports.io.istio.networking.SidecarSpecEgressCaptureMode captureMode;
        java.util.List<java.lang.String> hosts;
        imports.io.istio.networking.SidecarSpecEgressPort port;

        /**
         * Sets the value of {@link SidecarSpecEgress#getBind}
         * @param bind the value to be set.
         * @return {@code this}
         */
        public Builder bind(java.lang.String bind) {
            this.bind = bind;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecEgress#getCaptureMode}
         * @param captureMode the value to be set.
         * @return {@code this}
         */
        public Builder captureMode(imports.io.istio.networking.SidecarSpecEgressCaptureMode captureMode) {
            this.captureMode = captureMode;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecEgress#getHosts}
         * @param hosts the value to be set.
         * @return {@code this}
         */
        public Builder hosts(java.util.List<java.lang.String> hosts) {
            this.hosts = hosts;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecEgress#getPort}
         * @param port The port associated with the listener.
         * @return {@code this}
         */
        public Builder port(imports.io.istio.networking.SidecarSpecEgressPort port) {
            this.port = port;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link SidecarSpecEgress}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public SidecarSpecEgress build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link SidecarSpecEgress}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements SidecarSpecEgress {
        private final java.lang.String bind;
        private final imports.io.istio.networking.SidecarSpecEgressCaptureMode captureMode;
        private final java.util.List<java.lang.String> hosts;
        private final imports.io.istio.networking.SidecarSpecEgressPort port;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.bind = software.amazon.jsii.Kernel.get(this, "bind", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.captureMode = software.amazon.jsii.Kernel.get(this, "captureMode", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecEgressCaptureMode.class));
            this.hosts = software.amazon.jsii.Kernel.get(this, "hosts", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecEgressPort.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.bind = builder.bind;
            this.captureMode = builder.captureMode;
            this.hosts = builder.hosts;
            this.port = builder.port;
        }

        @Override
        public final java.lang.String getBind() {
            return this.bind;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecEgressCaptureMode getCaptureMode() {
            return this.captureMode;
        }

        @Override
        public final java.util.List<java.lang.String> getHosts() {
            return this.hosts;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecEgressPort getPort() {
            return this.port;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getBind() != null) {
                data.set("bind", om.valueToTree(this.getBind()));
            }
            if (this.getCaptureMode() != null) {
                data.set("captureMode", om.valueToTree(this.getCaptureMode()));
            }
            if (this.getHosts() != null) {
                data.set("hosts", om.valueToTree(this.getHosts()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.SidecarSpecEgress"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SidecarSpecEgress.Jsii$Proxy that = (SidecarSpecEgress.Jsii$Proxy) o;

            if (this.bind != null ? !this.bind.equals(that.bind) : that.bind != null) return false;
            if (this.captureMode != null ? !this.captureMode.equals(that.captureMode) : that.captureMode != null) return false;
            if (this.hosts != null ? !this.hosts.equals(that.hosts) : that.hosts != null) return false;
            return this.port != null ? this.port.equals(that.port) : that.port == null;
        }

        @Override
        public final int hashCode() {
            int result = this.bind != null ? this.bind.hashCode() : 0;
            result = 31 * result + (this.captureMode != null ? this.captureMode.hashCode() : 0);
            result = 31 * result + (this.hosts != null ? this.hosts.hashCode() : 0);
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            return result;
        }
    }
}
