package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.470Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.WorkloadGroupSpecProbeHttpGet")
@software.amazon.jsii.Jsii.Proxy(WorkloadGroupSpecProbeHttpGet.Jsii$Proxy.class)
public interface WorkloadGroupSpecProbeHttpGet extends software.amazon.jsii.JsiiSerializable {

    /**
     * Host name to connect to, defaults to the pod IP.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getHost() {
        return null;
    }

    /**
     * Headers the proxy will pass on to make the request.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.WorkloadGroupSpecProbeHttpGetHttpHeaders> getHttpHeaders() {
        return null;
    }

    /**
     * Path to access on the HTTP server.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getPath() {
        return null;
    }

    /**
     * Port on which the endpoint lives.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPort() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getScheme() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link WorkloadGroupSpecProbeHttpGet}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link WorkloadGroupSpecProbeHttpGet}
     */
    public static final class Builder implements software.amazon.jsii.Builder<WorkloadGroupSpecProbeHttpGet> {
        java.lang.String host;
        java.util.List<imports.io.istio.networking.WorkloadGroupSpecProbeHttpGetHttpHeaders> httpHeaders;
        java.lang.String path;
        java.lang.Number port;
        java.lang.String scheme;

        /**
         * Sets the value of {@link WorkloadGroupSpecProbeHttpGet#getHost}
         * @param host Host name to connect to, defaults to the pod IP.
         * @return {@code this}
         */
        public Builder host(java.lang.String host) {
            this.host = host;
            return this;
        }

        /**
         * Sets the value of {@link WorkloadGroupSpecProbeHttpGet#getHttpHeaders}
         * @param httpHeaders Headers the proxy will pass on to make the request.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder httpHeaders(java.util.List<? extends imports.io.istio.networking.WorkloadGroupSpecProbeHttpGetHttpHeaders> httpHeaders) {
            this.httpHeaders = (java.util.List<imports.io.istio.networking.WorkloadGroupSpecProbeHttpGetHttpHeaders>)httpHeaders;
            return this;
        }

        /**
         * Sets the value of {@link WorkloadGroupSpecProbeHttpGet#getPath}
         * @param path Path to access on the HTTP server.
         * @return {@code this}
         */
        public Builder path(java.lang.String path) {
            this.path = path;
            return this;
        }

        /**
         * Sets the value of {@link WorkloadGroupSpecProbeHttpGet#getPort}
         * @param port Port on which the endpoint lives.
         * @return {@code this}
         */
        public Builder port(java.lang.Number port) {
            this.port = port;
            return this;
        }

        /**
         * Sets the value of {@link WorkloadGroupSpecProbeHttpGet#getScheme}
         * @param scheme the value to be set.
         * @return {@code this}
         */
        public Builder scheme(java.lang.String scheme) {
            this.scheme = scheme;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link WorkloadGroupSpecProbeHttpGet}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public WorkloadGroupSpecProbeHttpGet build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link WorkloadGroupSpecProbeHttpGet}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements WorkloadGroupSpecProbeHttpGet {
        private final java.lang.String host;
        private final java.util.List<imports.io.istio.networking.WorkloadGroupSpecProbeHttpGetHttpHeaders> httpHeaders;
        private final java.lang.String path;
        private final java.lang.Number port;
        private final java.lang.String scheme;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.host = software.amazon.jsii.Kernel.get(this, "host", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.httpHeaders = software.amazon.jsii.Kernel.get(this, "httpHeaders", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.WorkloadGroupSpecProbeHttpGetHttpHeaders.class)));
            this.path = software.amazon.jsii.Kernel.get(this, "path", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.scheme = software.amazon.jsii.Kernel.get(this, "scheme", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.host = builder.host;
            this.httpHeaders = (java.util.List<imports.io.istio.networking.WorkloadGroupSpecProbeHttpGetHttpHeaders>)builder.httpHeaders;
            this.path = builder.path;
            this.port = builder.port;
            this.scheme = builder.scheme;
        }

        @Override
        public final java.lang.String getHost() {
            return this.host;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.WorkloadGroupSpecProbeHttpGetHttpHeaders> getHttpHeaders() {
            return this.httpHeaders;
        }

        @Override
        public final java.lang.String getPath() {
            return this.path;
        }

        @Override
        public final java.lang.Number getPort() {
            return this.port;
        }

        @Override
        public final java.lang.String getScheme() {
            return this.scheme;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getHost() != null) {
                data.set("host", om.valueToTree(this.getHost()));
            }
            if (this.getHttpHeaders() != null) {
                data.set("httpHeaders", om.valueToTree(this.getHttpHeaders()));
            }
            if (this.getPath() != null) {
                data.set("path", om.valueToTree(this.getPath()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }
            if (this.getScheme() != null) {
                data.set("scheme", om.valueToTree(this.getScheme()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.WorkloadGroupSpecProbeHttpGet"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            WorkloadGroupSpecProbeHttpGet.Jsii$Proxy that = (WorkloadGroupSpecProbeHttpGet.Jsii$Proxy) o;

            if (this.host != null ? !this.host.equals(that.host) : that.host != null) return false;
            if (this.httpHeaders != null ? !this.httpHeaders.equals(that.httpHeaders) : that.httpHeaders != null) return false;
            if (this.path != null ? !this.path.equals(that.path) : that.path != null) return false;
            if (this.port != null ? !this.port.equals(that.port) : that.port != null) return false;
            return this.scheme != null ? this.scheme.equals(that.scheme) : that.scheme == null;
        }

        @Override
        public final int hashCode() {
            int result = this.host != null ? this.host.hashCode() : 0;
            result = 31 * result + (this.httpHeaders != null ? this.httpHeaders.hashCode() : 0);
            result = 31 * result + (this.path != null ? this.path.hashCode() : 0);
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            result = 31 * result + (this.scheme != null ? this.scheme.hashCode() : 0);
            return result;
        }
    }
}
