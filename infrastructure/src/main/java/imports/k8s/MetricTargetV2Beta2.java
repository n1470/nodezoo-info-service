package imports.k8s;

/**
 * MetricTarget defines the target value, average value, or average utilization of a specific metric.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.316Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.MetricTargetV2Beta2")
@software.amazon.jsii.Jsii.Proxy(MetricTargetV2Beta2.Jsii$Proxy.class)
public interface MetricTargetV2Beta2 extends software.amazon.jsii.JsiiSerializable {

    /**
     * type represents whether the metric type is Utilization, Value, or AverageValue.
     */
    @org.jetbrains.annotations.NotNull String getType();

    /**
     * averageUtilization is the target value of the average of the resource metric across all relevant pods, represented as a percentage of the requested value of the resource for the pods.
     * <p>
     * Currently only valid for Resource metric source type
     */
    default @org.jetbrains.annotations.Nullable Number getAverageUtilization() {
        return null;
    }

    /**
     * averageValue is the target value of the average of the metric across all relevant pods (as a quantity).
     */
    default @org.jetbrains.annotations.Nullable Quantity getAverageValue() {
        return null;
    }

    /**
     * value is the target value of the metric (as a quantity).
     */
    default @org.jetbrains.annotations.Nullable Quantity getValue() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link MetricTargetV2Beta2}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link MetricTargetV2Beta2}
     */
    public static final class Builder implements software.amazon.jsii.Builder<MetricTargetV2Beta2> {
        String type;
        Number averageUtilization;
        Quantity averageValue;
        Quantity value;

        /**
         * Sets the value of {@link MetricTargetV2Beta2#getType}
         * @param type type represents whether the metric type is Utilization, Value, or AverageValue. This parameter is required.
         * @return {@code this}
         */
        public Builder type(String type) {
            this.type = type;
            return this;
        }

        /**
         * Sets the value of {@link MetricTargetV2Beta2#getAverageUtilization}
         * @param averageUtilization averageUtilization is the target value of the average of the resource metric across all relevant pods, represented as a percentage of the requested value of the resource for the pods.
         *                           Currently only valid for Resource metric source type
         * @return {@code this}
         */
        public Builder averageUtilization(Number averageUtilization) {
            this.averageUtilization = averageUtilization;
            return this;
        }

        /**
         * Sets the value of {@link MetricTargetV2Beta2#getAverageValue}
         * @param averageValue averageValue is the target value of the average of the metric across all relevant pods (as a quantity).
         * @return {@code this}
         */
        public Builder averageValue(Quantity averageValue) {
            this.averageValue = averageValue;
            return this;
        }

        /**
         * Sets the value of {@link MetricTargetV2Beta2#getValue}
         * @param value value is the target value of the metric (as a quantity).
         * @return {@code this}
         */
        public Builder value(Quantity value) {
            this.value = value;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link MetricTargetV2Beta2}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public MetricTargetV2Beta2 build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link MetricTargetV2Beta2}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements MetricTargetV2Beta2 {
        private final String type;
        private final Number averageUtilization;
        private final Quantity averageValue;
        private final Quantity value;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.type = software.amazon.jsii.Kernel.get(this, "type", software.amazon.jsii.NativeType.forClass(String.class));
            this.averageUtilization = software.amazon.jsii.Kernel.get(this, "averageUtilization", software.amazon.jsii.NativeType.forClass(Number.class));
            this.averageValue = software.amazon.jsii.Kernel.get(this, "averageValue", software.amazon.jsii.NativeType.forClass(Quantity.class));
            this.value = software.amazon.jsii.Kernel.get(this, "value", software.amazon.jsii.NativeType.forClass(Quantity.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.type = java.util.Objects.requireNonNull(builder.type, "type is required");
            this.averageUtilization = builder.averageUtilization;
            this.averageValue = builder.averageValue;
            this.value = builder.value;
        }

        @Override
        public final String getType() {
            return this.type;
        }

        @Override
        public final Number getAverageUtilization() {
            return this.averageUtilization;
        }

        @Override
        public final Quantity getAverageValue() {
            return this.averageValue;
        }

        @Override
        public final Quantity getValue() {
            return this.value;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            data.set("type", om.valueToTree(this.getType()));
            if (this.getAverageUtilization() != null) {
                data.set("averageUtilization", om.valueToTree(this.getAverageUtilization()));
            }
            if (this.getAverageValue() != null) {
                data.set("averageValue", om.valueToTree(this.getAverageValue()));
            }
            if (this.getValue() != null) {
                data.set("value", om.valueToTree(this.getValue()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("k8s.MetricTargetV2Beta2"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Jsii$Proxy that = (Jsii$Proxy) o;

            if (!type.equals(that.type)) return false;
            if (this.averageUtilization != null ? !this.averageUtilization.equals(that.averageUtilization) : that.averageUtilization != null) return false;
            if (this.averageValue != null ? !this.averageValue.equals(that.averageValue) : that.averageValue != null) return false;
            return this.value != null ? this.value.equals(that.value) : that.value == null;
        }

        @Override
        public final int hashCode() {
            int result = this.type.hashCode();
            result = 31 * result + (this.averageUtilization != null ? this.averageUtilization.hashCode() : 0);
            result = 31 * result + (this.averageValue != null ? this.averageValue.hashCode() : 0);
            result = 31 * result + (this.value != null ? this.value.hashCode() : 0);
            return result;
        }
    }
}
