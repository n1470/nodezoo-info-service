package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.435Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyTunnel")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecSubsetsTrafficPolicyTunnel.Jsii$Proxy.class)
public interface DestinationRuleSpecSubsetsTrafficPolicyTunnel extends software.amazon.jsii.JsiiSerializable {

    /**
     * Specifies which protocol to use for tunneling the downstream connection.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getProtocol() {
        return null;
    }

    /**
     * Specifies a host to which the downstream connection is tunneled.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getTargetHost() {
        return null;
    }

    /**
     * Specifies a port to which the downstream connection is tunneled.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getTargetPort() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecSubsetsTrafficPolicyTunnel}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecSubsetsTrafficPolicyTunnel}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecSubsetsTrafficPolicyTunnel> {
        java.lang.String protocol;
        java.lang.String targetHost;
        java.lang.Number targetPort;

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyTunnel#getProtocol}
         * @param protocol Specifies which protocol to use for tunneling the downstream connection.
         * @return {@code this}
         */
        public Builder protocol(java.lang.String protocol) {
            this.protocol = protocol;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyTunnel#getTargetHost}
         * @param targetHost Specifies a host to which the downstream connection is tunneled.
         * @return {@code this}
         */
        public Builder targetHost(java.lang.String targetHost) {
            this.targetHost = targetHost;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyTunnel#getTargetPort}
         * @param targetPort Specifies a port to which the downstream connection is tunneled.
         * @return {@code this}
         */
        public Builder targetPort(java.lang.Number targetPort) {
            this.targetPort = targetPort;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecSubsetsTrafficPolicyTunnel}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecSubsetsTrafficPolicyTunnel build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecSubsetsTrafficPolicyTunnel}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecSubsetsTrafficPolicyTunnel {
        private final java.lang.String protocol;
        private final java.lang.String targetHost;
        private final java.lang.Number targetPort;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.protocol = software.amazon.jsii.Kernel.get(this, "protocol", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.targetHost = software.amazon.jsii.Kernel.get(this, "targetHost", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.targetPort = software.amazon.jsii.Kernel.get(this, "targetPort", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.protocol = builder.protocol;
            this.targetHost = builder.targetHost;
            this.targetPort = builder.targetPort;
        }

        @Override
        public final java.lang.String getProtocol() {
            return this.protocol;
        }

        @Override
        public final java.lang.String getTargetHost() {
            return this.targetHost;
        }

        @Override
        public final java.lang.Number getTargetPort() {
            return this.targetPort;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getProtocol() != null) {
                data.set("protocol", om.valueToTree(this.getProtocol()));
            }
            if (this.getTargetHost() != null) {
                data.set("targetHost", om.valueToTree(this.getTargetHost()));
            }
            if (this.getTargetPort() != null) {
                data.set("targetPort", om.valueToTree(this.getTargetPort()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyTunnel"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecSubsetsTrafficPolicyTunnel.Jsii$Proxy that = (DestinationRuleSpecSubsetsTrafficPolicyTunnel.Jsii$Proxy) o;

            if (this.protocol != null ? !this.protocol.equals(that.protocol) : that.protocol != null) return false;
            if (this.targetHost != null ? !this.targetHost.equals(that.targetHost) : that.targetHost != null) return false;
            return this.targetPort != null ? this.targetPort.equals(that.targetPort) : that.targetPort == null;
        }

        @Override
        public final int hashCode() {
            int result = this.protocol != null ? this.protocol.hashCode() : 0;
            result = 31 * result + (this.targetHost != null ? this.targetHost.hashCode() : 0);
            result = 31 * result + (this.targetPort != null ? this.targetPort.hashCode() : 0);
            return result;
        }
    }
}
