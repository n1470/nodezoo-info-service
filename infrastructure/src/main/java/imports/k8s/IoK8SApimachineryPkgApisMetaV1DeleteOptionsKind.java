package imports.k8s;

/**
 * Kind is a string value representing the REST resource this object represents.
 * <p>
 * Servers may infer this from the endpoint the client submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.209Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.IoK8SApimachineryPkgApisMetaV1DeleteOptionsKind")
public enum IoK8SApimachineryPkgApisMetaV1DeleteOptionsKind {
    /**
     * DeleteOptions.
     */
    DELETE_OPTIONS,
}
