package imports.io.istio.security;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.457Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.AuthorizationPolicySpecRulesWhen")
@software.amazon.jsii.Jsii.Proxy(AuthorizationPolicySpecRulesWhen.Jsii$Proxy.class)
public interface AuthorizationPolicySpecRulesWhen extends software.amazon.jsii.JsiiSerializable {

    /**
     * The name of an Istio attribute.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getKey() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotValues() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getValues() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link AuthorizationPolicySpecRulesWhen}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link AuthorizationPolicySpecRulesWhen}
     */
    public static final class Builder implements software.amazon.jsii.Builder<AuthorizationPolicySpecRulesWhen> {
        java.lang.String key;
        java.util.List<java.lang.String> notValues;
        java.util.List<java.lang.String> values;

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesWhen#getKey}
         * @param key The name of an Istio attribute.
         * @return {@code this}
         */
        public Builder key(java.lang.String key) {
            this.key = key;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesWhen#getNotValues}
         * @param notValues Optional.
         * @return {@code this}
         */
        public Builder notValues(java.util.List<java.lang.String> notValues) {
            this.notValues = notValues;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesWhen#getValues}
         * @param values Optional.
         * @return {@code this}
         */
        public Builder values(java.util.List<java.lang.String> values) {
            this.values = values;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link AuthorizationPolicySpecRulesWhen}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public AuthorizationPolicySpecRulesWhen build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link AuthorizationPolicySpecRulesWhen}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements AuthorizationPolicySpecRulesWhen {
        private final java.lang.String key;
        private final java.util.List<java.lang.String> notValues;
        private final java.util.List<java.lang.String> values;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.key = software.amazon.jsii.Kernel.get(this, "key", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.notValues = software.amazon.jsii.Kernel.get(this, "notValues", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.values = software.amazon.jsii.Kernel.get(this, "values", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.key = builder.key;
            this.notValues = builder.notValues;
            this.values = builder.values;
        }

        @Override
        public final java.lang.String getKey() {
            return this.key;
        }

        @Override
        public final java.util.List<java.lang.String> getNotValues() {
            return this.notValues;
        }

        @Override
        public final java.util.List<java.lang.String> getValues() {
            return this.values;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getKey() != null) {
                data.set("key", om.valueToTree(this.getKey()));
            }
            if (this.getNotValues() != null) {
                data.set("notValues", om.valueToTree(this.getNotValues()));
            }
            if (this.getValues() != null) {
                data.set("values", om.valueToTree(this.getValues()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.AuthorizationPolicySpecRulesWhen"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AuthorizationPolicySpecRulesWhen.Jsii$Proxy that = (AuthorizationPolicySpecRulesWhen.Jsii$Proxy) o;

            if (this.key != null ? !this.key.equals(that.key) : that.key != null) return false;
            if (this.notValues != null ? !this.notValues.equals(that.notValues) : that.notValues != null) return false;
            return this.values != null ? this.values.equals(that.values) : that.values == null;
        }

        @Override
        public final int hashCode() {
            int result = this.key != null ? this.key.hashCode() : 0;
            result = 31 * result + (this.notValues != null ? this.notValues.hashCode() : 0);
            result = 31 * result + (this.values != null ? this.values.hashCode() : 0);
            return result;
        }
    }
}
