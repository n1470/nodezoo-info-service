package imports.io.istio.telemetry;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.288Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecMetricsOverridesTagOverrides")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpecMetricsOverridesTagOverrides.Jsii$Proxy.class)
public interface TelemetrySpecMetricsOverridesTagOverrides extends software.amazon.jsii.JsiiSerializable {

    /**
     * Operation controls whether or not to update/add a tag, or to remove it.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverridesOperation getOperation() {
        return null;
    }

    /**
     * Value is only considered if the operation is `UPSERT`.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getValue() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpecMetricsOverridesTagOverrides}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpecMetricsOverridesTagOverrides}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpecMetricsOverridesTagOverrides> {
        imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverridesOperation operation;
        java.lang.String value;

        /**
         * Sets the value of {@link TelemetrySpecMetricsOverridesTagOverrides#getOperation}
         * @param operation Operation controls whether or not to update/add a tag, or to remove it.
         * @return {@code this}
         */
        public Builder operation(imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverridesOperation operation) {
            this.operation = operation;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecMetricsOverridesTagOverrides#getValue}
         * @param value Value is only considered if the operation is `UPSERT`.
         * @return {@code this}
         */
        public Builder value(java.lang.String value) {
            this.value = value;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpecMetricsOverridesTagOverrides}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpecMetricsOverridesTagOverrides build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpecMetricsOverridesTagOverrides}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpecMetricsOverridesTagOverrides {
        private final imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverridesOperation operation;
        private final java.lang.String value;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.operation = software.amazon.jsii.Kernel.get(this, "operation", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverridesOperation.class));
            this.value = software.amazon.jsii.Kernel.get(this, "value", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.operation = builder.operation;
            this.value = builder.value;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecMetricsOverridesTagOverridesOperation getOperation() {
            return this.operation;
        }

        @Override
        public final java.lang.String getValue() {
            return this.value;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getOperation() != null) {
                data.set("operation", om.valueToTree(this.getOperation()));
            }
            if (this.getValue() != null) {
                data.set("value", om.valueToTree(this.getValue()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpecMetricsOverridesTagOverrides"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpecMetricsOverridesTagOverrides.Jsii$Proxy that = (TelemetrySpecMetricsOverridesTagOverrides.Jsii$Proxy) o;

            if (this.operation != null ? !this.operation.equals(that.operation) : that.operation != null) return false;
            return this.value != null ? this.value.equals(that.value) : that.value == null;
        }

        @Override
        public final int hashCode() {
            int result = this.operation != null ? this.operation.hashCode() : 0;
            result = 31 * result + (this.value != null ? this.value.hashCode() : 0);
            return result;
        }
    }
}
