package imports.io.istio.extensions;

/**
 * Determines where in the filter chain this `WasmPlugin` is to be injected.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:32:56.644Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.extensions.$Module.class, fqn = "ioistioextensions.WasmPluginSpecPhase")
public enum WasmPluginSpecPhase {
    /**
     * UNSPECIFIED_PHASE.
     */
    UNSPECIFIED_PHASE,
    /**
     * AUTHN.
     */
    AUTHN,
    /**
     * AUTHZ.
     */
    AUTHZ,
    /**
     * STATS.
     */
    STATS,
}
