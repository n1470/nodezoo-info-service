package tao.manifest;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import imports.k8s.KubeSecret;
import imports.k8s.KubeSecretProps;
import org.jetbrains.annotations.NotNull;
import tao.InfoServiceChart;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Base64;
import java.util.Map;

public class ImagePullSecretManifest {

    private final String secretName;

    public record Props(
            @NotNull String awsAccountId,
            @NotNull String awsRegion,
            @NotNull String authToken
    ) {

    }

    public ImagePullSecretManifest(@NotNull InfoServiceChart chart, @NotNull Props manifestProps) {
        var data = JsonNodeFactory.instance.objectNode();
        final var repositoryUrl = MessageFormat.format("{0}.dkr.ecr.{1}.amazonaws.com",
                manifestProps.awsAccountId(),
                manifestProps.awsRegion());
        data.with("auths")
                .with(repositoryUrl)
                .put("auth", manifestProps.authToken());
        String dockerConfig = Base64.getEncoder().encodeToString(data.toString().getBytes(StandardCharsets.UTF_8));
        var props = KubeSecretProps.builder()
                .data(Map.of(".dockerconfigjson", dockerConfig))
                .type("kubernetes.io/dockerconfigjson")
                .build();
        var kubeSecret = new KubeSecret(chart, "docker-registry", props);
        this.secretName = kubeSecret.getName();
    }

    public String getSecretName() {
        return secretName;
    }
}
