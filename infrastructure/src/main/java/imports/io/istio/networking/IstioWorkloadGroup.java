package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.459Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.IstioWorkloadGroup")
public class IstioWorkloadGroup extends org.cdk8s.ApiObject {

    protected IstioWorkloadGroup(final software.amazon.jsii.JsiiObjectRef objRef) {
        super(objRef);
    }

    protected IstioWorkloadGroup(final software.amazon.jsii.JsiiObject.InitializationMode initializationMode) {
        super(initializationMode);
    }

    static {
        GVK = software.amazon.jsii.JsiiObject.jsiiStaticGet(imports.io.istio.networking.IstioWorkloadGroup.class, "GVK", software.amazon.jsii.NativeType.forClass(org.cdk8s.GroupVersionKind.class));
    }

    /**
     * Defines a "WorkloadGroup" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     * @param props initialization props.
     */
    public IstioWorkloadGroup(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull java.lang.String id, final @org.jetbrains.annotations.Nullable imports.io.istio.networking.IstioWorkloadGroupProps props) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required"), props });
    }

    /**
     * Defines a "WorkloadGroup" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     */
    public IstioWorkloadGroup(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull java.lang.String id) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required") });
    }

    /**
     * Renders a Kubernetes manifest for "WorkloadGroup".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     * <p>
     * @param props initialization props.
     */
    public static @org.jetbrains.annotations.NotNull java.lang.Object manifest(final @org.jetbrains.annotations.Nullable imports.io.istio.networking.IstioWorkloadGroupProps props) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(imports.io.istio.networking.IstioWorkloadGroup.class, "manifest", software.amazon.jsii.NativeType.forClass(java.lang.Object.class), new Object[] { props });
    }

    /**
     * Renders a Kubernetes manifest for "WorkloadGroup".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     */
    public static @org.jetbrains.annotations.NotNull java.lang.Object manifest() {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(imports.io.istio.networking.IstioWorkloadGroup.class, "manifest", software.amazon.jsii.NativeType.forClass(java.lang.Object.class));
    }

    /**
     * Renders the object to Kubernetes JSON.
     */
    @Override
    public @org.jetbrains.annotations.NotNull java.lang.Object toJson() {
        return software.amazon.jsii.Kernel.call(this, "toJson", software.amazon.jsii.NativeType.forClass(java.lang.Object.class));
    }

    /**
     * Returns the apiVersion and kind for "WorkloadGroup".
     */
    public final static org.cdk8s.GroupVersionKind GVK;

    /**
     * A fluent builder for {@link imports.io.istio.networking.IstioWorkloadGroup}.
     */
    public static final class Builder implements software.amazon.jsii.Builder<imports.io.istio.networking.IstioWorkloadGroup> {
        /**
         * @return a new instance of {@link Builder}.
         * @param scope the scope in which to define this object. This parameter is required.
         * @param id a scope-local name for the object. This parameter is required.
         */
        public static Builder create(final software.constructs.Construct scope, final java.lang.String id) {
            return new Builder(scope, id);
        }

        private final software.constructs.Construct scope;
        private final java.lang.String id;
        private imports.io.istio.networking.IstioWorkloadGroupProps.Builder props;

        private Builder(final software.constructs.Construct scope, final java.lang.String id) {
            this.scope = scope;
            this.id = id;
        }

        /**
         * @return {@code this}
         * @param metadata This parameter is required.
         */
        public Builder metadata(final org.cdk8s.ApiObjectMetadata metadata) {
            this.props().metadata(metadata);
            return this;
        }

        /**
         * Describes a collection of workload instances.
         * <p>
         * See more details at: https://istio.io/docs/reference/config/networking/workload-group.html
         * <p>
         * @return {@code this}
         * @param spec Describes a collection of workload instances. This parameter is required.
         */
        public Builder spec(final imports.io.istio.networking.WorkloadGroupSpec spec) {
            this.props().spec(spec);
            return this;
        }

        /**
         * @returns a newly built instance of {@link imports.io.istio.networking.IstioWorkloadGroup}.
         */
        @Override
        public imports.io.istio.networking.IstioWorkloadGroup build() {
            return new imports.io.istio.networking.IstioWorkloadGroup(
                this.scope,
                this.id,
                this.props != null ? this.props.build() : null
            );
        }

        private imports.io.istio.networking.IstioWorkloadGroupProps.Builder props() {
            if (this.props == null) {
                this.props = new imports.io.istio.networking.IstioWorkloadGroupProps.Builder();
            }
            return this.props;
        }
    }
}
