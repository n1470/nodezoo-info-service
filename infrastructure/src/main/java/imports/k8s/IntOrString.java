package imports.k8s;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.208Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.IntOrString")
public class IntOrString extends software.amazon.jsii.JsiiObject {

    protected IntOrString(final software.amazon.jsii.JsiiObjectRef objRef) {
        super(objRef);
    }

    protected IntOrString(final software.amazon.jsii.JsiiObject.InitializationMode initializationMode) {
        super(initializationMode);
    }

    public static @org.jetbrains.annotations.NotNull IntOrString fromNumber(final @org.jetbrains.annotations.NotNull Number value) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(IntOrString.class, "fromNumber", software.amazon.jsii.NativeType.forClass(IntOrString.class), new Object[] { java.util.Objects.requireNonNull(value, "value is required") });
    }

    public static @org.jetbrains.annotations.NotNull IntOrString fromString(final @org.jetbrains.annotations.NotNull String value) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(IntOrString.class, "fromString", software.amazon.jsii.NativeType.forClass(IntOrString.class), new Object[] { java.util.Objects.requireNonNull(value, "value is required") });
    }

    public @org.jetbrains.annotations.NotNull Object getValue() {
        return software.amazon.jsii.Kernel.get(this, "value", software.amazon.jsii.NativeType.forClass(Object.class));
    }
}
