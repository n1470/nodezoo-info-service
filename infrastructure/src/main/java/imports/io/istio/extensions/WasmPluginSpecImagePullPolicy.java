package imports.io.istio.extensions;

/**
 * The pull behaviour to be applied when fetching an OCI image.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:32:56.644Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.extensions.$Module.class, fqn = "ioistioextensions.WasmPluginSpecImagePullPolicy")
public enum WasmPluginSpecImagePullPolicy {
    /**
     * UNSPECIFIED_POLICY.
     */
    UNSPECIFIED_POLICY,
    /**
     * IfNotPresent.
     */
    IF_NOT_PRESENT,
    /**
     * Always.
     */
    ALWAYS,
}
