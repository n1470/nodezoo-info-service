package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.430Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash.Jsii$Proxy.class)
public interface DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash extends software.amazon.jsii.JsiiSerializable {

    /**
     * Hash based on HTTP cookie.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHashHttpCookie getHttpCookie() {
        return null;
    }

    /**
     * Hash based on a specific HTTP header.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getHttpHeaderName() {
        return null;
    }

    /**
     * Hash based on a specific HTTP query parameter.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getHttpQueryParameterName() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getMinimumRingSize() {
        return null;
    }

    /**
     * Hash based on the source IP address.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getUseSourceIp() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash> {
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHashHttpCookie httpCookie;
        java.lang.String httpHeaderName;
        java.lang.String httpQueryParameterName;
        java.lang.Number minimumRingSize;
        java.lang.Boolean useSourceIp;

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash#getHttpCookie}
         * @param httpCookie Hash based on HTTP cookie.
         * @return {@code this}
         */
        public Builder httpCookie(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHashHttpCookie httpCookie) {
            this.httpCookie = httpCookie;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash#getHttpHeaderName}
         * @param httpHeaderName Hash based on a specific HTTP header.
         * @return {@code this}
         */
        public Builder httpHeaderName(java.lang.String httpHeaderName) {
            this.httpHeaderName = httpHeaderName;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash#getHttpQueryParameterName}
         * @param httpQueryParameterName Hash based on a specific HTTP query parameter.
         * @return {@code this}
         */
        public Builder httpQueryParameterName(java.lang.String httpQueryParameterName) {
            this.httpQueryParameterName = httpQueryParameterName;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash#getMinimumRingSize}
         * @param minimumRingSize the value to be set.
         * @return {@code this}
         */
        public Builder minimumRingSize(java.lang.Number minimumRingSize) {
            this.minimumRingSize = minimumRingSize;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash#getUseSourceIp}
         * @param useSourceIp Hash based on the source IP address.
         * @return {@code this}
         */
        public Builder useSourceIp(java.lang.Boolean useSourceIp) {
            this.useSourceIp = useSourceIp;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash {
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHashHttpCookie httpCookie;
        private final java.lang.String httpHeaderName;
        private final java.lang.String httpQueryParameterName;
        private final java.lang.Number minimumRingSize;
        private final java.lang.Boolean useSourceIp;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.httpCookie = software.amazon.jsii.Kernel.get(this, "httpCookie", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHashHttpCookie.class));
            this.httpHeaderName = software.amazon.jsii.Kernel.get(this, "httpHeaderName", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.httpQueryParameterName = software.amazon.jsii.Kernel.get(this, "httpQueryParameterName", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.minimumRingSize = software.amazon.jsii.Kernel.get(this, "minimumRingSize", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.useSourceIp = software.amazon.jsii.Kernel.get(this, "useSourceIp", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.httpCookie = builder.httpCookie;
            this.httpHeaderName = builder.httpHeaderName;
            this.httpQueryParameterName = builder.httpQueryParameterName;
            this.minimumRingSize = builder.minimumRingSize;
            this.useSourceIp = builder.useSourceIp;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHashHttpCookie getHttpCookie() {
            return this.httpCookie;
        }

        @Override
        public final java.lang.String getHttpHeaderName() {
            return this.httpHeaderName;
        }

        @Override
        public final java.lang.String getHttpQueryParameterName() {
            return this.httpQueryParameterName;
        }

        @Override
        public final java.lang.Number getMinimumRingSize() {
            return this.minimumRingSize;
        }

        @Override
        public final java.lang.Boolean getUseSourceIp() {
            return this.useSourceIp;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getHttpCookie() != null) {
                data.set("httpCookie", om.valueToTree(this.getHttpCookie()));
            }
            if (this.getHttpHeaderName() != null) {
                data.set("httpHeaderName", om.valueToTree(this.getHttpHeaderName()));
            }
            if (this.getHttpQueryParameterName() != null) {
                data.set("httpQueryParameterName", om.valueToTree(this.getHttpQueryParameterName()));
            }
            if (this.getMinimumRingSize() != null) {
                data.set("minimumRingSize", om.valueToTree(this.getMinimumRingSize()));
            }
            if (this.getUseSourceIp() != null) {
                data.set("useSourceIp", om.valueToTree(this.getUseSourceIp()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash.Jsii$Proxy that = (DestinationRuleSpecSubsetsTrafficPolicyLoadBalancerConsistentHash.Jsii$Proxy) o;

            if (this.httpCookie != null ? !this.httpCookie.equals(that.httpCookie) : that.httpCookie != null) return false;
            if (this.httpHeaderName != null ? !this.httpHeaderName.equals(that.httpHeaderName) : that.httpHeaderName != null) return false;
            if (this.httpQueryParameterName != null ? !this.httpQueryParameterName.equals(that.httpQueryParameterName) : that.httpQueryParameterName != null) return false;
            if (this.minimumRingSize != null ? !this.minimumRingSize.equals(that.minimumRingSize) : that.minimumRingSize != null) return false;
            return this.useSourceIp != null ? this.useSourceIp.equals(that.useSourceIp) : that.useSourceIp == null;
        }

        @Override
        public final int hashCode() {
            int result = this.httpCookie != null ? this.httpCookie.hashCode() : 0;
            result = 31 * result + (this.httpHeaderName != null ? this.httpHeaderName.hashCode() : 0);
            result = 31 * result + (this.httpQueryParameterName != null ? this.httpQueryParameterName.hashCode() : 0);
            result = 31 * result + (this.minimumRingSize != null ? this.minimumRingSize.hashCode() : 0);
            result = 31 * result + (this.useSourceIp != null ? this.useSourceIp.hashCode() : 0);
            return result;
        }
    }
}
