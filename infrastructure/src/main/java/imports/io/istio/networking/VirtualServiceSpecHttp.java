package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.463Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttp")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttp.Jsii$Proxy.class)
public interface VirtualServiceSpecHttp extends software.amazon.jsii.JsiiSerializable {

    /**
     * Cross-Origin Resource Sharing policy (CORS).
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicy getCorsPolicy() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpDelegate getDelegate() {
        return null;
    }

    /**
     * Fault injection policy to apply on HTTP traffic at the client side.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpFault getFault() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpHeaders getHeaders() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpMatch> getMatch() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpMirror getMirror() {
        return null;
    }

    /**
     * Percentage of the traffic to be mirrored by the `mirror` field.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getMirrorPercent() {
        return null;
    }

    /**
     * Percentage of the traffic to be mirrored by the `mirror` field.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpMirrorPercentage getMirrorPercentage() {
        return null;
    }

    /**
     * The name assigned to the route for debugging purposes.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * A HTTP rule can either redirect or forward (default) traffic.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpRedirect getRedirect() {
        return null;
    }

    /**
     * Retry policy for HTTP requests.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpRetries getRetries() {
        return null;
    }

    /**
     * Rewrite HTTP URIs and Authority headers.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpRewrite getRewrite() {
        return null;
    }

    /**
     * A HTTP rule can either redirect or forward (default) traffic.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpRoute> getRoute() {
        return null;
    }

    /**
     * Timeout for HTTP requests, default is disabled.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getTimeout() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttp}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttp}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttp> {
        imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicy corsPolicy;
        imports.io.istio.networking.VirtualServiceSpecHttpDelegate delegate;
        imports.io.istio.networking.VirtualServiceSpecHttpFault fault;
        imports.io.istio.networking.VirtualServiceSpecHttpHeaders headers;
        java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpMatch> match;
        imports.io.istio.networking.VirtualServiceSpecHttpMirror mirror;
        java.lang.Number mirrorPercent;
        imports.io.istio.networking.VirtualServiceSpecHttpMirrorPercentage mirrorPercentage;
        java.lang.String name;
        imports.io.istio.networking.VirtualServiceSpecHttpRedirect redirect;
        imports.io.istio.networking.VirtualServiceSpecHttpRetries retries;
        imports.io.istio.networking.VirtualServiceSpecHttpRewrite rewrite;
        java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpRoute> route;
        java.lang.String timeout;

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getCorsPolicy}
         * @param corsPolicy Cross-Origin Resource Sharing policy (CORS).
         * @return {@code this}
         */
        public Builder corsPolicy(imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicy corsPolicy) {
            this.corsPolicy = corsPolicy;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getDelegate}
         * @param delegate the value to be set.
         * @return {@code this}
         */
        public Builder delegate(imports.io.istio.networking.VirtualServiceSpecHttpDelegate delegate) {
            this.delegate = delegate;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getFault}
         * @param fault Fault injection policy to apply on HTTP traffic at the client side.
         * @return {@code this}
         */
        public Builder fault(imports.io.istio.networking.VirtualServiceSpecHttpFault fault) {
            this.fault = fault;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getHeaders}
         * @param headers the value to be set.
         * @return {@code this}
         */
        public Builder headers(imports.io.istio.networking.VirtualServiceSpecHttpHeaders headers) {
            this.headers = headers;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getMatch}
         * @param match the value to be set.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder match(java.util.List<? extends imports.io.istio.networking.VirtualServiceSpecHttpMatch> match) {
            this.match = (java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpMatch>)match;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getMirror}
         * @param mirror the value to be set.
         * @return {@code this}
         */
        public Builder mirror(imports.io.istio.networking.VirtualServiceSpecHttpMirror mirror) {
            this.mirror = mirror;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getMirrorPercent}
         * @param mirrorPercent Percentage of the traffic to be mirrored by the `mirror` field.
         * @return {@code this}
         */
        public Builder mirrorPercent(java.lang.Number mirrorPercent) {
            this.mirrorPercent = mirrorPercent;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getMirrorPercentage}
         * @param mirrorPercentage Percentage of the traffic to be mirrored by the `mirror` field.
         * @return {@code this}
         */
        public Builder mirrorPercentage(imports.io.istio.networking.VirtualServiceSpecHttpMirrorPercentage mirrorPercentage) {
            this.mirrorPercentage = mirrorPercentage;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getName}
         * @param name The name assigned to the route for debugging purposes.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getRedirect}
         * @param redirect A HTTP rule can either redirect or forward (default) traffic.
         * @return {@code this}
         */
        public Builder redirect(imports.io.istio.networking.VirtualServiceSpecHttpRedirect redirect) {
            this.redirect = redirect;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getRetries}
         * @param retries Retry policy for HTTP requests.
         * @return {@code this}
         */
        public Builder retries(imports.io.istio.networking.VirtualServiceSpecHttpRetries retries) {
            this.retries = retries;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getRewrite}
         * @param rewrite Rewrite HTTP URIs and Authority headers.
         * @return {@code this}
         */
        public Builder rewrite(imports.io.istio.networking.VirtualServiceSpecHttpRewrite rewrite) {
            this.rewrite = rewrite;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getRoute}
         * @param route A HTTP rule can either redirect or forward (default) traffic.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder route(java.util.List<? extends imports.io.istio.networking.VirtualServiceSpecHttpRoute> route) {
            this.route = (java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpRoute>)route;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttp#getTimeout}
         * @param timeout Timeout for HTTP requests, default is disabled.
         * @return {@code this}
         */
        public Builder timeout(java.lang.String timeout) {
            this.timeout = timeout;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttp}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttp build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttp}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttp {
        private final imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicy corsPolicy;
        private final imports.io.istio.networking.VirtualServiceSpecHttpDelegate delegate;
        private final imports.io.istio.networking.VirtualServiceSpecHttpFault fault;
        private final imports.io.istio.networking.VirtualServiceSpecHttpHeaders headers;
        private final java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpMatch> match;
        private final imports.io.istio.networking.VirtualServiceSpecHttpMirror mirror;
        private final java.lang.Number mirrorPercent;
        private final imports.io.istio.networking.VirtualServiceSpecHttpMirrorPercentage mirrorPercentage;
        private final java.lang.String name;
        private final imports.io.istio.networking.VirtualServiceSpecHttpRedirect redirect;
        private final imports.io.istio.networking.VirtualServiceSpecHttpRetries retries;
        private final imports.io.istio.networking.VirtualServiceSpecHttpRewrite rewrite;
        private final java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpRoute> route;
        private final java.lang.String timeout;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.corsPolicy = software.amazon.jsii.Kernel.get(this, "corsPolicy", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicy.class));
            this.delegate = software.amazon.jsii.Kernel.get(this, "delegate", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpDelegate.class));
            this.fault = software.amazon.jsii.Kernel.get(this, "fault", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpFault.class));
            this.headers = software.amazon.jsii.Kernel.get(this, "headers", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpHeaders.class));
            this.match = software.amazon.jsii.Kernel.get(this, "match", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMatch.class)));
            this.mirror = software.amazon.jsii.Kernel.get(this, "mirror", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMirror.class));
            this.mirrorPercent = software.amazon.jsii.Kernel.get(this, "mirrorPercent", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.mirrorPercentage = software.amazon.jsii.Kernel.get(this, "mirrorPercentage", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMirrorPercentage.class));
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.redirect = software.amazon.jsii.Kernel.get(this, "redirect", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpRedirect.class));
            this.retries = software.amazon.jsii.Kernel.get(this, "retries", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpRetries.class));
            this.rewrite = software.amazon.jsii.Kernel.get(this, "rewrite", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpRewrite.class));
            this.route = software.amazon.jsii.Kernel.get(this, "route", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpRoute.class)));
            this.timeout = software.amazon.jsii.Kernel.get(this, "timeout", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.corsPolicy = builder.corsPolicy;
            this.delegate = builder.delegate;
            this.fault = builder.fault;
            this.headers = builder.headers;
            this.match = (java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpMatch>)builder.match;
            this.mirror = builder.mirror;
            this.mirrorPercent = builder.mirrorPercent;
            this.mirrorPercentage = builder.mirrorPercentage;
            this.name = builder.name;
            this.redirect = builder.redirect;
            this.retries = builder.retries;
            this.rewrite = builder.rewrite;
            this.route = (java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpRoute>)builder.route;
            this.timeout = builder.timeout;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpCorsPolicy getCorsPolicy() {
            return this.corsPolicy;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpDelegate getDelegate() {
            return this.delegate;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpFault getFault() {
            return this.fault;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpHeaders getHeaders() {
            return this.headers;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpMatch> getMatch() {
            return this.match;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpMirror getMirror() {
            return this.mirror;
        }

        @Override
        public final java.lang.Number getMirrorPercent() {
            return this.mirrorPercent;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpMirrorPercentage getMirrorPercentage() {
            return this.mirrorPercentage;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpRedirect getRedirect() {
            return this.redirect;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpRetries getRetries() {
            return this.retries;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpRewrite getRewrite() {
            return this.rewrite;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.VirtualServiceSpecHttpRoute> getRoute() {
            return this.route;
        }

        @Override
        public final java.lang.String getTimeout() {
            return this.timeout;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getCorsPolicy() != null) {
                data.set("corsPolicy", om.valueToTree(this.getCorsPolicy()));
            }
            if (this.getDelegate() != null) {
                data.set("delegate", om.valueToTree(this.getDelegate()));
            }
            if (this.getFault() != null) {
                data.set("fault", om.valueToTree(this.getFault()));
            }
            if (this.getHeaders() != null) {
                data.set("headers", om.valueToTree(this.getHeaders()));
            }
            if (this.getMatch() != null) {
                data.set("match", om.valueToTree(this.getMatch()));
            }
            if (this.getMirror() != null) {
                data.set("mirror", om.valueToTree(this.getMirror()));
            }
            if (this.getMirrorPercent() != null) {
                data.set("mirrorPercent", om.valueToTree(this.getMirrorPercent()));
            }
            if (this.getMirrorPercentage() != null) {
                data.set("mirrorPercentage", om.valueToTree(this.getMirrorPercentage()));
            }
            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getRedirect() != null) {
                data.set("redirect", om.valueToTree(this.getRedirect()));
            }
            if (this.getRetries() != null) {
                data.set("retries", om.valueToTree(this.getRetries()));
            }
            if (this.getRewrite() != null) {
                data.set("rewrite", om.valueToTree(this.getRewrite()));
            }
            if (this.getRoute() != null) {
                data.set("route", om.valueToTree(this.getRoute()));
            }
            if (this.getTimeout() != null) {
                data.set("timeout", om.valueToTree(this.getTimeout()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttp"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttp.Jsii$Proxy that = (VirtualServiceSpecHttp.Jsii$Proxy) o;

            if (this.corsPolicy != null ? !this.corsPolicy.equals(that.corsPolicy) : that.corsPolicy != null) return false;
            if (this.delegate != null ? !this.delegate.equals(that.delegate) : that.delegate != null) return false;
            if (this.fault != null ? !this.fault.equals(that.fault) : that.fault != null) return false;
            if (this.headers != null ? !this.headers.equals(that.headers) : that.headers != null) return false;
            if (this.match != null ? !this.match.equals(that.match) : that.match != null) return false;
            if (this.mirror != null ? !this.mirror.equals(that.mirror) : that.mirror != null) return false;
            if (this.mirrorPercent != null ? !this.mirrorPercent.equals(that.mirrorPercent) : that.mirrorPercent != null) return false;
            if (this.mirrorPercentage != null ? !this.mirrorPercentage.equals(that.mirrorPercentage) : that.mirrorPercentage != null) return false;
            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            if (this.redirect != null ? !this.redirect.equals(that.redirect) : that.redirect != null) return false;
            if (this.retries != null ? !this.retries.equals(that.retries) : that.retries != null) return false;
            if (this.rewrite != null ? !this.rewrite.equals(that.rewrite) : that.rewrite != null) return false;
            if (this.route != null ? !this.route.equals(that.route) : that.route != null) return false;
            return this.timeout != null ? this.timeout.equals(that.timeout) : that.timeout == null;
        }

        @Override
        public final int hashCode() {
            int result = this.corsPolicy != null ? this.corsPolicy.hashCode() : 0;
            result = 31 * result + (this.delegate != null ? this.delegate.hashCode() : 0);
            result = 31 * result + (this.fault != null ? this.fault.hashCode() : 0);
            result = 31 * result + (this.headers != null ? this.headers.hashCode() : 0);
            result = 31 * result + (this.match != null ? this.match.hashCode() : 0);
            result = 31 * result + (this.mirror != null ? this.mirror.hashCode() : 0);
            result = 31 * result + (this.mirrorPercent != null ? this.mirrorPercent.hashCode() : 0);
            result = 31 * result + (this.mirrorPercentage != null ? this.mirrorPercentage.hashCode() : 0);
            result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
            result = 31 * result + (this.redirect != null ? this.redirect.hashCode() : 0);
            result = 31 * result + (this.retries != null ? this.retries.hashCode() : 0);
            result = 31 * result + (this.rewrite != null ? this.rewrite.hashCode() : 0);
            result = 31 * result + (this.route != null ? this.route.hashCode() : 0);
            result = 31 * result + (this.timeout != null ? this.timeout.hashCode() : 0);
            return result;
        }
    }
}
