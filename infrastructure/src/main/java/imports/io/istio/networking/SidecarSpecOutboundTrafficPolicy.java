package imports.io.istio.networking;

/**
 * Configuration for the outbound traffic policy.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.463Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.SidecarSpecOutboundTrafficPolicy")
@software.amazon.jsii.Jsii.Proxy(SidecarSpecOutboundTrafficPolicy.Jsii$Proxy.class)
public interface SidecarSpecOutboundTrafficPolicy extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyEgressProxy getEgressProxy() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyMode getMode() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link SidecarSpecOutboundTrafficPolicy}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link SidecarSpecOutboundTrafficPolicy}
     */
    public static final class Builder implements software.amazon.jsii.Builder<SidecarSpecOutboundTrafficPolicy> {
        imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyEgressProxy egressProxy;
        imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyMode mode;

        /**
         * Sets the value of {@link SidecarSpecOutboundTrafficPolicy#getEgressProxy}
         * @param egressProxy the value to be set.
         * @return {@code this}
         */
        public Builder egressProxy(imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyEgressProxy egressProxy) {
            this.egressProxy = egressProxy;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecOutboundTrafficPolicy#getMode}
         * @param mode the value to be set.
         * @return {@code this}
         */
        public Builder mode(imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyMode mode) {
            this.mode = mode;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link SidecarSpecOutboundTrafficPolicy}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public SidecarSpecOutboundTrafficPolicy build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link SidecarSpecOutboundTrafficPolicy}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements SidecarSpecOutboundTrafficPolicy {
        private final imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyEgressProxy egressProxy;
        private final imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyMode mode;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.egressProxy = software.amazon.jsii.Kernel.get(this, "egressProxy", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyEgressProxy.class));
            this.mode = software.amazon.jsii.Kernel.get(this, "mode", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyMode.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.egressProxy = builder.egressProxy;
            this.mode = builder.mode;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyEgressProxy getEgressProxy() {
            return this.egressProxy;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecOutboundTrafficPolicyMode getMode() {
            return this.mode;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getEgressProxy() != null) {
                data.set("egressProxy", om.valueToTree(this.getEgressProxy()));
            }
            if (this.getMode() != null) {
                data.set("mode", om.valueToTree(this.getMode()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.SidecarSpecOutboundTrafficPolicy"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SidecarSpecOutboundTrafficPolicy.Jsii$Proxy that = (SidecarSpecOutboundTrafficPolicy.Jsii$Proxy) o;

            if (this.egressProxy != null ? !this.egressProxy.equals(that.egressProxy) : that.egressProxy != null) return false;
            return this.mode != null ? this.mode.equals(that.mode) : that.mode == null;
        }

        @Override
        public final int hashCode() {
            int result = this.egressProxy != null ? this.egressProxy.hashCode() : 0;
            result = 31 * result + (this.mode != null ? this.mode.hashCode() : 0);
            return result;
        }
    }
}
