import Dependencies._

ThisBuild / scalaVersion := "2.13.8"
ThisBuild / organization := "tao"
ThisBuild / organizationName := "tao"
ThisBuild / version := "0.0.1"

lazy val root = (project in file("."))
  .aggregate(app, infrastructure)
  .settings(
    name := "nodezoo-info-service"
  )

val jsonVersion = "1.1.4"
lazy val app = (project in file("app"))
  .settings(
    name := "app",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor-typed" % "2.6.19",
      "com.typesafe.akka" %% "akka-stream" % "2.6.19",
      "com.typesafe.akka" %% "akka-http" % "10.2.9",
      commonsCli,
      "javax.json" % "javax.json-api" % jsonVersion,
      "org.glassfish" % "javax.json" % jsonVersion % Runtime,
      "org.scalatest" %% "scalatest" % "3.2.11" % Test,
      "com.typesafe.akka" %% "akka-actor-testkit-typed" % "2.6.19" % Test,
      logback
    ),
    fork := true
  )
  .enablePlugins(JDKPackagerPlugin)

lazy val infrastructure = (project in file("infrastructure"))
  .settings(
    name := "infrastructure",
    libraryDependencies ++= Seq(
      "org.cdk8s" % "cdk8s" % "2.7.35",
      "software.constructs" % "constructs" % "10.1.280",
      commonsCli,
      "org.slf4j" % "slf4j-api" % "2.0.5",
      "software.amazon.awssdk" % "bom" % "2.20.26" % "runtime" pomOnly(),
      "software.amazon.awssdk" % "ecr" % "2.20.26",
      "software.amazon.awssdk" % "sts" % "2.20.26"
    ),
    libraryDependencies ++= unitTestDeps,
    libraryDependencies += logback,
    testOptions += Tests.Argument(TestFrameworks.JUnit),
    javacOptions ++= Seq("-source", "17", "-target", "17")
  )


// See https://www.scala-sbt.org/1.x/docs/Using-Sonatype.html for instructions on how to publish to Sonatype.
