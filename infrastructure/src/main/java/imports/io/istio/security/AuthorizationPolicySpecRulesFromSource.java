package imports.io.istio.security;

/**
 * Source specifies the source of a request.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.456Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.AuthorizationPolicySpecRulesFromSource")
@software.amazon.jsii.Jsii.Proxy(AuthorizationPolicySpecRulesFromSource.Jsii$Proxy.class)
public interface AuthorizationPolicySpecRulesFromSource extends software.amazon.jsii.JsiiSerializable {

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getIpBlocks() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNamespaces() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotIpBlocks() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotNamespaces() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotPrincipals() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotRemoteIpBlocks() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getNotRequestPrincipals() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getPrincipals() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getRemoteIpBlocks() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getRequestPrincipals() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link AuthorizationPolicySpecRulesFromSource}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link AuthorizationPolicySpecRulesFromSource}
     */
    public static final class Builder implements software.amazon.jsii.Builder<AuthorizationPolicySpecRulesFromSource> {
        java.util.List<java.lang.String> ipBlocks;
        java.util.List<java.lang.String> namespaces;
        java.util.List<java.lang.String> notIpBlocks;
        java.util.List<java.lang.String> notNamespaces;
        java.util.List<java.lang.String> notPrincipals;
        java.util.List<java.lang.String> notRemoteIpBlocks;
        java.util.List<java.lang.String> notRequestPrincipals;
        java.util.List<java.lang.String> principals;
        java.util.List<java.lang.String> remoteIpBlocks;
        java.util.List<java.lang.String> requestPrincipals;

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getIpBlocks}
         * @param ipBlocks Optional.
         * @return {@code this}
         */
        public Builder ipBlocks(java.util.List<java.lang.String> ipBlocks) {
            this.ipBlocks = ipBlocks;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getNamespaces}
         * @param namespaces Optional.
         * @return {@code this}
         */
        public Builder namespaces(java.util.List<java.lang.String> namespaces) {
            this.namespaces = namespaces;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getNotIpBlocks}
         * @param notIpBlocks Optional.
         * @return {@code this}
         */
        public Builder notIpBlocks(java.util.List<java.lang.String> notIpBlocks) {
            this.notIpBlocks = notIpBlocks;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getNotNamespaces}
         * @param notNamespaces Optional.
         * @return {@code this}
         */
        public Builder notNamespaces(java.util.List<java.lang.String> notNamespaces) {
            this.notNamespaces = notNamespaces;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getNotPrincipals}
         * @param notPrincipals Optional.
         * @return {@code this}
         */
        public Builder notPrincipals(java.util.List<java.lang.String> notPrincipals) {
            this.notPrincipals = notPrincipals;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getNotRemoteIpBlocks}
         * @param notRemoteIpBlocks Optional.
         * @return {@code this}
         */
        public Builder notRemoteIpBlocks(java.util.List<java.lang.String> notRemoteIpBlocks) {
            this.notRemoteIpBlocks = notRemoteIpBlocks;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getNotRequestPrincipals}
         * @param notRequestPrincipals Optional.
         * @return {@code this}
         */
        public Builder notRequestPrincipals(java.util.List<java.lang.String> notRequestPrincipals) {
            this.notRequestPrincipals = notRequestPrincipals;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getPrincipals}
         * @param principals Optional.
         * @return {@code this}
         */
        public Builder principals(java.util.List<java.lang.String> principals) {
            this.principals = principals;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getRemoteIpBlocks}
         * @param remoteIpBlocks Optional.
         * @return {@code this}
         */
        public Builder remoteIpBlocks(java.util.List<java.lang.String> remoteIpBlocks) {
            this.remoteIpBlocks = remoteIpBlocks;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpecRulesFromSource#getRequestPrincipals}
         * @param requestPrincipals Optional.
         * @return {@code this}
         */
        public Builder requestPrincipals(java.util.List<java.lang.String> requestPrincipals) {
            this.requestPrincipals = requestPrincipals;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link AuthorizationPolicySpecRulesFromSource}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public AuthorizationPolicySpecRulesFromSource build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link AuthorizationPolicySpecRulesFromSource}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements AuthorizationPolicySpecRulesFromSource {
        private final java.util.List<java.lang.String> ipBlocks;
        private final java.util.List<java.lang.String> namespaces;
        private final java.util.List<java.lang.String> notIpBlocks;
        private final java.util.List<java.lang.String> notNamespaces;
        private final java.util.List<java.lang.String> notPrincipals;
        private final java.util.List<java.lang.String> notRemoteIpBlocks;
        private final java.util.List<java.lang.String> notRequestPrincipals;
        private final java.util.List<java.lang.String> principals;
        private final java.util.List<java.lang.String> remoteIpBlocks;
        private final java.util.List<java.lang.String> requestPrincipals;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.ipBlocks = software.amazon.jsii.Kernel.get(this, "ipBlocks", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.namespaces = software.amazon.jsii.Kernel.get(this, "namespaces", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.notIpBlocks = software.amazon.jsii.Kernel.get(this, "notIpBlocks", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.notNamespaces = software.amazon.jsii.Kernel.get(this, "notNamespaces", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.notPrincipals = software.amazon.jsii.Kernel.get(this, "notPrincipals", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.notRemoteIpBlocks = software.amazon.jsii.Kernel.get(this, "notRemoteIpBlocks", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.notRequestPrincipals = software.amazon.jsii.Kernel.get(this, "notRequestPrincipals", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.principals = software.amazon.jsii.Kernel.get(this, "principals", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.remoteIpBlocks = software.amazon.jsii.Kernel.get(this, "remoteIpBlocks", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.requestPrincipals = software.amazon.jsii.Kernel.get(this, "requestPrincipals", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.ipBlocks = builder.ipBlocks;
            this.namespaces = builder.namespaces;
            this.notIpBlocks = builder.notIpBlocks;
            this.notNamespaces = builder.notNamespaces;
            this.notPrincipals = builder.notPrincipals;
            this.notRemoteIpBlocks = builder.notRemoteIpBlocks;
            this.notRequestPrincipals = builder.notRequestPrincipals;
            this.principals = builder.principals;
            this.remoteIpBlocks = builder.remoteIpBlocks;
            this.requestPrincipals = builder.requestPrincipals;
        }

        @Override
        public final java.util.List<java.lang.String> getIpBlocks() {
            return this.ipBlocks;
        }

        @Override
        public final java.util.List<java.lang.String> getNamespaces() {
            return this.namespaces;
        }

        @Override
        public final java.util.List<java.lang.String> getNotIpBlocks() {
            return this.notIpBlocks;
        }

        @Override
        public final java.util.List<java.lang.String> getNotNamespaces() {
            return this.notNamespaces;
        }

        @Override
        public final java.util.List<java.lang.String> getNotPrincipals() {
            return this.notPrincipals;
        }

        @Override
        public final java.util.List<java.lang.String> getNotRemoteIpBlocks() {
            return this.notRemoteIpBlocks;
        }

        @Override
        public final java.util.List<java.lang.String> getNotRequestPrincipals() {
            return this.notRequestPrincipals;
        }

        @Override
        public final java.util.List<java.lang.String> getPrincipals() {
            return this.principals;
        }

        @Override
        public final java.util.List<java.lang.String> getRemoteIpBlocks() {
            return this.remoteIpBlocks;
        }

        @Override
        public final java.util.List<java.lang.String> getRequestPrincipals() {
            return this.requestPrincipals;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getIpBlocks() != null) {
                data.set("ipBlocks", om.valueToTree(this.getIpBlocks()));
            }
            if (this.getNamespaces() != null) {
                data.set("namespaces", om.valueToTree(this.getNamespaces()));
            }
            if (this.getNotIpBlocks() != null) {
                data.set("notIpBlocks", om.valueToTree(this.getNotIpBlocks()));
            }
            if (this.getNotNamespaces() != null) {
                data.set("notNamespaces", om.valueToTree(this.getNotNamespaces()));
            }
            if (this.getNotPrincipals() != null) {
                data.set("notPrincipals", om.valueToTree(this.getNotPrincipals()));
            }
            if (this.getNotRemoteIpBlocks() != null) {
                data.set("notRemoteIpBlocks", om.valueToTree(this.getNotRemoteIpBlocks()));
            }
            if (this.getNotRequestPrincipals() != null) {
                data.set("notRequestPrincipals", om.valueToTree(this.getNotRequestPrincipals()));
            }
            if (this.getPrincipals() != null) {
                data.set("principals", om.valueToTree(this.getPrincipals()));
            }
            if (this.getRemoteIpBlocks() != null) {
                data.set("remoteIpBlocks", om.valueToTree(this.getRemoteIpBlocks()));
            }
            if (this.getRequestPrincipals() != null) {
                data.set("requestPrincipals", om.valueToTree(this.getRequestPrincipals()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.AuthorizationPolicySpecRulesFromSource"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AuthorizationPolicySpecRulesFromSource.Jsii$Proxy that = (AuthorizationPolicySpecRulesFromSource.Jsii$Proxy) o;

            if (this.ipBlocks != null ? !this.ipBlocks.equals(that.ipBlocks) : that.ipBlocks != null) return false;
            if (this.namespaces != null ? !this.namespaces.equals(that.namespaces) : that.namespaces != null) return false;
            if (this.notIpBlocks != null ? !this.notIpBlocks.equals(that.notIpBlocks) : that.notIpBlocks != null) return false;
            if (this.notNamespaces != null ? !this.notNamespaces.equals(that.notNamespaces) : that.notNamespaces != null) return false;
            if (this.notPrincipals != null ? !this.notPrincipals.equals(that.notPrincipals) : that.notPrincipals != null) return false;
            if (this.notRemoteIpBlocks != null ? !this.notRemoteIpBlocks.equals(that.notRemoteIpBlocks) : that.notRemoteIpBlocks != null) return false;
            if (this.notRequestPrincipals != null ? !this.notRequestPrincipals.equals(that.notRequestPrincipals) : that.notRequestPrincipals != null) return false;
            if (this.principals != null ? !this.principals.equals(that.principals) : that.principals != null) return false;
            if (this.remoteIpBlocks != null ? !this.remoteIpBlocks.equals(that.remoteIpBlocks) : that.remoteIpBlocks != null) return false;
            return this.requestPrincipals != null ? this.requestPrincipals.equals(that.requestPrincipals) : that.requestPrincipals == null;
        }

        @Override
        public final int hashCode() {
            int result = this.ipBlocks != null ? this.ipBlocks.hashCode() : 0;
            result = 31 * result + (this.namespaces != null ? this.namespaces.hashCode() : 0);
            result = 31 * result + (this.notIpBlocks != null ? this.notIpBlocks.hashCode() : 0);
            result = 31 * result + (this.notNamespaces != null ? this.notNamespaces.hashCode() : 0);
            result = 31 * result + (this.notPrincipals != null ? this.notPrincipals.hashCode() : 0);
            result = 31 * result + (this.notRemoteIpBlocks != null ? this.notRemoteIpBlocks.hashCode() : 0);
            result = 31 * result + (this.notRequestPrincipals != null ? this.notRequestPrincipals.hashCode() : 0);
            result = 31 * result + (this.principals != null ? this.principals.hashCode() : 0);
            result = 31 * result + (this.remoteIpBlocks != null ? this.remoteIpBlocks.hashCode() : 0);
            result = 31 * result + (this.requestPrincipals != null ? this.requestPrincipals.hashCode() : 0);
            return result;
        }
    }
}
