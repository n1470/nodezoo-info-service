package tao.core

final case class PackageInfo(sourceLabel: String, content: String)
