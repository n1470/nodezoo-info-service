package imports.k8s;

/**
 * ServiceAccount binds together: * a name, understood by users, and perhaps by peripheral systems, for an identity * a principal that can be authenticated and authorized * a set of secrets.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.301Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.KubeServiceAccount")
public class KubeServiceAccount extends org.cdk8s.ApiObject {

    protected KubeServiceAccount(final software.amazon.jsii.JsiiObjectRef objRef) {
        super(objRef);
    }

    protected KubeServiceAccount(final software.amazon.jsii.JsiiObject.InitializationMode initializationMode) {
        super(initializationMode);
    }

    static {
        GVK = software.amazon.jsii.JsiiObject.jsiiStaticGet(KubeServiceAccount.class, "GVK", software.amazon.jsii.NativeType.forClass(org.cdk8s.GroupVersionKind.class));
    }

    /**
     * Defines a "io.k8s.api.core.v1.ServiceAccount" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     * @param props initialization props.
     */
    public KubeServiceAccount(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull String id, final @org.jetbrains.annotations.Nullable KubeServiceAccountProps props) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required"), props });
    }

    /**
     * Defines a "io.k8s.api.core.v1.ServiceAccount" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     */
    public KubeServiceAccount(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull String id) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required") });
    }

    /**
     * Renders a Kubernetes manifest for "io.k8s.api.core.v1.ServiceAccount".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     * <p>
     * @param props initialization props.
     */
    public static @org.jetbrains.annotations.NotNull Object manifest(final @org.jetbrains.annotations.Nullable KubeServiceAccountProps props) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(KubeServiceAccount.class, "manifest", software.amazon.jsii.NativeType.forClass(Object.class), new Object[] { props });
    }

    /**
     * Renders a Kubernetes manifest for "io.k8s.api.core.v1.ServiceAccount".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     */
    public static @org.jetbrains.annotations.NotNull Object manifest() {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(KubeServiceAccount.class, "manifest", software.amazon.jsii.NativeType.forClass(Object.class));
    }

    /**
     * Renders the object to Kubernetes JSON.
     */
    @Override
    public @org.jetbrains.annotations.NotNull Object toJson() {
        return software.amazon.jsii.Kernel.call(this, "toJson", software.amazon.jsii.NativeType.forClass(Object.class));
    }

    /**
     * Returns the apiVersion and kind for "io.k8s.api.core.v1.ServiceAccount".
     */
    public final static org.cdk8s.GroupVersionKind GVK;

    /**
     * A fluent builder for {@link KubeServiceAccount}.
     */
    public static final class Builder implements software.amazon.jsii.Builder<KubeServiceAccount> {
        /**
         * @return a new instance of {@link Builder}.
         * @param scope the scope in which to define this object. This parameter is required.
         * @param id a scope-local name for the object. This parameter is required.
         */
        public static Builder create(final software.constructs.Construct scope, final String id) {
            return new Builder(scope, id);
        }

        private final software.constructs.Construct scope;
        private final String id;
        private KubeServiceAccountProps.Builder props;

        private Builder(final software.constructs.Construct scope, final String id) {
            this.scope = scope;
            this.id = id;
        }

        /**
         * AutomountServiceAccountToken indicates whether pods running as this service account should have an API token automatically mounted.
         * <p>
         * Can be overridden at the pod level.
         * <p>
         * @return {@code this}
         * @param automountServiceAccountToken AutomountServiceAccountToken indicates whether pods running as this service account should have an API token automatically mounted. This parameter is required.
         */
        public Builder automountServiceAccountToken(final Boolean automountServiceAccountToken) {
            this.props().automountServiceAccountToken(automountServiceAccountToken);
            return this;
        }

        /**
         * ImagePullSecrets is a list of references to secrets in the same namespace to use for pulling any images in pods that reference this ServiceAccount.
         * <p>
         * ImagePullSecrets are distinct from Secrets because Secrets can be mounted in the pod, but ImagePullSecrets are only accessed by the kubelet. More info: https://kubernetes.io/docs/concepts/containers/images/#specifying-imagepullsecrets-on-a-pod
         * <p>
         * @return {@code this}
         * @param imagePullSecrets ImagePullSecrets is a list of references to secrets in the same namespace to use for pulling any images in pods that reference this ServiceAccount. This parameter is required.
         */
        public Builder imagePullSecrets(final java.util.List<? extends LocalObjectReference> imagePullSecrets) {
            this.props().imagePullSecrets(imagePullSecrets);
            return this;
        }

        /**
         * Standard object's metadata.
         * <p>
         * More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
         * <p>
         * @return {@code this}
         * @param metadata Standard object's metadata. This parameter is required.
         */
        public Builder metadata(final ObjectMeta metadata) {
            this.props().metadata(metadata);
            return this;
        }

        /**
         * Secrets is the list of secrets allowed to be used by pods running using this ServiceAccount.
         * <p>
         * More info: https://kubernetes.io/docs/concepts/configuration/secret
         * <p>
         * @return {@code this}
         * @param secrets Secrets is the list of secrets allowed to be used by pods running using this ServiceAccount. This parameter is required.
         */
        public Builder secrets(final java.util.List<? extends ObjectReference> secrets) {
            this.props().secrets(secrets);
            return this;
        }

        /**
         * @returns a newly built instance of {@link KubeServiceAccount}.
         */
        @Override
        public KubeServiceAccount build() {
            return new KubeServiceAccount(
                this.scope,
                this.id,
                this.props != null ? this.props.build() : null
            );
        }

        private KubeServiceAccountProps.Builder props() {
            if (this.props == null) {
                this.props = new KubeServiceAccountProps.Builder();
            }
            return this.props;
        }
    }
}
