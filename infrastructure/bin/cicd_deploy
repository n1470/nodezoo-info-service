#!/usr/bin/env bash
# Copies the generated k8s manifests to a cloned manifests repository and pushes the changes to
# the project branch.

set -e

TEMP_PATH=/tmp/nodezoo-info-service-dist
MANIFESTS_REPO_NAME=nodezoo-k8s-manifests
MANIFESTS_REPO=gitlab.com:n1470/${MANIFESTS_REPO_NAME}.git

# Configure SSH client
SSH_DIR=.ssh
mkdir -p ~/$SSH_DIR
chmod 700 ~/$SSH_DIR
SSH_SECRET_PATH=$HOME/$SSH_DIR/id_ed25519
if [ -f "${MANIFESTS_DEPLOY_SECRET}" ]; then
  cp "${MANIFESTS_DEPLOY_SECRET}" "${SSH_SECRET_PATH}"
else
  echo "${MANIFESTS_DEPLOY_SECRET}" > "${SSH_SECRET_PATH}"
fi
chmod 400 "${SSH_SECRET_PATH}"
cat > ~/$SSH_DIR/config <<END_OF_SSH_CONFIG
Host gitlab.com
  IdentityFile ${SSH_SECRET_PATH}
  StrictHostKeyChecking no
  UserKnownHostsFile /dev/null
END_OF_SSH_CONFIG
chmod 400 ~/$SSH_DIR/config

# Copy generated manifests to a temporary path
mkdir -p $TEMP_PATH
cp "${DIST_REPO_ROOT}"/* $TEMP_PATH/.

# Clone the repo that stores all manifests
git clone git@$MANIFESTS_REPO ~/$MANIFESTS_REPO_NAME
cd ~/$MANIFESTS_REPO_NAME

# Configure git credentials
git config user.name "${GITLAB_USER_NAME}"
git config user.email "${GITLAB_USER_EMAIL}"

# Push the modified manifests back to the repo
git checkout "${PROJECT_NAME}"
cp $TEMP_PATH/* deploy/.
git add -A
git commit -m "Update k8s manifest via CI job #${CI_JOB_ID}"
git push origin "${PROJECT_NAME}"
