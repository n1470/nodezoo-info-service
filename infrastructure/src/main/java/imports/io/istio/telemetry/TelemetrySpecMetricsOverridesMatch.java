package imports.io.istio.telemetry;

/**
 * Match allows provides the scope of the override.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.288Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecMetricsOverridesMatch")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpecMetricsOverridesMatch.Jsii$Proxy.class)
public interface TelemetrySpecMetricsOverridesMatch extends software.amazon.jsii.JsiiSerializable {

    /**
     * Allows free-form specification of a metric.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getCustomMetric() {
        return null;
    }

    /**
     * One of the well-known Istio Standard Metrics.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMetric getMetric() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMode getMode() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpecMetricsOverridesMatch}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpecMetricsOverridesMatch}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpecMetricsOverridesMatch> {
        java.lang.String customMetric;
        imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMetric metric;
        imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMode mode;

        /**
         * Sets the value of {@link TelemetrySpecMetricsOverridesMatch#getCustomMetric}
         * @param customMetric Allows free-form specification of a metric.
         * @return {@code this}
         */
        public Builder customMetric(java.lang.String customMetric) {
            this.customMetric = customMetric;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecMetricsOverridesMatch#getMetric}
         * @param metric One of the well-known Istio Standard Metrics.
         * @return {@code this}
         */
        public Builder metric(imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMetric metric) {
            this.metric = metric;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecMetricsOverridesMatch#getMode}
         * @param mode the value to be set.
         * @return {@code this}
         */
        public Builder mode(imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMode mode) {
            this.mode = mode;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpecMetricsOverridesMatch}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpecMetricsOverridesMatch build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpecMetricsOverridesMatch}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpecMetricsOverridesMatch {
        private final java.lang.String customMetric;
        private final imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMetric metric;
        private final imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMode mode;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.customMetric = software.amazon.jsii.Kernel.get(this, "customMetric", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.metric = software.amazon.jsii.Kernel.get(this, "metric", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMetric.class));
            this.mode = software.amazon.jsii.Kernel.get(this, "mode", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMode.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.customMetric = builder.customMetric;
            this.metric = builder.metric;
            this.mode = builder.mode;
        }

        @Override
        public final java.lang.String getCustomMetric() {
            return this.customMetric;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMetric getMetric() {
            return this.metric;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecMetricsOverridesMatchMode getMode() {
            return this.mode;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getCustomMetric() != null) {
                data.set("customMetric", om.valueToTree(this.getCustomMetric()));
            }
            if (this.getMetric() != null) {
                data.set("metric", om.valueToTree(this.getMetric()));
            }
            if (this.getMode() != null) {
                data.set("mode", om.valueToTree(this.getMode()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpecMetricsOverridesMatch"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpecMetricsOverridesMatch.Jsii$Proxy that = (TelemetrySpecMetricsOverridesMatch.Jsii$Proxy) o;

            if (this.customMetric != null ? !this.customMetric.equals(that.customMetric) : that.customMetric != null) return false;
            if (this.metric != null ? !this.metric.equals(that.metric) : that.metric != null) return false;
            return this.mode != null ? this.mode.equals(that.mode) : that.mode == null;
        }

        @Override
        public final int hashCode() {
            int result = this.customMetric != null ? this.customMetric.hashCode() : 0;
            result = 31 * result + (this.metric != null ? this.metric.hashCode() : 0);
            result = 31 * result + (this.mode != null ? this.mode.hashCode() : 0);
            return result;
        }
    }
}
