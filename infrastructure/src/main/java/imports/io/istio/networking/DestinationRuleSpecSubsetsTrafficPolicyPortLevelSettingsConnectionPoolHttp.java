package imports.io.istio.networking;

/**
 * HTTP connection pool settings.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.432Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp.Jsii$Proxy.class)
public interface DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp extends software.amazon.jsii.JsiiSerializable {

    /**
     * Specify if http1.1 connection should be upgraded to http2 for the associated destination.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttpH2UpgradePolicy getH2UpgradePolicy() {
        return null;
    }

    /**
     * Maximum number of pending HTTP requests to a destination.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getHttp1MaxPendingRequests() {
        return null;
    }

    /**
     * Maximum number of requests to a backend.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getHttp2MaxRequests() {
        return null;
    }

    /**
     * The idle timeout for upstream connection pool connections.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getIdleTimeout() {
        return null;
    }

    /**
     * Maximum number of requests per connection to a backend.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getMaxRequestsPerConnection() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getMaxRetries() {
        return null;
    }

    /**
     * If set to true, client protocol will be preserved while initiating connection to backend.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getUseClientProtocol() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp> {
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttpH2UpgradePolicy h2UpgradePolicy;
        java.lang.Number http1MaxPendingRequests;
        java.lang.Number http2MaxRequests;
        java.lang.String idleTimeout;
        java.lang.Number maxRequestsPerConnection;
        java.lang.Number maxRetries;
        java.lang.Boolean useClientProtocol;

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp#getH2UpgradePolicy}
         * @param h2UpgradePolicy Specify if http1.1 connection should be upgraded to http2 for the associated destination.
         * @return {@code this}
         */
        public Builder h2UpgradePolicy(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttpH2UpgradePolicy h2UpgradePolicy) {
            this.h2UpgradePolicy = h2UpgradePolicy;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp#getHttp1MaxPendingRequests}
         * @param http1MaxPendingRequests Maximum number of pending HTTP requests to a destination.
         * @return {@code this}
         */
        public Builder http1MaxPendingRequests(java.lang.Number http1MaxPendingRequests) {
            this.http1MaxPendingRequests = http1MaxPendingRequests;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp#getHttp2MaxRequests}
         * @param http2MaxRequests Maximum number of requests to a backend.
         * @return {@code this}
         */
        public Builder http2MaxRequests(java.lang.Number http2MaxRequests) {
            this.http2MaxRequests = http2MaxRequests;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp#getIdleTimeout}
         * @param idleTimeout The idle timeout for upstream connection pool connections.
         * @return {@code this}
         */
        public Builder idleTimeout(java.lang.String idleTimeout) {
            this.idleTimeout = idleTimeout;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp#getMaxRequestsPerConnection}
         * @param maxRequestsPerConnection Maximum number of requests per connection to a backend.
         * @return {@code this}
         */
        public Builder maxRequestsPerConnection(java.lang.Number maxRequestsPerConnection) {
            this.maxRequestsPerConnection = maxRequestsPerConnection;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp#getMaxRetries}
         * @param maxRetries the value to be set.
         * @return {@code this}
         */
        public Builder maxRetries(java.lang.Number maxRetries) {
            this.maxRetries = maxRetries;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp#getUseClientProtocol}
         * @param useClientProtocol If set to true, client protocol will be preserved while initiating connection to backend.
         * @return {@code this}
         */
        public Builder useClientProtocol(java.lang.Boolean useClientProtocol) {
            this.useClientProtocol = useClientProtocol;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp {
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttpH2UpgradePolicy h2UpgradePolicy;
        private final java.lang.Number http1MaxPendingRequests;
        private final java.lang.Number http2MaxRequests;
        private final java.lang.String idleTimeout;
        private final java.lang.Number maxRequestsPerConnection;
        private final java.lang.Number maxRetries;
        private final java.lang.Boolean useClientProtocol;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.h2UpgradePolicy = software.amazon.jsii.Kernel.get(this, "h2UpgradePolicy", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttpH2UpgradePolicy.class));
            this.http1MaxPendingRequests = software.amazon.jsii.Kernel.get(this, "http1MaxPendingRequests", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.http2MaxRequests = software.amazon.jsii.Kernel.get(this, "http2MaxRequests", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.idleTimeout = software.amazon.jsii.Kernel.get(this, "idleTimeout", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.maxRequestsPerConnection = software.amazon.jsii.Kernel.get(this, "maxRequestsPerConnection", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.maxRetries = software.amazon.jsii.Kernel.get(this, "maxRetries", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.useClientProtocol = software.amazon.jsii.Kernel.get(this, "useClientProtocol", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.h2UpgradePolicy = builder.h2UpgradePolicy;
            this.http1MaxPendingRequests = builder.http1MaxPendingRequests;
            this.http2MaxRequests = builder.http2MaxRequests;
            this.idleTimeout = builder.idleTimeout;
            this.maxRequestsPerConnection = builder.maxRequestsPerConnection;
            this.maxRetries = builder.maxRetries;
            this.useClientProtocol = builder.useClientProtocol;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttpH2UpgradePolicy getH2UpgradePolicy() {
            return this.h2UpgradePolicy;
        }

        @Override
        public final java.lang.Number getHttp1MaxPendingRequests() {
            return this.http1MaxPendingRequests;
        }

        @Override
        public final java.lang.Number getHttp2MaxRequests() {
            return this.http2MaxRequests;
        }

        @Override
        public final java.lang.String getIdleTimeout() {
            return this.idleTimeout;
        }

        @Override
        public final java.lang.Number getMaxRequestsPerConnection() {
            return this.maxRequestsPerConnection;
        }

        @Override
        public final java.lang.Number getMaxRetries() {
            return this.maxRetries;
        }

        @Override
        public final java.lang.Boolean getUseClientProtocol() {
            return this.useClientProtocol;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getH2UpgradePolicy() != null) {
                data.set("h2UpgradePolicy", om.valueToTree(this.getH2UpgradePolicy()));
            }
            if (this.getHttp1MaxPendingRequests() != null) {
                data.set("http1MaxPendingRequests", om.valueToTree(this.getHttp1MaxPendingRequests()));
            }
            if (this.getHttp2MaxRequests() != null) {
                data.set("http2MaxRequests", om.valueToTree(this.getHttp2MaxRequests()));
            }
            if (this.getIdleTimeout() != null) {
                data.set("idleTimeout", om.valueToTree(this.getIdleTimeout()));
            }
            if (this.getMaxRequestsPerConnection() != null) {
                data.set("maxRequestsPerConnection", om.valueToTree(this.getMaxRequestsPerConnection()));
            }
            if (this.getMaxRetries() != null) {
                data.set("maxRetries", om.valueToTree(this.getMaxRetries()));
            }
            if (this.getUseClientProtocol() != null) {
                data.set("useClientProtocol", om.valueToTree(this.getUseClientProtocol()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp.Jsii$Proxy that = (DestinationRuleSpecSubsetsTrafficPolicyPortLevelSettingsConnectionPoolHttp.Jsii$Proxy) o;

            if (this.h2UpgradePolicy != null ? !this.h2UpgradePolicy.equals(that.h2UpgradePolicy) : that.h2UpgradePolicy != null) return false;
            if (this.http1MaxPendingRequests != null ? !this.http1MaxPendingRequests.equals(that.http1MaxPendingRequests) : that.http1MaxPendingRequests != null) return false;
            if (this.http2MaxRequests != null ? !this.http2MaxRequests.equals(that.http2MaxRequests) : that.http2MaxRequests != null) return false;
            if (this.idleTimeout != null ? !this.idleTimeout.equals(that.idleTimeout) : that.idleTimeout != null) return false;
            if (this.maxRequestsPerConnection != null ? !this.maxRequestsPerConnection.equals(that.maxRequestsPerConnection) : that.maxRequestsPerConnection != null) return false;
            if (this.maxRetries != null ? !this.maxRetries.equals(that.maxRetries) : that.maxRetries != null) return false;
            return this.useClientProtocol != null ? this.useClientProtocol.equals(that.useClientProtocol) : that.useClientProtocol == null;
        }

        @Override
        public final int hashCode() {
            int result = this.h2UpgradePolicy != null ? this.h2UpgradePolicy.hashCode() : 0;
            result = 31 * result + (this.http1MaxPendingRequests != null ? this.http1MaxPendingRequests.hashCode() : 0);
            result = 31 * result + (this.http2MaxRequests != null ? this.http2MaxRequests.hashCode() : 0);
            result = 31 * result + (this.idleTimeout != null ? this.idleTimeout.hashCode() : 0);
            result = 31 * result + (this.maxRequestsPerConnection != null ? this.maxRequestsPerConnection.hashCode() : 0);
            result = 31 * result + (this.maxRetries != null ? this.maxRetries.hashCode() : 0);
            result = 31 * result + (this.useClientProtocol != null ? this.useClientProtocol.hashCode() : 0);
            return result;
        }
    }
}
