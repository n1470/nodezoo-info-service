package imports.io.istio.security;

/**
 * RequestAuthentication defines what request authentication methods are supported by a workload.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.464Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.RequestAuthenticationSpec")
@software.amazon.jsii.Jsii.Proxy(RequestAuthenticationSpec.Jsii$Proxy.class)
public interface RequestAuthenticationSpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * Define the list of JWTs that can be validated at the selected workloads' proxy.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRules> getJwtRules() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.RequestAuthenticationSpecSelector getSelector() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link RequestAuthenticationSpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link RequestAuthenticationSpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<RequestAuthenticationSpec> {
        java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRules> jwtRules;
        imports.io.istio.security.RequestAuthenticationSpecSelector selector;

        /**
         * Sets the value of {@link RequestAuthenticationSpec#getJwtRules}
         * @param jwtRules Define the list of JWTs that can be validated at the selected workloads' proxy.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder jwtRules(java.util.List<? extends imports.io.istio.security.RequestAuthenticationSpecJwtRules> jwtRules) {
            this.jwtRules = (java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRules>)jwtRules;
            return this;
        }

        /**
         * Sets the value of {@link RequestAuthenticationSpec#getSelector}
         * @param selector Optional.
         * @return {@code this}
         */
        public Builder selector(imports.io.istio.security.RequestAuthenticationSpecSelector selector) {
            this.selector = selector;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link RequestAuthenticationSpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public RequestAuthenticationSpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link RequestAuthenticationSpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements RequestAuthenticationSpec {
        private final java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRules> jwtRules;
        private final imports.io.istio.security.RequestAuthenticationSpecSelector selector;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.jwtRules = software.amazon.jsii.Kernel.get(this, "jwtRules", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.security.RequestAuthenticationSpecJwtRules.class)));
            this.selector = software.amazon.jsii.Kernel.get(this, "selector", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.RequestAuthenticationSpecSelector.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.jwtRules = (java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRules>)builder.jwtRules;
            this.selector = builder.selector;
        }

        @Override
        public final java.util.List<imports.io.istio.security.RequestAuthenticationSpecJwtRules> getJwtRules() {
            return this.jwtRules;
        }

        @Override
        public final imports.io.istio.security.RequestAuthenticationSpecSelector getSelector() {
            return this.selector;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getJwtRules() != null) {
                data.set("jwtRules", om.valueToTree(this.getJwtRules()));
            }
            if (this.getSelector() != null) {
                data.set("selector", om.valueToTree(this.getSelector()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.RequestAuthenticationSpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RequestAuthenticationSpec.Jsii$Proxy that = (RequestAuthenticationSpec.Jsii$Proxy) o;

            if (this.jwtRules != null ? !this.jwtRules.equals(that.jwtRules) : that.jwtRules != null) return false;
            return this.selector != null ? this.selector.equals(that.selector) : that.selector == null;
        }

        @Override
        public final int hashCode() {
            int result = this.jwtRules != null ? this.jwtRules.hashCode() : 0;
            result = 31 * result + (this.selector != null ? this.selector.hashCode() : 0);
            return result;
        }
    }
}
