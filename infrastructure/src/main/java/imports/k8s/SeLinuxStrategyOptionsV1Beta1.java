package imports.k8s;

/**
 * SELinuxStrategyOptions defines the strategy type and any options used to create the strategy.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.361Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.SeLinuxStrategyOptionsV1Beta1")
@software.amazon.jsii.Jsii.Proxy(SeLinuxStrategyOptionsV1Beta1.Jsii$Proxy.class)
public interface SeLinuxStrategyOptionsV1Beta1 extends software.amazon.jsii.JsiiSerializable {

    /**
     * rule is the strategy that will dictate the allowable labels that may be set.
     */
    @org.jetbrains.annotations.NotNull String getRule();

    /**
     * seLinuxOptions required to run as;
     * <p>
     * required for MustRunAs More info: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
     */
    default @org.jetbrains.annotations.Nullable SeLinuxOptions getSeLinuxOptions() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link SeLinuxStrategyOptionsV1Beta1}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link SeLinuxStrategyOptionsV1Beta1}
     */
    public static final class Builder implements software.amazon.jsii.Builder<SeLinuxStrategyOptionsV1Beta1> {
        String rule;
        SeLinuxOptions seLinuxOptions;

        /**
         * Sets the value of {@link SeLinuxStrategyOptionsV1Beta1#getRule}
         * @param rule rule is the strategy that will dictate the allowable labels that may be set. This parameter is required.
         * @return {@code this}
         */
        public Builder rule(String rule) {
            this.rule = rule;
            return this;
        }

        /**
         * Sets the value of {@link SeLinuxStrategyOptionsV1Beta1#getSeLinuxOptions}
         * @param seLinuxOptions seLinuxOptions required to run as;.
         *                       required for MustRunAs More info: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
         * @return {@code this}
         */
        public Builder seLinuxOptions(SeLinuxOptions seLinuxOptions) {
            this.seLinuxOptions = seLinuxOptions;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link SeLinuxStrategyOptionsV1Beta1}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public SeLinuxStrategyOptionsV1Beta1 build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link SeLinuxStrategyOptionsV1Beta1}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements SeLinuxStrategyOptionsV1Beta1 {
        private final String rule;
        private final SeLinuxOptions seLinuxOptions;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.rule = software.amazon.jsii.Kernel.get(this, "rule", software.amazon.jsii.NativeType.forClass(String.class));
            this.seLinuxOptions = software.amazon.jsii.Kernel.get(this, "seLinuxOptions", software.amazon.jsii.NativeType.forClass(SeLinuxOptions.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.rule = java.util.Objects.requireNonNull(builder.rule, "rule is required");
            this.seLinuxOptions = builder.seLinuxOptions;
        }

        @Override
        public final String getRule() {
            return this.rule;
        }

        @Override
        public final SeLinuxOptions getSeLinuxOptions() {
            return this.seLinuxOptions;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            data.set("rule", om.valueToTree(this.getRule()));
            if (this.getSeLinuxOptions() != null) {
                data.set("seLinuxOptions", om.valueToTree(this.getSeLinuxOptions()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("k8s.SeLinuxStrategyOptionsV1Beta1"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Jsii$Proxy that = (Jsii$Proxy) o;

            if (!rule.equals(that.rule)) return false;
            return this.seLinuxOptions != null ? this.seLinuxOptions.equals(that.seLinuxOptions) : that.seLinuxOptions == null;
        }

        @Override
        public final int hashCode() {
            int result = this.rule.hashCode();
            result = 31 * result + (this.seLinuxOptions != null ? this.seLinuxOptions.hashCode() : 0);
            return result;
        }
    }
}
