package imports.io.istio.telemetry;

/**
 * Allows tailoring of behavior to specific conditions.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.290Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecTracingMatch")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpecTracingMatch.Jsii$Proxy.class)
public interface TelemetrySpecTracingMatch extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecTracingMatchMode getMode() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpecTracingMatch}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpecTracingMatch}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpecTracingMatch> {
        imports.io.istio.telemetry.TelemetrySpecTracingMatchMode mode;

        /**
         * Sets the value of {@link TelemetrySpecTracingMatch#getMode}
         * @param mode the value to be set.
         * @return {@code this}
         */
        public Builder mode(imports.io.istio.telemetry.TelemetrySpecTracingMatchMode mode) {
            this.mode = mode;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpecTracingMatch}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpecTracingMatch build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpecTracingMatch}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpecTracingMatch {
        private final imports.io.istio.telemetry.TelemetrySpecTracingMatchMode mode;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.mode = software.amazon.jsii.Kernel.get(this, "mode", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecTracingMatchMode.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.mode = builder.mode;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecTracingMatchMode getMode() {
            return this.mode;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getMode() != null) {
                data.set("mode", om.valueToTree(this.getMode()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpecTracingMatch"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpecTracingMatch.Jsii$Proxy that = (TelemetrySpecTracingMatch.Jsii$Proxy) o;

            return this.mode != null ? this.mode.equals(that.mode) : that.mode == null;
        }

        @Override
        public final int hashCode() {
            int result = this.mode != null ? this.mode.hashCode() : 0;
            return result;
        }
    }
}
