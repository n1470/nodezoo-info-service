package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.437Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecTrafficPolicyPortLevelSettings")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecTrafficPolicyPortLevelSettings.Jsii$Proxy.class)
public interface DestinationRuleSpecTrafficPolicyPortLevelSettings extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool getConnectionPool() {
        return null;
    }

    /**
     * Settings controlling the load balancer algorithms.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsLoadBalancer getLoadBalancer() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsOutlierDetection getOutlierDetection() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsPort getPort() {
        return null;
    }

    /**
     * TLS related settings for connections to the upstream service.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsTls getTls() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecTrafficPolicyPortLevelSettings}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecTrafficPolicyPortLevelSettings}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecTrafficPolicyPortLevelSettings> {
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool connectionPool;
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsLoadBalancer loadBalancer;
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsOutlierDetection outlierDetection;
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsPort port;
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsTls tls;

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettings#getConnectionPool}
         * @param connectionPool the value to be set.
         * @return {@code this}
         */
        public Builder connectionPool(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool connectionPool) {
            this.connectionPool = connectionPool;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettings#getLoadBalancer}
         * @param loadBalancer Settings controlling the load balancer algorithms.
         * @return {@code this}
         */
        public Builder loadBalancer(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsLoadBalancer loadBalancer) {
            this.loadBalancer = loadBalancer;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettings#getOutlierDetection}
         * @param outlierDetection the value to be set.
         * @return {@code this}
         */
        public Builder outlierDetection(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsOutlierDetection outlierDetection) {
            this.outlierDetection = outlierDetection;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettings#getPort}
         * @param port the value to be set.
         * @return {@code this}
         */
        public Builder port(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsPort port) {
            this.port = port;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettings#getTls}
         * @param tls TLS related settings for connections to the upstream service.
         * @return {@code this}
         */
        public Builder tls(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsTls tls) {
            this.tls = tls;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecTrafficPolicyPortLevelSettings}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecTrafficPolicyPortLevelSettings build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecTrafficPolicyPortLevelSettings}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecTrafficPolicyPortLevelSettings {
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool connectionPool;
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsLoadBalancer loadBalancer;
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsOutlierDetection outlierDetection;
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsPort port;
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsTls tls;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.connectionPool = software.amazon.jsii.Kernel.get(this, "connectionPool", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool.class));
            this.loadBalancer = software.amazon.jsii.Kernel.get(this, "loadBalancer", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsLoadBalancer.class));
            this.outlierDetection = software.amazon.jsii.Kernel.get(this, "outlierDetection", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsOutlierDetection.class));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsPort.class));
            this.tls = software.amazon.jsii.Kernel.get(this, "tls", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsTls.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.connectionPool = builder.connectionPool;
            this.loadBalancer = builder.loadBalancer;
            this.outlierDetection = builder.outlierDetection;
            this.port = builder.port;
            this.tls = builder.tls;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool getConnectionPool() {
            return this.connectionPool;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsLoadBalancer getLoadBalancer() {
            return this.loadBalancer;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsOutlierDetection getOutlierDetection() {
            return this.outlierDetection;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsPort getPort() {
            return this.port;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsTls getTls() {
            return this.tls;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getConnectionPool() != null) {
                data.set("connectionPool", om.valueToTree(this.getConnectionPool()));
            }
            if (this.getLoadBalancer() != null) {
                data.set("loadBalancer", om.valueToTree(this.getLoadBalancer()));
            }
            if (this.getOutlierDetection() != null) {
                data.set("outlierDetection", om.valueToTree(this.getOutlierDetection()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }
            if (this.getTls() != null) {
                data.set("tls", om.valueToTree(this.getTls()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecTrafficPolicyPortLevelSettings"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecTrafficPolicyPortLevelSettings.Jsii$Proxy that = (DestinationRuleSpecTrafficPolicyPortLevelSettings.Jsii$Proxy) o;

            if (this.connectionPool != null ? !this.connectionPool.equals(that.connectionPool) : that.connectionPool != null) return false;
            if (this.loadBalancer != null ? !this.loadBalancer.equals(that.loadBalancer) : that.loadBalancer != null) return false;
            if (this.outlierDetection != null ? !this.outlierDetection.equals(that.outlierDetection) : that.outlierDetection != null) return false;
            if (this.port != null ? !this.port.equals(that.port) : that.port != null) return false;
            return this.tls != null ? this.tls.equals(that.tls) : that.tls == null;
        }

        @Override
        public final int hashCode() {
            int result = this.connectionPool != null ? this.connectionPool.hashCode() : 0;
            result = 31 * result + (this.loadBalancer != null ? this.loadBalancer.hashCode() : 0);
            result = 31 * result + (this.outlierDetection != null ? this.outlierDetection.hashCode() : 0);
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            result = 31 * result + (this.tls != null ? this.tls.hashCode() : 0);
            return result;
        }
    }
}
