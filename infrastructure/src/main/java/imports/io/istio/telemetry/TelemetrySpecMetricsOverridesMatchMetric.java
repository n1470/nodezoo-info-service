package imports.io.istio.telemetry;

/**
 * One of the well-known Istio Standard Metrics.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.288Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecMetricsOverridesMatchMetric")
public enum TelemetrySpecMetricsOverridesMatchMetric {
    /**
     * ALL_METRICS.
     */
    ALL_METRICS,
    /**
     * REQUEST_COUNT.
     */
    REQUEST_COUNT,
    /**
     * REQUEST_DURATION.
     */
    REQUEST_DURATION,
    /**
     * REQUEST_SIZE.
     */
    REQUEST_SIZE,
    /**
     * RESPONSE_SIZE.
     */
    RESPONSE_SIZE,
    /**
     * TCP_OPENED_CONNECTIONS.
     */
    TCP_OPENED_CONNECTIONS,
    /**
     * TCP_CLOSED_CONNECTIONS.
     */
    TCP_CLOSED_CONNECTIONS,
    /**
     * TCP_SENT_BYTES.
     */
    TCP_SENT_BYTES,
    /**
     * TCP_RECEIVED_BYTES.
     */
    TCP_RECEIVED_BYTES,
    /**
     * GRPC_REQUEST_MESSAGES.
     */
    GRPC_REQUEST_MESSAGES,
    /**
     * GRPC_RESPONSE_MESSAGES.
     */
    GRPC_RESPONSE_MESSAGES,
}
