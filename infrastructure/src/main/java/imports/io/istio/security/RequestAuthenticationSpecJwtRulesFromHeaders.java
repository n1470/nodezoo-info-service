package imports.io.istio.security;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.464Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.RequestAuthenticationSpecJwtRulesFromHeaders")
@software.amazon.jsii.Jsii.Proxy(RequestAuthenticationSpecJwtRulesFromHeaders.Jsii$Proxy.class)
public interface RequestAuthenticationSpecJwtRulesFromHeaders extends software.amazon.jsii.JsiiSerializable {

    /**
     * The HTTP header name.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * The prefix that should be stripped before decoding the token.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getPrefix() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link RequestAuthenticationSpecJwtRulesFromHeaders}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link RequestAuthenticationSpecJwtRulesFromHeaders}
     */
    public static final class Builder implements software.amazon.jsii.Builder<RequestAuthenticationSpecJwtRulesFromHeaders> {
        java.lang.String name;
        java.lang.String prefix;

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRulesFromHeaders#getName}
         * @param name The HTTP header name.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link RequestAuthenticationSpecJwtRulesFromHeaders#getPrefix}
         * @param prefix The prefix that should be stripped before decoding the token.
         * @return {@code this}
         */
        public Builder prefix(java.lang.String prefix) {
            this.prefix = prefix;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link RequestAuthenticationSpecJwtRulesFromHeaders}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public RequestAuthenticationSpecJwtRulesFromHeaders build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link RequestAuthenticationSpecJwtRulesFromHeaders}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements RequestAuthenticationSpecJwtRulesFromHeaders {
        private final java.lang.String name;
        private final java.lang.String prefix;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.prefix = software.amazon.jsii.Kernel.get(this, "prefix", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.name = builder.name;
            this.prefix = builder.prefix;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final java.lang.String getPrefix() {
            return this.prefix;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getPrefix() != null) {
                data.set("prefix", om.valueToTree(this.getPrefix()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.RequestAuthenticationSpecJwtRulesFromHeaders"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RequestAuthenticationSpecJwtRulesFromHeaders.Jsii$Proxy that = (RequestAuthenticationSpecJwtRulesFromHeaders.Jsii$Proxy) o;

            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            return this.prefix != null ? this.prefix.equals(that.prefix) : that.prefix == null;
        }

        @Override
        public final int hashCode() {
            int result = this.name != null ? this.name.hashCode() : 0;
            result = 31 * result + (this.prefix != null ? this.prefix.hashCode() : 0);
            return result;
        }
    }
}
