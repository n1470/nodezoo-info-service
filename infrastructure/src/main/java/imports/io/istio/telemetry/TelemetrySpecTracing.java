package imports.io.istio.telemetry;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.289Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecTracing")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpecTracing.Jsii$Proxy.class)
public interface TelemetrySpecTracing extends software.amazon.jsii.JsiiSerializable {

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecTracingCustomTags> getCustomTags() {
        return null;
    }

    /**
     * Controls span reporting.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getDisableSpanReporting() {
        return null;
    }

    /**
     * Allows tailoring of behavior to specific conditions.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecTracingMatch getMatch() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.telemetry.TelemetrySpecTracingProviders> getProviders() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getRandomSamplingPercentage() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getUseRequestIdForTraceSampling() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpecTracing}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpecTracing}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpecTracing> {
        java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecTracingCustomTags> customTags;
        java.lang.Boolean disableSpanReporting;
        imports.io.istio.telemetry.TelemetrySpecTracingMatch match;
        java.util.List<imports.io.istio.telemetry.TelemetrySpecTracingProviders> providers;
        java.lang.Number randomSamplingPercentage;
        java.lang.Boolean useRequestIdForTraceSampling;

        /**
         * Sets the value of {@link TelemetrySpecTracing#getCustomTags}
         * @param customTags Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder customTags(java.util.Map<java.lang.String, ? extends imports.io.istio.telemetry.TelemetrySpecTracingCustomTags> customTags) {
            this.customTags = (java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecTracingCustomTags>)customTags;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecTracing#getDisableSpanReporting}
         * @param disableSpanReporting Controls span reporting.
         * @return {@code this}
         */
        public Builder disableSpanReporting(java.lang.Boolean disableSpanReporting) {
            this.disableSpanReporting = disableSpanReporting;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecTracing#getMatch}
         * @param match Allows tailoring of behavior to specific conditions.
         * @return {@code this}
         */
        public Builder match(imports.io.istio.telemetry.TelemetrySpecTracingMatch match) {
            this.match = match;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecTracing#getProviders}
         * @param providers Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder providers(java.util.List<? extends imports.io.istio.telemetry.TelemetrySpecTracingProviders> providers) {
            this.providers = (java.util.List<imports.io.istio.telemetry.TelemetrySpecTracingProviders>)providers;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecTracing#getRandomSamplingPercentage}
         * @param randomSamplingPercentage the value to be set.
         * @return {@code this}
         */
        public Builder randomSamplingPercentage(java.lang.Number randomSamplingPercentage) {
            this.randomSamplingPercentage = randomSamplingPercentage;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecTracing#getUseRequestIdForTraceSampling}
         * @param useRequestIdForTraceSampling the value to be set.
         * @return {@code this}
         */
        public Builder useRequestIdForTraceSampling(java.lang.Boolean useRequestIdForTraceSampling) {
            this.useRequestIdForTraceSampling = useRequestIdForTraceSampling;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpecTracing}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpecTracing build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpecTracing}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpecTracing {
        private final java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecTracingCustomTags> customTags;
        private final java.lang.Boolean disableSpanReporting;
        private final imports.io.istio.telemetry.TelemetrySpecTracingMatch match;
        private final java.util.List<imports.io.istio.telemetry.TelemetrySpecTracingProviders> providers;
        private final java.lang.Number randomSamplingPercentage;
        private final java.lang.Boolean useRequestIdForTraceSampling;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.customTags = software.amazon.jsii.Kernel.get(this, "customTags", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecTracingCustomTags.class)));
            this.disableSpanReporting = software.amazon.jsii.Kernel.get(this, "disableSpanReporting", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
            this.match = software.amazon.jsii.Kernel.get(this, "match", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecTracingMatch.class));
            this.providers = software.amazon.jsii.Kernel.get(this, "providers", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecTracingProviders.class)));
            this.randomSamplingPercentage = software.amazon.jsii.Kernel.get(this, "randomSamplingPercentage", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.useRequestIdForTraceSampling = software.amazon.jsii.Kernel.get(this, "useRequestIdForTraceSampling", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.customTags = (java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecTracingCustomTags>)builder.customTags;
            this.disableSpanReporting = builder.disableSpanReporting;
            this.match = builder.match;
            this.providers = (java.util.List<imports.io.istio.telemetry.TelemetrySpecTracingProviders>)builder.providers;
            this.randomSamplingPercentage = builder.randomSamplingPercentage;
            this.useRequestIdForTraceSampling = builder.useRequestIdForTraceSampling;
        }

        @Override
        public final java.util.Map<java.lang.String, imports.io.istio.telemetry.TelemetrySpecTracingCustomTags> getCustomTags() {
            return this.customTags;
        }

        @Override
        public final java.lang.Boolean getDisableSpanReporting() {
            return this.disableSpanReporting;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecTracingMatch getMatch() {
            return this.match;
        }

        @Override
        public final java.util.List<imports.io.istio.telemetry.TelemetrySpecTracingProviders> getProviders() {
            return this.providers;
        }

        @Override
        public final java.lang.Number getRandomSamplingPercentage() {
            return this.randomSamplingPercentage;
        }

        @Override
        public final java.lang.Boolean getUseRequestIdForTraceSampling() {
            return this.useRequestIdForTraceSampling;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getCustomTags() != null) {
                data.set("customTags", om.valueToTree(this.getCustomTags()));
            }
            if (this.getDisableSpanReporting() != null) {
                data.set("disableSpanReporting", om.valueToTree(this.getDisableSpanReporting()));
            }
            if (this.getMatch() != null) {
                data.set("match", om.valueToTree(this.getMatch()));
            }
            if (this.getProviders() != null) {
                data.set("providers", om.valueToTree(this.getProviders()));
            }
            if (this.getRandomSamplingPercentage() != null) {
                data.set("randomSamplingPercentage", om.valueToTree(this.getRandomSamplingPercentage()));
            }
            if (this.getUseRequestIdForTraceSampling() != null) {
                data.set("useRequestIdForTraceSampling", om.valueToTree(this.getUseRequestIdForTraceSampling()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpecTracing"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpecTracing.Jsii$Proxy that = (TelemetrySpecTracing.Jsii$Proxy) o;

            if (this.customTags != null ? !this.customTags.equals(that.customTags) : that.customTags != null) return false;
            if (this.disableSpanReporting != null ? !this.disableSpanReporting.equals(that.disableSpanReporting) : that.disableSpanReporting != null) return false;
            if (this.match != null ? !this.match.equals(that.match) : that.match != null) return false;
            if (this.providers != null ? !this.providers.equals(that.providers) : that.providers != null) return false;
            if (this.randomSamplingPercentage != null ? !this.randomSamplingPercentage.equals(that.randomSamplingPercentage) : that.randomSamplingPercentage != null) return false;
            return this.useRequestIdForTraceSampling != null ? this.useRequestIdForTraceSampling.equals(that.useRequestIdForTraceSampling) : that.useRequestIdForTraceSampling == null;
        }

        @Override
        public final int hashCode() {
            int result = this.customTags != null ? this.customTags.hashCode() : 0;
            result = 31 * result + (this.disableSpanReporting != null ? this.disableSpanReporting.hashCode() : 0);
            result = 31 * result + (this.match != null ? this.match.hashCode() : 0);
            result = 31 * result + (this.providers != null ? this.providers.hashCode() : 0);
            result = 31 * result + (this.randomSamplingPercentage != null ? this.randomSamplingPercentage.hashCode() : 0);
            result = 31 * result + (this.useRequestIdForTraceSampling != null ? this.useRequestIdForTraceSampling.hashCode() : 0);
            return result;
        }
    }
}
