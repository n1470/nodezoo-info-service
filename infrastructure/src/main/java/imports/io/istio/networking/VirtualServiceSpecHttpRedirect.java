package imports.io.istio.networking;

/**
 * A HTTP rule can either redirect or forward (default) traffic.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.467Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpRedirect")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpRedirect.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpRedirect extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getAuthority() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpRedirectDerivePort getDerivePort() {
        return null;
    }

    /**
     * On a redirect, overwrite the port portion of the URL with this value.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPort() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getRedirectCode() {
        return null;
    }

    /**
     * On a redirect, overwrite the scheme portion of the URL with this value.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getScheme() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getUri() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpRedirect}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpRedirect}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpRedirect> {
        java.lang.String authority;
        imports.io.istio.networking.VirtualServiceSpecHttpRedirectDerivePort derivePort;
        java.lang.Number port;
        java.lang.Number redirectCode;
        java.lang.String scheme;
        java.lang.String uri;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRedirect#getAuthority}
         * @param authority the value to be set.
         * @return {@code this}
         */
        public Builder authority(java.lang.String authority) {
            this.authority = authority;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRedirect#getDerivePort}
         * @param derivePort the value to be set.
         * @return {@code this}
         */
        public Builder derivePort(imports.io.istio.networking.VirtualServiceSpecHttpRedirectDerivePort derivePort) {
            this.derivePort = derivePort;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRedirect#getPort}
         * @param port On a redirect, overwrite the port portion of the URL with this value.
         * @return {@code this}
         */
        public Builder port(java.lang.Number port) {
            this.port = port;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRedirect#getRedirectCode}
         * @param redirectCode the value to be set.
         * @return {@code this}
         */
        public Builder redirectCode(java.lang.Number redirectCode) {
            this.redirectCode = redirectCode;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRedirect#getScheme}
         * @param scheme On a redirect, overwrite the scheme portion of the URL with this value.
         * @return {@code this}
         */
        public Builder scheme(java.lang.String scheme) {
            this.scheme = scheme;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpRedirect#getUri}
         * @param uri the value to be set.
         * @return {@code this}
         */
        public Builder uri(java.lang.String uri) {
            this.uri = uri;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpRedirect}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpRedirect build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpRedirect}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpRedirect {
        private final java.lang.String authority;
        private final imports.io.istio.networking.VirtualServiceSpecHttpRedirectDerivePort derivePort;
        private final java.lang.Number port;
        private final java.lang.Number redirectCode;
        private final java.lang.String scheme;
        private final java.lang.String uri;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.authority = software.amazon.jsii.Kernel.get(this, "authority", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.derivePort = software.amazon.jsii.Kernel.get(this, "derivePort", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpRedirectDerivePort.class));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.redirectCode = software.amazon.jsii.Kernel.get(this, "redirectCode", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.scheme = software.amazon.jsii.Kernel.get(this, "scheme", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.uri = software.amazon.jsii.Kernel.get(this, "uri", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.authority = builder.authority;
            this.derivePort = builder.derivePort;
            this.port = builder.port;
            this.redirectCode = builder.redirectCode;
            this.scheme = builder.scheme;
            this.uri = builder.uri;
        }

        @Override
        public final java.lang.String getAuthority() {
            return this.authority;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpRedirectDerivePort getDerivePort() {
            return this.derivePort;
        }

        @Override
        public final java.lang.Number getPort() {
            return this.port;
        }

        @Override
        public final java.lang.Number getRedirectCode() {
            return this.redirectCode;
        }

        @Override
        public final java.lang.String getScheme() {
            return this.scheme;
        }

        @Override
        public final java.lang.String getUri() {
            return this.uri;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAuthority() != null) {
                data.set("authority", om.valueToTree(this.getAuthority()));
            }
            if (this.getDerivePort() != null) {
                data.set("derivePort", om.valueToTree(this.getDerivePort()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }
            if (this.getRedirectCode() != null) {
                data.set("redirectCode", om.valueToTree(this.getRedirectCode()));
            }
            if (this.getScheme() != null) {
                data.set("scheme", om.valueToTree(this.getScheme()));
            }
            if (this.getUri() != null) {
                data.set("uri", om.valueToTree(this.getUri()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpRedirect"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpRedirect.Jsii$Proxy that = (VirtualServiceSpecHttpRedirect.Jsii$Proxy) o;

            if (this.authority != null ? !this.authority.equals(that.authority) : that.authority != null) return false;
            if (this.derivePort != null ? !this.derivePort.equals(that.derivePort) : that.derivePort != null) return false;
            if (this.port != null ? !this.port.equals(that.port) : that.port != null) return false;
            if (this.redirectCode != null ? !this.redirectCode.equals(that.redirectCode) : that.redirectCode != null) return false;
            if (this.scheme != null ? !this.scheme.equals(that.scheme) : that.scheme != null) return false;
            return this.uri != null ? this.uri.equals(that.uri) : that.uri == null;
        }

        @Override
        public final int hashCode() {
            int result = this.authority != null ? this.authority.hashCode() : 0;
            result = 31 * result + (this.derivePort != null ? this.derivePort.hashCode() : 0);
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            result = 31 * result + (this.redirectCode != null ? this.redirectCode.hashCode() : 0);
            result = 31 * result + (this.scheme != null ? this.scheme.hashCode() : 0);
            result = 31 * result + (this.uri != null ? this.uri.hashCode() : 0);
            return result;
        }
    }
}
