package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.461Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.ServiceEntrySpecEndpoints")
@software.amazon.jsii.Jsii.Proxy(ServiceEntrySpecEndpoints.Jsii$Proxy.class)
public interface ServiceEntrySpecEndpoints extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getAddress() {
        return null;
    }

    /**
     * One or more labels associated with the endpoint.
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getLabels() {
        return null;
    }

    /**
     * The locality associated with the endpoint.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getLocality() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getNetwork() {
        return null;
    }

    /**
     * Set of ports associated with the endpoint.
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.Number> getPorts() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getServiceAccount() {
        return null;
    }

    /**
     * The load balancing weight associated with the endpoint.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getWeight() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link ServiceEntrySpecEndpoints}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link ServiceEntrySpecEndpoints}
     */
    public static final class Builder implements software.amazon.jsii.Builder<ServiceEntrySpecEndpoints> {
        java.lang.String address;
        java.util.Map<java.lang.String, java.lang.String> labels;
        java.lang.String locality;
        java.lang.String network;
        java.util.Map<java.lang.String, java.lang.Number> ports;
        java.lang.String serviceAccount;
        java.lang.Number weight;

        /**
         * Sets the value of {@link ServiceEntrySpecEndpoints#getAddress}
         * @param address the value to be set.
         * @return {@code this}
         */
        public Builder address(java.lang.String address) {
            this.address = address;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpecEndpoints#getLabels}
         * @param labels One or more labels associated with the endpoint.
         * @return {@code this}
         */
        public Builder labels(java.util.Map<java.lang.String, java.lang.String> labels) {
            this.labels = labels;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpecEndpoints#getLocality}
         * @param locality The locality associated with the endpoint.
         * @return {@code this}
         */
        public Builder locality(java.lang.String locality) {
            this.locality = locality;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpecEndpoints#getNetwork}
         * @param network the value to be set.
         * @return {@code this}
         */
        public Builder network(java.lang.String network) {
            this.network = network;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpecEndpoints#getPorts}
         * @param ports Set of ports associated with the endpoint.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder ports(java.util.Map<java.lang.String, ? extends java.lang.Number> ports) {
            this.ports = (java.util.Map<java.lang.String, java.lang.Number>)ports;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpecEndpoints#getServiceAccount}
         * @param serviceAccount the value to be set.
         * @return {@code this}
         */
        public Builder serviceAccount(java.lang.String serviceAccount) {
            this.serviceAccount = serviceAccount;
            return this;
        }

        /**
         * Sets the value of {@link ServiceEntrySpecEndpoints#getWeight}
         * @param weight The load balancing weight associated with the endpoint.
         * @return {@code this}
         */
        public Builder weight(java.lang.Number weight) {
            this.weight = weight;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link ServiceEntrySpecEndpoints}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public ServiceEntrySpecEndpoints build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link ServiceEntrySpecEndpoints}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements ServiceEntrySpecEndpoints {
        private final java.lang.String address;
        private final java.util.Map<java.lang.String, java.lang.String> labels;
        private final java.lang.String locality;
        private final java.lang.String network;
        private final java.util.Map<java.lang.String, java.lang.Number> ports;
        private final java.lang.String serviceAccount;
        private final java.lang.Number weight;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.address = software.amazon.jsii.Kernel.get(this, "address", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.labels = software.amazon.jsii.Kernel.get(this, "labels", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.locality = software.amazon.jsii.Kernel.get(this, "locality", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.network = software.amazon.jsii.Kernel.get(this, "network", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.ports = software.amazon.jsii.Kernel.get(this, "ports", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.Number.class)));
            this.serviceAccount = software.amazon.jsii.Kernel.get(this, "serviceAccount", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.weight = software.amazon.jsii.Kernel.get(this, "weight", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.address = builder.address;
            this.labels = builder.labels;
            this.locality = builder.locality;
            this.network = builder.network;
            this.ports = (java.util.Map<java.lang.String, java.lang.Number>)builder.ports;
            this.serviceAccount = builder.serviceAccount;
            this.weight = builder.weight;
        }

        @Override
        public final java.lang.String getAddress() {
            return this.address;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getLabels() {
            return this.labels;
        }

        @Override
        public final java.lang.String getLocality() {
            return this.locality;
        }

        @Override
        public final java.lang.String getNetwork() {
            return this.network;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.Number> getPorts() {
            return this.ports;
        }

        @Override
        public final java.lang.String getServiceAccount() {
            return this.serviceAccount;
        }

        @Override
        public final java.lang.Number getWeight() {
            return this.weight;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAddress() != null) {
                data.set("address", om.valueToTree(this.getAddress()));
            }
            if (this.getLabels() != null) {
                data.set("labels", om.valueToTree(this.getLabels()));
            }
            if (this.getLocality() != null) {
                data.set("locality", om.valueToTree(this.getLocality()));
            }
            if (this.getNetwork() != null) {
                data.set("network", om.valueToTree(this.getNetwork()));
            }
            if (this.getPorts() != null) {
                data.set("ports", om.valueToTree(this.getPorts()));
            }
            if (this.getServiceAccount() != null) {
                data.set("serviceAccount", om.valueToTree(this.getServiceAccount()));
            }
            if (this.getWeight() != null) {
                data.set("weight", om.valueToTree(this.getWeight()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.ServiceEntrySpecEndpoints"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ServiceEntrySpecEndpoints.Jsii$Proxy that = (ServiceEntrySpecEndpoints.Jsii$Proxy) o;

            if (this.address != null ? !this.address.equals(that.address) : that.address != null) return false;
            if (this.labels != null ? !this.labels.equals(that.labels) : that.labels != null) return false;
            if (this.locality != null ? !this.locality.equals(that.locality) : that.locality != null) return false;
            if (this.network != null ? !this.network.equals(that.network) : that.network != null) return false;
            if (this.ports != null ? !this.ports.equals(that.ports) : that.ports != null) return false;
            if (this.serviceAccount != null ? !this.serviceAccount.equals(that.serviceAccount) : that.serviceAccount != null) return false;
            return this.weight != null ? this.weight.equals(that.weight) : that.weight == null;
        }

        @Override
        public final int hashCode() {
            int result = this.address != null ? this.address.hashCode() : 0;
            result = 31 * result + (this.labels != null ? this.labels.hashCode() : 0);
            result = 31 * result + (this.locality != null ? this.locality.hashCode() : 0);
            result = 31 * result + (this.network != null ? this.network.hashCode() : 0);
            result = 31 * result + (this.ports != null ? this.ports.hashCode() : 0);
            result = 31 * result + (this.serviceAccount != null ? this.serviceAccount.hashCode() : 0);
            result = 31 * result + (this.weight != null ? this.weight.hashCode() : 0);
            return result;
        }
    }
}
