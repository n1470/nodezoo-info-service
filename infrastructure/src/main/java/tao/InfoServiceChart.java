package tao;

import org.cdk8s.App;
import org.cdk8s.AppProps;
import org.cdk8s.Chart;
import org.cdk8s.ChartProps;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.constructs.Construct;
import tao.manifest.*;

import java.io.IOException;
import java.nio.file.Files;

import static tao.manifest.IngressManifest.IngressPropsBuilder;

public class InfoServiceChart extends Chart {

    private static final String NAMESPACE = "nodezoo";
    private static final int SERVICE_PORT = 80;
    private static final int TARGET_PORT = 8080;

    private static final Logger logger = LoggerFactory.getLogger(InfoServiceChart.class);
    public static final String NODEZOO_INFO_SERVICE = "nodezoo-info-service";

    public InfoServiceChart(@NotNull Construct scope,
                            @NotNull String id,
                            @NotNull InfoServiceChartProps infoServiceChartProps,
                            @Nullable ChartProps props) {
        super(scope, id, props);

        final var serviceManifestProps = new ServiceManifest.Props(SERVICE_PORT, TARGET_PORT,
                infoServiceChartProps.isEks(), NODEZOO_INFO_SERVICE);
        final var serviceManifest = new ServiceManifest(this, serviceManifestProps);

        final var secretManifestName = infoServiceChartProps.getSecretManifestProps() != null ?
                new ImagePullSecretManifest(this, infoServiceChartProps.getSecretManifestProps()).getSecretName() :
                null;

        final var deploymentManifestProps = new DeploymentManifest.Props(
                TARGET_PORT,
                infoServiceChartProps.getReplicas(),
                serviceManifest.getSelector(),
                secretManifestName,
                infoServiceChartProps.getImageTag()
        );
        new DeploymentManifest(this, deploymentManifestProps);

        if (infoServiceChartProps.isEks()) {
            final var ingressProps = IngressPropsBuilder.buildAlb(
                    serviceManifest.getService().getName(),
                    SERVICE_PORT);
            new IngressManifest(this, ingressProps);
        } else {
            final var gatewayProps = new IstioGatewayManifest.Props("info-service-gateway", NAMESPACE, SERVICE_PORT);
            final var istioGateway = new IstioGatewayManifest(this, gatewayProps);
            final var virtualServiceProps = new IstioVirtualServiceManifest.Props("info-service",
                    NAMESPACE,
                    istioGateway.getName(),
                    NODEZOO_INFO_SERVICE,
                    SERVICE_PORT);
            new IstioVirtualServiceManifest(this, virtualServiceProps);
        }
    }

    public InfoServiceChart(@NotNull Construct scope,
                            @NotNull String id,
                            @NotNull InfoServiceChartProps npmServiceChartProps) {
        this(scope, id, npmServiceChartProps, ChartProps.builder().namespace(NAMESPACE).build());
    }

    public static void main(String[] args) throws IOException {
        logger.info("Chart options: " + String.join(" ", args));
        final var infoServiceChartProps = InfoServiceChartProps.builder(args).build();
        logger.info("Synthesizing manifests...");
        Files.createDirectories(infoServiceChartProps.getOutputPath());
        final var appProps = AppProps.builder().outdir(infoServiceChartProps.getOutputPath().toString()).build();
        final var app = new App(appProps);
        new InfoServiceChart(app, "nodezoo-info-service", infoServiceChartProps);
        app.synth();
    }
}
