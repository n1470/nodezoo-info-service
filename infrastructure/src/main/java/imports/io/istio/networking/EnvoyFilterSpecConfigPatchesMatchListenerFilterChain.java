package imports.io.istio.networking;

/**
 * Match a specific filter chain in a listener.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.442Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChain")
@software.amazon.jsii.Jsii.Proxy(EnvoyFilterSpecConfigPatchesMatchListenerFilterChain.Jsii$Proxy.class)
public interface EnvoyFilterSpecConfigPatchesMatchListenerFilterChain extends software.amazon.jsii.JsiiSerializable {

    /**
     * Applies only to sidecars.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getApplicationProtocols() {
        return null;
    }

    /**
     * The destination_port value used by a filter chain's match condition.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getDestinationPort() {
        return null;
    }

    /**
     * The name of a specific filter to apply the patch to.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChainFilter getFilter() {
        return null;
    }

    /**
     * The name assigned to the filter chain.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * The SNI value used by a filter chain's match condition.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getSni() {
        return null;
    }

    /**
     * Applies only to `SIDECAR_INBOUND` context.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getTransportProtocol() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain}
     */
    public static final class Builder implements software.amazon.jsii.Builder<EnvoyFilterSpecConfigPatchesMatchListenerFilterChain> {
        java.lang.String applicationProtocols;
        java.lang.Number destinationPort;
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChainFilter filter;
        java.lang.String name;
        java.lang.String sni;
        java.lang.String transportProtocol;

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain#getApplicationProtocols}
         * @param applicationProtocols Applies only to sidecars.
         * @return {@code this}
         */
        public Builder applicationProtocols(java.lang.String applicationProtocols) {
            this.applicationProtocols = applicationProtocols;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain#getDestinationPort}
         * @param destinationPort The destination_port value used by a filter chain's match condition.
         * @return {@code this}
         */
        public Builder destinationPort(java.lang.Number destinationPort) {
            this.destinationPort = destinationPort;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain#getFilter}
         * @param filter The name of a specific filter to apply the patch to.
         * @return {@code this}
         */
        public Builder filter(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChainFilter filter) {
            this.filter = filter;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain#getName}
         * @param name The name assigned to the filter chain.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain#getSni}
         * @param sni The SNI value used by a filter chain's match condition.
         * @return {@code this}
         */
        public Builder sni(java.lang.String sni) {
            this.sni = sni;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain#getTransportProtocol}
         * @param transportProtocol Applies only to `SIDECAR_INBOUND` context.
         * @return {@code this}
         */
        public Builder transportProtocol(java.lang.String transportProtocol) {
            this.transportProtocol = transportProtocol;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public EnvoyFilterSpecConfigPatchesMatchListenerFilterChain build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link EnvoyFilterSpecConfigPatchesMatchListenerFilterChain}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements EnvoyFilterSpecConfigPatchesMatchListenerFilterChain {
        private final java.lang.String applicationProtocols;
        private final java.lang.Number destinationPort;
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChainFilter filter;
        private final java.lang.String name;
        private final java.lang.String sni;
        private final java.lang.String transportProtocol;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.applicationProtocols = software.amazon.jsii.Kernel.get(this, "applicationProtocols", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.destinationPort = software.amazon.jsii.Kernel.get(this, "destinationPort", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.filter = software.amazon.jsii.Kernel.get(this, "filter", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChainFilter.class));
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.sni = software.amazon.jsii.Kernel.get(this, "sni", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.transportProtocol = software.amazon.jsii.Kernel.get(this, "transportProtocol", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.applicationProtocols = builder.applicationProtocols;
            this.destinationPort = builder.destinationPort;
            this.filter = builder.filter;
            this.name = builder.name;
            this.sni = builder.sni;
            this.transportProtocol = builder.transportProtocol;
        }

        @Override
        public final java.lang.String getApplicationProtocols() {
            return this.applicationProtocols;
        }

        @Override
        public final java.lang.Number getDestinationPort() {
            return this.destinationPort;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChainFilter getFilter() {
            return this.filter;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final java.lang.String getSni() {
            return this.sni;
        }

        @Override
        public final java.lang.String getTransportProtocol() {
            return this.transportProtocol;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getApplicationProtocols() != null) {
                data.set("applicationProtocols", om.valueToTree(this.getApplicationProtocols()));
            }
            if (this.getDestinationPort() != null) {
                data.set("destinationPort", om.valueToTree(this.getDestinationPort()));
            }
            if (this.getFilter() != null) {
                data.set("filter", om.valueToTree(this.getFilter()));
            }
            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getSni() != null) {
                data.set("sni", om.valueToTree(this.getSni()));
            }
            if (this.getTransportProtocol() != null) {
                data.set("transportProtocol", om.valueToTree(this.getTransportProtocol()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.EnvoyFilterSpecConfigPatchesMatchListenerFilterChain"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EnvoyFilterSpecConfigPatchesMatchListenerFilterChain.Jsii$Proxy that = (EnvoyFilterSpecConfigPatchesMatchListenerFilterChain.Jsii$Proxy) o;

            if (this.applicationProtocols != null ? !this.applicationProtocols.equals(that.applicationProtocols) : that.applicationProtocols != null) return false;
            if (this.destinationPort != null ? !this.destinationPort.equals(that.destinationPort) : that.destinationPort != null) return false;
            if (this.filter != null ? !this.filter.equals(that.filter) : that.filter != null) return false;
            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            if (this.sni != null ? !this.sni.equals(that.sni) : that.sni != null) return false;
            return this.transportProtocol != null ? this.transportProtocol.equals(that.transportProtocol) : that.transportProtocol == null;
        }

        @Override
        public final int hashCode() {
            int result = this.applicationProtocols != null ? this.applicationProtocols.hashCode() : 0;
            result = 31 * result + (this.destinationPort != null ? this.destinationPort.hashCode() : 0);
            result = 31 * result + (this.filter != null ? this.filter.hashCode() : 0);
            result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
            result = 31 * result + (this.sni != null ? this.sni.hashCode() : 0);
            result = 31 * result + (this.transportProtocol != null ? this.transportProtocol.hashCode() : 0);
            return result;
        }
    }
}
