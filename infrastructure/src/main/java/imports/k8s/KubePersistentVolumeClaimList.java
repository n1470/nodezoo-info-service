package imports.k8s;

/**
 * PersistentVolumeClaimList is a list of PersistentVolumeClaim items.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.274Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.KubePersistentVolumeClaimList")
public class KubePersistentVolumeClaimList extends org.cdk8s.ApiObject {

    protected KubePersistentVolumeClaimList(final software.amazon.jsii.JsiiObjectRef objRef) {
        super(objRef);
    }

    protected KubePersistentVolumeClaimList(final software.amazon.jsii.JsiiObject.InitializationMode initializationMode) {
        super(initializationMode);
    }

    static {
        GVK = software.amazon.jsii.JsiiObject.jsiiStaticGet(KubePersistentVolumeClaimList.class, "GVK", software.amazon.jsii.NativeType.forClass(org.cdk8s.GroupVersionKind.class));
    }

    /**
     * Defines a "io.k8s.api.core.v1.PersistentVolumeClaimList" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     * @param props initialization props. This parameter is required.
     */
    public KubePersistentVolumeClaimList(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull String id, final @org.jetbrains.annotations.NotNull KubePersistentVolumeClaimListProps props) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required"), java.util.Objects.requireNonNull(props, "props is required") });
    }

    /**
     * Renders a Kubernetes manifest for "io.k8s.api.core.v1.PersistentVolumeClaimList".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     * <p>
     * @param props initialization props. This parameter is required.
     */
    public static @org.jetbrains.annotations.NotNull Object manifest(final @org.jetbrains.annotations.NotNull KubePersistentVolumeClaimListProps props) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(KubePersistentVolumeClaimList.class, "manifest", software.amazon.jsii.NativeType.forClass(Object.class), new Object[] { java.util.Objects.requireNonNull(props, "props is required") });
    }

    /**
     * Renders the object to Kubernetes JSON.
     */
    @Override
    public @org.jetbrains.annotations.NotNull Object toJson() {
        return software.amazon.jsii.Kernel.call(this, "toJson", software.amazon.jsii.NativeType.forClass(Object.class));
    }

    /**
     * Returns the apiVersion and kind for "io.k8s.api.core.v1.PersistentVolumeClaimList".
     */
    public final static org.cdk8s.GroupVersionKind GVK;

    /**
     * A fluent builder for {@link KubePersistentVolumeClaimList}.
     */
    public static final class Builder implements software.amazon.jsii.Builder<KubePersistentVolumeClaimList> {
        /**
         * @return a new instance of {@link Builder}.
         * @param scope the scope in which to define this object. This parameter is required.
         * @param id a scope-local name for the object. This parameter is required.
         */
        public static Builder create(final software.constructs.Construct scope, final String id) {
            return new Builder(scope, id);
        }

        private final software.constructs.Construct scope;
        private final String id;
        private final KubePersistentVolumeClaimListProps.Builder props;

        private Builder(final software.constructs.Construct scope, final String id) {
            this.scope = scope;
            this.id = id;
            this.props = new KubePersistentVolumeClaimListProps.Builder();
        }

        /**
         * A list of persistent volume claims.
         * <p>
         * More info: https://kubernetes.io/docs/concepts/storage/persistent-volumes#persistentvolumeclaims
         * <p>
         * @return {@code this}
         * @param items A list of persistent volume claims. This parameter is required.
         */
        public Builder items(final java.util.List<? extends KubePersistentVolumeClaimProps> items) {
            this.props.items(items);
            return this;
        }

        /**
         * Standard list metadata.
         * <p>
         * More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
         * <p>
         * @return {@code this}
         * @param metadata Standard list metadata. This parameter is required.
         */
        public Builder metadata(final ListMeta metadata) {
            this.props.metadata(metadata);
            return this;
        }

        /**
         * @returns a newly built instance of {@link KubePersistentVolumeClaimList}.
         */
        @Override
        public KubePersistentVolumeClaimList build() {
            return new KubePersistentVolumeClaimList(
                this.scope,
                this.id,
                this.props.build()
            );
        }
    }
}
