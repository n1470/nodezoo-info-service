package imports.io.istio.networking;

/**
 * The patch to apply along with the operation.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.443Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesPatch")
@software.amazon.jsii.Jsii.Proxy(EnvoyFilterSpecConfigPatchesPatch.Jsii$Proxy.class)
public interface EnvoyFilterSpecConfigPatchesPatch extends software.amazon.jsii.JsiiSerializable {

    /**
     * Determines the filter insertion order.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchFilterClass getFilterClass() {
        return null;
    }

    /**
     * Determines how the patch should be applied.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchOperation getOperation() {
        return null;
    }

    /**
     * The JSON config of the object being patched.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Object getValue() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link EnvoyFilterSpecConfigPatchesPatch}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link EnvoyFilterSpecConfigPatchesPatch}
     */
    public static final class Builder implements software.amazon.jsii.Builder<EnvoyFilterSpecConfigPatchesPatch> {
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchFilterClass filterClass;
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchOperation operation;
        java.lang.Object value;

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesPatch#getFilterClass}
         * @param filterClass Determines the filter insertion order.
         * @return {@code this}
         */
        public Builder filterClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchFilterClass filterClass) {
            this.filterClass = filterClass;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesPatch#getOperation}
         * @param operation Determines how the patch should be applied.
         * @return {@code this}
         */
        public Builder operation(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchOperation operation) {
            this.operation = operation;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesPatch#getValue}
         * @param value The JSON config of the object being patched.
         * @return {@code this}
         */
        public Builder value(java.lang.Object value) {
            this.value = value;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link EnvoyFilterSpecConfigPatchesPatch}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public EnvoyFilterSpecConfigPatchesPatch build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link EnvoyFilterSpecConfigPatchesPatch}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements EnvoyFilterSpecConfigPatchesPatch {
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchFilterClass filterClass;
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchOperation operation;
        private final java.lang.Object value;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.filterClass = software.amazon.jsii.Kernel.get(this, "filterClass", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchFilterClass.class));
            this.operation = software.amazon.jsii.Kernel.get(this, "operation", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchOperation.class));
            this.value = software.amazon.jsii.Kernel.get(this, "value", software.amazon.jsii.NativeType.forClass(java.lang.Object.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.filterClass = builder.filterClass;
            this.operation = builder.operation;
            this.value = builder.value;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchFilterClass getFilterClass() {
            return this.filterClass;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatchOperation getOperation() {
            return this.operation;
        }

        @Override
        public final java.lang.Object getValue() {
            return this.value;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getFilterClass() != null) {
                data.set("filterClass", om.valueToTree(this.getFilterClass()));
            }
            if (this.getOperation() != null) {
                data.set("operation", om.valueToTree(this.getOperation()));
            }
            if (this.getValue() != null) {
                data.set("value", om.valueToTree(this.getValue()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.EnvoyFilterSpecConfigPatchesPatch"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EnvoyFilterSpecConfigPatchesPatch.Jsii$Proxy that = (EnvoyFilterSpecConfigPatchesPatch.Jsii$Proxy) o;

            if (this.filterClass != null ? !this.filterClass.equals(that.filterClass) : that.filterClass != null) return false;
            if (this.operation != null ? !this.operation.equals(that.operation) : that.operation != null) return false;
            return this.value != null ? this.value.equals(that.value) : that.value == null;
        }

        @Override
        public final int hashCode() {
            int result = this.filterClass != null ? this.filterClass.hashCode() : 0;
            result = 31 * result + (this.operation != null ? this.operation.hashCode() : 0);
            result = 31 * result + (this.value != null ? this.value.hashCode() : 0);
            return result;
        }
    }
}
