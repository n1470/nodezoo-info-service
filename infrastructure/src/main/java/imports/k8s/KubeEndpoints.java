package imports.k8s;

/**
 * Endpoints is a collection of endpoints that implement the actual service.
 * <p>
 * Example:
 * Name: "mysvc",
 * Subsets: [
 * {
 * Addresses: [{"ip": "10.10.1.1"}, {"ip": "10.10.2.2"}],
 * Ports: [{"name": "a", "port": 8675}, {"name": "b", "port": 309}]
 * },
 * {
 * Addresses: [{"ip": "10.10.3.3"}],
 * Ports: [{"name": "a", "port": 93}, {"name": "b", "port": 76}]
 * },
 * ]
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.249Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.KubeEndpoints")
public class KubeEndpoints extends org.cdk8s.ApiObject {

    protected KubeEndpoints(final software.amazon.jsii.JsiiObjectRef objRef) {
        super(objRef);
    }

    protected KubeEndpoints(final software.amazon.jsii.JsiiObject.InitializationMode initializationMode) {
        super(initializationMode);
    }

    static {
        GVK = software.amazon.jsii.JsiiObject.jsiiStaticGet(KubeEndpoints.class, "GVK", software.amazon.jsii.NativeType.forClass(org.cdk8s.GroupVersionKind.class));
    }

    /**
     * Defines a "io.k8s.api.core.v1.Endpoints" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     * @param props initialization props.
     */
    public KubeEndpoints(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull String id, final @org.jetbrains.annotations.Nullable KubeEndpointsProps props) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required"), props });
    }

    /**
     * Defines a "io.k8s.api.core.v1.Endpoints" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     */
    public KubeEndpoints(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull String id) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required") });
    }

    /**
     * Renders a Kubernetes manifest for "io.k8s.api.core.v1.Endpoints".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     * <p>
     * @param props initialization props.
     */
    public static @org.jetbrains.annotations.NotNull Object manifest(final @org.jetbrains.annotations.Nullable KubeEndpointsProps props) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(KubeEndpoints.class, "manifest", software.amazon.jsii.NativeType.forClass(Object.class), new Object[] { props });
    }

    /**
     * Renders a Kubernetes manifest for "io.k8s.api.core.v1.Endpoints".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     */
    public static @org.jetbrains.annotations.NotNull Object manifest() {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(KubeEndpoints.class, "manifest", software.amazon.jsii.NativeType.forClass(Object.class));
    }

    /**
     * Renders the object to Kubernetes JSON.
     */
    @Override
    public @org.jetbrains.annotations.NotNull Object toJson() {
        return software.amazon.jsii.Kernel.call(this, "toJson", software.amazon.jsii.NativeType.forClass(Object.class));
    }

    /**
     * Returns the apiVersion and kind for "io.k8s.api.core.v1.Endpoints".
     */
    public final static org.cdk8s.GroupVersionKind GVK;

    /**
     * A fluent builder for {@link KubeEndpoints}.
     */
    public static final class Builder implements software.amazon.jsii.Builder<KubeEndpoints> {
        /**
         * @return a new instance of {@link Builder}.
         * @param scope the scope in which to define this object. This parameter is required.
         * @param id a scope-local name for the object. This parameter is required.
         */
        public static Builder create(final software.constructs.Construct scope, final String id) {
            return new Builder(scope, id);
        }

        private final software.constructs.Construct scope;
        private final String id;
        private KubeEndpointsProps.Builder props;

        private Builder(final software.constructs.Construct scope, final String id) {
            this.scope = scope;
            this.id = id;
        }

        /**
         * Standard object's metadata.
         * <p>
         * More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#metadata
         * <p>
         * @return {@code this}
         * @param metadata Standard object's metadata. This parameter is required.
         */
        public Builder metadata(final ObjectMeta metadata) {
            this.props().metadata(metadata);
            return this;
        }

        /**
         * The set of all endpoints is the union of all subsets.
         * <p>
         * Addresses are placed into subsets according to the IPs they share. A single address with multiple ports, some of which are ready and some of which are not (because they come from different containers) will result in the address being displayed in different subsets for the different ports. No address will appear in both Addresses and NotReadyAddresses in the same subset. Sets of addresses and ports that comprise a service.
         * <p>
         * @return {@code this}
         * @param subsets The set of all endpoints is the union of all subsets. This parameter is required.
         */
        public Builder subsets(final java.util.List<? extends EndpointSubset> subsets) {
            this.props().subsets(subsets);
            return this;
        }

        /**
         * @returns a newly built instance of {@link KubeEndpoints}.
         */
        @Override
        public KubeEndpoints build() {
            return new KubeEndpoints(
                this.scope,
                this.id,
                this.props != null ? this.props.build() : null
            );
        }

        private KubeEndpointsProps.Builder props() {
            if (this.props == null) {
                this.props = new KubeEndpointsProps.Builder();
            }
            return this.props;
        }
    }
}
