package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.441Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatches")
@software.amazon.jsii.Jsii.Proxy(EnvoyFilterSpecConfigPatches.Jsii$Proxy.class)
public interface EnvoyFilterSpecConfigPatches extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesApplyTo getApplyTo() {
        return null;
    }

    /**
     * Match on listener/route configuration/cluster.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatch getMatch() {
        return null;
    }

    /**
     * The patch to apply along with the operation.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatch getPatch() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link EnvoyFilterSpecConfigPatches}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link EnvoyFilterSpecConfigPatches}
     */
    public static final class Builder implements software.amazon.jsii.Builder<EnvoyFilterSpecConfigPatches> {
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesApplyTo applyTo;
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatch match;
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatch patch;

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatches#getApplyTo}
         * @param applyTo the value to be set.
         * @return {@code this}
         */
        public Builder applyTo(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesApplyTo applyTo) {
            this.applyTo = applyTo;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatches#getMatch}
         * @param match Match on listener/route configuration/cluster.
         * @return {@code this}
         */
        public Builder match(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatch match) {
            this.match = match;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatches#getPatch}
         * @param patch The patch to apply along with the operation.
         * @return {@code this}
         */
        public Builder patch(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatch patch) {
            this.patch = patch;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link EnvoyFilterSpecConfigPatches}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public EnvoyFilterSpecConfigPatches build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link EnvoyFilterSpecConfigPatches}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements EnvoyFilterSpecConfigPatches {
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesApplyTo applyTo;
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatch match;
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatch patch;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.applyTo = software.amazon.jsii.Kernel.get(this, "applyTo", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesApplyTo.class));
            this.match = software.amazon.jsii.Kernel.get(this, "match", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatch.class));
            this.patch = software.amazon.jsii.Kernel.get(this, "patch", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatch.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.applyTo = builder.applyTo;
            this.match = builder.match;
            this.patch = builder.patch;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesApplyTo getApplyTo() {
            return this.applyTo;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatch getMatch() {
            return this.match;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesPatch getPatch() {
            return this.patch;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getApplyTo() != null) {
                data.set("applyTo", om.valueToTree(this.getApplyTo()));
            }
            if (this.getMatch() != null) {
                data.set("match", om.valueToTree(this.getMatch()));
            }
            if (this.getPatch() != null) {
                data.set("patch", om.valueToTree(this.getPatch()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.EnvoyFilterSpecConfigPatches"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EnvoyFilterSpecConfigPatches.Jsii$Proxy that = (EnvoyFilterSpecConfigPatches.Jsii$Proxy) o;

            if (this.applyTo != null ? !this.applyTo.equals(that.applyTo) : that.applyTo != null) return false;
            if (this.match != null ? !this.match.equals(that.match) : that.match != null) return false;
            return this.patch != null ? this.patch.equals(that.patch) : that.patch == null;
        }

        @Override
        public final int hashCode() {
            int result = this.applyTo != null ? this.applyTo.hashCode() : 0;
            result = 31 * result + (this.match != null ? this.match.hashCode() : 0);
            result = 31 * result + (this.patch != null ? this.patch.hashCode() : 0);
            return result;
        }
    }
}
