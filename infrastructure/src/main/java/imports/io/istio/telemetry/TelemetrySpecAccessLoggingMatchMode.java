package imports.io.istio.telemetry;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.287Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecAccessLoggingMatchMode")
public enum TelemetrySpecAccessLoggingMatchMode {
    /**
     * CLIENT_AND_SERVER.
     */
    CLIENT_AND_SERVER,
    /**
     * CLIENT.
     */
    CLIENT,
    /**
     * SERVER.
     */
    SERVER,
}
