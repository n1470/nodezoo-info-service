package imports.io.istio.security;

/**
 * PeerAuthentication defines how traffic will be tunneled (or not) to the sidecar.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.463Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.PeerAuthenticationSpec")
@software.amazon.jsii.Jsii.Proxy(PeerAuthenticationSpec.Jsii$Proxy.class)
public interface PeerAuthenticationSpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * Mutual TLS settings for workload.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.PeerAuthenticationSpecMtls getMtls() {
        return null;
    }

    /**
     * Port specific mutual TLS settings.
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, imports.io.istio.security.PeerAuthenticationSpecPortLevelMtls> getPortLevelMtls() {
        return null;
    }

    /**
     * The selector determines the workloads to apply the ChannelAuthentication on.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.PeerAuthenticationSpecSelector getSelector() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link PeerAuthenticationSpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link PeerAuthenticationSpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<PeerAuthenticationSpec> {
        imports.io.istio.security.PeerAuthenticationSpecMtls mtls;
        java.util.Map<java.lang.String, imports.io.istio.security.PeerAuthenticationSpecPortLevelMtls> portLevelMtls;
        imports.io.istio.security.PeerAuthenticationSpecSelector selector;

        /**
         * Sets the value of {@link PeerAuthenticationSpec#getMtls}
         * @param mtls Mutual TLS settings for workload.
         * @return {@code this}
         */
        public Builder mtls(imports.io.istio.security.PeerAuthenticationSpecMtls mtls) {
            this.mtls = mtls;
            return this;
        }

        /**
         * Sets the value of {@link PeerAuthenticationSpec#getPortLevelMtls}
         * @param portLevelMtls Port specific mutual TLS settings.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder portLevelMtls(java.util.Map<java.lang.String, ? extends imports.io.istio.security.PeerAuthenticationSpecPortLevelMtls> portLevelMtls) {
            this.portLevelMtls = (java.util.Map<java.lang.String, imports.io.istio.security.PeerAuthenticationSpecPortLevelMtls>)portLevelMtls;
            return this;
        }

        /**
         * Sets the value of {@link PeerAuthenticationSpec#getSelector}
         * @param selector The selector determines the workloads to apply the ChannelAuthentication on.
         * @return {@code this}
         */
        public Builder selector(imports.io.istio.security.PeerAuthenticationSpecSelector selector) {
            this.selector = selector;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link PeerAuthenticationSpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public PeerAuthenticationSpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link PeerAuthenticationSpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements PeerAuthenticationSpec {
        private final imports.io.istio.security.PeerAuthenticationSpecMtls mtls;
        private final java.util.Map<java.lang.String, imports.io.istio.security.PeerAuthenticationSpecPortLevelMtls> portLevelMtls;
        private final imports.io.istio.security.PeerAuthenticationSpecSelector selector;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.mtls = software.amazon.jsii.Kernel.get(this, "mtls", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.PeerAuthenticationSpecMtls.class));
            this.portLevelMtls = software.amazon.jsii.Kernel.get(this, "portLevelMtls", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.security.PeerAuthenticationSpecPortLevelMtls.class)));
            this.selector = software.amazon.jsii.Kernel.get(this, "selector", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.PeerAuthenticationSpecSelector.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.mtls = builder.mtls;
            this.portLevelMtls = (java.util.Map<java.lang.String, imports.io.istio.security.PeerAuthenticationSpecPortLevelMtls>)builder.portLevelMtls;
            this.selector = builder.selector;
        }

        @Override
        public final imports.io.istio.security.PeerAuthenticationSpecMtls getMtls() {
            return this.mtls;
        }

        @Override
        public final java.util.Map<java.lang.String, imports.io.istio.security.PeerAuthenticationSpecPortLevelMtls> getPortLevelMtls() {
            return this.portLevelMtls;
        }

        @Override
        public final imports.io.istio.security.PeerAuthenticationSpecSelector getSelector() {
            return this.selector;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getMtls() != null) {
                data.set("mtls", om.valueToTree(this.getMtls()));
            }
            if (this.getPortLevelMtls() != null) {
                data.set("portLevelMtls", om.valueToTree(this.getPortLevelMtls()));
            }
            if (this.getSelector() != null) {
                data.set("selector", om.valueToTree(this.getSelector()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.PeerAuthenticationSpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PeerAuthenticationSpec.Jsii$Proxy that = (PeerAuthenticationSpec.Jsii$Proxy) o;

            if (this.mtls != null ? !this.mtls.equals(that.mtls) : that.mtls != null) return false;
            if (this.portLevelMtls != null ? !this.portLevelMtls.equals(that.portLevelMtls) : that.portLevelMtls != null) return false;
            return this.selector != null ? this.selector.equals(that.selector) : that.selector == null;
        }

        @Override
        public final int hashCode() {
            int result = this.mtls != null ? this.mtls.hashCode() : 0;
            result = 31 * result + (this.portLevelMtls != null ? this.portLevelMtls.hashCode() : 0);
            result = 31 * result + (this.selector != null ? this.selector.hashCode() : 0);
            return result;
        }
    }
}
