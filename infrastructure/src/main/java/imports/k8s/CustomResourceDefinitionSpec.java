package imports.k8s;

/**
 * CustomResourceDefinitionSpec describes how a user wants their resource to appear.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.178Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.CustomResourceDefinitionSpec")
@software.amazon.jsii.Jsii.Proxy(CustomResourceDefinitionSpec.Jsii$Proxy.class)
public interface CustomResourceDefinitionSpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * group is the API group of the defined custom resource.
     * <p>
     * The custom resources are served under <code>/apis/&lt;group&gt;/...</code>. Must match the name of the CustomResourceDefinition (in the form <code>&lt;names.plural&gt;.&lt;group&gt;</code>).
     */
    @org.jetbrains.annotations.NotNull String getGroup();

    /**
     * names specify the resource and kind names for the custom resource.
     */
    @org.jetbrains.annotations.NotNull CustomResourceDefinitionNames getNames();

    /**
     * scope indicates whether the defined custom resource is cluster- or namespace-scoped.
     * <p>
     * Allowed values are <code>Cluster</code> and <code>Namespaced</code>.
     */
    @org.jetbrains.annotations.NotNull String getScope();

    /**
     * versions is the list of all API versions of the defined custom resource.
     * <p>
     * Version names are used to compute the order in which served versions are listed in API discovery. If the version string is "kube-like", it will sort above non "kube-like" version strings, which are ordered lexicographically. "Kube-like" versions start with a "v", then are followed by a number (the major version), then optionally the string "alpha" or "beta" and another number (the minor version). These are sorted first by GA &gt; beta &gt; alpha (where GA is a version with no suffix such as beta or alpha), and then by comparing major version, then minor version. An example sorted list of versions: v10, v2, v1, v11beta2, v10beta3, v3beta1, v12alpha1, v11alpha2, foo1, foo10.
     */
    @org.jetbrains.annotations.NotNull java.util.List<CustomResourceDefinitionVersion> getVersions();

    /**
     * conversion defines conversion settings for the CRD.
     */
    default @org.jetbrains.annotations.Nullable CustomResourceConversion getConversion() {
        return null;
    }

    /**
     * preserveUnknownFields indicates that object fields which are not specified in the OpenAPI schema should be preserved when persisting to storage.
     * <p>
     * apiVersion, kind, metadata and known fields inside metadata are always preserved. This field is deprecated in favor of setting <code>x-preserve-unknown-fields</code> to true in <code>spec.versions[*].schema.openAPIV3Schema</code>. See https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions/#pruning-versus-preserving-unknown-fields for details.
     */
    default @org.jetbrains.annotations.Nullable Boolean getPreserveUnknownFields() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link CustomResourceDefinitionSpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link CustomResourceDefinitionSpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<CustomResourceDefinitionSpec> {
        String group;
        CustomResourceDefinitionNames names;
        String scope;
        java.util.List<CustomResourceDefinitionVersion> versions;
        CustomResourceConversion conversion;
        Boolean preserveUnknownFields;

        /**
         * Sets the value of {@link CustomResourceDefinitionSpec#getGroup}
         * @param group group is the API group of the defined custom resource. This parameter is required.
         *              The custom resources are served under <code>/apis/&lt;group&gt;/...</code>. Must match the name of the CustomResourceDefinition (in the form <code>&lt;names.plural&gt;.&lt;group&gt;</code>).
         * @return {@code this}
         */
        public Builder group(String group) {
            this.group = group;
            return this;
        }

        /**
         * Sets the value of {@link CustomResourceDefinitionSpec#getNames}
         * @param names names specify the resource and kind names for the custom resource. This parameter is required.
         * @return {@code this}
         */
        public Builder names(CustomResourceDefinitionNames names) {
            this.names = names;
            return this;
        }

        /**
         * Sets the value of {@link CustomResourceDefinitionSpec#getScope}
         * @param scope scope indicates whether the defined custom resource is cluster- or namespace-scoped. This parameter is required.
         *              Allowed values are <code>Cluster</code> and <code>Namespaced</code>.
         * @return {@code this}
         */
        public Builder scope(String scope) {
            this.scope = scope;
            return this;
        }

        /**
         * Sets the value of {@link CustomResourceDefinitionSpec#getVersions}
         * @param versions versions is the list of all API versions of the defined custom resource. This parameter is required.
         *                 Version names are used to compute the order in which served versions are listed in API discovery. If the version string is "kube-like", it will sort above non "kube-like" version strings, which are ordered lexicographically. "Kube-like" versions start with a "v", then are followed by a number (the major version), then optionally the string "alpha" or "beta" and another number (the minor version). These are sorted first by GA &gt; beta &gt; alpha (where GA is a version with no suffix such as beta or alpha), and then by comparing major version, then minor version. An example sorted list of versions: v10, v2, v1, v11beta2, v10beta3, v3beta1, v12alpha1, v11alpha2, foo1, foo10.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder versions(java.util.List<? extends CustomResourceDefinitionVersion> versions) {
            this.versions = (java.util.List<CustomResourceDefinitionVersion>)versions;
            return this;
        }

        /**
         * Sets the value of {@link CustomResourceDefinitionSpec#getConversion}
         * @param conversion conversion defines conversion settings for the CRD.
         * @return {@code this}
         */
        public Builder conversion(CustomResourceConversion conversion) {
            this.conversion = conversion;
            return this;
        }

        /**
         * Sets the value of {@link CustomResourceDefinitionSpec#getPreserveUnknownFields}
         * @param preserveUnknownFields preserveUnknownFields indicates that object fields which are not specified in the OpenAPI schema should be preserved when persisting to storage.
         *                              apiVersion, kind, metadata and known fields inside metadata are always preserved. This field is deprecated in favor of setting <code>x-preserve-unknown-fields</code> to true in <code>spec.versions[*].schema.openAPIV3Schema</code>. See https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions/#pruning-versus-preserving-unknown-fields for details.
         * @return {@code this}
         */
        public Builder preserveUnknownFields(Boolean preserveUnknownFields) {
            this.preserveUnknownFields = preserveUnknownFields;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link CustomResourceDefinitionSpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public CustomResourceDefinitionSpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link CustomResourceDefinitionSpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements CustomResourceDefinitionSpec {
        private final String group;
        private final CustomResourceDefinitionNames names;
        private final String scope;
        private final java.util.List<CustomResourceDefinitionVersion> versions;
        private final CustomResourceConversion conversion;
        private final Boolean preserveUnknownFields;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.group = software.amazon.jsii.Kernel.get(this, "group", software.amazon.jsii.NativeType.forClass(String.class));
            this.names = software.amazon.jsii.Kernel.get(this, "names", software.amazon.jsii.NativeType.forClass(CustomResourceDefinitionNames.class));
            this.scope = software.amazon.jsii.Kernel.get(this, "scope", software.amazon.jsii.NativeType.forClass(String.class));
            this.versions = software.amazon.jsii.Kernel.get(this, "versions", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(CustomResourceDefinitionVersion.class)));
            this.conversion = software.amazon.jsii.Kernel.get(this, "conversion", software.amazon.jsii.NativeType.forClass(CustomResourceConversion.class));
            this.preserveUnknownFields = software.amazon.jsii.Kernel.get(this, "preserveUnknownFields", software.amazon.jsii.NativeType.forClass(Boolean.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.group = java.util.Objects.requireNonNull(builder.group, "group is required");
            this.names = java.util.Objects.requireNonNull(builder.names, "names is required");
            this.scope = java.util.Objects.requireNonNull(builder.scope, "scope is required");
            this.versions = (java.util.List<CustomResourceDefinitionVersion>)java.util.Objects.requireNonNull(builder.versions, "versions is required");
            this.conversion = builder.conversion;
            this.preserveUnknownFields = builder.preserveUnknownFields;
        }

        @Override
        public final String getGroup() {
            return this.group;
        }

        @Override
        public final CustomResourceDefinitionNames getNames() {
            return this.names;
        }

        @Override
        public final String getScope() {
            return this.scope;
        }

        @Override
        public final java.util.List<CustomResourceDefinitionVersion> getVersions() {
            return this.versions;
        }

        @Override
        public final CustomResourceConversion getConversion() {
            return this.conversion;
        }

        @Override
        public final Boolean getPreserveUnknownFields() {
            return this.preserveUnknownFields;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            data.set("group", om.valueToTree(this.getGroup()));
            data.set("names", om.valueToTree(this.getNames()));
            data.set("scope", om.valueToTree(this.getScope()));
            data.set("versions", om.valueToTree(this.getVersions()));
            if (this.getConversion() != null) {
                data.set("conversion", om.valueToTree(this.getConversion()));
            }
            if (this.getPreserveUnknownFields() != null) {
                data.set("preserveUnknownFields", om.valueToTree(this.getPreserveUnknownFields()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("k8s.CustomResourceDefinitionSpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Jsii$Proxy that = (Jsii$Proxy) o;

            if (!group.equals(that.group)) return false;
            if (!names.equals(that.names)) return false;
            if (!scope.equals(that.scope)) return false;
            if (!versions.equals(that.versions)) return false;
            if (this.conversion != null ? !this.conversion.equals(that.conversion) : that.conversion != null) return false;
            return this.preserveUnknownFields != null ? this.preserveUnknownFields.equals(that.preserveUnknownFields) : that.preserveUnknownFields == null;
        }

        @Override
        public final int hashCode() {
            int result = this.group.hashCode();
            result = 31 * result + (this.names.hashCode());
            result = 31 * result + (this.scope.hashCode());
            result = 31 * result + (this.versions.hashCode());
            result = 31 * result + (this.conversion != null ? this.conversion.hashCode() : 0);
            result = 31 * result + (this.preserveUnknownFields != null ? this.preserveUnknownFields.hashCode() : 0);
            return result;
        }
    }
}
