package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.460Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.IstioWorkloadGroupProps")
@software.amazon.jsii.Jsii.Proxy(IstioWorkloadGroupProps.Jsii$Proxy.class)
public interface IstioWorkloadGroupProps extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable org.cdk8s.ApiObjectMetadata getMetadata() {
        return null;
    }

    /**
     * Describes a collection of workload instances.
     * <p>
     * See more details at: https://istio.io/docs/reference/config/networking/workload-group.html
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.WorkloadGroupSpec getSpec() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link IstioWorkloadGroupProps}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link IstioWorkloadGroupProps}
     */
    public static final class Builder implements software.amazon.jsii.Builder<IstioWorkloadGroupProps> {
        org.cdk8s.ApiObjectMetadata metadata;
        imports.io.istio.networking.WorkloadGroupSpec spec;

        /**
         * Sets the value of {@link IstioWorkloadGroupProps#getMetadata}
         * @param metadata the value to be set.
         * @return {@code this}
         */
        public Builder metadata(org.cdk8s.ApiObjectMetadata metadata) {
            this.metadata = metadata;
            return this;
        }

        /**
         * Sets the value of {@link IstioWorkloadGroupProps#getSpec}
         * @param spec Describes a collection of workload instances.
         *             See more details at: https://istio.io/docs/reference/config/networking/workload-group.html
         * @return {@code this}
         */
        public Builder spec(imports.io.istio.networking.WorkloadGroupSpec spec) {
            this.spec = spec;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link IstioWorkloadGroupProps}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public IstioWorkloadGroupProps build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link IstioWorkloadGroupProps}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements IstioWorkloadGroupProps {
        private final org.cdk8s.ApiObjectMetadata metadata;
        private final imports.io.istio.networking.WorkloadGroupSpec spec;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.metadata = software.amazon.jsii.Kernel.get(this, "metadata", software.amazon.jsii.NativeType.forClass(org.cdk8s.ApiObjectMetadata.class));
            this.spec = software.amazon.jsii.Kernel.get(this, "spec", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.WorkloadGroupSpec.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.metadata = builder.metadata;
            this.spec = builder.spec;
        }

        @Override
        public final org.cdk8s.ApiObjectMetadata getMetadata() {
            return this.metadata;
        }

        @Override
        public final imports.io.istio.networking.WorkloadGroupSpec getSpec() {
            return this.spec;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getMetadata() != null) {
                data.set("metadata", om.valueToTree(this.getMetadata()));
            }
            if (this.getSpec() != null) {
                data.set("spec", om.valueToTree(this.getSpec()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.IstioWorkloadGroupProps"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            IstioWorkloadGroupProps.Jsii$Proxy that = (IstioWorkloadGroupProps.Jsii$Proxy) o;

            if (this.metadata != null ? !this.metadata.equals(that.metadata) : that.metadata != null) return false;
            return this.spec != null ? this.spec.equals(that.spec) : that.spec == null;
        }

        @Override
        public final int hashCode() {
            int result = this.metadata != null ? this.metadata.hashCode() : 0;
            result = 31 * result + (this.spec != null ? this.spec.hashCode() : 0);
            return result;
        }
    }
}
