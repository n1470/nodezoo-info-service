package imports.io.istio.telemetry;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.289Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecTracingCustomTags")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpecTracingCustomTags.Jsii$Proxy.class)
public interface TelemetrySpecTracingCustomTags extends software.amazon.jsii.JsiiSerializable {

    /**
     * Environment adds the value of an environment variable to each span.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsEnvironment getEnvironment() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsHeader getHeader() {
        return null;
    }

    /**
     * Literal adds the same, hard-coded value to each span.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsLiteral getLiteral() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpecTracingCustomTags}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpecTracingCustomTags}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpecTracingCustomTags> {
        imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsEnvironment environment;
        imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsHeader header;
        imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsLiteral literal;

        /**
         * Sets the value of {@link TelemetrySpecTracingCustomTags#getEnvironment}
         * @param environment Environment adds the value of an environment variable to each span.
         * @return {@code this}
         */
        public Builder environment(imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsEnvironment environment) {
            this.environment = environment;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecTracingCustomTags#getHeader}
         * @param header the value to be set.
         * @return {@code this}
         */
        public Builder header(imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsHeader header) {
            this.header = header;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecTracingCustomTags#getLiteral}
         * @param literal Literal adds the same, hard-coded value to each span.
         * @return {@code this}
         */
        public Builder literal(imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsLiteral literal) {
            this.literal = literal;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpecTracingCustomTags}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpecTracingCustomTags build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpecTracingCustomTags}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpecTracingCustomTags {
        private final imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsEnvironment environment;
        private final imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsHeader header;
        private final imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsLiteral literal;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.environment = software.amazon.jsii.Kernel.get(this, "environment", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsEnvironment.class));
            this.header = software.amazon.jsii.Kernel.get(this, "header", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsHeader.class));
            this.literal = software.amazon.jsii.Kernel.get(this, "literal", software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsLiteral.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.environment = builder.environment;
            this.header = builder.header;
            this.literal = builder.literal;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsEnvironment getEnvironment() {
            return this.environment;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsHeader getHeader() {
            return this.header;
        }

        @Override
        public final imports.io.istio.telemetry.TelemetrySpecTracingCustomTagsLiteral getLiteral() {
            return this.literal;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getEnvironment() != null) {
                data.set("environment", om.valueToTree(this.getEnvironment()));
            }
            if (this.getHeader() != null) {
                data.set("header", om.valueToTree(this.getHeader()));
            }
            if (this.getLiteral() != null) {
                data.set("literal", om.valueToTree(this.getLiteral()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpecTracingCustomTags"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpecTracingCustomTags.Jsii$Proxy that = (TelemetrySpecTracingCustomTags.Jsii$Proxy) o;

            if (this.environment != null ? !this.environment.equals(that.environment) : that.environment != null) return false;
            if (this.header != null ? !this.header.equals(that.header) : that.header != null) return false;
            return this.literal != null ? this.literal.equals(that.literal) : that.literal == null;
        }

        @Override
        public final int hashCode() {
            int result = this.environment != null ? this.environment.hashCode() : 0;
            result = 31 * result + (this.header != null ? this.header.hashCode() : 0);
            result = 31 * result + (this.literal != null ? this.literal.hashCode() : 0);
            return result;
        }
    }
}
