package imports.io.istio.networking;

/**
 * Match a specific route within the virtual host.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.442Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute")
@software.amazon.jsii.Jsii.Proxy(EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute.Jsii$Proxy.class)
public interface EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute extends software.amazon.jsii.JsiiSerializable {

    /**
     * Match a route with specific action type.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRouteAction getAction() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute}
     */
    public static final class Builder implements software.amazon.jsii.Builder<EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute> {
        imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRouteAction action;
        java.lang.String name;

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute#getAction}
         * @param action Match a route with specific action type.
         * @return {@code this}
         */
        public Builder action(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRouteAction action) {
            this.action = action;
            return this;
        }

        /**
         * Sets the value of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute#getName}
         * @param name the value to be set.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute {
        private final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRouteAction action;
        private final java.lang.String name;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.action = software.amazon.jsii.Kernel.get(this, "action", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRouteAction.class));
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.action = builder.action;
            this.name = builder.name;
        }

        @Override
        public final imports.io.istio.networking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRouteAction getAction() {
            return this.action;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAction() != null) {
                data.set("action", om.valueToTree(this.getAction()));
            }
            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute.Jsii$Proxy that = (EnvoyFilterSpecConfigPatchesMatchRouteConfigurationVhostRoute.Jsii$Proxy) o;

            if (this.action != null ? !this.action.equals(that.action) : that.action != null) return false;
            return this.name != null ? this.name.equals(that.name) : that.name == null;
        }

        @Override
        public final int hashCode() {
            int result = this.action != null ? this.action.hashCode() : 0;
            result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
            return result;
        }
    }
}
