package imports.k8s;

/**
 * Sysctl defines a kernel parameter to be set.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.366Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.Sysctl")
@software.amazon.jsii.Jsii.Proxy(Sysctl.Jsii$Proxy.class)
public interface Sysctl extends software.amazon.jsii.JsiiSerializable {

    /**
     * Name of a property to set.
     */
    @org.jetbrains.annotations.NotNull String getName();

    /**
     * Value of a property to set.
     */
    @org.jetbrains.annotations.NotNull String getValue();

    /**
     * @return a {@link Builder} of {@link Sysctl}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link Sysctl}
     */
    public static final class Builder implements software.amazon.jsii.Builder<Sysctl> {
        String name;
        String value;

        /**
         * Sets the value of {@link Sysctl#getName}
         * @param name Name of a property to set. This parameter is required.
         * @return {@code this}
         */
        public Builder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link Sysctl#getValue}
         * @param value Value of a property to set. This parameter is required.
         * @return {@code this}
         */
        public Builder value(String value) {
            this.value = value;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link Sysctl}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public Sysctl build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link Sysctl}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements Sysctl {
        private final String name;
        private final String value;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(String.class));
            this.value = software.amazon.jsii.Kernel.get(this, "value", software.amazon.jsii.NativeType.forClass(String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.name = java.util.Objects.requireNonNull(builder.name, "name is required");
            this.value = java.util.Objects.requireNonNull(builder.value, "value is required");
        }

        @Override
        public final String getName() {
            return this.name;
        }

        @Override
        public final String getValue() {
            return this.value;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            data.set("name", om.valueToTree(this.getName()));
            data.set("value", om.valueToTree(this.getValue()));

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("k8s.Sysctl"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Jsii$Proxy that = (Jsii$Proxy) o;

            if (!name.equals(that.name)) return false;
            return this.value.equals(that.value);
        }

        @Override
        public final int hashCode() {
            int result = this.name.hashCode();
            result = 31 * result + (this.value.hashCode());
            return result;
        }
    }
}
