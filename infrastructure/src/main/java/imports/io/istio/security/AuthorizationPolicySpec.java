package imports.io.istio.security;

/**
 * Configuration for access control on workloads.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/security/authorization-policy.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.453Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.AuthorizationPolicySpec")
@software.amazon.jsii.Jsii.Proxy(AuthorizationPolicySpec.Jsii$Proxy.class)
public interface AuthorizationPolicySpec extends software.amazon.jsii.JsiiSerializable {

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.AuthorizationPolicySpecAction getAction() {
        return null;
    }

    /**
     * Specifies detailed configuration of the CUSTOM action.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.AuthorizationPolicySpecProvider getProvider() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.security.AuthorizationPolicySpecRules> getRules() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.AuthorizationPolicySpecSelector getSelector() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link AuthorizationPolicySpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link AuthorizationPolicySpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<AuthorizationPolicySpec> {
        imports.io.istio.security.AuthorizationPolicySpecAction action;
        imports.io.istio.security.AuthorizationPolicySpecProvider provider;
        java.util.List<imports.io.istio.security.AuthorizationPolicySpecRules> rules;
        imports.io.istio.security.AuthorizationPolicySpecSelector selector;

        /**
         * Sets the value of {@link AuthorizationPolicySpec#getAction}
         * @param action Optional.
         * @return {@code this}
         */
        public Builder action(imports.io.istio.security.AuthorizationPolicySpecAction action) {
            this.action = action;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpec#getProvider}
         * @param provider Specifies detailed configuration of the CUSTOM action.
         * @return {@code this}
         */
        public Builder provider(imports.io.istio.security.AuthorizationPolicySpecProvider provider) {
            this.provider = provider;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpec#getRules}
         * @param rules Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder rules(java.util.List<? extends imports.io.istio.security.AuthorizationPolicySpecRules> rules) {
            this.rules = (java.util.List<imports.io.istio.security.AuthorizationPolicySpecRules>)rules;
            return this;
        }

        /**
         * Sets the value of {@link AuthorizationPolicySpec#getSelector}
         * @param selector Optional.
         * @return {@code this}
         */
        public Builder selector(imports.io.istio.security.AuthorizationPolicySpecSelector selector) {
            this.selector = selector;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link AuthorizationPolicySpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public AuthorizationPolicySpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link AuthorizationPolicySpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements AuthorizationPolicySpec {
        private final imports.io.istio.security.AuthorizationPolicySpecAction action;
        private final imports.io.istio.security.AuthorizationPolicySpecProvider provider;
        private final java.util.List<imports.io.istio.security.AuthorizationPolicySpecRules> rules;
        private final imports.io.istio.security.AuthorizationPolicySpecSelector selector;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.action = software.amazon.jsii.Kernel.get(this, "action", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpecAction.class));
            this.provider = software.amazon.jsii.Kernel.get(this, "provider", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpecProvider.class));
            this.rules = software.amazon.jsii.Kernel.get(this, "rules", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpecRules.class)));
            this.selector = software.amazon.jsii.Kernel.get(this, "selector", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.AuthorizationPolicySpecSelector.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.action = builder.action;
            this.provider = builder.provider;
            this.rules = (java.util.List<imports.io.istio.security.AuthorizationPolicySpecRules>)builder.rules;
            this.selector = builder.selector;
        }

        @Override
        public final imports.io.istio.security.AuthorizationPolicySpecAction getAction() {
            return this.action;
        }

        @Override
        public final imports.io.istio.security.AuthorizationPolicySpecProvider getProvider() {
            return this.provider;
        }

        @Override
        public final java.util.List<imports.io.istio.security.AuthorizationPolicySpecRules> getRules() {
            return this.rules;
        }

        @Override
        public final imports.io.istio.security.AuthorizationPolicySpecSelector getSelector() {
            return this.selector;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAction() != null) {
                data.set("action", om.valueToTree(this.getAction()));
            }
            if (this.getProvider() != null) {
                data.set("provider", om.valueToTree(this.getProvider()));
            }
            if (this.getRules() != null) {
                data.set("rules", om.valueToTree(this.getRules()));
            }
            if (this.getSelector() != null) {
                data.set("selector", om.valueToTree(this.getSelector()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.AuthorizationPolicySpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            AuthorizationPolicySpec.Jsii$Proxy that = (AuthorizationPolicySpec.Jsii$Proxy) o;

            if (this.action != null ? !this.action.equals(that.action) : that.action != null) return false;
            if (this.provider != null ? !this.provider.equals(that.provider) : that.provider != null) return false;
            if (this.rules != null ? !this.rules.equals(that.rules) : that.rules != null) return false;
            return this.selector != null ? this.selector.equals(that.selector) : that.selector == null;
        }

        @Override
        public final int hashCode() {
            int result = this.action != null ? this.action.hashCode() : 0;
            result = 31 * result + (this.provider != null ? this.provider.hashCode() : 0);
            result = 31 * result + (this.rules != null ? this.rules.hashCode() : 0);
            result = 31 * result + (this.selector != null ? this.selector.hashCode() : 0);
            return result;
        }
    }
}
