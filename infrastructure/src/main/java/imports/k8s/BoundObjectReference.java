package imports.k8s;

/**
 * BoundObjectReference is a reference to an object that a token is bound to.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.154Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.BoundObjectReference")
@software.amazon.jsii.Jsii.Proxy(BoundObjectReference.Jsii$Proxy.class)
public interface BoundObjectReference extends software.amazon.jsii.JsiiSerializable {

    /**
     * API version of the referent.
     */
    default @org.jetbrains.annotations.Nullable String getApiVersion() {
        return null;
    }

    /**
     * Kind of the referent.
     * <p>
     * Valid kinds are 'Pod' and 'Secret'.
     */
    default @org.jetbrains.annotations.Nullable String getKind() {
        return null;
    }

    /**
     * Name of the referent.
     */
    default @org.jetbrains.annotations.Nullable String getName() {
        return null;
    }

    /**
     * UID of the referent.
     */
    default @org.jetbrains.annotations.Nullable String getUid() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link BoundObjectReference}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link BoundObjectReference}
     */
    public static final class Builder implements software.amazon.jsii.Builder<BoundObjectReference> {
        String apiVersion;
        String kind;
        String name;
        String uid;

        /**
         * Sets the value of {@link BoundObjectReference#getApiVersion}
         * @param apiVersion API version of the referent.
         * @return {@code this}
         */
        public Builder apiVersion(String apiVersion) {
            this.apiVersion = apiVersion;
            return this;
        }

        /**
         * Sets the value of {@link BoundObjectReference#getKind}
         * @param kind Kind of the referent.
         *             Valid kinds are 'Pod' and 'Secret'.
         * @return {@code this}
         */
        public Builder kind(String kind) {
            this.kind = kind;
            return this;
        }

        /**
         * Sets the value of {@link BoundObjectReference#getName}
         * @param name Name of the referent.
         * @return {@code this}
         */
        public Builder name(String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link BoundObjectReference#getUid}
         * @param uid UID of the referent.
         * @return {@code this}
         */
        public Builder uid(String uid) {
            this.uid = uid;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link BoundObjectReference}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public BoundObjectReference build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link BoundObjectReference}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements BoundObjectReference {
        private final String apiVersion;
        private final String kind;
        private final String name;
        private final String uid;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.apiVersion = software.amazon.jsii.Kernel.get(this, "apiVersion", software.amazon.jsii.NativeType.forClass(String.class));
            this.kind = software.amazon.jsii.Kernel.get(this, "kind", software.amazon.jsii.NativeType.forClass(String.class));
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(String.class));
            this.uid = software.amazon.jsii.Kernel.get(this, "uid", software.amazon.jsii.NativeType.forClass(String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.apiVersion = builder.apiVersion;
            this.kind = builder.kind;
            this.name = builder.name;
            this.uid = builder.uid;
        }

        @Override
        public final String getApiVersion() {
            return this.apiVersion;
        }

        @Override
        public final String getKind() {
            return this.kind;
        }

        @Override
        public final String getName() {
            return this.name;
        }

        @Override
        public final String getUid() {
            return this.uid;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getApiVersion() != null) {
                data.set("apiVersion", om.valueToTree(this.getApiVersion()));
            }
            if (this.getKind() != null) {
                data.set("kind", om.valueToTree(this.getKind()));
            }
            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getUid() != null) {
                data.set("uid", om.valueToTree(this.getUid()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("k8s.BoundObjectReference"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Jsii$Proxy that = (Jsii$Proxy) o;

            if (this.apiVersion != null ? !this.apiVersion.equals(that.apiVersion) : that.apiVersion != null) return false;
            if (this.kind != null ? !this.kind.equals(that.kind) : that.kind != null) return false;
            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            return this.uid != null ? this.uid.equals(that.uid) : that.uid == null;
        }

        @Override
        public final int hashCode() {
            int result = this.apiVersion != null ? this.apiVersion.hashCode() : 0;
            result = 31 * result + (this.kind != null ? this.kind.hashCode() : 0);
            result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
            result = 31 * result + (this.uid != null ? this.uid.hashCode() : 0);
            return result;
        }
    }
}
