package imports.k8s;

/**
 * IDRange provides a min/max of an allowed range of IDs.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.206Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.IdRangeV1Beta1")
@software.amazon.jsii.Jsii.Proxy(IdRangeV1Beta1.Jsii$Proxy.class)
public interface IdRangeV1Beta1 extends software.amazon.jsii.JsiiSerializable {

    /**
     * max is the end of the range, inclusive.
     */
    @org.jetbrains.annotations.NotNull Number getMax();

    /**
     * min is the start of the range, inclusive.
     */
    @org.jetbrains.annotations.NotNull Number getMin();

    /**
     * @return a {@link Builder} of {@link IdRangeV1Beta1}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link IdRangeV1Beta1}
     */
    public static final class Builder implements software.amazon.jsii.Builder<IdRangeV1Beta1> {
        Number max;
        Number min;

        /**
         * Sets the value of {@link IdRangeV1Beta1#getMax}
         * @param max max is the end of the range, inclusive. This parameter is required.
         * @return {@code this}
         */
        public Builder max(Number max) {
            this.max = max;
            return this;
        }

        /**
         * Sets the value of {@link IdRangeV1Beta1#getMin}
         * @param min min is the start of the range, inclusive. This parameter is required.
         * @return {@code this}
         */
        public Builder min(Number min) {
            this.min = min;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link IdRangeV1Beta1}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public IdRangeV1Beta1 build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link IdRangeV1Beta1}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements IdRangeV1Beta1 {
        private final Number max;
        private final Number min;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.max = software.amazon.jsii.Kernel.get(this, "max", software.amazon.jsii.NativeType.forClass(Number.class));
            this.min = software.amazon.jsii.Kernel.get(this, "min", software.amazon.jsii.NativeType.forClass(Number.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.max = java.util.Objects.requireNonNull(builder.max, "max is required");
            this.min = java.util.Objects.requireNonNull(builder.min, "min is required");
        }

        @Override
        public final Number getMax() {
            return this.max;
        }

        @Override
        public final Number getMin() {
            return this.min;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            data.set("max", om.valueToTree(this.getMax()));
            data.set("min", om.valueToTree(this.getMin()));

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("k8s.IdRangeV1Beta1"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Jsii$Proxy that = (Jsii$Proxy) o;

            if (!max.equals(that.max)) return false;
            return this.min.equals(that.min);
        }

        @Override
        public final int hashCode() {
            int result = this.max.hashCode();
            result = 31 * result + (this.min.hashCode());
            return result;
        }
    }
}
