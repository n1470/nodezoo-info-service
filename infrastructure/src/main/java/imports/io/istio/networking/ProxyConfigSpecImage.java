package imports.io.istio.networking;

/**
 * Specifies the details of the proxy image.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.460Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.ProxyConfigSpecImage")
@software.amazon.jsii.Jsii.Proxy(ProxyConfigSpecImage.Jsii$Proxy.class)
public interface ProxyConfigSpecImage extends software.amazon.jsii.JsiiSerializable {

    /**
     * The image type of the image.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getImageType() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link ProxyConfigSpecImage}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link ProxyConfigSpecImage}
     */
    public static final class Builder implements software.amazon.jsii.Builder<ProxyConfigSpecImage> {
        java.lang.String imageType;

        /**
         * Sets the value of {@link ProxyConfigSpecImage#getImageType}
         * @param imageType The image type of the image.
         * @return {@code this}
         */
        public Builder imageType(java.lang.String imageType) {
            this.imageType = imageType;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link ProxyConfigSpecImage}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public ProxyConfigSpecImage build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link ProxyConfigSpecImage}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements ProxyConfigSpecImage {
        private final java.lang.String imageType;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.imageType = software.amazon.jsii.Kernel.get(this, "imageType", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.imageType = builder.imageType;
        }

        @Override
        public final java.lang.String getImageType() {
            return this.imageType;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getImageType() != null) {
                data.set("imageType", om.valueToTree(this.getImageType()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.ProxyConfigSpecImage"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            ProxyConfigSpecImage.Jsii$Proxy that = (ProxyConfigSpecImage.Jsii$Proxy) o;

            return this.imageType != null ? this.imageType.equals(that.imageType) : that.imageType == null;
        }

        @Override
        public final int hashCode() {
            int result = this.imageType != null ? this.imageType.hashCode() : 0;
            return result;
        }
    }
}
