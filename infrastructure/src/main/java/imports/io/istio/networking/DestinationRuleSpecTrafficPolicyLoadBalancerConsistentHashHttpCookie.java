package imports.io.istio.networking;

/**
 * Hash based on HTTP cookie.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.437Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie.Jsii$Proxy.class)
public interface DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie extends software.amazon.jsii.JsiiSerializable {

    /**
     * Name of the cookie.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * Path to set for the cookie.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getPath() {
        return null;
    }

    /**
     * Lifetime of the cookie.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getTtl() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie> {
        java.lang.String name;
        java.lang.String path;
        java.lang.String ttl;

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie#getName}
         * @param name Name of the cookie.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie#getPath}
         * @param path Path to set for the cookie.
         * @return {@code this}
         */
        public Builder path(java.lang.String path) {
            this.path = path;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie#getTtl}
         * @param ttl Lifetime of the cookie.
         * @return {@code this}
         */
        public Builder ttl(java.lang.String ttl) {
            this.ttl = ttl;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie {
        private final java.lang.String name;
        private final java.lang.String path;
        private final java.lang.String ttl;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.path = software.amazon.jsii.Kernel.get(this, "path", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.ttl = software.amazon.jsii.Kernel.get(this, "ttl", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.name = builder.name;
            this.path = builder.path;
            this.ttl = builder.ttl;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final java.lang.String getPath() {
            return this.path;
        }

        @Override
        public final java.lang.String getTtl() {
            return this.ttl;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getPath() != null) {
                data.set("path", om.valueToTree(this.getPath()));
            }
            if (this.getTtl() != null) {
                data.set("ttl", om.valueToTree(this.getTtl()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie.Jsii$Proxy that = (DestinationRuleSpecTrafficPolicyLoadBalancerConsistentHashHttpCookie.Jsii$Proxy) o;

            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            if (this.path != null ? !this.path.equals(that.path) : that.path != null) return false;
            return this.ttl != null ? this.ttl.equals(that.ttl) : that.ttl == null;
        }

        @Override
        public final int hashCode() {
            int result = this.name != null ? this.name.hashCode() : 0;
            result = 31 * result + (this.path != null ? this.path.hashCode() : 0);
            result = 31 * result + (this.ttl != null ? this.ttl.hashCode() : 0);
            return result;
        }
    }
}
