package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.431Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection.Jsii$Proxy.class)
public interface DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection extends software.amazon.jsii.JsiiSerializable {

    /**
     * Minimum ejection duration.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getBaseEjectionTime() {
        return null;
    }

    /**
     * Number of 5xx errors before a host is ejected from the connection pool.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getConsecutive5XxErrors() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getConsecutiveErrors() {
        return null;
    }

    /**
     * Number of gateway errors before a host is ejected from the connection pool.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getConsecutiveGatewayErrors() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getConsecutiveLocalOriginFailures() {
        return null;
    }

    /**
     * Time interval between ejection sweep analysis.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getInterval() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getMaxEjectionPercent() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getMinHealthPercent() {
        return null;
    }

    /**
     * Determines whether to distinguish local origin failures from external errors.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getSplitExternalLocalOriginErrors() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection> {
        java.lang.String baseEjectionTime;
        java.lang.Number consecutive5XxErrors;
        java.lang.Number consecutiveErrors;
        java.lang.Number consecutiveGatewayErrors;
        java.lang.Number consecutiveLocalOriginFailures;
        java.lang.String interval;
        java.lang.Number maxEjectionPercent;
        java.lang.Number minHealthPercent;
        java.lang.Boolean splitExternalLocalOriginErrors;

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection#getBaseEjectionTime}
         * @param baseEjectionTime Minimum ejection duration.
         * @return {@code this}
         */
        public Builder baseEjectionTime(java.lang.String baseEjectionTime) {
            this.baseEjectionTime = baseEjectionTime;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection#getConsecutive5XxErrors}
         * @param consecutive5XxErrors Number of 5xx errors before a host is ejected from the connection pool.
         * @return {@code this}
         */
        public Builder consecutive5XxErrors(java.lang.Number consecutive5XxErrors) {
            this.consecutive5XxErrors = consecutive5XxErrors;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection#getConsecutiveErrors}
         * @param consecutiveErrors the value to be set.
         * @return {@code this}
         */
        public Builder consecutiveErrors(java.lang.Number consecutiveErrors) {
            this.consecutiveErrors = consecutiveErrors;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection#getConsecutiveGatewayErrors}
         * @param consecutiveGatewayErrors Number of gateway errors before a host is ejected from the connection pool.
         * @return {@code this}
         */
        public Builder consecutiveGatewayErrors(java.lang.Number consecutiveGatewayErrors) {
            this.consecutiveGatewayErrors = consecutiveGatewayErrors;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection#getConsecutiveLocalOriginFailures}
         * @param consecutiveLocalOriginFailures the value to be set.
         * @return {@code this}
         */
        public Builder consecutiveLocalOriginFailures(java.lang.Number consecutiveLocalOriginFailures) {
            this.consecutiveLocalOriginFailures = consecutiveLocalOriginFailures;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection#getInterval}
         * @param interval Time interval between ejection sweep analysis.
         * @return {@code this}
         */
        public Builder interval(java.lang.String interval) {
            this.interval = interval;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection#getMaxEjectionPercent}
         * @param maxEjectionPercent the value to be set.
         * @return {@code this}
         */
        public Builder maxEjectionPercent(java.lang.Number maxEjectionPercent) {
            this.maxEjectionPercent = maxEjectionPercent;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection#getMinHealthPercent}
         * @param minHealthPercent the value to be set.
         * @return {@code this}
         */
        public Builder minHealthPercent(java.lang.Number minHealthPercent) {
            this.minHealthPercent = minHealthPercent;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection#getSplitExternalLocalOriginErrors}
         * @param splitExternalLocalOriginErrors Determines whether to distinguish local origin failures from external errors.
         * @return {@code this}
         */
        public Builder splitExternalLocalOriginErrors(java.lang.Boolean splitExternalLocalOriginErrors) {
            this.splitExternalLocalOriginErrors = splitExternalLocalOriginErrors;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection {
        private final java.lang.String baseEjectionTime;
        private final java.lang.Number consecutive5XxErrors;
        private final java.lang.Number consecutiveErrors;
        private final java.lang.Number consecutiveGatewayErrors;
        private final java.lang.Number consecutiveLocalOriginFailures;
        private final java.lang.String interval;
        private final java.lang.Number maxEjectionPercent;
        private final java.lang.Number minHealthPercent;
        private final java.lang.Boolean splitExternalLocalOriginErrors;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.baseEjectionTime = software.amazon.jsii.Kernel.get(this, "baseEjectionTime", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.consecutive5XxErrors = software.amazon.jsii.Kernel.get(this, "consecutive5XxErrors", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.consecutiveErrors = software.amazon.jsii.Kernel.get(this, "consecutiveErrors", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.consecutiveGatewayErrors = software.amazon.jsii.Kernel.get(this, "consecutiveGatewayErrors", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.consecutiveLocalOriginFailures = software.amazon.jsii.Kernel.get(this, "consecutiveLocalOriginFailures", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.interval = software.amazon.jsii.Kernel.get(this, "interval", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.maxEjectionPercent = software.amazon.jsii.Kernel.get(this, "maxEjectionPercent", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.minHealthPercent = software.amazon.jsii.Kernel.get(this, "minHealthPercent", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.splitExternalLocalOriginErrors = software.amazon.jsii.Kernel.get(this, "splitExternalLocalOriginErrors", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.baseEjectionTime = builder.baseEjectionTime;
            this.consecutive5XxErrors = builder.consecutive5XxErrors;
            this.consecutiveErrors = builder.consecutiveErrors;
            this.consecutiveGatewayErrors = builder.consecutiveGatewayErrors;
            this.consecutiveLocalOriginFailures = builder.consecutiveLocalOriginFailures;
            this.interval = builder.interval;
            this.maxEjectionPercent = builder.maxEjectionPercent;
            this.minHealthPercent = builder.minHealthPercent;
            this.splitExternalLocalOriginErrors = builder.splitExternalLocalOriginErrors;
        }

        @Override
        public final java.lang.String getBaseEjectionTime() {
            return this.baseEjectionTime;
        }

        @Override
        public final java.lang.Number getConsecutive5XxErrors() {
            return this.consecutive5XxErrors;
        }

        @Override
        public final java.lang.Number getConsecutiveErrors() {
            return this.consecutiveErrors;
        }

        @Override
        public final java.lang.Number getConsecutiveGatewayErrors() {
            return this.consecutiveGatewayErrors;
        }

        @Override
        public final java.lang.Number getConsecutiveLocalOriginFailures() {
            return this.consecutiveLocalOriginFailures;
        }

        @Override
        public final java.lang.String getInterval() {
            return this.interval;
        }

        @Override
        public final java.lang.Number getMaxEjectionPercent() {
            return this.maxEjectionPercent;
        }

        @Override
        public final java.lang.Number getMinHealthPercent() {
            return this.minHealthPercent;
        }

        @Override
        public final java.lang.Boolean getSplitExternalLocalOriginErrors() {
            return this.splitExternalLocalOriginErrors;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getBaseEjectionTime() != null) {
                data.set("baseEjectionTime", om.valueToTree(this.getBaseEjectionTime()));
            }
            if (this.getConsecutive5XxErrors() != null) {
                data.set("consecutive5XxErrors", om.valueToTree(this.getConsecutive5XxErrors()));
            }
            if (this.getConsecutiveErrors() != null) {
                data.set("consecutiveErrors", om.valueToTree(this.getConsecutiveErrors()));
            }
            if (this.getConsecutiveGatewayErrors() != null) {
                data.set("consecutiveGatewayErrors", om.valueToTree(this.getConsecutiveGatewayErrors()));
            }
            if (this.getConsecutiveLocalOriginFailures() != null) {
                data.set("consecutiveLocalOriginFailures", om.valueToTree(this.getConsecutiveLocalOriginFailures()));
            }
            if (this.getInterval() != null) {
                data.set("interval", om.valueToTree(this.getInterval()));
            }
            if (this.getMaxEjectionPercent() != null) {
                data.set("maxEjectionPercent", om.valueToTree(this.getMaxEjectionPercent()));
            }
            if (this.getMinHealthPercent() != null) {
                data.set("minHealthPercent", om.valueToTree(this.getMinHealthPercent()));
            }
            if (this.getSplitExternalLocalOriginErrors() != null) {
                data.set("splitExternalLocalOriginErrors", om.valueToTree(this.getSplitExternalLocalOriginErrors()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection.Jsii$Proxy that = (DestinationRuleSpecSubsetsTrafficPolicyOutlierDetection.Jsii$Proxy) o;

            if (this.baseEjectionTime != null ? !this.baseEjectionTime.equals(that.baseEjectionTime) : that.baseEjectionTime != null) return false;
            if (this.consecutive5XxErrors != null ? !this.consecutive5XxErrors.equals(that.consecutive5XxErrors) : that.consecutive5XxErrors != null) return false;
            if (this.consecutiveErrors != null ? !this.consecutiveErrors.equals(that.consecutiveErrors) : that.consecutiveErrors != null) return false;
            if (this.consecutiveGatewayErrors != null ? !this.consecutiveGatewayErrors.equals(that.consecutiveGatewayErrors) : that.consecutiveGatewayErrors != null) return false;
            if (this.consecutiveLocalOriginFailures != null ? !this.consecutiveLocalOriginFailures.equals(that.consecutiveLocalOriginFailures) : that.consecutiveLocalOriginFailures != null) return false;
            if (this.interval != null ? !this.interval.equals(that.interval) : that.interval != null) return false;
            if (this.maxEjectionPercent != null ? !this.maxEjectionPercent.equals(that.maxEjectionPercent) : that.maxEjectionPercent != null) return false;
            if (this.minHealthPercent != null ? !this.minHealthPercent.equals(that.minHealthPercent) : that.minHealthPercent != null) return false;
            return this.splitExternalLocalOriginErrors != null ? this.splitExternalLocalOriginErrors.equals(that.splitExternalLocalOriginErrors) : that.splitExternalLocalOriginErrors == null;
        }

        @Override
        public final int hashCode() {
            int result = this.baseEjectionTime != null ? this.baseEjectionTime.hashCode() : 0;
            result = 31 * result + (this.consecutive5XxErrors != null ? this.consecutive5XxErrors.hashCode() : 0);
            result = 31 * result + (this.consecutiveErrors != null ? this.consecutiveErrors.hashCode() : 0);
            result = 31 * result + (this.consecutiveGatewayErrors != null ? this.consecutiveGatewayErrors.hashCode() : 0);
            result = 31 * result + (this.consecutiveLocalOriginFailures != null ? this.consecutiveLocalOriginFailures.hashCode() : 0);
            result = 31 * result + (this.interval != null ? this.interval.hashCode() : 0);
            result = 31 * result + (this.maxEjectionPercent != null ? this.maxEjectionPercent.hashCode() : 0);
            result = 31 * result + (this.minHealthPercent != null ? this.minHealthPercent.hashCode() : 0);
            result = 31 * result + (this.splitExternalLocalOriginErrors != null ? this.splitExternalLocalOriginErrors.hashCode() : 0);
            return result;
        }
    }
}
