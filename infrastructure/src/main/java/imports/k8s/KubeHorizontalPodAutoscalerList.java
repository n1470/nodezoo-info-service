package imports.k8s;

/**
 * list of horizontal pod autoscaler objects.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.262Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.KubeHorizontalPodAutoscalerList")
public class KubeHorizontalPodAutoscalerList extends org.cdk8s.ApiObject {

    protected KubeHorizontalPodAutoscalerList(final software.amazon.jsii.JsiiObjectRef objRef) {
        super(objRef);
    }

    protected KubeHorizontalPodAutoscalerList(final software.amazon.jsii.JsiiObject.InitializationMode initializationMode) {
        super(initializationMode);
    }

    static {
        GVK = software.amazon.jsii.JsiiObject.jsiiStaticGet(KubeHorizontalPodAutoscalerList.class, "GVK", software.amazon.jsii.NativeType.forClass(org.cdk8s.GroupVersionKind.class));
    }

    /**
     * Defines a "io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerList" API object.
     * <p>
     * @param scope the scope in which to define this object. This parameter is required.
     * @param id a scope-local name for the object. This parameter is required.
     * @param props initialization props. This parameter is required.
     */
    public KubeHorizontalPodAutoscalerList(final @org.jetbrains.annotations.NotNull software.constructs.Construct scope, final @org.jetbrains.annotations.NotNull String id, final @org.jetbrains.annotations.NotNull KubeHorizontalPodAutoscalerListProps props) {
        super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
        software.amazon.jsii.JsiiEngine.getInstance().createNewObject(this, new Object[] { java.util.Objects.requireNonNull(scope, "scope is required"), java.util.Objects.requireNonNull(id, "id is required"), java.util.Objects.requireNonNull(props, "props is required") });
    }

    /**
     * Renders a Kubernetes manifest for "io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerList".
     * <p>
     * This can be used to inline resource manifests inside other objects (e.g. as templates).
     * <p>
     * @param props initialization props. This parameter is required.
     */
    public static @org.jetbrains.annotations.NotNull Object manifest(final @org.jetbrains.annotations.NotNull KubeHorizontalPodAutoscalerListProps props) {
        return software.amazon.jsii.JsiiObject.jsiiStaticCall(KubeHorizontalPodAutoscalerList.class, "manifest", software.amazon.jsii.NativeType.forClass(Object.class), new Object[] { java.util.Objects.requireNonNull(props, "props is required") });
    }

    /**
     * Renders the object to Kubernetes JSON.
     */
    @Override
    public @org.jetbrains.annotations.NotNull Object toJson() {
        return software.amazon.jsii.Kernel.call(this, "toJson", software.amazon.jsii.NativeType.forClass(Object.class));
    }

    /**
     * Returns the apiVersion and kind for "io.k8s.api.autoscaling.v1.HorizontalPodAutoscalerList".
     */
    public final static org.cdk8s.GroupVersionKind GVK;

    /**
     * A fluent builder for {@link KubeHorizontalPodAutoscalerList}.
     */
    public static final class Builder implements software.amazon.jsii.Builder<KubeHorizontalPodAutoscalerList> {
        /**
         * @return a new instance of {@link Builder}.
         * @param scope the scope in which to define this object. This parameter is required.
         * @param id a scope-local name for the object. This parameter is required.
         */
        public static Builder create(final software.constructs.Construct scope, final String id) {
            return new Builder(scope, id);
        }

        private final software.constructs.Construct scope;
        private final String id;
        private final KubeHorizontalPodAutoscalerListProps.Builder props;

        private Builder(final software.constructs.Construct scope, final String id) {
            this.scope = scope;
            this.id = id;
            this.props = new KubeHorizontalPodAutoscalerListProps.Builder();
        }

        /**
         * list of horizontal pod autoscaler objects.
         * <p>
         * @return {@code this}
         * @param items list of horizontal pod autoscaler objects. This parameter is required.
         */
        public Builder items(final java.util.List<? extends KubeHorizontalPodAutoscalerProps> items) {
            this.props.items(items);
            return this;
        }

        /**
         * Standard list metadata.
         * <p>
         * @return {@code this}
         * @param metadata Standard list metadata. This parameter is required.
         */
        public Builder metadata(final ListMeta metadata) {
            this.props.metadata(metadata);
            return this;
        }

        /**
         * @returns a newly built instance of {@link KubeHorizontalPodAutoscalerList}.
         */
        @Override
        public KubeHorizontalPodAutoscalerList build() {
            return new KubeHorizontalPodAutoscalerList(
                this.scope,
                this.id,
                this.props.build()
            );
        }
    }
}
