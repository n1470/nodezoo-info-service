package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.463Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.SidecarSpecOutboundTrafficPolicyMode")
public enum SidecarSpecOutboundTrafficPolicyMode {
    /**
     * REGISTRY_ONLY.
     */
    REGISTRY_ONLY,
    /**
     * ALLOW_ANY.
     */
    ALLOW_ANY,
}
