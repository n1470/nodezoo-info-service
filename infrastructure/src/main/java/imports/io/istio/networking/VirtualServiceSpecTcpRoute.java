package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.468Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecTcpRoute")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecTcpRoute.Jsii$Proxy.class)
public interface VirtualServiceSpecTcpRoute extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecTcpRouteDestination getDestination() {
        return null;
    }

    /**
     * Weight specifies the relative proportion of traffic to be forwarded to the destination.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getWeight() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecTcpRoute}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecTcpRoute}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecTcpRoute> {
        imports.io.istio.networking.VirtualServiceSpecTcpRouteDestination destination;
        java.lang.Number weight;

        /**
         * Sets the value of {@link VirtualServiceSpecTcpRoute#getDestination}
         * @param destination the value to be set.
         * @return {@code this}
         */
        public Builder destination(imports.io.istio.networking.VirtualServiceSpecTcpRouteDestination destination) {
            this.destination = destination;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTcpRoute#getWeight}
         * @param weight Weight specifies the relative proportion of traffic to be forwarded to the destination.
         * @return {@code this}
         */
        public Builder weight(java.lang.Number weight) {
            this.weight = weight;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecTcpRoute}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecTcpRoute build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecTcpRoute}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecTcpRoute {
        private final imports.io.istio.networking.VirtualServiceSpecTcpRouteDestination destination;
        private final java.lang.Number weight;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.destination = software.amazon.jsii.Kernel.get(this, "destination", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecTcpRouteDestination.class));
            this.weight = software.amazon.jsii.Kernel.get(this, "weight", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.destination = builder.destination;
            this.weight = builder.weight;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecTcpRouteDestination getDestination() {
            return this.destination;
        }

        @Override
        public final java.lang.Number getWeight() {
            return this.weight;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getDestination() != null) {
                data.set("destination", om.valueToTree(this.getDestination()));
            }
            if (this.getWeight() != null) {
                data.set("weight", om.valueToTree(this.getWeight()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecTcpRoute"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecTcpRoute.Jsii$Proxy that = (VirtualServiceSpecTcpRoute.Jsii$Proxy) o;

            if (this.destination != null ? !this.destination.equals(that.destination) : that.destination != null) return false;
            return this.weight != null ? this.weight.equals(that.weight) : that.weight == null;
        }

        @Override
        public final int hashCode() {
            int result = this.destination != null ? this.destination.hashCode() : 0;
            result = 31 * result + (this.weight != null ? this.weight.hashCode() : 0);
            return result;
        }
    }
}
