package tao.behavior

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorSystem, Behavior}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.util.ByteString
import tao.core.{PackageInfo, PackageQuery}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

object NpmService extends GenericPackageService {

  def apply(baseUrl: String): Behavior[PackageQuery] = Behaviors.receive { (context, message) =>
    receive(
      requestUrl = s"$baseUrl/info/${message.packageName}",
      sourceLabel = "npm",
      context,
      message
    )

    Behaviors.same
  }
}
