package tao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import tao.core.AwsGateway;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

class InfoServiceChartPropsTest {

    private InfoServiceChartProps.Builder createBuilder(String[] args) {
        final var mockAwsGw = mock(AwsGateway.class);
        when(mockAwsGw.fetchAwsAccountId(Mockito.any(AwsCredentialsProvider.class))).thenReturn("123");
        when(mockAwsGw.fetchEcrAuthToken(Mockito.any(AwsCredentialsProvider.class))).thenReturn("pass");
        return InfoServiceChartProps
                .builder(args)
                .withInfraAwsGateway(mockAwsGw);
    }

    @Test
    void testNoArgs() {
        Assertions.assertThrows(Exception.class, () -> createBuilder(new String[] {}).build());
    }

    @Test
    void testWithOnlyArguments() {
        String[] args = {"123.dkr.ecr.us-east-1.amazonaws.com"};
        final var props = createBuilder(args).build();
        assertThat(props.getOutputPath().toString(),
                is(String.join("/", InfoServiceChartProps.DEFAULT_PATH_FRAGMENTS)));
        assertThat(props.getReplicas(), is(equalTo(InfoServiceChartProps.DEFAULT_REPLICAS)));
    }

    @Test
    void testWithOutputDir() {
        final String path = "foo/bar/baz";
        final String[] args = {"--output", path, "123.dkr.ecr.us-east-1.amazonaws.com"};
        final var props = createBuilder(args).build();
        assertThat(props.getOutputPath().toString(), is(path));
        assertThat(props.getReplicas(), is(equalTo(InfoServiceChartProps.DEFAULT_REPLICAS)));
    }

    @Test
    void testWithSingleLevelOutputDir() {
        final String path = "dist";
        final String[] args = {"--output", path, "123.dkr.ecr.us-east-1.amazonaws.com"};
        final var props = createBuilder(args).build();
        assertThat(props.getOutputPath().toString(), is(path));
    }

    @Test
    void testWithReplicas() {
        String[] args = {"--output", "fu/bar/baz", "--count", "7", "123.dkr.ecr.us-east-1.amazonaws.com"};
        final var props = createBuilder(args).build();
        assertThat(props.getOutputPath().toString(), is("fu/bar/baz"));
        assertThat(props.getReplicas(), is(equalTo(7)));
    }
}