package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.464Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpFaultAbort")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpFaultAbort.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpFaultAbort extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getGrpcStatus() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getHttp2Error() {
        return null;
    }

    /**
     * HTTP status code to use to abort the Http request.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getHttpStatus() {
        return null;
    }

    /**
     * Percentage of requests to be aborted with the error code provided.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpFaultAbortPercentage getPercentage() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpFaultAbort}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpFaultAbort}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpFaultAbort> {
        java.lang.String grpcStatus;
        java.lang.String http2Error;
        java.lang.Number httpStatus;
        imports.io.istio.networking.VirtualServiceSpecHttpFaultAbortPercentage percentage;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFaultAbort#getGrpcStatus}
         * @param grpcStatus the value to be set.
         * @return {@code this}
         */
        public Builder grpcStatus(java.lang.String grpcStatus) {
            this.grpcStatus = grpcStatus;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFaultAbort#getHttp2Error}
         * @param http2Error the value to be set.
         * @return {@code this}
         */
        public Builder http2Error(java.lang.String http2Error) {
            this.http2Error = http2Error;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFaultAbort#getHttpStatus}
         * @param httpStatus HTTP status code to use to abort the Http request.
         * @return {@code this}
         */
        public Builder httpStatus(java.lang.Number httpStatus) {
            this.httpStatus = httpStatus;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFaultAbort#getPercentage}
         * @param percentage Percentage of requests to be aborted with the error code provided.
         * @return {@code this}
         */
        public Builder percentage(imports.io.istio.networking.VirtualServiceSpecHttpFaultAbortPercentage percentage) {
            this.percentage = percentage;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpFaultAbort}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpFaultAbort build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpFaultAbort}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpFaultAbort {
        private final java.lang.String grpcStatus;
        private final java.lang.String http2Error;
        private final java.lang.Number httpStatus;
        private final imports.io.istio.networking.VirtualServiceSpecHttpFaultAbortPercentage percentage;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.grpcStatus = software.amazon.jsii.Kernel.get(this, "grpcStatus", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.http2Error = software.amazon.jsii.Kernel.get(this, "http2Error", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.httpStatus = software.amazon.jsii.Kernel.get(this, "httpStatus", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.percentage = software.amazon.jsii.Kernel.get(this, "percentage", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpFaultAbortPercentage.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.grpcStatus = builder.grpcStatus;
            this.http2Error = builder.http2Error;
            this.httpStatus = builder.httpStatus;
            this.percentage = builder.percentage;
        }

        @Override
        public final java.lang.String getGrpcStatus() {
            return this.grpcStatus;
        }

        @Override
        public final java.lang.String getHttp2Error() {
            return this.http2Error;
        }

        @Override
        public final java.lang.Number getHttpStatus() {
            return this.httpStatus;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpFaultAbortPercentage getPercentage() {
            return this.percentage;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getGrpcStatus() != null) {
                data.set("grpcStatus", om.valueToTree(this.getGrpcStatus()));
            }
            if (this.getHttp2Error() != null) {
                data.set("http2Error", om.valueToTree(this.getHttp2Error()));
            }
            if (this.getHttpStatus() != null) {
                data.set("httpStatus", om.valueToTree(this.getHttpStatus()));
            }
            if (this.getPercentage() != null) {
                data.set("percentage", om.valueToTree(this.getPercentage()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpFaultAbort"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpFaultAbort.Jsii$Proxy that = (VirtualServiceSpecHttpFaultAbort.Jsii$Proxy) o;

            if (this.grpcStatus != null ? !this.grpcStatus.equals(that.grpcStatus) : that.grpcStatus != null) return false;
            if (this.http2Error != null ? !this.http2Error.equals(that.http2Error) : that.http2Error != null) return false;
            if (this.httpStatus != null ? !this.httpStatus.equals(that.httpStatus) : that.httpStatus != null) return false;
            return this.percentage != null ? this.percentage.equals(that.percentage) : that.percentage == null;
        }

        @Override
        public final int hashCode() {
            int result = this.grpcStatus != null ? this.grpcStatus.hashCode() : 0;
            result = 31 * result + (this.http2Error != null ? this.http2Error.hashCode() : 0);
            result = 31 * result + (this.httpStatus != null ? this.httpStatus.hashCode() : 0);
            result = 31 * result + (this.percentage != null ? this.percentage.hashCode() : 0);
            return result;
        }
    }
}
