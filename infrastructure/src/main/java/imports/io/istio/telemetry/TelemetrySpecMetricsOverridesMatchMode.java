package imports.io.istio.telemetry;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.288Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecMetricsOverridesMatchMode")
public enum TelemetrySpecMetricsOverridesMatchMode {
    /**
     * CLIENT_AND_SERVER.
     */
    CLIENT_AND_SERVER,
    /**
     * CLIENT.
     */
    CLIENT,
    /**
     * SERVER.
     */
    SERVER,
}
