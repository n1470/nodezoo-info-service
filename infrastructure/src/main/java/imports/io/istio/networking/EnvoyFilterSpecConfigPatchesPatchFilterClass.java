package imports.io.istio.networking;

/**
 * Determines the filter insertion order.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.443Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesPatchFilterClass")
public enum EnvoyFilterSpecConfigPatchesPatchFilterClass {
    /**
     * UNSPECIFIED.
     */
    UNSPECIFIED,
    /**
     * AUTHN.
     */
    AUTHN,
    /**
     * AUTHZ.
     */
    AUTHZ,
    /**
     * STATS.
     */
    STATS,
}
