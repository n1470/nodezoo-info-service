package imports.io.istio.networking;

/**
 * Determines how the patch should be applied.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.443Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.EnvoyFilterSpecConfigPatchesPatchOperation")
public enum EnvoyFilterSpecConfigPatchesPatchOperation {
    /**
     * INVALID.
     */
    INVALID,
    /**
     * MERGE.
     */
    MERGE,
    /**
     * ADD.
     */
    ADD,
    /**
     * REMOVE.
     */
    REMOVE,
    /**
     * INSERT_BEFORE.
     */
    INSERT_BEFORE,
    /**
     * INSERT_AFTER.
     */
    INSERT_AFTER,
    /**
     * INSERT_FIRST.
     */
    INSERT_FIRST,
    /**
     * REPLACE.
     */
    REPLACE,
}
