package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.427Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecSubsets")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecSubsets.Jsii$Proxy.class)
public interface DestinationRuleSpecSubsets extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getLabels() {
        return null;
    }

    /**
     * Name of the subset.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     * Traffic policies that apply to this subset.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicy getTrafficPolicy() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecSubsets}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecSubsets}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecSubsets> {
        java.util.Map<java.lang.String, java.lang.String> labels;
        java.lang.String name;
        imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicy trafficPolicy;

        /**
         * Sets the value of {@link DestinationRuleSpecSubsets#getLabels}
         * @param labels the value to be set.
         * @return {@code this}
         */
        public Builder labels(java.util.Map<java.lang.String, java.lang.String> labels) {
            this.labels = labels;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsets#getName}
         * @param name Name of the subset.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecSubsets#getTrafficPolicy}
         * @param trafficPolicy Traffic policies that apply to this subset.
         * @return {@code this}
         */
        public Builder trafficPolicy(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicy trafficPolicy) {
            this.trafficPolicy = trafficPolicy;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecSubsets}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecSubsets build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecSubsets}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecSubsets {
        private final java.util.Map<java.lang.String, java.lang.String> labels;
        private final java.lang.String name;
        private final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicy trafficPolicy;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.labels = software.amazon.jsii.Kernel.get(this, "labels", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.trafficPolicy = software.amazon.jsii.Kernel.get(this, "trafficPolicy", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicy.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.labels = builder.labels;
            this.name = builder.name;
            this.trafficPolicy = builder.trafficPolicy;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getLabels() {
            return this.labels;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecSubsetsTrafficPolicy getTrafficPolicy() {
            return this.trafficPolicy;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getLabels() != null) {
                data.set("labels", om.valueToTree(this.getLabels()));
            }
            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getTrafficPolicy() != null) {
                data.set("trafficPolicy", om.valueToTree(this.getTrafficPolicy()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecSubsets"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecSubsets.Jsii$Proxy that = (DestinationRuleSpecSubsets.Jsii$Proxy) o;

            if (this.labels != null ? !this.labels.equals(that.labels) : that.labels != null) return false;
            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            return this.trafficPolicy != null ? this.trafficPolicy.equals(that.trafficPolicy) : that.trafficPolicy == null;
        }

        @Override
        public final int hashCode() {
            int result = this.labels != null ? this.labels.hashCode() : 0;
            result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
            result = 31 * result + (this.trafficPolicy != null ? this.trafficPolicy.hashCode() : 0);
            return result;
        }
    }
}
