package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.464Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpFaultDelay")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpFaultDelay.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpFaultDelay extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getExponentialDelay() {
        return null;
    }

    /**
     * Add a fixed delay before forwarding the request.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getFixedDelay() {
        return null;
    }

    /**
     * Percentage of requests on which the delay will be injected (0-100).
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPercent() {
        return null;
    }

    /**
     * Percentage of requests on which the delay will be injected.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpFaultDelayPercentage getPercentage() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpFaultDelay}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpFaultDelay}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpFaultDelay> {
        java.lang.String exponentialDelay;
        java.lang.String fixedDelay;
        java.lang.Number percent;
        imports.io.istio.networking.VirtualServiceSpecHttpFaultDelayPercentage percentage;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFaultDelay#getExponentialDelay}
         * @param exponentialDelay the value to be set.
         * @return {@code this}
         */
        public Builder exponentialDelay(java.lang.String exponentialDelay) {
            this.exponentialDelay = exponentialDelay;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFaultDelay#getFixedDelay}
         * @param fixedDelay Add a fixed delay before forwarding the request.
         * @return {@code this}
         */
        public Builder fixedDelay(java.lang.String fixedDelay) {
            this.fixedDelay = fixedDelay;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFaultDelay#getPercent}
         * @param percent Percentage of requests on which the delay will be injected (0-100).
         * @return {@code this}
         */
        public Builder percent(java.lang.Number percent) {
            this.percent = percent;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFaultDelay#getPercentage}
         * @param percentage Percentage of requests on which the delay will be injected.
         * @return {@code this}
         */
        public Builder percentage(imports.io.istio.networking.VirtualServiceSpecHttpFaultDelayPercentage percentage) {
            this.percentage = percentage;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpFaultDelay}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpFaultDelay build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpFaultDelay}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpFaultDelay {
        private final java.lang.String exponentialDelay;
        private final java.lang.String fixedDelay;
        private final java.lang.Number percent;
        private final imports.io.istio.networking.VirtualServiceSpecHttpFaultDelayPercentage percentage;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.exponentialDelay = software.amazon.jsii.Kernel.get(this, "exponentialDelay", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.fixedDelay = software.amazon.jsii.Kernel.get(this, "fixedDelay", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.percent = software.amazon.jsii.Kernel.get(this, "percent", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.percentage = software.amazon.jsii.Kernel.get(this, "percentage", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpFaultDelayPercentage.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.exponentialDelay = builder.exponentialDelay;
            this.fixedDelay = builder.fixedDelay;
            this.percent = builder.percent;
            this.percentage = builder.percentage;
        }

        @Override
        public final java.lang.String getExponentialDelay() {
            return this.exponentialDelay;
        }

        @Override
        public final java.lang.String getFixedDelay() {
            return this.fixedDelay;
        }

        @Override
        public final java.lang.Number getPercent() {
            return this.percent;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpFaultDelayPercentage getPercentage() {
            return this.percentage;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getExponentialDelay() != null) {
                data.set("exponentialDelay", om.valueToTree(this.getExponentialDelay()));
            }
            if (this.getFixedDelay() != null) {
                data.set("fixedDelay", om.valueToTree(this.getFixedDelay()));
            }
            if (this.getPercent() != null) {
                data.set("percent", om.valueToTree(this.getPercent()));
            }
            if (this.getPercentage() != null) {
                data.set("percentage", om.valueToTree(this.getPercentage()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpFaultDelay"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpFaultDelay.Jsii$Proxy that = (VirtualServiceSpecHttpFaultDelay.Jsii$Proxy) o;

            if (this.exponentialDelay != null ? !this.exponentialDelay.equals(that.exponentialDelay) : that.exponentialDelay != null) return false;
            if (this.fixedDelay != null ? !this.fixedDelay.equals(that.fixedDelay) : that.fixedDelay != null) return false;
            if (this.percent != null ? !this.percent.equals(that.percent) : that.percent != null) return false;
            return this.percentage != null ? this.percentage.equals(that.percentage) : that.percentage == null;
        }

        @Override
        public final int hashCode() {
            int result = this.exponentialDelay != null ? this.exponentialDelay.hashCode() : 0;
            result = 31 * result + (this.fixedDelay != null ? this.fixedDelay.hashCode() : 0);
            result = 31 * result + (this.percent != null ? this.percent.hashCode() : 0);
            result = 31 * result + (this.percentage != null ? this.percentage.hashCode() : 0);
            return result;
        }
    }
}
