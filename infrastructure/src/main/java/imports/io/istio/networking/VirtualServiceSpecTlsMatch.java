package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.469Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecTlsMatch")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecTlsMatch.Jsii$Proxy.class)
public interface VirtualServiceSpecTlsMatch extends software.amazon.jsii.JsiiSerializable {

    /**
     * IPv4 or IPv6 ip addresses of destination with optional subnet.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getDestinationSubnets() {
        return null;
    }

    /**
     * Names of gateways where the rule should be applied.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getGateways() {
        return null;
    }

    /**
     * Specifies the port on the host that is being addressed.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getPort() {
        return null;
    }

    /**
     * SNI (server name indicator) to match on.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getSniHosts() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getSourceLabels() {
        return null;
    }

    /**
     * Source namespace constraining the applicability of a rule to workloads in that namespace.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getSourceNamespace() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecTlsMatch}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecTlsMatch}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecTlsMatch> {
        java.util.List<java.lang.String> destinationSubnets;
        java.util.List<java.lang.String> gateways;
        java.lang.Number port;
        java.util.List<java.lang.String> sniHosts;
        java.util.Map<java.lang.String, java.lang.String> sourceLabels;
        java.lang.String sourceNamespace;

        /**
         * Sets the value of {@link VirtualServiceSpecTlsMatch#getDestinationSubnets}
         * @param destinationSubnets IPv4 or IPv6 ip addresses of destination with optional subnet.
         * @return {@code this}
         */
        public Builder destinationSubnets(java.util.List<java.lang.String> destinationSubnets) {
            this.destinationSubnets = destinationSubnets;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTlsMatch#getGateways}
         * @param gateways Names of gateways where the rule should be applied.
         * @return {@code this}
         */
        public Builder gateways(java.util.List<java.lang.String> gateways) {
            this.gateways = gateways;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTlsMatch#getPort}
         * @param port Specifies the port on the host that is being addressed.
         * @return {@code this}
         */
        public Builder port(java.lang.Number port) {
            this.port = port;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTlsMatch#getSniHosts}
         * @param sniHosts SNI (server name indicator) to match on.
         * @return {@code this}
         */
        public Builder sniHosts(java.util.List<java.lang.String> sniHosts) {
            this.sniHosts = sniHosts;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTlsMatch#getSourceLabels}
         * @param sourceLabels the value to be set.
         * @return {@code this}
         */
        public Builder sourceLabels(java.util.Map<java.lang.String, java.lang.String> sourceLabels) {
            this.sourceLabels = sourceLabels;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTlsMatch#getSourceNamespace}
         * @param sourceNamespace Source namespace constraining the applicability of a rule to workloads in that namespace.
         * @return {@code this}
         */
        public Builder sourceNamespace(java.lang.String sourceNamespace) {
            this.sourceNamespace = sourceNamespace;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecTlsMatch}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecTlsMatch build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecTlsMatch}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecTlsMatch {
        private final java.util.List<java.lang.String> destinationSubnets;
        private final java.util.List<java.lang.String> gateways;
        private final java.lang.Number port;
        private final java.util.List<java.lang.String> sniHosts;
        private final java.util.Map<java.lang.String, java.lang.String> sourceLabels;
        private final java.lang.String sourceNamespace;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.destinationSubnets = software.amazon.jsii.Kernel.get(this, "destinationSubnets", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.gateways = software.amazon.jsii.Kernel.get(this, "gateways", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.sniHosts = software.amazon.jsii.Kernel.get(this, "sniHosts", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.sourceLabels = software.amazon.jsii.Kernel.get(this, "sourceLabels", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.sourceNamespace = software.amazon.jsii.Kernel.get(this, "sourceNamespace", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.destinationSubnets = builder.destinationSubnets;
            this.gateways = builder.gateways;
            this.port = builder.port;
            this.sniHosts = builder.sniHosts;
            this.sourceLabels = builder.sourceLabels;
            this.sourceNamespace = builder.sourceNamespace;
        }

        @Override
        public final java.util.List<java.lang.String> getDestinationSubnets() {
            return this.destinationSubnets;
        }

        @Override
        public final java.util.List<java.lang.String> getGateways() {
            return this.gateways;
        }

        @Override
        public final java.lang.Number getPort() {
            return this.port;
        }

        @Override
        public final java.util.List<java.lang.String> getSniHosts() {
            return this.sniHosts;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getSourceLabels() {
            return this.sourceLabels;
        }

        @Override
        public final java.lang.String getSourceNamespace() {
            return this.sourceNamespace;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getDestinationSubnets() != null) {
                data.set("destinationSubnets", om.valueToTree(this.getDestinationSubnets()));
            }
            if (this.getGateways() != null) {
                data.set("gateways", om.valueToTree(this.getGateways()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }
            if (this.getSniHosts() != null) {
                data.set("sniHosts", om.valueToTree(this.getSniHosts()));
            }
            if (this.getSourceLabels() != null) {
                data.set("sourceLabels", om.valueToTree(this.getSourceLabels()));
            }
            if (this.getSourceNamespace() != null) {
                data.set("sourceNamespace", om.valueToTree(this.getSourceNamespace()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecTlsMatch"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecTlsMatch.Jsii$Proxy that = (VirtualServiceSpecTlsMatch.Jsii$Proxy) o;

            if (this.destinationSubnets != null ? !this.destinationSubnets.equals(that.destinationSubnets) : that.destinationSubnets != null) return false;
            if (this.gateways != null ? !this.gateways.equals(that.gateways) : that.gateways != null) return false;
            if (this.port != null ? !this.port.equals(that.port) : that.port != null) return false;
            if (this.sniHosts != null ? !this.sniHosts.equals(that.sniHosts) : that.sniHosts != null) return false;
            if (this.sourceLabels != null ? !this.sourceLabels.equals(that.sourceLabels) : that.sourceLabels != null) return false;
            return this.sourceNamespace != null ? this.sourceNamespace.equals(that.sourceNamespace) : that.sourceNamespace == null;
        }

        @Override
        public final int hashCode() {
            int result = this.destinationSubnets != null ? this.destinationSubnets.hashCode() : 0;
            result = 31 * result + (this.gateways != null ? this.gateways.hashCode() : 0);
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            result = 31 * result + (this.sniHosts != null ? this.sniHosts.hashCode() : 0);
            result = 31 * result + (this.sourceLabels != null ? this.sourceLabels.hashCode() : 0);
            result = 31 * result + (this.sourceNamespace != null ? this.sourceNamespace.hashCode() : 0);
            return result;
        }
    }
}
