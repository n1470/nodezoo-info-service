FROM openjdk:17.0.2-bullseye

ARG PROJECT_PATH

RUN apt-get update && \
    apt-get -y upgrade && \
    curl -fsSL https://deb.nodesource.com/setup_16.x | bash - && \
    apt-get install -y nodejs zip libarchive-tools make && \
    curl -fsSL https://github.com/sbt/sbt/releases/download/v1.7.1/sbt-1.7.1.tgz | tar -xzC /opt && \
    rm -rf /var/lib/apt/lists/*

RUN npm install -g aws-cdk@2.23.0 cdk8s-cli@2.0.48

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    rm -rf ./aws awscliv2.zip

ENV PATH="/opt/sbt/bin:$PATH"

WORKDIR /builds/$PROJECT_PATH

COPY build.sbt /builds/$PROJECT_PATH/
COPY project/build.properties project/Dependencies.scala project/plugins.sbt /builds/$PROJECT_PATH/project/
RUN  sbt update --batch || true

COPY Makefile /builds/$PROJECT_PATH/

CMD ["bash"]
