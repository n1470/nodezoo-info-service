package imports.io.istio.networking;

/**
 * Configuration affecting edge load balancer.
 * <p>
 * See more details at: https://istio.io/docs/reference/config/networking/gateway.html
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.443Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.GatewaySpec")
@software.amazon.jsii.Jsii.Proxy(GatewaySpec.Jsii$Proxy.class)
public interface GatewaySpec extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.Map<java.lang.String, java.lang.String> getSelector() {
        return null;
    }

    /**
     * A list of server specifications.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.GatewaySpecServers> getServers() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link GatewaySpec}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link GatewaySpec}
     */
    public static final class Builder implements software.amazon.jsii.Builder<GatewaySpec> {
        java.util.Map<java.lang.String, java.lang.String> selector;
        java.util.List<imports.io.istio.networking.GatewaySpecServers> servers;

        /**
         * Sets the value of {@link GatewaySpec#getSelector}
         * @param selector the value to be set.
         * @return {@code this}
         */
        public Builder selector(java.util.Map<java.lang.String, java.lang.String> selector) {
            this.selector = selector;
            return this;
        }

        /**
         * Sets the value of {@link GatewaySpec#getServers}
         * @param servers A list of server specifications.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder servers(java.util.List<? extends imports.io.istio.networking.GatewaySpecServers> servers) {
            this.servers = (java.util.List<imports.io.istio.networking.GatewaySpecServers>)servers;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link GatewaySpec}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public GatewaySpec build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link GatewaySpec}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements GatewaySpec {
        private final java.util.Map<java.lang.String, java.lang.String> selector;
        private final java.util.List<imports.io.istio.networking.GatewaySpecServers> servers;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.selector = software.amazon.jsii.Kernel.get(this, "selector", software.amazon.jsii.NativeType.mapOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.servers = software.amazon.jsii.Kernel.get(this, "servers", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.GatewaySpecServers.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.selector = builder.selector;
            this.servers = (java.util.List<imports.io.istio.networking.GatewaySpecServers>)builder.servers;
        }

        @Override
        public final java.util.Map<java.lang.String, java.lang.String> getSelector() {
            return this.selector;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.GatewaySpecServers> getServers() {
            return this.servers;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getSelector() != null) {
                data.set("selector", om.valueToTree(this.getSelector()));
            }
            if (this.getServers() != null) {
                data.set("servers", om.valueToTree(this.getServers()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.GatewaySpec"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            GatewaySpec.Jsii$Proxy that = (GatewaySpec.Jsii$Proxy) o;

            if (this.selector != null ? !this.selector.equals(that.selector) : that.selector != null) return false;
            return this.servers != null ? this.servers.equals(that.servers) : that.servers == null;
        }

        @Override
        public final int hashCode() {
            int result = this.selector != null ? this.selector.hashCode() : 0;
            result = 31 * result + (this.servers != null ? this.servers.hashCode() : 0);
            return result;
        }
    }
}
