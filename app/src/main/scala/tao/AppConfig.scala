package tao

import org.apache.commons.cli.{DefaultParser, Options}

case class PackageServiceConfig(baseUrl: String)

case class AppConfig(port: Int,
                     timeout: Int,
                     npmServiceConfig: PackageServiceConfig,
                     gitHubServiceConfig: PackageServiceConfig)

object AppConfig {

  def apply(args: Array[String], defaults: AppConfig): AppConfig = {
    val options = createOptions(defaults)
    val parser = new DefaultParser()
    val commandLine = parser.parse(options, args)
    val port = commandLine.getOptionValue("p", defaults.port.toString).toInt
    val timeout = commandLine.getOptionValue("t", defaults.timeout.toString).toInt
    val npmServiceUrl = commandLine.getOptionValue("n", defaults.npmServiceConfig.baseUrl)
    val ghServiceUrl = commandLine.getOptionValue("g", defaults.gitHubServiceConfig.baseUrl)
    new AppConfig(port,
      timeout = timeout,
      npmServiceConfig = PackageServiceConfig(npmServiceUrl),
      gitHubServiceConfig = PackageServiceConfig(ghServiceUrl)
    )
  }

  private def createOptions(defaults: AppConfig): Options = {
    val options = new Options()
    options.addOption(
      "p",
      "port",
      true,
      s"Port to listen, default ${defaults.port}")
    options.addOption(
      "t",
      "timeout",
      true,
      s"Timeout waiting to collect information, default ${defaults.timeout} seconds")
    options.addOption(
      "n",
      "npmServiceUrl",
      true,
      s"nodezoo-npm-service URL, default ${defaults.npmServiceConfig.baseUrl}")
    options.addOption(
      "g",
      "githubServiceUrl",
      true,
      s"nodezoo-github-service URL, default ${defaults.gitHubServiceConfig.baseUrl}")
    options
  }
}
