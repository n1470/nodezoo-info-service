package tao.manifest;

import imports.k8s.*;
import org.jetbrains.annotations.NotNull;
import tao.InfoServiceChart;

import java.util.List;
import java.util.Map;

public class DeploymentManifest {

    public record Props(
            int containerPortNumber,
            int countOfReplicas,
            @NotNull Map<String, String> selector,
            String imagePullSecretName,
            @NotNull String imageTag
    ) {

    }

    public DeploymentManifest(@NotNull InfoServiceChart infoServiceChart,
                              @NotNull Props manifestProps) {
        final var labelSelector = LabelSelector.builder()
                .matchLabels(manifestProps.selector())
                .build();
        final var objectMeta = ObjectMeta.builder()
                .labels(manifestProps.selector())
                .build();
        final var containerPort = ContainerPort.builder()
                .containerPort(manifestProps.containerPortNumber())
                .build();
        final var containerPorts = List.of(containerPort);
        final var container = Container.builder()
                .name("info-service")
                .image(manifestProps.imageTag())
                .imagePullPolicy("Always")
                .ports(containerPorts)
                .build();
        final var containers = List.of(container);
        PodSpec.Builder podSpecBuilder = PodSpec.builder()
                .containers(containers);
        if (manifestProps.imagePullSecretName() != null) {
            final var lor = LocalObjectReference.builder()
                    .name(manifestProps.imagePullSecretName())
                    .build();
            podSpecBuilder.imagePullSecrets(List.of(lor));
        }
        final var podSpec = podSpecBuilder.build();
        final var podTemplateSpec = PodTemplateSpec.builder()
                .metadata(objectMeta)
                .spec(podSpec)
                .build();
        final var deploymentSpec = DeploymentSpec.builder()
                .replicas(manifestProps.countOfReplicas())
                .selector(labelSelector)
                .template(podTemplateSpec)
                .build();
        final var deploymentProps = KubeDeploymentProps.builder()
                .spec(deploymentSpec)
                .build();
        new KubeDeployment(infoServiceChart, "deployment", deploymentProps);
    }
}
