package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.462Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.SidecarSpecIngressTls")
@software.amazon.jsii.Jsii.Proxy(SidecarSpecIngressTls.Jsii$Proxy.class)
public interface SidecarSpecIngressTls extends software.amazon.jsii.JsiiSerializable {

    /**
     * REQUIRED if mode is `MUTUAL`.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getCaCertificates() {
        return null;
    }

    /**
     * Optional: If specified, only support the specified cipher list.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getCipherSuites() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getCredentialName() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getHttpsRedirect() {
        return null;
    }

    /**
     * Optional: Maximum TLS protocol version.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecIngressTlsMaxProtocolVersion getMaxProtocolVersion() {
        return null;
    }

    /**
     * Optional: Minimum TLS protocol version.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecIngressTlsMinProtocolVersion getMinProtocolVersion() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecIngressTlsMode getMode() {
        return null;
    }

    /**
     * REQUIRED if mode is `SIMPLE` or `MUTUAL`.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getPrivateKey() {
        return null;
    }

    /**
     * REQUIRED if mode is `SIMPLE` or `MUTUAL`.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getServerCertificate() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getSubjectAltNames() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getVerifyCertificateHash() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getVerifyCertificateSpki() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link SidecarSpecIngressTls}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link SidecarSpecIngressTls}
     */
    public static final class Builder implements software.amazon.jsii.Builder<SidecarSpecIngressTls> {
        java.lang.String caCertificates;
        java.util.List<java.lang.String> cipherSuites;
        java.lang.String credentialName;
        java.lang.Boolean httpsRedirect;
        imports.io.istio.networking.SidecarSpecIngressTlsMaxProtocolVersion maxProtocolVersion;
        imports.io.istio.networking.SidecarSpecIngressTlsMinProtocolVersion minProtocolVersion;
        imports.io.istio.networking.SidecarSpecIngressTlsMode mode;
        java.lang.String privateKey;
        java.lang.String serverCertificate;
        java.util.List<java.lang.String> subjectAltNames;
        java.util.List<java.lang.String> verifyCertificateHash;
        java.util.List<java.lang.String> verifyCertificateSpki;

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getCaCertificates}
         * @param caCertificates REQUIRED if mode is `MUTUAL`.
         * @return {@code this}
         */
        public Builder caCertificates(java.lang.String caCertificates) {
            this.caCertificates = caCertificates;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getCipherSuites}
         * @param cipherSuites Optional: If specified, only support the specified cipher list.
         * @return {@code this}
         */
        public Builder cipherSuites(java.util.List<java.lang.String> cipherSuites) {
            this.cipherSuites = cipherSuites;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getCredentialName}
         * @param credentialName the value to be set.
         * @return {@code this}
         */
        public Builder credentialName(java.lang.String credentialName) {
            this.credentialName = credentialName;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getHttpsRedirect}
         * @param httpsRedirect the value to be set.
         * @return {@code this}
         */
        public Builder httpsRedirect(java.lang.Boolean httpsRedirect) {
            this.httpsRedirect = httpsRedirect;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getMaxProtocolVersion}
         * @param maxProtocolVersion Optional: Maximum TLS protocol version.
         * @return {@code this}
         */
        public Builder maxProtocolVersion(imports.io.istio.networking.SidecarSpecIngressTlsMaxProtocolVersion maxProtocolVersion) {
            this.maxProtocolVersion = maxProtocolVersion;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getMinProtocolVersion}
         * @param minProtocolVersion Optional: Minimum TLS protocol version.
         * @return {@code this}
         */
        public Builder minProtocolVersion(imports.io.istio.networking.SidecarSpecIngressTlsMinProtocolVersion minProtocolVersion) {
            this.minProtocolVersion = minProtocolVersion;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getMode}
         * @param mode the value to be set.
         * @return {@code this}
         */
        public Builder mode(imports.io.istio.networking.SidecarSpecIngressTlsMode mode) {
            this.mode = mode;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getPrivateKey}
         * @param privateKey REQUIRED if mode is `SIMPLE` or `MUTUAL`.
         * @return {@code this}
         */
        public Builder privateKey(java.lang.String privateKey) {
            this.privateKey = privateKey;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getServerCertificate}
         * @param serverCertificate REQUIRED if mode is `SIMPLE` or `MUTUAL`.
         * @return {@code this}
         */
        public Builder serverCertificate(java.lang.String serverCertificate) {
            this.serverCertificate = serverCertificate;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getSubjectAltNames}
         * @param subjectAltNames the value to be set.
         * @return {@code this}
         */
        public Builder subjectAltNames(java.util.List<java.lang.String> subjectAltNames) {
            this.subjectAltNames = subjectAltNames;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getVerifyCertificateHash}
         * @param verifyCertificateHash the value to be set.
         * @return {@code this}
         */
        public Builder verifyCertificateHash(java.util.List<java.lang.String> verifyCertificateHash) {
            this.verifyCertificateHash = verifyCertificateHash;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngressTls#getVerifyCertificateSpki}
         * @param verifyCertificateSpki the value to be set.
         * @return {@code this}
         */
        public Builder verifyCertificateSpki(java.util.List<java.lang.String> verifyCertificateSpki) {
            this.verifyCertificateSpki = verifyCertificateSpki;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link SidecarSpecIngressTls}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public SidecarSpecIngressTls build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link SidecarSpecIngressTls}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements SidecarSpecIngressTls {
        private final java.lang.String caCertificates;
        private final java.util.List<java.lang.String> cipherSuites;
        private final java.lang.String credentialName;
        private final java.lang.Boolean httpsRedirect;
        private final imports.io.istio.networking.SidecarSpecIngressTlsMaxProtocolVersion maxProtocolVersion;
        private final imports.io.istio.networking.SidecarSpecIngressTlsMinProtocolVersion minProtocolVersion;
        private final imports.io.istio.networking.SidecarSpecIngressTlsMode mode;
        private final java.lang.String privateKey;
        private final java.lang.String serverCertificate;
        private final java.util.List<java.lang.String> subjectAltNames;
        private final java.util.List<java.lang.String> verifyCertificateHash;
        private final java.util.List<java.lang.String> verifyCertificateSpki;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.caCertificates = software.amazon.jsii.Kernel.get(this, "caCertificates", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.cipherSuites = software.amazon.jsii.Kernel.get(this, "cipherSuites", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.credentialName = software.amazon.jsii.Kernel.get(this, "credentialName", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.httpsRedirect = software.amazon.jsii.Kernel.get(this, "httpsRedirect", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
            this.maxProtocolVersion = software.amazon.jsii.Kernel.get(this, "maxProtocolVersion", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecIngressTlsMaxProtocolVersion.class));
            this.minProtocolVersion = software.amazon.jsii.Kernel.get(this, "minProtocolVersion", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecIngressTlsMinProtocolVersion.class));
            this.mode = software.amazon.jsii.Kernel.get(this, "mode", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecIngressTlsMode.class));
            this.privateKey = software.amazon.jsii.Kernel.get(this, "privateKey", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.serverCertificate = software.amazon.jsii.Kernel.get(this, "serverCertificate", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.subjectAltNames = software.amazon.jsii.Kernel.get(this, "subjectAltNames", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.verifyCertificateHash = software.amazon.jsii.Kernel.get(this, "verifyCertificateHash", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.verifyCertificateSpki = software.amazon.jsii.Kernel.get(this, "verifyCertificateSpki", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.caCertificates = builder.caCertificates;
            this.cipherSuites = builder.cipherSuites;
            this.credentialName = builder.credentialName;
            this.httpsRedirect = builder.httpsRedirect;
            this.maxProtocolVersion = builder.maxProtocolVersion;
            this.minProtocolVersion = builder.minProtocolVersion;
            this.mode = builder.mode;
            this.privateKey = builder.privateKey;
            this.serverCertificate = builder.serverCertificate;
            this.subjectAltNames = builder.subjectAltNames;
            this.verifyCertificateHash = builder.verifyCertificateHash;
            this.verifyCertificateSpki = builder.verifyCertificateSpki;
        }

        @Override
        public final java.lang.String getCaCertificates() {
            return this.caCertificates;
        }

        @Override
        public final java.util.List<java.lang.String> getCipherSuites() {
            return this.cipherSuites;
        }

        @Override
        public final java.lang.String getCredentialName() {
            return this.credentialName;
        }

        @Override
        public final java.lang.Boolean getHttpsRedirect() {
            return this.httpsRedirect;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecIngressTlsMaxProtocolVersion getMaxProtocolVersion() {
            return this.maxProtocolVersion;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecIngressTlsMinProtocolVersion getMinProtocolVersion() {
            return this.minProtocolVersion;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecIngressTlsMode getMode() {
            return this.mode;
        }

        @Override
        public final java.lang.String getPrivateKey() {
            return this.privateKey;
        }

        @Override
        public final java.lang.String getServerCertificate() {
            return this.serverCertificate;
        }

        @Override
        public final java.util.List<java.lang.String> getSubjectAltNames() {
            return this.subjectAltNames;
        }

        @Override
        public final java.util.List<java.lang.String> getVerifyCertificateHash() {
            return this.verifyCertificateHash;
        }

        @Override
        public final java.util.List<java.lang.String> getVerifyCertificateSpki() {
            return this.verifyCertificateSpki;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getCaCertificates() != null) {
                data.set("caCertificates", om.valueToTree(this.getCaCertificates()));
            }
            if (this.getCipherSuites() != null) {
                data.set("cipherSuites", om.valueToTree(this.getCipherSuites()));
            }
            if (this.getCredentialName() != null) {
                data.set("credentialName", om.valueToTree(this.getCredentialName()));
            }
            if (this.getHttpsRedirect() != null) {
                data.set("httpsRedirect", om.valueToTree(this.getHttpsRedirect()));
            }
            if (this.getMaxProtocolVersion() != null) {
                data.set("maxProtocolVersion", om.valueToTree(this.getMaxProtocolVersion()));
            }
            if (this.getMinProtocolVersion() != null) {
                data.set("minProtocolVersion", om.valueToTree(this.getMinProtocolVersion()));
            }
            if (this.getMode() != null) {
                data.set("mode", om.valueToTree(this.getMode()));
            }
            if (this.getPrivateKey() != null) {
                data.set("privateKey", om.valueToTree(this.getPrivateKey()));
            }
            if (this.getServerCertificate() != null) {
                data.set("serverCertificate", om.valueToTree(this.getServerCertificate()));
            }
            if (this.getSubjectAltNames() != null) {
                data.set("subjectAltNames", om.valueToTree(this.getSubjectAltNames()));
            }
            if (this.getVerifyCertificateHash() != null) {
                data.set("verifyCertificateHash", om.valueToTree(this.getVerifyCertificateHash()));
            }
            if (this.getVerifyCertificateSpki() != null) {
                data.set("verifyCertificateSpki", om.valueToTree(this.getVerifyCertificateSpki()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.SidecarSpecIngressTls"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SidecarSpecIngressTls.Jsii$Proxy that = (SidecarSpecIngressTls.Jsii$Proxy) o;

            if (this.caCertificates != null ? !this.caCertificates.equals(that.caCertificates) : that.caCertificates != null) return false;
            if (this.cipherSuites != null ? !this.cipherSuites.equals(that.cipherSuites) : that.cipherSuites != null) return false;
            if (this.credentialName != null ? !this.credentialName.equals(that.credentialName) : that.credentialName != null) return false;
            if (this.httpsRedirect != null ? !this.httpsRedirect.equals(that.httpsRedirect) : that.httpsRedirect != null) return false;
            if (this.maxProtocolVersion != null ? !this.maxProtocolVersion.equals(that.maxProtocolVersion) : that.maxProtocolVersion != null) return false;
            if (this.minProtocolVersion != null ? !this.minProtocolVersion.equals(that.minProtocolVersion) : that.minProtocolVersion != null) return false;
            if (this.mode != null ? !this.mode.equals(that.mode) : that.mode != null) return false;
            if (this.privateKey != null ? !this.privateKey.equals(that.privateKey) : that.privateKey != null) return false;
            if (this.serverCertificate != null ? !this.serverCertificate.equals(that.serverCertificate) : that.serverCertificate != null) return false;
            if (this.subjectAltNames != null ? !this.subjectAltNames.equals(that.subjectAltNames) : that.subjectAltNames != null) return false;
            if (this.verifyCertificateHash != null ? !this.verifyCertificateHash.equals(that.verifyCertificateHash) : that.verifyCertificateHash != null) return false;
            return this.verifyCertificateSpki != null ? this.verifyCertificateSpki.equals(that.verifyCertificateSpki) : that.verifyCertificateSpki == null;
        }

        @Override
        public final int hashCode() {
            int result = this.caCertificates != null ? this.caCertificates.hashCode() : 0;
            result = 31 * result + (this.cipherSuites != null ? this.cipherSuites.hashCode() : 0);
            result = 31 * result + (this.credentialName != null ? this.credentialName.hashCode() : 0);
            result = 31 * result + (this.httpsRedirect != null ? this.httpsRedirect.hashCode() : 0);
            result = 31 * result + (this.maxProtocolVersion != null ? this.maxProtocolVersion.hashCode() : 0);
            result = 31 * result + (this.minProtocolVersion != null ? this.minProtocolVersion.hashCode() : 0);
            result = 31 * result + (this.mode != null ? this.mode.hashCode() : 0);
            result = 31 * result + (this.privateKey != null ? this.privateKey.hashCode() : 0);
            result = 31 * result + (this.serverCertificate != null ? this.serverCertificate.hashCode() : 0);
            result = 31 * result + (this.subjectAltNames != null ? this.subjectAltNames.hashCode() : 0);
            result = 31 * result + (this.verifyCertificateHash != null ? this.verifyCertificateHash.hashCode() : 0);
            result = 31 * result + (this.verifyCertificateSpki != null ? this.verifyCertificateSpki.hashCode() : 0);
            return result;
        }
    }
}
