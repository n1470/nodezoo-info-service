package imports.io.istio.networking;

/**
 * Settings common to both HTTP and TCP upstream connections.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.438Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp.Jsii$Proxy.class)
public interface DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp extends software.amazon.jsii.JsiiSerializable {

    /**
     * TCP connection timeout.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getConnectTimeout() {
        return null;
    }

    /**
     * Maximum number of HTTP1 /TCP connections to a destination host.
     */
    default @org.jetbrains.annotations.Nullable java.lang.Number getMaxConnections() {
        return null;
    }

    /**
     * If set then set SO_KEEPALIVE on the socket to enable TCP Keepalives.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcpTcpKeepalive getTcpKeepalive() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp> {
        java.lang.String connectTimeout;
        java.lang.Number maxConnections;
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcpTcpKeepalive tcpKeepalive;

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp#getConnectTimeout}
         * @param connectTimeout TCP connection timeout.
         * @return {@code this}
         */
        public Builder connectTimeout(java.lang.String connectTimeout) {
            this.connectTimeout = connectTimeout;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp#getMaxConnections}
         * @param maxConnections Maximum number of HTTP1 /TCP connections to a destination host.
         * @return {@code this}
         */
        public Builder maxConnections(java.lang.Number maxConnections) {
            this.maxConnections = maxConnections;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp#getTcpKeepalive}
         * @param tcpKeepalive If set then set SO_KEEPALIVE on the socket to enable TCP Keepalives.
         * @return {@code this}
         */
        public Builder tcpKeepalive(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcpTcpKeepalive tcpKeepalive) {
            this.tcpKeepalive = tcpKeepalive;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp {
        private final java.lang.String connectTimeout;
        private final java.lang.Number maxConnections;
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcpTcpKeepalive tcpKeepalive;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.connectTimeout = software.amazon.jsii.Kernel.get(this, "connectTimeout", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.maxConnections = software.amazon.jsii.Kernel.get(this, "maxConnections", software.amazon.jsii.NativeType.forClass(java.lang.Number.class));
            this.tcpKeepalive = software.amazon.jsii.Kernel.get(this, "tcpKeepalive", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcpTcpKeepalive.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.connectTimeout = builder.connectTimeout;
            this.maxConnections = builder.maxConnections;
            this.tcpKeepalive = builder.tcpKeepalive;
        }

        @Override
        public final java.lang.String getConnectTimeout() {
            return this.connectTimeout;
        }

        @Override
        public final java.lang.Number getMaxConnections() {
            return this.maxConnections;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcpTcpKeepalive getTcpKeepalive() {
            return this.tcpKeepalive;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getConnectTimeout() != null) {
                data.set("connectTimeout", om.valueToTree(this.getConnectTimeout()));
            }
            if (this.getMaxConnections() != null) {
                data.set("maxConnections", om.valueToTree(this.getMaxConnections()));
            }
            if (this.getTcpKeepalive() != null) {
                data.set("tcpKeepalive", om.valueToTree(this.getTcpKeepalive()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp.Jsii$Proxy that = (DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp.Jsii$Proxy) o;

            if (this.connectTimeout != null ? !this.connectTimeout.equals(that.connectTimeout) : that.connectTimeout != null) return false;
            if (this.maxConnections != null ? !this.maxConnections.equals(that.maxConnections) : that.maxConnections != null) return false;
            return this.tcpKeepalive != null ? this.tcpKeepalive.equals(that.tcpKeepalive) : that.tcpKeepalive == null;
        }

        @Override
        public final int hashCode() {
            int result = this.connectTimeout != null ? this.connectTimeout.hashCode() : 0;
            result = 31 * result + (this.maxConnections != null ? this.maxConnections.hashCode() : 0);
            result = 31 * result + (this.tcpKeepalive != null ? this.tcpKeepalive.hashCode() : 0);
            return result;
        }
    }
}
