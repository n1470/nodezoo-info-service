package tao.core

import akka.actor.typed.ActorRef

final case class PackageQuery(packageName: String, actorRef: ActorRef[PackageInfo])
