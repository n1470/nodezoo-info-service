package imports.k8s;

/**
 * Projection that may be projected along with other supported volume types.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.58.0 (build f8ba112)", date = "2022-05-12T13:49:24.376Z")
@software.amazon.jsii.Jsii(module = $Module.class, fqn = "k8s.VolumeProjection")
@software.amazon.jsii.Jsii.Proxy(VolumeProjection.Jsii$Proxy.class)
public interface VolumeProjection extends software.amazon.jsii.JsiiSerializable {

    /**
     * information about the configMap data to project.
     */
    default @org.jetbrains.annotations.Nullable ConfigMapProjection getConfigMap() {
        return null;
    }

    /**
     * information about the downwardAPI data to project.
     */
    default @org.jetbrains.annotations.Nullable DownwardApiProjection getDownwardApi() {
        return null;
    }

    /**
     * information about the secret data to project.
     */
    default @org.jetbrains.annotations.Nullable SecretProjection getSecret() {
        return null;
    }

    /**
     * information about the serviceAccountToken data to project.
     */
    default @org.jetbrains.annotations.Nullable ServiceAccountTokenProjection getServiceAccountToken() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VolumeProjection}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VolumeProjection}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VolumeProjection> {
        ConfigMapProjection configMap;
        DownwardApiProjection downwardApi;
        SecretProjection secret;
        ServiceAccountTokenProjection serviceAccountToken;

        /**
         * Sets the value of {@link VolumeProjection#getConfigMap}
         * @param configMap information about the configMap data to project.
         * @return {@code this}
         */
        public Builder configMap(ConfigMapProjection configMap) {
            this.configMap = configMap;
            return this;
        }

        /**
         * Sets the value of {@link VolumeProjection#getDownwardApi}
         * @param downwardApi information about the downwardAPI data to project.
         * @return {@code this}
         */
        public Builder downwardApi(DownwardApiProjection downwardApi) {
            this.downwardApi = downwardApi;
            return this;
        }

        /**
         * Sets the value of {@link VolumeProjection#getSecret}
         * @param secret information about the secret data to project.
         * @return {@code this}
         */
        public Builder secret(SecretProjection secret) {
            this.secret = secret;
            return this;
        }

        /**
         * Sets the value of {@link VolumeProjection#getServiceAccountToken}
         * @param serviceAccountToken information about the serviceAccountToken data to project.
         * @return {@code this}
         */
        public Builder serviceAccountToken(ServiceAccountTokenProjection serviceAccountToken) {
            this.serviceAccountToken = serviceAccountToken;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VolumeProjection}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VolumeProjection build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VolumeProjection}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VolumeProjection {
        private final ConfigMapProjection configMap;
        private final DownwardApiProjection downwardApi;
        private final SecretProjection secret;
        private final ServiceAccountTokenProjection serviceAccountToken;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.configMap = software.amazon.jsii.Kernel.get(this, "configMap", software.amazon.jsii.NativeType.forClass(ConfigMapProjection.class));
            this.downwardApi = software.amazon.jsii.Kernel.get(this, "downwardApi", software.amazon.jsii.NativeType.forClass(DownwardApiProjection.class));
            this.secret = software.amazon.jsii.Kernel.get(this, "secret", software.amazon.jsii.NativeType.forClass(SecretProjection.class));
            this.serviceAccountToken = software.amazon.jsii.Kernel.get(this, "serviceAccountToken", software.amazon.jsii.NativeType.forClass(ServiceAccountTokenProjection.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.configMap = builder.configMap;
            this.downwardApi = builder.downwardApi;
            this.secret = builder.secret;
            this.serviceAccountToken = builder.serviceAccountToken;
        }

        @Override
        public final ConfigMapProjection getConfigMap() {
            return this.configMap;
        }

        @Override
        public final DownwardApiProjection getDownwardApi() {
            return this.downwardApi;
        }

        @Override
        public final SecretProjection getSecret() {
            return this.secret;
        }

        @Override
        public final ServiceAccountTokenProjection getServiceAccountToken() {
            return this.serviceAccountToken;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getConfigMap() != null) {
                data.set("configMap", om.valueToTree(this.getConfigMap()));
            }
            if (this.getDownwardApi() != null) {
                data.set("downwardApi", om.valueToTree(this.getDownwardApi()));
            }
            if (this.getSecret() != null) {
                data.set("secret", om.valueToTree(this.getSecret()));
            }
            if (this.getServiceAccountToken() != null) {
                data.set("serviceAccountToken", om.valueToTree(this.getServiceAccountToken()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("k8s.VolumeProjection"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Jsii$Proxy that = (Jsii$Proxy) o;

            if (this.configMap != null ? !this.configMap.equals(that.configMap) : that.configMap != null) return false;
            if (this.downwardApi != null ? !this.downwardApi.equals(that.downwardApi) : that.downwardApi != null) return false;
            if (this.secret != null ? !this.secret.equals(that.secret) : that.secret != null) return false;
            return this.serviceAccountToken != null ? this.serviceAccountToken.equals(that.serviceAccountToken) : that.serviceAccountToken == null;
        }

        @Override
        public final int hashCode() {
            int result = this.configMap != null ? this.configMap.hashCode() : 0;
            result = 31 * result + (this.downwardApi != null ? this.downwardApi.hashCode() : 0);
            result = 31 * result + (this.secret != null ? this.secret.hashCode() : 0);
            result = 31 * result + (this.serviceAccountToken != null ? this.serviceAccountToken.hashCode() : 0);
            return result;
        }
    }
}
