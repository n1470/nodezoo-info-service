package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.462Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.SidecarSpecIngress")
@software.amazon.jsii.Jsii.Proxy(SidecarSpecIngress.Jsii$Proxy.class)
public interface SidecarSpecIngress extends software.amazon.jsii.JsiiSerializable {

    /**
     * The IP to which the listener should be bound.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getBind() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecIngressCaptureMode getCaptureMode() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getDefaultEndpoint() {
        return null;
    }

    /**
     * The port associated with the listener.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecIngressPort getPort() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.SidecarSpecIngressTls getTls() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link SidecarSpecIngress}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link SidecarSpecIngress}
     */
    public static final class Builder implements software.amazon.jsii.Builder<SidecarSpecIngress> {
        java.lang.String bind;
        imports.io.istio.networking.SidecarSpecIngressCaptureMode captureMode;
        java.lang.String defaultEndpoint;
        imports.io.istio.networking.SidecarSpecIngressPort port;
        imports.io.istio.networking.SidecarSpecIngressTls tls;

        /**
         * Sets the value of {@link SidecarSpecIngress#getBind}
         * @param bind The IP to which the listener should be bound.
         * @return {@code this}
         */
        public Builder bind(java.lang.String bind) {
            this.bind = bind;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngress#getCaptureMode}
         * @param captureMode the value to be set.
         * @return {@code this}
         */
        public Builder captureMode(imports.io.istio.networking.SidecarSpecIngressCaptureMode captureMode) {
            this.captureMode = captureMode;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngress#getDefaultEndpoint}
         * @param defaultEndpoint the value to be set.
         * @return {@code this}
         */
        public Builder defaultEndpoint(java.lang.String defaultEndpoint) {
            this.defaultEndpoint = defaultEndpoint;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngress#getPort}
         * @param port The port associated with the listener.
         * @return {@code this}
         */
        public Builder port(imports.io.istio.networking.SidecarSpecIngressPort port) {
            this.port = port;
            return this;
        }

        /**
         * Sets the value of {@link SidecarSpecIngress#getTls}
         * @param tls the value to be set.
         * @return {@code this}
         */
        public Builder tls(imports.io.istio.networking.SidecarSpecIngressTls tls) {
            this.tls = tls;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link SidecarSpecIngress}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public SidecarSpecIngress build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link SidecarSpecIngress}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements SidecarSpecIngress {
        private final java.lang.String bind;
        private final imports.io.istio.networking.SidecarSpecIngressCaptureMode captureMode;
        private final java.lang.String defaultEndpoint;
        private final imports.io.istio.networking.SidecarSpecIngressPort port;
        private final imports.io.istio.networking.SidecarSpecIngressTls tls;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.bind = software.amazon.jsii.Kernel.get(this, "bind", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.captureMode = software.amazon.jsii.Kernel.get(this, "captureMode", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecIngressCaptureMode.class));
            this.defaultEndpoint = software.amazon.jsii.Kernel.get(this, "defaultEndpoint", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecIngressPort.class));
            this.tls = software.amazon.jsii.Kernel.get(this, "tls", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.SidecarSpecIngressTls.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.bind = builder.bind;
            this.captureMode = builder.captureMode;
            this.defaultEndpoint = builder.defaultEndpoint;
            this.port = builder.port;
            this.tls = builder.tls;
        }

        @Override
        public final java.lang.String getBind() {
            return this.bind;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecIngressCaptureMode getCaptureMode() {
            return this.captureMode;
        }

        @Override
        public final java.lang.String getDefaultEndpoint() {
            return this.defaultEndpoint;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecIngressPort getPort() {
            return this.port;
        }

        @Override
        public final imports.io.istio.networking.SidecarSpecIngressTls getTls() {
            return this.tls;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getBind() != null) {
                data.set("bind", om.valueToTree(this.getBind()));
            }
            if (this.getCaptureMode() != null) {
                data.set("captureMode", om.valueToTree(this.getCaptureMode()));
            }
            if (this.getDefaultEndpoint() != null) {
                data.set("defaultEndpoint", om.valueToTree(this.getDefaultEndpoint()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }
            if (this.getTls() != null) {
                data.set("tls", om.valueToTree(this.getTls()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.SidecarSpecIngress"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            SidecarSpecIngress.Jsii$Proxy that = (SidecarSpecIngress.Jsii$Proxy) o;

            if (this.bind != null ? !this.bind.equals(that.bind) : that.bind != null) return false;
            if (this.captureMode != null ? !this.captureMode.equals(that.captureMode) : that.captureMode != null) return false;
            if (this.defaultEndpoint != null ? !this.defaultEndpoint.equals(that.defaultEndpoint) : that.defaultEndpoint != null) return false;
            if (this.port != null ? !this.port.equals(that.port) : that.port != null) return false;
            return this.tls != null ? this.tls.equals(that.tls) : that.tls == null;
        }

        @Override
        public final int hashCode() {
            int result = this.bind != null ? this.bind.hashCode() : 0;
            result = 31 * result + (this.captureMode != null ? this.captureMode.hashCode() : 0);
            result = 31 * result + (this.defaultEndpoint != null ? this.defaultEndpoint.hashCode() : 0);
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            result = 31 * result + (this.tls != null ? this.tls.hashCode() : 0);
            return result;
        }
    }
}
