package imports.io.istio.networking;

/**
 * TLS related settings for connections to the upstream service.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.440Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecTrafficPolicyTls")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecTrafficPolicyTls.Jsii$Proxy.class)
public interface DestinationRuleSpecTrafficPolicyTls extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getCaCertificates() {
        return null;
    }

    /**
     * REQUIRED if mode is `MUTUAL`.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getClientCertificate() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getCredentialName() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.Boolean getInsecureSkipVerify() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicyTlsMode getMode() {
        return null;
    }

    /**
     * REQUIRED if mode is `MUTUAL`.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getPrivateKey() {
        return null;
    }

    /**
     * SNI string to present to the server during TLS handshake.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getSni() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getSubjectAltNames() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecTrafficPolicyTls}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecTrafficPolicyTls}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecTrafficPolicyTls> {
        java.lang.String caCertificates;
        java.lang.String clientCertificate;
        java.lang.String credentialName;
        java.lang.Boolean insecureSkipVerify;
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicyTlsMode mode;
        java.lang.String privateKey;
        java.lang.String sni;
        java.util.List<java.lang.String> subjectAltNames;

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyTls#getCaCertificates}
         * @param caCertificates the value to be set.
         * @return {@code this}
         */
        public Builder caCertificates(java.lang.String caCertificates) {
            this.caCertificates = caCertificates;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyTls#getClientCertificate}
         * @param clientCertificate REQUIRED if mode is `MUTUAL`.
         * @return {@code this}
         */
        public Builder clientCertificate(java.lang.String clientCertificate) {
            this.clientCertificate = clientCertificate;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyTls#getCredentialName}
         * @param credentialName the value to be set.
         * @return {@code this}
         */
        public Builder credentialName(java.lang.String credentialName) {
            this.credentialName = credentialName;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyTls#getInsecureSkipVerify}
         * @param insecureSkipVerify the value to be set.
         * @return {@code this}
         */
        public Builder insecureSkipVerify(java.lang.Boolean insecureSkipVerify) {
            this.insecureSkipVerify = insecureSkipVerify;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyTls#getMode}
         * @param mode the value to be set.
         * @return {@code this}
         */
        public Builder mode(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyTlsMode mode) {
            this.mode = mode;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyTls#getPrivateKey}
         * @param privateKey REQUIRED if mode is `MUTUAL`.
         * @return {@code this}
         */
        public Builder privateKey(java.lang.String privateKey) {
            this.privateKey = privateKey;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyTls#getSni}
         * @param sni SNI string to present to the server during TLS handshake.
         * @return {@code this}
         */
        public Builder sni(java.lang.String sni) {
            this.sni = sni;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyTls#getSubjectAltNames}
         * @param subjectAltNames the value to be set.
         * @return {@code this}
         */
        public Builder subjectAltNames(java.util.List<java.lang.String> subjectAltNames) {
            this.subjectAltNames = subjectAltNames;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecTrafficPolicyTls}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecTrafficPolicyTls build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecTrafficPolicyTls}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecTrafficPolicyTls {
        private final java.lang.String caCertificates;
        private final java.lang.String clientCertificate;
        private final java.lang.String credentialName;
        private final java.lang.Boolean insecureSkipVerify;
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyTlsMode mode;
        private final java.lang.String privateKey;
        private final java.lang.String sni;
        private final java.util.List<java.lang.String> subjectAltNames;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.caCertificates = software.amazon.jsii.Kernel.get(this, "caCertificates", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.clientCertificate = software.amazon.jsii.Kernel.get(this, "clientCertificate", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.credentialName = software.amazon.jsii.Kernel.get(this, "credentialName", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.insecureSkipVerify = software.amazon.jsii.Kernel.get(this, "insecureSkipVerify", software.amazon.jsii.NativeType.forClass(java.lang.Boolean.class));
            this.mode = software.amazon.jsii.Kernel.get(this, "mode", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyTlsMode.class));
            this.privateKey = software.amazon.jsii.Kernel.get(this, "privateKey", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.sni = software.amazon.jsii.Kernel.get(this, "sni", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.subjectAltNames = software.amazon.jsii.Kernel.get(this, "subjectAltNames", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.caCertificates = builder.caCertificates;
            this.clientCertificate = builder.clientCertificate;
            this.credentialName = builder.credentialName;
            this.insecureSkipVerify = builder.insecureSkipVerify;
            this.mode = builder.mode;
            this.privateKey = builder.privateKey;
            this.sni = builder.sni;
            this.subjectAltNames = builder.subjectAltNames;
        }

        @Override
        public final java.lang.String getCaCertificates() {
            return this.caCertificates;
        }

        @Override
        public final java.lang.String getClientCertificate() {
            return this.clientCertificate;
        }

        @Override
        public final java.lang.String getCredentialName() {
            return this.credentialName;
        }

        @Override
        public final java.lang.Boolean getInsecureSkipVerify() {
            return this.insecureSkipVerify;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyTlsMode getMode() {
            return this.mode;
        }

        @Override
        public final java.lang.String getPrivateKey() {
            return this.privateKey;
        }

        @Override
        public final java.lang.String getSni() {
            return this.sni;
        }

        @Override
        public final java.util.List<java.lang.String> getSubjectAltNames() {
            return this.subjectAltNames;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getCaCertificates() != null) {
                data.set("caCertificates", om.valueToTree(this.getCaCertificates()));
            }
            if (this.getClientCertificate() != null) {
                data.set("clientCertificate", om.valueToTree(this.getClientCertificate()));
            }
            if (this.getCredentialName() != null) {
                data.set("credentialName", om.valueToTree(this.getCredentialName()));
            }
            if (this.getInsecureSkipVerify() != null) {
                data.set("insecureSkipVerify", om.valueToTree(this.getInsecureSkipVerify()));
            }
            if (this.getMode() != null) {
                data.set("mode", om.valueToTree(this.getMode()));
            }
            if (this.getPrivateKey() != null) {
                data.set("privateKey", om.valueToTree(this.getPrivateKey()));
            }
            if (this.getSni() != null) {
                data.set("sni", om.valueToTree(this.getSni()));
            }
            if (this.getSubjectAltNames() != null) {
                data.set("subjectAltNames", om.valueToTree(this.getSubjectAltNames()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecTrafficPolicyTls"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecTrafficPolicyTls.Jsii$Proxy that = (DestinationRuleSpecTrafficPolicyTls.Jsii$Proxy) o;

            if (this.caCertificates != null ? !this.caCertificates.equals(that.caCertificates) : that.caCertificates != null) return false;
            if (this.clientCertificate != null ? !this.clientCertificate.equals(that.clientCertificate) : that.clientCertificate != null) return false;
            if (this.credentialName != null ? !this.credentialName.equals(that.credentialName) : that.credentialName != null) return false;
            if (this.insecureSkipVerify != null ? !this.insecureSkipVerify.equals(that.insecureSkipVerify) : that.insecureSkipVerify != null) return false;
            if (this.mode != null ? !this.mode.equals(that.mode) : that.mode != null) return false;
            if (this.privateKey != null ? !this.privateKey.equals(that.privateKey) : that.privateKey != null) return false;
            if (this.sni != null ? !this.sni.equals(that.sni) : that.sni != null) return false;
            return this.subjectAltNames != null ? this.subjectAltNames.equals(that.subjectAltNames) : that.subjectAltNames == null;
        }

        @Override
        public final int hashCode() {
            int result = this.caCertificates != null ? this.caCertificates.hashCode() : 0;
            result = 31 * result + (this.clientCertificate != null ? this.clientCertificate.hashCode() : 0);
            result = 31 * result + (this.credentialName != null ? this.credentialName.hashCode() : 0);
            result = 31 * result + (this.insecureSkipVerify != null ? this.insecureSkipVerify.hashCode() : 0);
            result = 31 * result + (this.mode != null ? this.mode.hashCode() : 0);
            result = 31 * result + (this.privateKey != null ? this.privateKey.hashCode() : 0);
            result = 31 * result + (this.sni != null ? this.sni.hashCode() : 0);
            result = 31 * result + (this.subjectAltNames != null ? this.subjectAltNames.hashCode() : 0);
            return result;
        }
    }
}
