import sbt._

object Dependencies {
  lazy val logback = "ch.qos.logback" % "logback-classic" % "1.2.3" % "runtime"
  lazy val commonsCli = "commons-cli" % "commons-cli" % "1.5.0"
  lazy val unitTestDeps = Seq(
    "org.junit.jupiter" % "junit-jupiter" % "5.8.2" % Test,
    "org.hamcrest" % "hamcrest" % "2.2" % Test,
    "net.aichler" % "jupiter-interface" % "0.10.0" % Test,
    "org.mockito" % "mockito-core" % "4.5.1" % Test
  )
}
