package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.467Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpMirror")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpMirror.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpMirror extends software.amazon.jsii.JsiiSerializable {

    /**
     * The name of a service from the service registry.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getHost() {
        return null;
    }

    /**
     * Specifies the port on the host that is being addressed.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpMirrorPort getPort() {
        return null;
    }

    /**
     * The name of a subset within the service.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getSubset() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpMirror}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpMirror}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpMirror> {
        java.lang.String host;
        imports.io.istio.networking.VirtualServiceSpecHttpMirrorPort port;
        java.lang.String subset;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMirror#getHost}
         * @param host The name of a service from the service registry.
         * @return {@code this}
         */
        public Builder host(java.lang.String host) {
            this.host = host;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMirror#getPort}
         * @param port Specifies the port on the host that is being addressed.
         * @return {@code this}
         */
        public Builder port(imports.io.istio.networking.VirtualServiceSpecHttpMirrorPort port) {
            this.port = port;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpMirror#getSubset}
         * @param subset The name of a subset within the service.
         * @return {@code this}
         */
        public Builder subset(java.lang.String subset) {
            this.subset = subset;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpMirror}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpMirror build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpMirror}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpMirror {
        private final java.lang.String host;
        private final imports.io.istio.networking.VirtualServiceSpecHttpMirrorPort port;
        private final java.lang.String subset;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.host = software.amazon.jsii.Kernel.get(this, "host", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpMirrorPort.class));
            this.subset = software.amazon.jsii.Kernel.get(this, "subset", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.host = builder.host;
            this.port = builder.port;
            this.subset = builder.subset;
        }

        @Override
        public final java.lang.String getHost() {
            return this.host;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpMirrorPort getPort() {
            return this.port;
        }

        @Override
        public final java.lang.String getSubset() {
            return this.subset;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getHost() != null) {
                data.set("host", om.valueToTree(this.getHost()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }
            if (this.getSubset() != null) {
                data.set("subset", om.valueToTree(this.getSubset()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpMirror"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpMirror.Jsii$Proxy that = (VirtualServiceSpecHttpMirror.Jsii$Proxy) o;

            if (this.host != null ? !this.host.equals(that.host) : that.host != null) return false;
            if (this.port != null ? !this.port.equals(that.port) : that.port != null) return false;
            return this.subset != null ? this.subset.equals(that.subset) : that.subset == null;
        }

        @Override
        public final int hashCode() {
            int result = this.host != null ? this.host.hashCode() : 0;
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            result = 31 * result + (this.subset != null ? this.subset.hashCode() : 0);
            return result;
        }
    }
}
