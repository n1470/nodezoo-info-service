package imports.io.istio.networking;

/**
 * Fault injection policy to apply on HTTP traffic at the client side.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.464Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecHttpFault")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecHttpFault.Jsii$Proxy.class)
public interface VirtualServiceSpecHttpFault extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpFaultAbort getAbort() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.VirtualServiceSpecHttpFaultDelay getDelay() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecHttpFault}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecHttpFault}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecHttpFault> {
        imports.io.istio.networking.VirtualServiceSpecHttpFaultAbort abort;
        imports.io.istio.networking.VirtualServiceSpecHttpFaultDelay delay;

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFault#getAbort}
         * @param abort the value to be set.
         * @return {@code this}
         */
        public Builder abort(imports.io.istio.networking.VirtualServiceSpecHttpFaultAbort abort) {
            this.abort = abort;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecHttpFault#getDelay}
         * @param delay the value to be set.
         * @return {@code this}
         */
        public Builder delay(imports.io.istio.networking.VirtualServiceSpecHttpFaultDelay delay) {
            this.delay = delay;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecHttpFault}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecHttpFault build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecHttpFault}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecHttpFault {
        private final imports.io.istio.networking.VirtualServiceSpecHttpFaultAbort abort;
        private final imports.io.istio.networking.VirtualServiceSpecHttpFaultDelay delay;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.abort = software.amazon.jsii.Kernel.get(this, "abort", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpFaultAbort.class));
            this.delay = software.amazon.jsii.Kernel.get(this, "delay", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecHttpFaultDelay.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.abort = builder.abort;
            this.delay = builder.delay;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpFaultAbort getAbort() {
            return this.abort;
        }

        @Override
        public final imports.io.istio.networking.VirtualServiceSpecHttpFaultDelay getDelay() {
            return this.delay;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getAbort() != null) {
                data.set("abort", om.valueToTree(this.getAbort()));
            }
            if (this.getDelay() != null) {
                data.set("delay", om.valueToTree(this.getDelay()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecHttpFault"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecHttpFault.Jsii$Proxy that = (VirtualServiceSpecHttpFault.Jsii$Proxy) o;

            if (this.abort != null ? !this.abort.equals(that.abort) : that.abort != null) return false;
            return this.delay != null ? this.delay.equals(that.delay) : that.delay == null;
        }

        @Override
        public final int hashCode() {
            int result = this.abort != null ? this.abort.hashCode() : 0;
            result = 31 * result + (this.delay != null ? this.delay.hashCode() : 0);
            return result;
        }
    }
}
