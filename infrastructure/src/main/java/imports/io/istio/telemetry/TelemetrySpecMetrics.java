package imports.io.istio.telemetry;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:09.287Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.telemetry.$Module.class, fqn = "ioistiotelemetry.TelemetrySpecMetrics")
@software.amazon.jsii.Jsii.Proxy(TelemetrySpecMetrics.Jsii$Proxy.class)
public interface TelemetrySpecMetrics extends software.amazon.jsii.JsiiSerializable {

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsOverrides> getOverrides() {
        return null;
    }

    /**
     * Optional.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsProviders> getProviders() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link TelemetrySpecMetrics}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link TelemetrySpecMetrics}
     */
    public static final class Builder implements software.amazon.jsii.Builder<TelemetrySpecMetrics> {
        java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsOverrides> overrides;
        java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsProviders> providers;

        /**
         * Sets the value of {@link TelemetrySpecMetrics#getOverrides}
         * @param overrides Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder overrides(java.util.List<? extends imports.io.istio.telemetry.TelemetrySpecMetricsOverrides> overrides) {
            this.overrides = (java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsOverrides>)overrides;
            return this;
        }

        /**
         * Sets the value of {@link TelemetrySpecMetrics#getProviders}
         * @param providers Optional.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder providers(java.util.List<? extends imports.io.istio.telemetry.TelemetrySpecMetricsProviders> providers) {
            this.providers = (java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsProviders>)providers;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link TelemetrySpecMetrics}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public TelemetrySpecMetrics build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link TelemetrySpecMetrics}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements TelemetrySpecMetrics {
        private final java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsOverrides> overrides;
        private final java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsProviders> providers;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.overrides = software.amazon.jsii.Kernel.get(this, "overrides", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecMetricsOverrides.class)));
            this.providers = software.amazon.jsii.Kernel.get(this, "providers", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.telemetry.TelemetrySpecMetricsProviders.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.overrides = (java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsOverrides>)builder.overrides;
            this.providers = (java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsProviders>)builder.providers;
        }

        @Override
        public final java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsOverrides> getOverrides() {
            return this.overrides;
        }

        @Override
        public final java.util.List<imports.io.istio.telemetry.TelemetrySpecMetricsProviders> getProviders() {
            return this.providers;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getOverrides() != null) {
                data.set("overrides", om.valueToTree(this.getOverrides()));
            }
            if (this.getProviders() != null) {
                data.set("providers", om.valueToTree(this.getProviders()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiotelemetry.TelemetrySpecMetrics"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TelemetrySpecMetrics.Jsii$Proxy that = (TelemetrySpecMetrics.Jsii$Proxy) o;

            if (this.overrides != null ? !this.overrides.equals(that.overrides) : that.overrides != null) return false;
            return this.providers != null ? this.providers.equals(that.providers) : that.providers == null;
        }

        @Override
        public final int hashCode() {
            int result = this.overrides != null ? this.overrides.hashCode() : 0;
            result = 31 * result + (this.providers != null ? this.providers.hashCode() : 0);
            return result;
        }
    }
}
