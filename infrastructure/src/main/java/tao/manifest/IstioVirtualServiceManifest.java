package tao.manifest;

import imports.io.istio.networking.*;
import org.cdk8s.ApiObjectMetadata;
import org.cdk8s.Chart;
import org.jetbrains.annotations.NotNull;

import java.text.MessageFormat;
import java.util.List;

public class IstioVirtualServiceManifest {

    public record Props(
            @NotNull String name,
            @NotNull String namespace,
            @NotNull String gateway,
            @NotNull String serviceName,
            int httpPort
    ) {
        public String id() {
            return name;
        }
    }
    public IstioVirtualServiceManifest(@NotNull Chart chart, @NotNull Props virtualServiceProps) {
        final var metadata = ApiObjectMetadata
                .builder()
                .namespace(virtualServiceProps.namespace)
                .name(virtualServiceProps.name)
                .build();
        final var uri = VirtualServiceSpecHttpMatchUri.builder().prefix("/").build();
        final var httpMatch = VirtualServiceSpecHttpMatch.builder().uri(uri).build();
        final var port = VirtualServiceSpecHttpRouteDestinationPort
                .builder()
                .number(virtualServiceProps.httpPort)
                .build();
        final var destination = VirtualServiceSpecHttpRouteDestination
                .builder()
                .host(virtualServiceProps.serviceName)
                .port(port)
                .build();
        final var route = VirtualServiceSpecHttpRoute
                .builder()
                .destination(destination).build();
        final var httpSpec = VirtualServiceSpecHttp
                .builder()
                .match(List.of(httpMatch))
                .route(List.of(route))
                .build();
        final var gateway = MessageFormat.format("{0}/{1}",
                virtualServiceProps.namespace,
                virtualServiceProps.gateway);
        final var spec = VirtualServiceSpec
                .builder()
                .hosts(List.of("*"))
                .gateways(List.of(gateway))
                .http(List.of(httpSpec))
                .build();
        final var props = IstioVirtualServiceProps
                .builder()
                .metadata(metadata)
                .spec(spec)
                .build();
        new IstioVirtualService(chart, virtualServiceProps.id(), props);
    }
}
