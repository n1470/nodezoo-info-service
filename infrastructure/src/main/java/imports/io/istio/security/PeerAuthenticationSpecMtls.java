package imports.io.istio.security;

/**
 * Mutual TLS settings for workload.
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:51:06.464Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.security.$Module.class, fqn = "ioistiosecurity.PeerAuthenticationSpecMtls")
@software.amazon.jsii.Jsii.Proxy(PeerAuthenticationSpecMtls.Jsii$Proxy.class)
public interface PeerAuthenticationSpecMtls extends software.amazon.jsii.JsiiSerializable {

    /**
     * Defines the mTLS mode used for peer authentication.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.security.PeerAuthenticationSpecMtlsMode getMode() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link PeerAuthenticationSpecMtls}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link PeerAuthenticationSpecMtls}
     */
    public static final class Builder implements software.amazon.jsii.Builder<PeerAuthenticationSpecMtls> {
        imports.io.istio.security.PeerAuthenticationSpecMtlsMode mode;

        /**
         * Sets the value of {@link PeerAuthenticationSpecMtls#getMode}
         * @param mode Defines the mTLS mode used for peer authentication.
         * @return {@code this}
         */
        public Builder mode(imports.io.istio.security.PeerAuthenticationSpecMtlsMode mode) {
            this.mode = mode;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link PeerAuthenticationSpecMtls}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public PeerAuthenticationSpecMtls build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link PeerAuthenticationSpecMtls}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements PeerAuthenticationSpecMtls {
        private final imports.io.istio.security.PeerAuthenticationSpecMtlsMode mode;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.mode = software.amazon.jsii.Kernel.get(this, "mode", software.amazon.jsii.NativeType.forClass(imports.io.istio.security.PeerAuthenticationSpecMtlsMode.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.mode = builder.mode;
        }

        @Override
        public final imports.io.istio.security.PeerAuthenticationSpecMtlsMode getMode() {
            return this.mode;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getMode() != null) {
                data.set("mode", om.valueToTree(this.getMode()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistiosecurity.PeerAuthenticationSpecMtls"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            PeerAuthenticationSpecMtls.Jsii$Proxy that = (PeerAuthenticationSpecMtls.Jsii$Proxy) o;

            return this.mode != null ? this.mode.equals(that.mode) : that.mode == null;
        }

        @Override
        public final int hashCode() {
            int result = this.mode != null ? this.mode.hashCode() : 0;
            return result;
        }
    }
}
