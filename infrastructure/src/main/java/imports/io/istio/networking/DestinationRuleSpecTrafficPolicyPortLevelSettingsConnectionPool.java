package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.438Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool")
@software.amazon.jsii.Jsii.Proxy(DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool.Jsii$Proxy.class)
public interface DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool extends software.amazon.jsii.JsiiSerializable {

    /**
     * HTTP connection pool settings.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolHttp getHttp() {
        return null;
    }

    /**
     * Settings common to both HTTP and TCP upstream connections.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp getTcp() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool}
     */
    public static final class Builder implements software.amazon.jsii.Builder<DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool> {
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolHttp http;
        imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp tcp;

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool#getHttp}
         * @param http HTTP connection pool settings.
         * @return {@code this}
         */
        public Builder http(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolHttp http) {
            this.http = http;
            return this;
        }

        /**
         * Sets the value of {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool#getTcp}
         * @param tcp Settings common to both HTTP and TCP upstream connections.
         * @return {@code this}
         */
        public Builder tcp(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp tcp) {
            this.tcp = tcp;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool {
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolHttp http;
        private final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp tcp;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.http = software.amazon.jsii.Kernel.get(this, "http", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolHttp.class));
            this.tcp = software.amazon.jsii.Kernel.get(this, "tcp", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.http = builder.http;
            this.tcp = builder.tcp;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolHttp getHttp() {
            return this.http;
        }

        @Override
        public final imports.io.istio.networking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPoolTcp getTcp() {
            return this.tcp;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getHttp() != null) {
                data.set("http", om.valueToTree(this.getHttp()));
            }
            if (this.getTcp() != null) {
                data.set("tcp", om.valueToTree(this.getTcp()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool.Jsii$Proxy that = (DestinationRuleSpecTrafficPolicyPortLevelSettingsConnectionPool.Jsii$Proxy) o;

            if (this.http != null ? !this.http.equals(that.http) : that.http != null) return false;
            return this.tcp != null ? this.tcp.equals(that.tcp) : that.tcp == null;
        }

        @Override
        public final int hashCode() {
            int result = this.http != null ? this.http.hashCode() : 0;
            result = 31 * result + (this.tcp != null ? this.tcp.hashCode() : 0);
            return result;
        }
    }
}
