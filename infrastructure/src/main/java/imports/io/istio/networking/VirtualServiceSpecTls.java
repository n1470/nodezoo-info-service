package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.469Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.VirtualServiceSpecTls")
@software.amazon.jsii.Jsii.Proxy(VirtualServiceSpecTls.Jsii$Proxy.class)
public interface VirtualServiceSpecTls extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsMatch> getMatch() {
        return null;
    }

    /**
     * The destination to which the connection should be forwarded to.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsRoute> getRoute() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link VirtualServiceSpecTls}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link VirtualServiceSpecTls}
     */
    public static final class Builder implements software.amazon.jsii.Builder<VirtualServiceSpecTls> {
        java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsMatch> match;
        java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsRoute> route;

        /**
         * Sets the value of {@link VirtualServiceSpecTls#getMatch}
         * @param match the value to be set.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder match(java.util.List<? extends imports.io.istio.networking.VirtualServiceSpecTlsMatch> match) {
            this.match = (java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsMatch>)match;
            return this;
        }

        /**
         * Sets the value of {@link VirtualServiceSpecTls#getRoute}
         * @param route The destination to which the connection should be forwarded to.
         * @return {@code this}
         */
        @SuppressWarnings("unchecked")
        public Builder route(java.util.List<? extends imports.io.istio.networking.VirtualServiceSpecTlsRoute> route) {
            this.route = (java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsRoute>)route;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link VirtualServiceSpecTls}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public VirtualServiceSpecTls build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link VirtualServiceSpecTls}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements VirtualServiceSpecTls {
        private final java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsMatch> match;
        private final java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsRoute> route;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.match = software.amazon.jsii.Kernel.get(this, "match", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecTlsMatch.class)));
            this.route = software.amazon.jsii.Kernel.get(this, "route", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.VirtualServiceSpecTlsRoute.class)));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        @SuppressWarnings("unchecked")
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.match = (java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsMatch>)builder.match;
            this.route = (java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsRoute>)builder.route;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsMatch> getMatch() {
            return this.match;
        }

        @Override
        public final java.util.List<imports.io.istio.networking.VirtualServiceSpecTlsRoute> getRoute() {
            return this.route;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getMatch() != null) {
                data.set("match", om.valueToTree(this.getMatch()));
            }
            if (this.getRoute() != null) {
                data.set("route", om.valueToTree(this.getRoute()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.VirtualServiceSpecTls"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            VirtualServiceSpecTls.Jsii$Proxy that = (VirtualServiceSpecTls.Jsii$Proxy) o;

            if (this.match != null ? !this.match.equals(that.match) : that.match != null) return false;
            return this.route != null ? this.route.equals(that.route) : that.route == null;
        }

        @Override
        public final int hashCode() {
            int result = this.match != null ? this.match.hashCode() : 0;
            result = 31 * result + (this.route != null ? this.route.hashCode() : 0);
            return result;
        }
    }
}
