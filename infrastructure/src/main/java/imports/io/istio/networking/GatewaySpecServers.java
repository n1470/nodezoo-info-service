package imports.io.istio.networking;

/**
 */
@javax.annotation.Generated(value = "jsii-pacmak/1.61.0 (build abf4039)", date = "2022-12-30T07:48:36.443Z")
@software.amazon.jsii.Jsii(module = imports.io.istio.networking.$Module.class, fqn = "ioistionetworking.GatewaySpecServers")
@software.amazon.jsii.Jsii.Proxy(GatewaySpecServers.Jsii$Proxy.class)
public interface GatewaySpecServers extends software.amazon.jsii.JsiiSerializable {

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getBind() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getDefaultEndpoint() {
        return null;
    }

    /**
     * One or more hosts exposed by this gateway.
     */
    default @org.jetbrains.annotations.Nullable java.util.List<java.lang.String> getHosts() {
        return null;
    }

    /**
     * An optional name of the server, when set must be unique across all servers.
     */
    default @org.jetbrains.annotations.Nullable java.lang.String getName() {
        return null;
    }

    /**
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.GatewaySpecServersPort getPort() {
        return null;
    }

    /**
     * Set of TLS related options that govern the server's behavior.
     */
    default @org.jetbrains.annotations.Nullable imports.io.istio.networking.GatewaySpecServersTls getTls() {
        return null;
    }

    /**
     * @return a {@link Builder} of {@link GatewaySpecServers}
     */
    static Builder builder() {
        return new Builder();
    }
    /**
     * A builder for {@link GatewaySpecServers}
     */
    public static final class Builder implements software.amazon.jsii.Builder<GatewaySpecServers> {
        java.lang.String bind;
        java.lang.String defaultEndpoint;
        java.util.List<java.lang.String> hosts;
        java.lang.String name;
        imports.io.istio.networking.GatewaySpecServersPort port;
        imports.io.istio.networking.GatewaySpecServersTls tls;

        /**
         * Sets the value of {@link GatewaySpecServers#getBind}
         * @param bind the value to be set.
         * @return {@code this}
         */
        public Builder bind(java.lang.String bind) {
            this.bind = bind;
            return this;
        }

        /**
         * Sets the value of {@link GatewaySpecServers#getDefaultEndpoint}
         * @param defaultEndpoint the value to be set.
         * @return {@code this}
         */
        public Builder defaultEndpoint(java.lang.String defaultEndpoint) {
            this.defaultEndpoint = defaultEndpoint;
            return this;
        }

        /**
         * Sets the value of {@link GatewaySpecServers#getHosts}
         * @param hosts One or more hosts exposed by this gateway.
         * @return {@code this}
         */
        public Builder hosts(java.util.List<java.lang.String> hosts) {
            this.hosts = hosts;
            return this;
        }

        /**
         * Sets the value of {@link GatewaySpecServers#getName}
         * @param name An optional name of the server, when set must be unique across all servers.
         * @return {@code this}
         */
        public Builder name(java.lang.String name) {
            this.name = name;
            return this;
        }

        /**
         * Sets the value of {@link GatewaySpecServers#getPort}
         * @param port the value to be set.
         * @return {@code this}
         */
        public Builder port(imports.io.istio.networking.GatewaySpecServersPort port) {
            this.port = port;
            return this;
        }

        /**
         * Sets the value of {@link GatewaySpecServers#getTls}
         * @param tls Set of TLS related options that govern the server's behavior.
         * @return {@code this}
         */
        public Builder tls(imports.io.istio.networking.GatewaySpecServersTls tls) {
            this.tls = tls;
            return this;
        }

        /**
         * Builds the configured instance.
         * @return a new instance of {@link GatewaySpecServers}
         * @throws NullPointerException if any required attribute was not provided
         */
        @Override
        public GatewaySpecServers build() {
            return new Jsii$Proxy(this);
        }
    }

    /**
     * An implementation for {@link GatewaySpecServers}
     */
    @software.amazon.jsii.Internal
    final class Jsii$Proxy extends software.amazon.jsii.JsiiObject implements GatewaySpecServers {
        private final java.lang.String bind;
        private final java.lang.String defaultEndpoint;
        private final java.util.List<java.lang.String> hosts;
        private final java.lang.String name;
        private final imports.io.istio.networking.GatewaySpecServersPort port;
        private final imports.io.istio.networking.GatewaySpecServersTls tls;

        /**
         * Constructor that initializes the object based on values retrieved from the JsiiObject.
         * @param objRef Reference to the JSII managed object.
         */
        protected Jsii$Proxy(final software.amazon.jsii.JsiiObjectRef objRef) {
            super(objRef);
            this.bind = software.amazon.jsii.Kernel.get(this, "bind", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.defaultEndpoint = software.amazon.jsii.Kernel.get(this, "defaultEndpoint", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.hosts = software.amazon.jsii.Kernel.get(this, "hosts", software.amazon.jsii.NativeType.listOf(software.amazon.jsii.NativeType.forClass(java.lang.String.class)));
            this.name = software.amazon.jsii.Kernel.get(this, "name", software.amazon.jsii.NativeType.forClass(java.lang.String.class));
            this.port = software.amazon.jsii.Kernel.get(this, "port", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.GatewaySpecServersPort.class));
            this.tls = software.amazon.jsii.Kernel.get(this, "tls", software.amazon.jsii.NativeType.forClass(imports.io.istio.networking.GatewaySpecServersTls.class));
        }

        /**
         * Constructor that initializes the object based on literal property values passed by the {@link Builder}.
         */
        protected Jsii$Proxy(final Builder builder) {
            super(software.amazon.jsii.JsiiObject.InitializationMode.JSII);
            this.bind = builder.bind;
            this.defaultEndpoint = builder.defaultEndpoint;
            this.hosts = builder.hosts;
            this.name = builder.name;
            this.port = builder.port;
            this.tls = builder.tls;
        }

        @Override
        public final java.lang.String getBind() {
            return this.bind;
        }

        @Override
        public final java.lang.String getDefaultEndpoint() {
            return this.defaultEndpoint;
        }

        @Override
        public final java.util.List<java.lang.String> getHosts() {
            return this.hosts;
        }

        @Override
        public final java.lang.String getName() {
            return this.name;
        }

        @Override
        public final imports.io.istio.networking.GatewaySpecServersPort getPort() {
            return this.port;
        }

        @Override
        public final imports.io.istio.networking.GatewaySpecServersTls getTls() {
            return this.tls;
        }

        @Override
        @software.amazon.jsii.Internal
        public com.fasterxml.jackson.databind.JsonNode $jsii$toJson() {
            final com.fasterxml.jackson.databind.ObjectMapper om = software.amazon.jsii.JsiiObjectMapper.INSTANCE;
            final com.fasterxml.jackson.databind.node.ObjectNode data = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();

            if (this.getBind() != null) {
                data.set("bind", om.valueToTree(this.getBind()));
            }
            if (this.getDefaultEndpoint() != null) {
                data.set("defaultEndpoint", om.valueToTree(this.getDefaultEndpoint()));
            }
            if (this.getHosts() != null) {
                data.set("hosts", om.valueToTree(this.getHosts()));
            }
            if (this.getName() != null) {
                data.set("name", om.valueToTree(this.getName()));
            }
            if (this.getPort() != null) {
                data.set("port", om.valueToTree(this.getPort()));
            }
            if (this.getTls() != null) {
                data.set("tls", om.valueToTree(this.getTls()));
            }

            final com.fasterxml.jackson.databind.node.ObjectNode struct = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            struct.set("fqn", om.valueToTree("ioistionetworking.GatewaySpecServers"));
            struct.set("data", data);

            final com.fasterxml.jackson.databind.node.ObjectNode obj = com.fasterxml.jackson.databind.node.JsonNodeFactory.instance.objectNode();
            obj.set("$jsii.struct", struct);

            return obj;
        }

        @Override
        public final boolean equals(final Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            GatewaySpecServers.Jsii$Proxy that = (GatewaySpecServers.Jsii$Proxy) o;

            if (this.bind != null ? !this.bind.equals(that.bind) : that.bind != null) return false;
            if (this.defaultEndpoint != null ? !this.defaultEndpoint.equals(that.defaultEndpoint) : that.defaultEndpoint != null) return false;
            if (this.hosts != null ? !this.hosts.equals(that.hosts) : that.hosts != null) return false;
            if (this.name != null ? !this.name.equals(that.name) : that.name != null) return false;
            if (this.port != null ? !this.port.equals(that.port) : that.port != null) return false;
            return this.tls != null ? this.tls.equals(that.tls) : that.tls == null;
        }

        @Override
        public final int hashCode() {
            int result = this.bind != null ? this.bind.hashCode() : 0;
            result = 31 * result + (this.defaultEndpoint != null ? this.defaultEndpoint.hashCode() : 0);
            result = 31 * result + (this.hosts != null ? this.hosts.hashCode() : 0);
            result = 31 * result + (this.name != null ? this.name.hashCode() : 0);
            result = 31 * result + (this.port != null ? this.port.hashCode() : 0);
            result = 31 * result + (this.tls != null ? this.tls.hashCode() : 0);
            return result;
        }
    }
}
